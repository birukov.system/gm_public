from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.template.loader import render_to_string
from django.utils.functional import cached_property
from django.utils.text import gettext_lazy as _

from report.tasks import send_report_task
from translation.models import SiteInterfaceDictionary
from utils.models import ProjectBaseMixin


class ReportManager(models.Manager):
    """Manager for model Report."""

    def make(self, source: int, category, url: str, description: str, locale: str):
        """Make object."""
        obj = self.create(
            source=source,
            category=category,
            url=url,
            description=description,
            locale=locale,
        )
        if settings.USE_CELERY:
            send_report_task.delay(obj.id)
        else:
            send_report_task(obj.id)
        return obj


class ReportQuerySet(models.QuerySet):
    """QuerySet for model Report."""

    def by_source(self, source: int):
        """Return QuerySet filtered by a source."""
        return self.filter(source=source)


class Report(ProjectBaseMixin):
    """Report model."""

    BACK_OFFICE = 0
    WEB = 1
    MOBILE = 2

    SOURCE_CHOICES = (
        (BACK_OFFICE, _('Back office')),
        (WEB, _('Web')),
        (MOBILE, _('Mobile')),
    )

    BUG = 0
    SUGGESTION_IMPROVEMENT = 1

    CATEGORY_CHOICES = (
        (BUG, _('Bug')),
        (SUGGESTION_IMPROVEMENT, _('Suggestion/improvement')),
    )

    source = models.PositiveSmallIntegerField(choices=SOURCE_CHOICES,
                                              verbose_name=_('source'))
    category = models.PositiveSmallIntegerField(choices=CATEGORY_CHOICES,
                                                verbose_name=_('category'))
    url = models.URLField(verbose_name=_('URL'))
    description = models.TextField(verbose_name=_('description'))
    locale = models.CharField(max_length=10, null=True,
                              verbose_name=_('locale'))

    objects = ReportManager.from_queryset(ReportQuerySet)()

    class Meta:
        """Meta class."""
        verbose_name = _('Report')
        verbose_name_plural = _('Reports')

    def __str__(self):
        """Implement `str` dunder method."""
        return f'{self.id}: {self.get_category_display()} ({self.url}, {self.locale})'

    @cached_property
    def support_email_note(self):
        keyword = 'support.email.note'
        default_note = (
            'You received this message because you are an '
            'administrator with privileges to manage this request.'
        )

        note_qs = SiteInterfaceDictionary.objects.filter(keywords=keyword)
        if note_qs.exists():
            return note_qs.first().text.get(self.locale, default_note)
        return default_note

    @cached_property
    def report_message(self):
        return render_to_string(
            template_name=settings.REPORT_TEMPLATE,
            context={
                'source_value': self.get_source_display(),
                'source_page_url': self.url,
                'source_screen_language': self.locale,
                'request_category': self.get_category_display(),
                'request_description': self.description,
                'support_email_note': self.support_email_note,
            }
        )

    @cached_property
    def base_template(self):
        """Prepare the body of the email message"""
        return {
            'subject': _('[TECH_REQUEST] A new technical request has been created.'),
            'message': self.report_message,
            'html_message': self.report_message,
            'from_email': settings.EMAIL_HOST_USER,
            'recipient_list': settings.EMAIL_TECHNICAL_SUPPORT,
        }

    def send_email(self):
        """Send an email reset user password"""
        send_mail(**self.base_template)
