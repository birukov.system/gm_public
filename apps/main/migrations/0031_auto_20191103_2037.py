# Generated by Django 2.2.4 on 2019-11-03 20:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0030_auto_20191103_1350'),
    ]

    operations = [
        migrations.AddField(
            model_name='currency',
            name='code',
            field=models.CharField(default=None, max_length=5, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='currency',
            name='slug',
            field=models.SlugField(max_length=5, unique=True),
        ),
    ]
