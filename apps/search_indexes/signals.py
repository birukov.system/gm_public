"""Search indexes app signals."""
from django.db.models.signals import post_save
from django.dispatch import receiver


# @receiver(post_save)
# def update_document(sender, **kwargs):
#     from establishment.models import Establishment
#     from search_indexes.tasks import es_update
#     app_label = sender._meta.app_label
#     model_name = sender._meta.model_name
#     instance = kwargs['instance']
#
#     app_label_model_name_to_filter = {
#         ('location', 'country'): 'address__city__country',
#         # ('location', 'city'): 'address__city',
#         ('location', 'address'): 'address',
#         ('gallery', 'image'): 'gallery',
#         ('establishment', 'establishmentgallery'): 'establishment_gallery',
#         # todo: remove after migration
#         ('establishment', 'establishmenttype'): 'establishment_type',
#         ('establishment', 'establishmentsubtype'): 'establishment_subtypes',
#         ('tag', 'tag'): 'tags',
#     }
#     filter_name = app_label_model_name_to_filter.get((app_label, model_name))
#     if filter_name:
#         es_update(
#             related_ids=Establishment.objects.filter(**{filter_name: instance}).values_list('id', flat=True),
#             key=Establishment._meta.model_name
#         )


# temporary disabled update news
# ------------------------------
# @receiver(post_save)
# def update_news(sender, **kwargs):
#     from news.models import News
#     from search_indexes.tasks import es_update
#     app_label = sender._meta.app_label
#     model_name = sender._meta.model_name
#     instance = kwargs['instance']
#     app_label_model_name_to_filter = {
#         # ('location', 'country'): 'country',
#         ('news', 'newstype'): 'news_type',
#         ('tag', 'tag'): 'tags',
#     }
#     filter_name = app_label_model_name_to_filter.get((app_label, model_name))
#     if filter_name:
#         es_update(
#             related_ids=News.objects.filter(**{filter_name: instance}).values_list('id', flat=True),
#             key=News._meta.model_name
#         )


# temporary disabled update product
# -----------------------------------
# @receiver(post_save)
# def update_product(sender, **kwargs):
#     from product.models import Product
#     from search_indexes.tasks import es_update
#     app_label = sender._meta.app_label
#     model_name = sender._meta.model_name
#     instance = kwargs['instance']
#     app_label_model_name_to_filter = {
#         ('product', 'productstandard'): 'standards',
#         ('product', 'producttype'): 'product_type',
#         ('tag', 'tag'): 'tags',
#         # ('location', 'wineregion'): 'wine_region',
#         # ('location', 'winesubregion'): 'wine_sub_region',
#         # ('location', 'winevillage'): 'wine_village',
#         ('establishment', 'establishment'): 'establishment',
#     }
#     filter_name = app_label_model_name_to_filter.get((app_label, model_name))
#     if filter_name:
#         es_update(
#             related_ids=Product.objects.filter(**{filter_name: instance}).values_list('id', flat=True),
#             key=Product._meta.model_name
#         )
