from transfer.models import PageTexts, PageCounters
from news.models import News
from rating.models import ViewCount


def transfer_news_view_count():
    news_list = News.objects.filter(old_id__isnull=False)
    for news_object in news_list:
        try:
            mysql_page_text = PageTexts.objects.get(id=news_object.old_id)
        except PageTexts.DoesNotExist:
            continue

        try:
            mysql_views_count = PageCounters.objects.get(page_id=mysql_page_text.page_id)
        except PageCounters.DoesNotExist:
            continue

        view_count = ViewCount.objects.create(
            count=mysql_views_count.count
        )

        news_object.views_count = view_count
        news_object.save()


data_types = {
    "rating_count": [transfer_news_view_count]
}
