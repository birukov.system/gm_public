from pprint import pprint

from transfer.models import MercuryImages
from transfer.serializers.gallery import ImageSerializer


def transfer_gallery():
    queryset = MercuryImages.objects.all()

    serialized_data = ImageSerializer(data=list(queryset.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"News serializer errors: {serialized_data.errors}")


data_types = {
    "gallery": [transfer_gallery]
}

