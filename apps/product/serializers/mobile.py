from rest_framework import serializers

from comment.serializers import mobile as comment_mobile_serializers
from utils.serializers import TranslatedField
from .common import ProductBaseSerializer, ProductDetailSerializer


class ProductMobilePreviewSerializer(ProductBaseSerializer):

    class Meta(ProductBaseSerializer.Meta):
        fields = ProductBaseSerializer.Meta.fields.copy()
        fields.remove('establishment_detail')


class ProductMobileDetailSerializer(ProductDetailSerializer):
    precaution = TranslatedField()
    last_comment = comment_mobile_serializers.MobileCommentSerializer(allow_null=True)

    class Meta(ProductDetailSerializer.Meta):
        fields = [
            'precaution',
            'last_comment',
        ] + [
            field
            for field in ProductDetailSerializer.Meta.fields
            if not (field in ['last_review', 'last_published_review'])
        ]
