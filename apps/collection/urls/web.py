"""Collection app urlconf."""
from collection.urls.common import urlpatterns as common_url_patterns


app_name = 'web'

urlpatterns_api = []


urlpatterns = urlpatterns_api + \
              common_url_patterns
