from django.contrib import admin

from . import models


@admin.register(models.Comment)
class CommentModelAdmin(admin.ModelAdmin):
    """Model admin for model Comment"""
    raw_id_fields = ('user',)
