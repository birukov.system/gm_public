from django.urls import path

from search_indexes.views import TranslationsDocumentViewSet
from translation import views
from translation.views import back as views_back

common_urlpatterns = [
    path('site/confirm/', views_back.SiteInterfaceDictionaryConfirmView.as_view(), name='site-confirm-list'),
    path('languages/<int:pk>/', views.LanguageView.as_view(), name='language'),
    path('site/', views.SiteInterfaceDictionaryListView.as_view(), name='site-interface-list-create'),
    path('site/<int:pk>/', views.SiteInterfaceDictionaryView.as_view(), name='site-interface'),
    path('languages/', views.LanguageListView.as_view(), name='languages-list'),
    path('list/', TranslationsDocumentViewSet.as_view({'get': 'list'}), name='back-list'),
]

