"""User model index."""
from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from search_indexes.utils import trigram_analyzer
from account.models import User, UserRole, Role

UsersIndex = Index(settings.ELASTICSEARCH_INDEX_NAMES.get(__name__, 'user'))
UsersIndex.settings(number_of_shards=2, number_of_replicas=1, max_result_window=500000)


@UsersIndex.doc_type
class UserDocument(Document):

    roles_indexing = fields.ListField(
        fields.ObjectField(
            properties={
                'role': fields.IntegerField(),
                'role_display': fields.KeywordField(attr='get_role_display'),
                'country_code': fields.KeywordField(attr='country.code'),
            }
        )
    )
    roles_visible = fields.ListField(
        fields.ObjectField(
            properties={
                'role': fields.IntegerField(),
                'role_display': fields.KeywordField(attr='get_role_display'),
                'country_code': fields.KeywordField(attr='country.code'),
            }
        )
    )
    last_country = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'subdomain': fields.KeywordField(),
            'country_code': fields.KeywordField(),
        }
    )
    email = fields.TextField(analyzer=trigram_analyzer)
    username = fields.TextField(analyzer=trigram_analyzer)
    last_name = fields.TextField(analyzer=trigram_analyzer)
    first_name = fields.TextField(analyzer=trigram_analyzer)
    phone = fields.TextField(analyzer=trigram_analyzer, attr='national_calling_number')

    class Django:
        model = User
        fields = (
            'id',
            'is_superuser',
            'last_login',
            'locale',
            'date_joined',
        )
        related_models = [
            UserRole, Role,
        ]

    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Role):
            return related_instance.userrole_set.user
        if isinstance(related_instance, UserRole):
            return related_instance.user

    def get_queryset(self):
        return super().get_queryset().with_extend_related().prefetch_related('role__country', 'last_country')
