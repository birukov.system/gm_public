from django.core.management.base import BaseCommand

from news.models import News


class Command(BaseCommand):
    help = 'Remove old news from new bd'

    def handle(self, *args, **kwargs):
        old_news = News.objects.exclude(old_id__isnull=True)
        count = old_news.count()
        old_news.delete()
        self.stdout.write(self.style.WARNING(f'Deleted {count} objects.'))
