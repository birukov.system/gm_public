"""Local settings."""
from .base import *
import sys

# from .amazon_s3 import *

ALLOWED_HOSTS = ['*', ]

SEND_SMS = False
SMS_CODE_SHOW = True
USE_CELERY = False

SCHEMA_URI = 'http'
DEFAULT_SUBDOMAIN = 'www'
SITE_DOMAIN_URI = 'testserver.com:8000'
DOMAIN_URI = '0.0.0.0:8000'

# CELERY
# RabbitMQ
# BROKER_URL = 'amqp://rabbitmq:5672'
# Redis
BROKER_URL = 'redis://redis:6379/1'
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_BROKER_URL = BROKER_URL

# MEDIA
MEDIA_URL = f'{SCHEMA_URI}://{DOMAIN_URI}/{MEDIA_LOCATION}/'
MEDIA_ROOT = os.path.join(PUBLIC_ROOT, MEDIA_LOCATION)

# SORL thumbnails
THUMBNAIL_DEBUG = True

# DATABASES
DATABASES.update({
    'legacy': {
        'ENGINE': 'django.db.backends.mysql',
        # 'HOST': '172.22.0.1',
        'HOST': 'mysql_db',
        'PORT': 3306,
        'NAME': 'dev',
        'USER': 'dev',
        'PASSWORD': 'octosecret123',
    },
})

# LOGGING
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'filters': {
#         'require_debug_false': {
#             '()': 'django.utils.log.RequireDebugFalse',
#         },
#         'require_debug_true': {
#             '()': 'django.utils.log.RequireDebugTrue',
#         },
#     },
#     'handlers': {
#         'console': {
#             'level': 'DEBUG',
#             'filters': ['require_debug_true'],
#             'class': 'logging.StreamHandler',
#         },
#         'null': {
#             'class': 'logging.NullHandler',
#         },
#     },
#     'loggers': {
#         'django': {
#             'handlers': ['console'],
#         },
#         'py.warnings': {
#             'handlers': ['console'],
#         },
#         'django.db.backends': {
#             'handlers': ['console', ],
#             'level': 'DEBUG',
#             'propagate': False,
#         },
#     }
# }

# ELASTICSEARCH SETTINGS
ELASTICSEARCH_DSL = {
    'default': {
        'hosts': 'elasticsearch:9200'
    }
}

ELASTICSEARCH_INDEX_NAMES = {
    # 'search_indexes.documents.news': 'local_news',
    'search_indexes.documents.establishment': 'local_establishment',
    'search_indexes.documents.product': 'local_product',
    'search_indexes.documents.tag_category': 'local_tag_category',
}
ELASTICSEARCH_DSL_AUTOSYNC = False

TESTING = sys.argv[1:2] == ['test']
if TESTING:
    ELASTICSEARCH_INDEX_NAMES = {}
ELASTICSEARCH_DSL_AUTOSYNC = False

INSTALLED_APPS += ['transfer']
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
