from django.urls import path, include

app_name = 'back'

urlpatterns = [
    path('account/', include('account.urls.back')),
    path('comment/', include('comment.urls.back')),
    path('establishments/', include('establishment.urls.back')),
    path('collections/', include('collection.urls.back')),
    path('gallery/', include(('gallery.urls', 'gallery'), namespace='gallery')),
    path('location/', include('location.urls.back')),
    path('news/', include('news.urls.back')),
    path('notifications/', include(('notification.urls.back', 'notification'),
                                   namespace='notification')),
    path('review/', include('review.urls.back')),
    path('tags/', include(('tag.urls.back', 'tag'), namespace='tag')),
    path('products/', include(('product.urls.back', 'product'), namespace='product')),
    path('re_blocks/', include(('advertisement.urls.back', 'advertisement'),
                               namespace='advertisement')),
    path('main/', include('main.urls.back')),
    path('partner/', include('partner.urls.back')),
    path('report/', include('report.urls.back')),
    path('recipes/', include('recipe.urls.back')),
    path('translations/', include('translation.urls.back')),
]
