# Generated by Django 2.2.4 on 2019-09-01 10:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('establishment', '0006_merge_20190901_0846'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactemail',
            name='contact',
        ),
        migrations.RemoveField(
            model_name='contactphone',
            name='contact',
        ),
        migrations.DeleteModel(
            name='Contact',
        ),
        migrations.DeleteModel(
            name='ContactEmail',
        ),
        migrations.DeleteModel(
            name='ContactPhone',
        ),
    ]
