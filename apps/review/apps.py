from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ReviewConfig(AppConfig):
    name = 'review'
    verbose_name = _('reviews')

    def ready(self):
        import review.signals
