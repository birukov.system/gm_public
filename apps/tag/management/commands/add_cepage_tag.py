from django.core.management.base import BaseCommand

from transfer.models import Cepages
from transfer.serializers.tag import CepageTagSerializer


class Command(BaseCommand):
    help = 'Add cepage tags'

    def handle(self, *args, **kwarg):

        errors = []
        queryset = Cepages.objects.all()
        serialized_data = CepageTagSerializer(
            data=list(queryset.values()),
            many=True)
        if serialized_data.is_valid():
            serialized_data.save()
        else:
            for d in serialized_data.errors: errors.append(d) if d else None

        self.stdout.write(self.style.WARNING(f'Error count: {len(errors)}\nErrors: {errors}'))
