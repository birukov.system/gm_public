# Generated by Django 2.2.7 on 2019-12-26 06:21

from django.db import migrations, models

GUIDE_TYPE_RESTAURANT = 0
GUIDE_TYPE_WINE = 1


def transform_guide_type(apps, schema_editor):
    Guide = apps.get_model('collection', 'Guide')
    to_update = []
    for guide in Guide.objects.all():
        if guide.guide_type.name.startswith('restaurant'):
            guide.guide_type_2 = GUIDE_TYPE_RESTAURANT
        elif guide.guide_type.name.startswith('wine'):
            guide.guide_type_2 = GUIDE_TYPE_WINE
    Guide.objects.bulk_update(to_update, ['guide_type_2', ])


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0030_guidefilter_guide_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='guidefilter',
            name='guide_type',
        ),
        migrations.AddField(
            model_name='guide',
            name='guide_type_2',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Restaurant'), (1, 'Artisan'), (2, 'Wine')], default=0, verbose_name='guide type'),
        ),
        migrations.RunPython(transform_guide_type, migrations.RunPython.noop),
        migrations.RemoveField(
            model_name='guide',
            name='guide_type',
        ),
        migrations.DeleteModel(
            name='GuideType',
        ),
        migrations.RenameField(
            model_name='guide',
            old_name='guide_type_2',
            new_name='guide_type',
        ),
    ]
