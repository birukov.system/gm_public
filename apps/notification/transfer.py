"""
Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql


NOTE: среди legacy таблиц совпадение для таблицы Address не найдено (Возможно для Address подходит Locations в legacy)
"""

card = {
    # нету аналога для NewsType
    "Subscriber": {
        "data_type": "objects",
        # "dependencies": ("User", ),
        "dependencies": None,
        "fields": {
            "EmailAddresses": {
                "email": "email",
                "state": ("state", "django.db.models.PositiveIntegerField") # из legacy брать только те записи у которых state=usable
            },
            # "relations": [
            #     # отложено до выяснения Уточнения и вопросы по мигратору(Как поступать со сбором данных)
            #     # "user": ""
            # ]
        }
    },
}

used_apps = ("account", )
