from http.cookies import SimpleCookie

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from account.models import Role, User
from authorization.tests.tests_authorization import get_tokens_for_user
from location.models import Country


class RoleTests(APITestCase):
    def setUp(self):
        self.data = get_tokens_for_user()
        self.client.cookies = SimpleCookie(
            {'access_token': self.data['tokens'].get('access_token'),
             'refresh_token': self.data['tokens'].get('access_token')})

    def test_role_get(self):
        url = reverse('back:account:role-list-create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_role_post(self):
        url = reverse('back:account:role-list-create')
        country = Country.objects.create(
            name='{"ru-RU":"Russia"}',
            code='23',
            low_price=15,
            high_price=150000
        )
        country.save()

        data = {
            "role": 2,
            "country": country.pk
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class UserRoleTests(APITestCase):
    def setUp(self):
        self.data = get_tokens_for_user()
        self.client.cookies = SimpleCookie(
            {'access_token': self.data['tokens'].get('access_token'),
             'refresh_token': self.data['tokens'].get('access_token')})

        self.country_ru = Country.objects.create(
            name='{"ru-RU":"Russia"}',
            code='23',
            low_price=15,
            high_price=150000
        )
        self.country_ru.save()

        self.country_en = Country.objects.create(
            name='{"en-GB":"England"}',
            code='25',
            low_price=15,
            high_price=150000
        )
        self.country_en.save()

        self.role = Role.objects.create(
            role=2,
            country=self.country_ru
        )
        self.role.save()

        self.user_test = User.objects.create_user(
            username='test',
            email='testemail@mail.com',
            password='passwordtest'
        )

    def test_user_role_post(self):
        url = reverse('back:account:user-role-list-create')

        data = {
            "user": self.user_test.id,
            "role": self.role.id
        }

        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class UserTestCase(APITestCase):

    def setUp(self):
        self.user_1 = User.objects.create_user(
            username='alex',
            email='alex@mail.com',
            password='alex_password',
            is_staff=True,
        )

        self.user_2 = User.objects.create_user(
            username='boris',
            email='boris@mail.com',
            password='boris_password',
        )

        # get tokens
        tokens = User.create_jwt_tokens(self.user_1)
        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('refresh_token')})

    def test_user_CRUD(self):
        response = self.client.get('/api/back/account/user/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = {
            'username': 'roman',
            'email': 'roman@mail.com',
            'password': 'roman_password',
        }

        response = self.client.post('/api/back/account/user/', data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get(f'/api/back/account/user/{self.user_2.id}/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        update_data = {
            'first_name': 'Boris'
        }

        response = self.client.patch(f'/api/back/account/user/{self.user_2.id}/', data=update_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.delete(f'/api/back/account/user/{self.user_2.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
