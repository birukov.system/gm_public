from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TranslationConfig(AppConfig):
    name = 'translation'
    verbose_name = _('Translation')
