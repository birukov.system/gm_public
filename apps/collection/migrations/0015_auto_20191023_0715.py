# Generated by Django 2.2.4 on 2019-10-23 07:15

from django.db import migrations

import utils.models


def fill_title_json_from_title(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Collection = apps.get_model('collection', 'Collection')
    for collection in Collection.objects.all():
        collection.name_json = {'en-GB': collection.name}
        collection.save()


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0014_auto_20191022_1242'),
    ]

    operations = [
        migrations.AddField(
            model_name='collection',
            name='name_json',
            field=utils.models.TJSONField(blank=True, default=None, help_text='{"en-GB":"some text"}', null=True, verbose_name='name'),
        ),
        migrations.RunPython(fill_title_json_from_title, migrations.RunPython.noop),
        migrations.RemoveField(
            model_name='collection',
            name='name',
        ),
        migrations.RenameField(
            model_name='collection',
            old_name='name_json',
            new_name='name',
        ),
        migrations.AlterField(
            model_name='collection',
            name='name',
            field=utils.models.TJSONField(help_text='{"en-GB":"some text"}', verbose_name='name'),
        ),
    ]
