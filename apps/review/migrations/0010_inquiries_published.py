# Generated by Django 2.2.7 on 2019-11-11 11:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0009_auto_20191110_0615'),
    ]

    operations = [
        migrations.AddField(
            model_name='inquiries',
            name='published',
            field=models.BooleanField(default=False, verbose_name='is published'),
        ),
    ]
