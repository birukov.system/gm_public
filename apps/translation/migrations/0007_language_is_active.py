# Generated by Django 2.2.4 on 2019-11-14 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '0006_language_old_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='language',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='is active'),
        ),
    ]
