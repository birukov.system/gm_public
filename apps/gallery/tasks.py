"""Gallery app celery tasks."""
import logging

from celery import shared_task

from . import models

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


@shared_task
def delete_image(image_id: int, completely: bool = True):
    """Delete an image from remote storage."""
    image_qs = models.Image.objects.filter(id=image_id)
    if image_qs.exists():
        try:
            image = image_qs.first()
            image.delete_image(completely=completely)
        except:
            logger.error(f'TASK_NAME: delete_image\n'
                         f'DETAIL: Exception occurred while deleting an image '
                         f'and related crops from remote storage: image_id - {image_id}')
