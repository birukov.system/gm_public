"""Product admin conf."""
from django.contrib import admin

from utils.admin import BaseModelAdminMixin
from .models import Product, ProductType, ProductSubType, ProductGallery, Unit


class ProductGalleryInline(admin.TabularInline):
    """Product gallery inline."""
    model = ProductGallery
    extra = 0


@admin.register(Product)
class ProductAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin page for model Product."""
    search_fields = ('name', )
    list_filter = ('available', 'product_type')
    list_display = ('id', '__str__', 'get_category_display', 'product_type')
    inlines = [ProductGalleryInline, ]
    raw_id_fields = ('subtypes', 'classifications', 'standards',
                     'tags', 'gallery', 'establishment',)


@admin.register(ProductType)
class ProductTypeAdmin(admin.ModelAdmin):
    """Admin page for model ProductType."""
    raw_id_fields = ['tag_categories', 'default_image', ]


@admin.register(ProductSubType)
class ProductSubTypeAdmin(admin.ModelAdmin):
    """Admin page for model ProductSubType."""
    raw_id_fields = ['product_type', 'default_image', ]


@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    """Admin page for model Unit."""
