"""Views for application report."""
from rest_framework.generics import ListCreateAPIView

from report.models import Report
from report.views.common import ReportBaseView


class ReportListCreateView(ReportBaseView, ListCreateAPIView):
    """
    ## View for getting list of reports or create a new one.
    ### POST-request data
    Request data attributes:
    * category: integer (0 - Bug, 1 - Suggestion improvement)
    * url: char (URL)
    * description: text (problem description)
    E.g.:
    ```
    {
        "category": 1,
        "url": "http://google.com",
        "description": "Description"
    }
    ```

    ### Response
    *GET*
    Return paginated list of reports.

    *POST*
    Creates a new report with a source - `WEB`, and returns a serialized object.

    ### Description
    Method that allows getting list of reports or create a new one and return serialized object.
    """

    @staticmethod
    def get_source():
        """Return a source for view."""
        return Report.WEB
