"""Notification app common views."""
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions, status
from rest_framework.response import Response

from notification import models
from notification.serializers import common as serializers
from utils.methods import get_user_ip
from utils.permissions import IsAuthenticatedAndTokenIsValid


class CreateSubscribeView(generics.CreateAPIView):
    """Create subscribe View."""
    queryset = models.Subscriber.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.CreateAndUpdateSubscribeSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        instance = None
        if 'email' in request.data:
            # we shouldn't create new subscriber if we have one
            instance = models.Subscriber.objects.filter(email=request.data['email']).first()
        serializer = self.get_serializer(data=data) if instance is None else self.get_serializer(instance, data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UpdateSubscribeView(generics.UpdateAPIView):
    """Subscribe info view."""
    lookup_field = 'update_code'
    lookup_url_kwarg = 'code'
    permission_classes = (permissions.AllowAny,)
    queryset = models.Subscriber.objects.all()
    serializer_class = serializers.UpdateSubscribeSerializer


class SubscribeInfoView(generics.RetrieveAPIView):
    """Subscribe info view."""
    lookup_field = 'update_code'
    lookup_url_kwarg = 'code'
    permission_classes = (permissions.AllowAny,)
    queryset = models.Subscriber.objects.all()
    serializer_class = serializers.SubscribeSerializer


class SubscribeInfoAuthUserView(generics.RetrieveAPIView):
    """Subscribe info auth user view."""
    permission_classes = (IsAuthenticatedAndTokenIsValid,)
    serializer_class = serializers.SubscribeSerializer
    lookup_field = None

    def get_object(self):
        return get_object_or_404(models.Subscriber, user=self.request.user)


class UnsubscribeView(generics.UpdateAPIView):
    """Unsubscribe view."""
    lookup_field = 'update_code'
    lookup_url_kwarg = 'code'
    permission_classes = (permissions.AllowAny,)
    queryset = models.Subscriber.objects.all()
    serializer_class = serializers.SubscribeSerializer

    def put(self, request, *args, **kw):
        obj = self.get_object()
        obj.unsubscribe()
        serializer = self.get_serializer(instance=obj)
        return Response(data=serializer.data)


class UnsubscribeAuthUserView(generics.GenericAPIView):
    """Unsubscribe auth user view."""
    permission_classes = (IsAuthenticatedAndTokenIsValid,)
    queryset = models.Subscriber.objects.all()
    serializer_class = serializers.SubscribeSerializer

    def patch(self, request, *args, **kw):
        user = request.user
        obj = models.Subscriber.objects.filter(user=user).first()
        obj.unsubscribe()
        serializer = self.get_serializer(instance=obj)
        return Response(data=serializer.data)


class SubscriptionTypesView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.AllowAny,)
    queryset = models.SubscriptionType.objects.all()
    serializer_class = serializers.SubscriptionTypeSerializer
