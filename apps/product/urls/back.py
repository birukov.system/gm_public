"""Product backoffice url patterns."""
from django.urls import path

from product import views


urlpatterns = [
    path('', views.ProductListCreateBackOfficeView.as_view(), name='list-create'),
    path('slug/<slug:establishment_slug>/', views.ProductListCreateBackOfficeView.as_view(), name='list-by-slug'),
    path('<int:pk>/', views.ProductDetailBackOfficeView.as_view(), name='rud'),
    path('<int:pk>/notes/', views.ProductNoteListCreateView.as_view(), name='note-list-create'),
    path('<int:pk>/notes/<int:note_pk>/', views.ProductNoteRUDView.as_view(), name='note-rud'),
    path('<int:pk>/gallery/', views.ProductBackOfficeGalleryListView.as_view(),
         name='gallery-list'),
    path('<int:pk>/gallery/<int:image_id>/', views.ProductBackOfficeGalleryCreateDestroyView.as_view(),
         name='gallery-create-destroy'),
    # product types
    path('types/', views.ProductTypeListCreateBackOfficeView.as_view(), name='type-list-create'),
    path('types/<int:pk>/', views.ProductTypeRUDBackOfficeView.as_view(),
         name='type-retrieve-update-destroy'),
    path('types/attach-tag-category/', views.ProductTypeTagCategoryCreateBackOfficeView.as_view(),
         name='type-tag-category-create'),
    # product sub types
    path('subtypes/', views.ProductSubTypeListCreateBackOfficeView.as_view(),
         name='subtype-list-create'),
    path('subtypes/<int:pk>/', views.ProductSubTypeRUDBackOfficeView.as_view(),
         name='subtype-retrieve-update-destroy'),
    path('available_states/', views.ProductAvailableStatesSetView.as_view(), name='product-available-states',),
    path('<int:pk>/awards/bind-object/',
         views.ProductAwardBindViewSet.as_view({'post': 'bind_object', 'delete': 'bind_object'}),
         name='product-awards-create-and-bind', ),
]
