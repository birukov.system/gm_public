"""Location app web urlconf."""
from location.urls.common import urlpatterns as common_urlpatterns

urlpatterns = []

urlpatterns.extend(common_urlpatterns)
