"""Main app serializers."""
from typing import Union

from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers

from establishment.models import Employee
from location.models import Country
from location.serializers import CountrySerializer
from main import models
from tag.serializers import TagBackOfficeSerializer
from utils.exceptions import EmployeeNotFoundError
from utils.serializers import ProjectModelSerializer, RecursiveFieldSerializer, TranslatedField


class FeatureSerializer(serializers.ModelSerializer):
    """Feature serializer."""

    class Meta:
        """Meta class."""

        model = models.Feature
        fields = (
            'id',
            'slug',
            'priority',
            'route',
            'site_settings',
        )


class CurrencySerializer(ProjectModelSerializer):
    """Currency serializer."""

    name_translated = TranslatedField()

    class Meta:
        model = models.Currency
        fields = [
            'id',
            'name_translated',
            'sign'
        ]


class _FooterLinkSerializer(serializers.ModelSerializer):
    """FooterLink serializer"""

    link_label_translated = TranslatedField()

    class Meta:
        model = models.FooterLink
        fields = [
            'link',
            'link_label_translated',
            'is_highlighted',
            'is_last_element',
            'priority',
        ]


class FooterSerializer(serializers.ModelSerializer):
    """Footer serializer."""

    class Meta:
        model = models.Footer
        fields = [
            'id',
            'about_us',
            'copyright',
            'created',
            'modified',
            'links',
        ]


class _FooterSerializer(FooterSerializer):
    """Footer serializer."""
    links = _FooterLinkSerializer(many=True, read_only=True)


class FooterBackSerializer(_FooterSerializer):
    site_id = serializers.PrimaryKeyRelatedField(
        queryset=models.SiteSettings.objects.all(),
        source='site'
    )

    class Meta:
        model = models.Footer
        fields = FooterSerializer.Meta.fields + [
            'site_id',
        ]


class SiteFeatureSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='feature.id', allow_null=True, read_only=True)
    slug = serializers.CharField(source='feature.slug', allow_null=True, read_only=True)
    priority = serializers.IntegerField(source='feature.priority', allow_null=True, read_only=True)
    route = serializers.CharField(source='feature.route.name', allow_null=True, read_only=True)
    source = serializers.IntegerField(source='feature.source', allow_null=True, read_only=True)
    nested = RecursiveFieldSerializer(many=True, read_only=True, allow_null=True)
    chosen_tags = TagBackOfficeSerializer(
        source='feature.get_chosen_tags', many=True, read_only=True)
    feature_id = serializers.PrimaryKeyRelatedField(
        queryset=models.Feature.objects.all(),
        write_only=True,
        source='feature',
    )
    site_settings_id = serializers.PrimaryKeyRelatedField(
        queryset=models.SiteSettings.objects.all(),
        write_only=True,
        source='site_settings',
    )
    link_label_translated = TranslatedField()

    class Meta:
        """Meta class."""
        model = models.SiteFeature
        fields = (
            'id',
            'main',
            'slug',
            'priority',
            'route',
            'source',
            'nested',
            'chosen_tags',
            'feature_id',
            'site_settings_id',
            'published',
            'main',
            'backoffice',
            'is_highlighted',
            'link_url',
            'link_label_translated',
        )
        extra_kwargs = {
            'published': {'write_only': True},
            'main': {'write_only': True},
            'backoffice': {'write_only': True},
        }


class NavigationBarSectionBaseSerializer(SiteFeatureSerializer):
    """Serializer for navigation bar."""
    source_display = serializers.CharField(source='feature.get_source_display',
                                           read_only=True)

    class Meta(SiteFeatureSerializer.Meta):
        model = models.SiteFeature
        fields = [
            'id',
            'slug',
            'route',
            'source',
            'source_display',
            'priority',
            'nested',
        ]


class SiteSettingsSerializer(serializers.ModelSerializer):
    """Site settings serializer."""

    published_features = SiteFeatureSerializer(source='published_sitefeatures',
                                               many=True, allow_null=True, read_only=True)
    currency = CurrencySerializer(read_only=True)
    # todo: remove this
    country_code = serializers.CharField(source='subdomain', read_only=True)

    country_name = serializers.CharField(source='country.name_translated', read_only=True)
    time_format = serializers.CharField(source='country.time_format', read_only=True)
    footers = _FooterSerializer(many=True, read_only=True)

    class Meta:
        """Meta class."""

        model = models.SiteSettings
        fields = [
            'country_code',
            'time_format',
            'subdomain',
            'pinterest_page_url',
            'twitter_page_url',
            'facebook_page_url',
            'instagram_page_url',
            'contact_email',
            'config',
            'ad_config',
            'published_features',
            'currency',
            'country_name',
            'footers',
        ]


class SiteSettingsBackOfficeSerializer(SiteSettingsSerializer):
    """Site settings serializer for back office."""
    country_locales = serializers.SlugRelatedField(source='country.languages', slug_field='locale', many=True,
                                                   read_only=True)

    class Meta(SiteSettingsSerializer.Meta):
        """Meta class."""
        fields = SiteSettingsSerializer.Meta.fields + [
            'id',
            'country_locales'
        ]


class SiteSerializer(SiteSettingsSerializer):
    country = CountrySerializer(read_only=True)

    class Meta:
        """Meta class."""
        model = models.SiteSettings
        fields = SiteSettingsSerializer.Meta.fields + [
            'id',
            'country'
        ]


class SiteShortSerializer(serializers.ModelSerializer):
    """Short serializer for model SiteSettings."""

    class Meta(SiteSerializer.Meta):
        """Meta class."""
        fields = [
            'subdomain',
        ]


class AwardEntityTypeBaseSerializer(serializers.ModelSerializer):
    """Serializer for Award entity type model."""
    related_model_name = serializers.CharField(read_only=True)
    content_object_name = serializers.CharField(read_only=True)
    index_name = serializers.CharField(read_only=True)

    class Meta:
        """Meta class."""
        model = models.AwardEntityType
        fields = [
            'id',
            'index_name',
            'related_model_name',
            'content_object_name',
            'content_type',
            'object_id'
        ]
        extra_kwargs = {
            'content_type': {'write_only': True, 'required': True},
            'object_id': {'write_only': True, 'required': True}
        }


class AwardTypeBaseSerializer(serializers.ModelSerializer):
    """AwardType base serializer."""
    country_id = serializers.PrimaryKeyRelatedField(
        source='country',
        queryset=Country.objects.all(),
        write_only=True,
    )
    entity_type_id = serializers.PrimaryKeyRelatedField(
        source='entity_type',
        queryset=models.AwardEntityType.objects.all(),
        write_only=True
    )
    entity_type = AwardEntityTypeBaseSerializer(read_only=True)

    class Meta:
        model = models.AwardType
        fields = (
            'id',
            'name',
            'years',
            'country_id',
            'entity_type',
            'entity_type_id',
        )

    def create(self, validated_data):
        """An overridden create method."""
        return self.Meta.model.objects.make(**validated_data)


class AwardBaseSerializer(serializers.ModelSerializer):
    """Award base serializer."""

    title_translated = serializers.CharField(read_only=True, allow_null=True)
    title = serializers.CharField(write_only=True, help_text='Title text')

    class Meta:
        model = models.Award
        fields = [
            'id',
            'title_translated',
            'vintage_year',
            'image_url',
            'title',
        ]

    @property
    def request(self):
        """Return a request object"""
        return self.context.get('request')

    @property
    def context_kwargs(self) -> Union[dict, None]:
        """Return a request kwargs."""
        if hasattr(self.request, 'parser_context'):
            return self.request.parser_context.get('kwargs')

    def validate_title(self, value) -> dict:
        """Construct title str to JSON that contains locale from request."""
        return {self.request.locale: value}


class AwardSerializer(AwardBaseSerializer):
    """Award serializer."""

    class Meta(AwardBaseSerializer.Meta):
        fields = AwardBaseSerializer.Meta.fields + ['award_type_id', ]


class BackAwardSerializer(AwardBaseSerializer):
    """Award serializer."""
    award_type_display = AwardTypeBaseSerializer(read_only=True,
                                                 source='award_type')
    award_type = serializers.PrimaryKeyRelatedField(
        queryset=models.AwardType.objects.all(),
        write_only=True,
        required=True,
    )

    class Meta(AwardBaseSerializer.Meta):
        fields = AwardBaseSerializer.Meta.fields + [
            'award_type',
            'award_type_display',
            'state',
            'content_type',
            'object_id',
        ]

    def to_representation(self, instance):
        data = super(BackAwardSerializer, self).to_representation(instance)
        data['award_type'] = data.pop('award_type_display', None)
        return data


class _MainAwardTypeSerializer(serializers.ModelSerializer):
    """AwardType serializer."""

    class Meta:
        model = models.AwardType
        fields = '__all__'


class BackAwardEmployeeCreateSerializer(AwardBaseSerializer):
    """Award, The Creator."""

    class Meta(AwardBaseSerializer.Meta):
        fields = (
            'id',
            'award_type',
            'title',
            'vintage_year',
        )

    def validate(self, attrs):
        """An overridden validate method."""
        employee_id = self.context_kwargs.get('employee_id')
        employee_qs = Employee.objects.filter(id=employee_id)

        if not employee_qs.exists():
            raise EmployeeNotFoundError()

        attrs['object_id'] = employee_id
        attrs['content_type'] = ContentType.objects.get_for_model(Employee)
        return attrs

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['award_type'] = _MainAwardTypeSerializer(
            models.AwardType.objects.get(pk=data['award_type'])
        ).data
        return data


class CarouselListSerializer(serializers.ModelSerializer):
    """Serializer for retrieving list of carousel items."""

    model_name = serializers.CharField()
    name = serializers.CharField()
    toque_number = serializers.IntegerField()
    public_mark = serializers.IntegerField()
    image = serializers.URLField(source='image_url')
    awards = AwardBaseSerializer(many=True)
    vintage_year = serializers.IntegerField()
    last_award = AwardBaseSerializer(source='the_most_recent_award', allow_null=True)

    class Meta:
        """Meta class."""
        model = models.Carousel
        fields = [
            'id',
            'model_name',
            'name',
            'awards',
            'toque_number',
            'public_mark',
            'image',
            'vintage_year',
            'last_award',
            'slug',
            'link',
        ]


class PageBaseSerializer(serializers.ModelSerializer):
    """Serializer for model Page"""

    class Meta:
        """Meta class."""
        model = models.Page
        fields = [
            'id',
            'image_url',
            'width',
            'height',
            'advertisement',
        ]
        extra_kwargs = {
            'advertisement': {'write_only': True},
            'image_url': {'required': True},
            'width': {'required': True},
            'height': {'required': True},
        }


class PageExtendedSerializer(PageBaseSerializer):
    """Extended serializer for model Page."""
    source_display = serializers.CharField(read_only=True,
                                           source='get_source_display')

    class Meta(PageBaseSerializer.Meta):
        """Meta class."""
        fields = PageBaseSerializer.Meta.fields + [
            'source',
            'source_display',
        ]


class PageTypeBaseSerializer(serializers.ModelSerializer):
    """Serializer for model PageType."""

    class Meta:
        """Meta class."""
        model = models.PageType
        fields = [
            'id',
            'name',
        ]


class ContentTypeBackSerializer(serializers.ModelSerializer):
    """Serializer for model ContentType."""

    class Meta:
        model = ContentType
        fields = '__all__'


class PanelExecuteSerializer(serializers.ModelSerializer):
    """Panel execute serializer."""

    class Meta:
        model = models.Panel
        fields = [
            'id',
            'name',
            'display',
            'description',
            'query',
            'created',
            'modified',
            'user',
            'user_id'
        ]


class NavigationBarPermissionBaseSerializer(serializers.ModelSerializer):
    """Navigation bar permission serializer."""

    sections = NavigationBarSectionBaseSerializer(many=True, read_only=True)
    permission_mode_display = serializers.CharField(source='get_permission_mode_display',
                                                    read_only=True)

    class Meta:
        """Meta class."""
        model = models.NavigationBarPermission
        fields = [
            'id',
            'sections',
            'permission_mode_display',
        ]
