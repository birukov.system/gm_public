from django.contrib import admin

from booking.models import models


@admin.register(models.Booking)
class BookingModelAdmin(admin.ModelAdmin):
    """Model admin for model Comment"""