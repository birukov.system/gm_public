from django.contrib import admin
from translation import models


@admin.register(models.Language)
class LanguageAdmin(admin.ModelAdmin):
    """Language admin."""


@admin.register(models.SiteInterfaceDictionary)
class SiteInterfaceDictionaryAdmin(admin.ModelAdmin):
    """Site interface dictionary admin conf."""
