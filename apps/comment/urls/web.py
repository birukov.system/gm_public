"""Web urlpaths."""
from comment.urls.common import urlpatterns as common_urlpatterns

app_name = 'comment'

urlpatterns_api = []

urlpatterns = common_urlpatterns + \
              urlpatterns_api
