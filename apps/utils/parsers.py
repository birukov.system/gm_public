

class NewsSlug:
    def __init__(self, value=None, country_code=None, count=0):
        self.value = value
        self.country_code = country_code
        self.count = count

    @classmethod
    def parse(cls, raw_slug, country_codes):
        slug, *rest = raw_slug.split('-')
        instance = NewsSlug()

        if len(rest) >= 1 and rest[-1] in country_codes:
            # It is like 'slug-en'

            instance.value = '-'.join([slug, *rest[:-1]])
            instance.country_code = rest[-1]
        elif len(rest) >= 2 and rest[-1].isdigit() and rest[-2] in country_codes:
            # It is like 'slug-en-1'

            instance.value = '-'.join([slug, *rest[:-2]])
            instance.country_code = rest[-2]
            instance.count = int(rest[-1])
        else:
            # It is like 'slug'

            instance.value = '-'.join([slug, *rest])

        return instance

    def __lt__(self, other):
        return self.value < other.value

    def __str__(self):
        if self.value is None:
            raise ValueError('No value for slug')

        slug_parts = [self.value]
        if self.country_code is not None:
            slug_parts.append(self.country_code)

        if self.count != 0:
            slug_parts.append(str(self.count))

        return '-'.join(slug_parts)

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.value}, {self.country_code}, {self.count}>'
