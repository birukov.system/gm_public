from main.urls.common import common_urlpatterns

from django.urls import path

from main.views.mobile import FeaturesView, SiteSettingsMobileView

urlpatterns = [
    path('site-settings/', SiteSettingsMobileView.as_view(), name='mobile-site-settings'),
    path('features/', FeaturesView.as_view(), name='features'),
]

urlpatterns.extend(common_urlpatterns)
