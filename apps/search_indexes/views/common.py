"""Search indexes app views."""
from django_elasticsearch_dsl_drf import constants, pagination
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    GeoSpatialOrderingFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
)
from establishment.models import Establishment
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet
from rest_framework import permissions

from product.models import Product
from search_indexes import serializers, filters, utils
from search_indexes.documents import EstablishmentDocument, NewsDocument
from search_indexes.documents.product import ProductDocument
from utils.methods import get_permission_classes
from utils.permissions import (IsEstablishmentAdministrator, IsEstablishmentManager)
from utils.pagination import ESDocumentPagination


class NewsDocumentViewSet(BaseDocumentViewSet):
    """
    ## News ES-document ViewSet.
    ### *GET*
    #### Description
    Return paginated ES document.

    #### Ordering
    * start

    #### Faceted search fields
    * tag

    #### Search fields
    * title
    * subtitle
    * description

    #### Filter fields
    * tags_id (`int`) - filter by tag identifiers
    * tag (`int`) - filter by tag identifier
    * type (`str`) - filter by name of news type
    * tag_value (`str`) - filter by tag value
    * slug (`str`) - filter by news slug
    * country_id (`int`) - filter by a country identifier
    * country (`str`) - filter by country code

    ##### Response
    E.g.
    ```
    {
        "count": 3314,
        "next": 2,
        "previous": null,
        "facets": {
          ...
        },
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```
    """

    def get_queryset(self):
        qs = super(NewsDocumentViewSet, self).get_queryset()
        qs = qs.filter('match', has_any_desc_active=True)
        return qs

    document = NewsDocument
    lookup_field = 'slug'
    pagination_class = ESDocumentPagination
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.NewsDocumentSerializer

    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.CustomSearchFilterBackend,
        FilteringFilterBackend,
        filters.CustomFacetedSearchFilterBackend,
        OrderingFilterBackend
    ]

    ordering_fields = {
        'start': {
            'field': 'start',
        },
    }

    faceted_search_fields = {
        'tag': {
            'field': 'tags.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
    }

    search_fields = {
        'title_searchable': {'boost': 3},
        'subtitle_searchable': {'boost': 2},
        'description_searchable': {},
    }
    translated_search_fields = (
        'title',
        'subtitle',
        'description',
    )

    filter_fields = {
        'tags_id': {
            'field': 'tags.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'tag': {
            'field': 'tags.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ]
        },
        'type': {
            'field': 'news_type.name',
        },
        'tag_value': {
            'field': 'tags.value',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE
            ]
        },
        'slug': 'slug',
        'country_id': {
            'field': 'country.id'
        },
        'country': {
            'field': 'country.code'
        },
    }


class MobileNewsDocumentViewSet(NewsDocumentViewSet):

    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.CustomSearchFilterBackend,
        FilteringFilterBackend,
        DefaultOrderingFilterBackend,
    ]

    ordering_fields = {
        'start': {
            'field': 'start',
        },
    }

    ordering = ('-start',)


class EstablishmentDocumentViewSet(BaseDocumentViewSet):
    """Establishment document ViewSet."""

    document = EstablishmentDocument
    pagination_class = ESDocumentPagination
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.EstablishmentDocumentSerializer

    def get_queryset(self):
        qs = super(EstablishmentDocumentViewSet, self).get_queryset()
        qs = qs.filter('match', status=Establishment.PUBLISHED)
        return qs

    filter_backends = [
        filters.AccurateEsCountBackend,
        FilteringFilterBackend,
        filters.CustomSearchFilterBackend,
        filters.CustomGeoSpatialFilteringFilterBackend,
        filters.CustomFacetedSearchFilterBackend,
        GeoSpatialOrderingFilterBackend,
    ]

    faceted_search_fields = {
        'works_at_weekday': {
            'field': 'works_at_weekday',
            'facet': TermsFacet,
            'enabled': True,
        },
        'toque_number': {
            'field': 'toque_number',
            'enabled': True,
            'facet': TermsFacet,
        },
        'works_noon': {
            'field': 'works_noon',
            'facet': TermsFacet,
            'enabled': True,
        },
        'works_evening': {
            'field': 'works_evening',
            'facet': TermsFacet,
            'enabled': True,
        },
        'works_now': {
            'field': 'works_now',
            'facet': TermsFacet,
            'enabled': True,
        },
        'tag': {
            'field': 'tags.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_colors': {
            'field': 'products.wine_colors.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_region_id': {
            'field': 'wine_origins.wine_region.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_sub_region_id': {
            'field': 'wine_origins.wine_sub_region.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        }
    }

    search_fields = {
        'name': {'boost': 4},
        'transliterated_name': {'boost': 3},
        'description_searchable': {},
    }
    filter_fields = {
        'slug': 'slug',
        'tag': {
            'field': 'tags.id',
            'lookups': [constants.LOOKUP_QUERY_IN]
        },
        'toque_number': {
            'field': 'toque_number',
            'lookups': [
                constants.LOOKUP_FILTER_RANGE,
                constants.LOOKUP_QUERY_GT,
                constants.LOOKUP_QUERY_GTE,
                constants.LOOKUP_QUERY_LT,
                constants.LOOKUP_QUERY_LTE,
                constants.LOOKUP_QUERY_IN,
            ]
        },
        'price_level': {
            'field': 'price_level',
            'lookups': [
                constants.LOOKUP_FILTER_RANGE,
                constants.LOOKUP_QUERY_GT,
                constants.LOOKUP_QUERY_GTE,
                constants.LOOKUP_QUERY_LT,
                constants.LOOKUP_QUERY_LTE,
                constants.LOOKUP_QUERY_IN,
            ]
        },
        'wine_colors_id': {
            'field': 'products.wine_colors.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'wine_region_id': {
            'field': 'wine_origins.wine_region.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'wine_sub_region_id': {
            'field': 'wine_origins.wine_sub_region.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'country_id': {
            'field': 'address.city.country.id'
        },
        'country': {
            'field': 'address.city.country.code'
        },
        'tags_id': {
            'field': 'tags.id',
        },
        'tags_category_id': {
            'field': 'tags.category.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
            ],
        },
        'collection_type': {
            'field': 'collections.collection_type'
        },
        'establishment_type': {
            'field': 'establishment_type.id'
        },
        'establishment_subtypes': {
            'field': 'establishment_subtypes.id'
        },
        'type': {
            'field': 'establishment_type.index_name'
        },
        'subtype': {
            'field': 'establishment_subtypes.index_name',
        },
        'generic_type': {
            'field': 'generic_types',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
            ]
        },
        'works_noon': {
            'field': 'works_noon',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
            ],
        },
        'works_at_weekday': {
            'field': 'works_at_weekday',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
            ],
        },
        'works_evening': {
            'field': 'works_evening',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
            ],
        },
        'works_now': {
            'field': 'works_now',
            'lookups': [
                constants.LOOKUP_FILTER_TERM,
            ]
        },
    }

    geo_spatial_filter_fields = {
        'location': {
            'field': 'address.coordinates',
            'lookups': [
                constants.LOOKUP_FILTER_GEO_BOUNDING_BOX,
            ]
        }
    }

    geo_spatial_ordering_fields = {
        'location': {
            'field': 'address.coordinates',
        },
    }


class MobileEstablishmentDocumentViewSet(EstablishmentDocumentViewSet):

    serializer_class = serializers.EstablishmentMobileDocumentSerializer
    filter_backends = [
        filters.AccurateEsCountBackend,
        FilteringFilterBackend,
        filters.CustomSearchFilterBackend,
        filters.CustomGeoSpatialFilteringFilterBackend,
        GeoSpatialOrderingFilterBackend,
    ]


class ProductDocumentViewSet(BaseDocumentViewSet):
    """Product document ViewSet."""

    document = ProductDocument
    pagination_class = ESDocumentPagination
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.ProductDocumentSerializer

    filter_backends = [
        filters.AccurateEsCountBackend,
        FilteringFilterBackend,
        filters.CustomSearchFilterBackend,
        filters.CustomFacetedSearchFilterBackend,
        OrderingFilterBackend,
        # GeoSpatialOrderingFilterBackend,
    ]


    def get_queryset(self):
        qs = super(ProductDocumentViewSet, self).get_queryset()
        qs = qs.filter('match', state=Product.PUBLISHED)
        return qs

    ordering_fields = {
        'created': {
            'field': 'created',
        },
    }

    search_fields = {
        'name': {'boost': 8},
        'description_searchable': {},
    }

    faceted_search_fields = {
        'tag': {
            'field': 'tags.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_region_id': {
            'field': 'wine_origins.wine_region.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_sub_region_id': {
            'field': 'wine_origins.wine_sub_region.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_colors': {
            'field': 'wine_colors.id',
            'facet': TermsFacet,
            'enabled': True,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
    }

    translated_search_fields = (
        'description',
    )

    filter_fields = {
        'slug': 'slug',
        'tags_id': {
            'field': 'tags.id',
            'lookups': [constants.LOOKUP_QUERY_IN],
        },
        'country': {
            'field': 'establishment.city.country.code',
        },
        'wine_colors_id': {
            'field': 'wine_colors.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'wine_region_id': {
            'field': 'wine_origins.wine_region.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'wine_sub_region_id': {
            'field': 'wine_origins.wine_sub_region.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'grape_variety_id': {
            'field': 'grape_variety.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ]
        },
        # 'wine_from_country_code': {
        #     'field': 'wine_origins.wine_region.country.code',
        # },
        'for_establishment': {
            'field': 'establishment.slug',
        },
        'product_type': {
            'field': 'product_type.index_name',
        },
        'subtype': {
            'field': 'subtypes.index_name',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
    }


class MobileProductDocumentViewSet(ProductDocumentViewSet):

    filter_backends = [
        filters.AccurateEsCountBackend,
        FilteringFilterBackend,
        filters.CustomSearchFilterBackend,
    ]
