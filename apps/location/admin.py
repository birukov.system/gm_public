from django.contrib.gis import admin
from location import models
from django.contrib.gis.geos import Point
from django.utils.translation import gettext_lazy as _
from utils.admin import BaseModelAdminMixin


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    """Country admin."""


@admin.register(models.Region)
class RegionAdmin(admin.ModelAdmin):
    """Region admin."""


@admin.register(models.City)
class CityAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """City admin."""
    raw_id_fields = ['country', 'region', 'image', ]


@admin.register(models.Address)
class AddressAdmin(admin.OSMGeoAdmin):
    """Address admin."""
    list_display = ('__str__', 'geo_lon', 'geo_lat')
    readonly_fields = ['geo_lon', 'geo_lat', ]
    fieldsets = (
        (_('Main'), {
            'fields': ['street_name_1', 'street_name_2', 'number']
        }),
        (_('Location'), {
            'fields': ['coordinates', 'geo_lon', 'geo_lat', ]
        }),
        (_('Address detail'), {
            'fields': ['city', 'postal_code', ]
        }),

    )

    def geo_lon(self, item):
        # Point(longitude, latitude)
        if isinstance(item.coordinates, Point):
            return item.coordinates.x

    def geo_lat(self, item):
        if isinstance(item.coordinates, Point):
            return item.coordinates.y


@admin.register(models.EstablishmentWineOriginAddress)
class EstablishmentWineOriginAddress(admin.ModelAdmin):
    """Admin model for EstablishmentWineOriginAddress."""
    raw_id_fields = ['establishment', ]


@admin.register(models.WineOriginAddress)
class WineOriginAddress(admin.ModelAdmin):
    """Admin page for model WineOriginAddress."""
    raw_id_fields = ['product', ]
