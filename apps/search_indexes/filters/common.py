"""Search indexes filters."""
from elasticsearch_dsl.query import Q
from django_elasticsearch_dsl_drf.filter_backends import SearchFilterBackend, \
    FacetedSearchFilterBackend, GeoSpatialFilteringFilterBackend, FilteringFilterBackend
from django_elasticsearch_dsl_drf.constants import ALL_LOOKUP_FILTERS_AND_QUERIES
from six import iteritems, string_types
from search_indexes.documents import TagCategoryDocument
from tag.models import TagCategory
from rest_framework.filters import BaseFilterBackend
from establishment.models import Establishment


class AccurateEsCountBackend(BaseFilterBackend):
    """
    Backend which tells es to track total documents count accurately
    https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes-7.0.html#track-total-hits-10000-default
    """

    def filter_queryset(self, request, queryset, view):
        return queryset.extra(track_total_hits=True)


class RbacObjectsBackend(BaseFilterBackend):
    """
    Implements role based access to objects by narrowing queryset
    """
    @staticmethod
    def get_filter(request, queryset, view):
        if hasattr(view, 'rbac_filter'):
            return view.rbac_filter(request)
        return Establishment.objects.available_establishments(request.user, request.country_code)

    def filter_queryset(self, request, queryset, view):

        assert hasattr(view, 'country_field'), 'request country field is not declared on view class'
        assert hasattr(request, 'country_code'), 'request has no country code at all'
        assert request.country_code is not None, 'request country code is empty'

        if request.user.has_admin_roles:
            #  in this case we should narrow by country_code only
            return queryset.query('term', **{view.country_field: request.country_code})
        ids = self.get_filter(request, queryset, view).values_list('pk', flat=True)
        return queryset.filter('terms', id=list(ids))


class CustomGeoSpatialFilteringFilterBackend(GeoSpatialFilteringFilterBackend):
    """Automatically adds centering and sorting within bounding box."""

    @staticmethod
    def calculate_center(first, second):
        if second[1] < first[1]:
            res_longtitude = first[1] + (360 + abs(first[1]) - abs(second[1])) / 2
        else:
            res_longtitude = first[1] + (second[1] - first[1]) / 2

        # return (first[0] + second[0]) / 2, result_part
        return (first[0] + second[0]) / 2, res_longtitude

    def filter_queryset(self, request, queryset, view):
        ret = super().filter_queryset(request, queryset, view)
        bb = request.query_params.get('location__geo_bounding_box')
        if bb:
            center = self.calculate_center(*map(lambda point: list(map(float, point.split(','))), bb.split('__')))
            request.GET._mutable = True
            request.query_params.update({
                'ordering': f'location__{center[0]}__{center[1]}__km'
            })
            request.GET._mutable = False
        return ret


class CustomFacetedSearchFilterBackend(FacetedSearchFilterBackend):

    def __init__(self):
        self.facets_computed = {}

    def aggregate(self, request, queryset, view):
        """Aggregate.

        :param request:
        :param queryset:
        :param view:
        :return:
        """

        def make_filter(cur_facet):
            def _filter(x):
                return cur_facet['facet']._params['field'] != next(iter(x._params))

            return _filter

        def make_tags_filter(cur_facet, tags_to_remove_ids):
            def _filter(x):
                if hasattr(x, '_params') and (x._params.get('must') or x._params.get('should')):
                    ret = []
                    for t in ['must', 'should']:
                        terms = x._params.get(t)
                        if terms:
                            for term in terms:
                                if cur_facet['facet']._params['field'] != next(iter(term._params)):
                                    return True  # different fields. preserve filter
                                else:
                                    ret.append(next(iter(term._params.values())) not in tags_to_remove_ids)
                    return all(ret)
                if cur_facet['facet']._params['field'] != next(iter(x._params)):
                    return True  # different fields. preserve filter
                else:
                    return next(iter(x._params.values())) not in tags_to_remove_ids

            return _filter

        __facets = self.construct_facets(request, view)
        setattr(view.paginator, 'facets_computed', {})
        for __field, __facet in iteritems(__facets):
            agg = __facet['facet'].get_aggregation()
            agg_filter = Q('match_all')
            if __facet['global']:
                queryset.aggs.bucket(
                    '_filter_' + __field,
                    'global'
                ).bucket(__field, agg)
            else:
                if __field != 'tag':
                    qs = queryset.__copy__()
                    qs.query = queryset.query._clone()
                    filterer = make_filter(__facet)
                    for param_type in ['must', 'must_not', 'should']:
                        if qs.query._proxied._params.get(param_type):
                            qs.query._proxied._params[param_type] = list(
                                filter(
                                    filterer, qs.query._proxied._params[param_type]
                                )
                            )
                    sh = qs.query._proxied._params.get('should')
                    if (not sh or not len(sh)) \
                            and qs.query._proxied._params.get('minimum_should_match'):
                        qs.query._proxied._params.pop('minimum_should_match')
                    facet_name = '_filter_' + __field
                    qs.aggs.bucket(
                        facet_name,
                        'filter',
                        filter=agg_filter
                    ).bucket(__field, agg)
                    view.paginator.facets_computed.update({facet_name: qs.execute().aggregations[facet_name]})
                else:
                    tag_facets = []
                    preserve_ids = []
                    facet_name = '_filter_' + __field
                    all_tag_categories = list(TagCategoryDocument.search() \
                                              .filter('term', public=True) \
                                              .filter(
                        Q('term', value_type=TagCategory.LIST) | Q('match', index_name='wine-color'))[0:1000])
                    for category in all_tag_categories:
                        tags_to_remove = list(map(lambda t: str(t.id), category.tags))
                        qs = queryset.__copy__()
                        qs.query = queryset.query._clone()
                        filterer = make_tags_filter(__facet, tags_to_remove)
                        for param_type in ['must', 'should']:
                            if qs.query._proxied._params.get(param_type):
                                if qs.query._proxied._params.get(param_type):
                                    qs.query._proxied._params[param_type] = list(
                                        filter(
                                            filterer, qs.query._proxied._params[param_type]
                                        )
                                    )
                        sh = qs.query._proxied._params.get('should')
                        if (not sh or not len(sh)) \
                                and qs.query._proxied._params.get('minimum_should_match'):
                            qs.query._proxied._params.pop('minimum_should_match')
                        qs.aggs.bucket(
                            facet_name,
                            'filter',
                            filter=agg_filter
                        ).bucket(__field, agg)
                        tag_facets.append(qs.execute().aggregations[facet_name])
                        preserve_ids.append(list(map(int, tags_to_remove)))
                    view.paginator.facets_computed.update({facet_name: self.merge_buckets(tag_facets, preserve_ids)})
        return queryset

    @staticmethod
    def merge_buckets(buckets: list, preserve_ids: list):
        """Reduces all buckets preserving class"""
        result_bucket = buckets[0]
        result_bucket.tag.buckets = list(filter(lambda x: x['key'] in preserve_ids[0], result_bucket.tag.buckets._l_))
        for bucket, ids in list(zip(buckets, preserve_ids))[1:]:
            for tag in bucket.tag.buckets._l_:
                if tag['key'] in ids:
                    result_bucket.tag.buckets.append(tag)
        return result_bucket


class CustomSearchFilterBackend(SearchFilterBackend):
    """Custom SearchFilterBackend."""

    def construct_search(self, request, view):
        """Construct search.

        We have to deal with two types of structures:

        Type 1:

        >>> search_fields = (
        >>>     'title',
        >>>     'description',
        >>>     'summary',
        >>> )

        Type 2:

        >>> search_fields = {
        >>>     'title': {'boost': 2},
        >>>     'description': None,
        >>>     'summary': None,
        >>> }

        :param request: Django REST framework request.
        :param queryset: Base queryset.
        :param view: View.
        :type request: rest_framework.request.Request
        :type queryset: elasticsearch_dsl.search.Search
        :type view: rest_framework.viewsets.ReadOnlyModelViewSet
        :return: Updated queryset.
        :rtype: elasticsearch_dsl.search.Search
        """
        query_params = self.get_search_query_params(request)
        __queries = []
        for search_term in query_params:
            __values = self.split_lookup_name(search_term, 1)
            __len_values = len(__values)
            if __len_values > 1:
                field, value = __values
                if field in view.search_fields:
                    # Initial kwargs for the match query
                    field_kwargs = {field: {'query': value}}
                    # In case if we deal with structure 2
                    if isinstance(view.search_fields, dict):
                        extra_field_kwargs = view.search_fields[field]
                        if 'fuzzy_rewrite' not in extra_field_kwargs:
                            extra_field_kwargs['fuzzy_rewrite'] = 'top_terms_boost_1'
                        if extra_field_kwargs:
                            field_kwargs[field].update(extra_field_kwargs)
                    # The match query
                    __queries.append(
                        Q("match", **field_kwargs)
                    )
                    __queries.append(
                        Q('wildcard', **{field: {'value': f'*{search_term.lower()}*',
                                                 'boost': field_kwargs[field].get('boost', 1) + 30,
                                                 'rewrite': 'top_terms_boost_1'}})
                    )
            else:
                for field in view.search_fields:
                    # Initial kwargs for the match query
                    field_kwargs = {field: {'query': search_term}}

                    # In case if we deal with structure 2
                    if isinstance(view.search_fields, dict):
                        extra_field_kwargs = view.search_fields[field]
                        if 'fuzzy_rewrite' not in extra_field_kwargs:
                            extra_field_kwargs['fuzzy_rewrite'] = 'top_terms_boost_1'
                        if extra_field_kwargs:
                            field_kwargs[field].update(extra_field_kwargs)
                    # The match query
                    __queries.append(
                        Q("match", **field_kwargs)
                    )
                    __queries.append(
                        Q('wildcard', **{field: {'value': f'*{search_term.lower()}*',
                                                 'boost': field_kwargs[field].get('boost', 1) + 30,
                                                 'rewrite': 'top_terms_boost_1'}})
                    )
        return __queries

    def filter_queryset(self, request, queryset, view):
        __queries = self.construct_search(request, view) + \
            self.construct_nested_search(request, view)

        if __queries:
            queryset = queryset.query('bool', should=__queries)
        return queryset


class PostFilterBackend(FilteringFilterBackend):

    @classmethod
    def apply_query(cls, queryset, options=None, args=None, kwargs=None):
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        return queryset.post_filter(*args, **kwargs)

    @classmethod
    def apply_filter(cls, queryset, options=None, args=None, kwargs=None):
        """Apply filter.

        :param queryset:
        :param options:
        :param args:
        :param kwargs:
        :return:
        """
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        return queryset.post_filter(*args, **kwargs)

    @classmethod
    def prepare_filter_fields(cls, view):
        """Prepare filter fields.

        :param view:
        :type view: rest_framework.viewsets.ReadOnlyModelViewSet
        :return: Filtering options.
        :rtype: dict
        """
        filter_fields = view.post_filter_fields
        if hasattr(view, 'except_field'):
            filter_fields = filter_fields.pop(view.except_field)

        for field, options in filter_fields.items():
            if options is None or isinstance(options, string_types):
                filter_fields[field] = {
                    'field': options or field
                }
            elif 'field' not in filter_fields[field]:
                filter_fields[field]['field'] = field

            if 'lookups' not in filter_fields[field]:
                filter_fields[field]['lookups'] = tuple(
                    ALL_LOOKUP_FILTERS_AND_QUERIES
                )

        return filter_fields


class FilteringFilterHelper(FilteringFilterBackend):
    @classmethod
    def prepare_filter_fields(cls, view):
        """Prepare filter fields.

        :param view:
        :type view: rest_framework.viewsets.ReadOnlyModelViewSet
        :return: Filtering options.
        :rtype: dict
        """
        filter_fields = view.post_filter_fields.copy()
        assert hasattr(view, 'except_field')

        filter_fields.pop(view.except_field)

        for field, options in filter_fields.items():
            if options is None or isinstance(options, string_types):
                filter_fields[field] = {
                    'field': options or field
                }
            elif 'field' not in filter_fields[field]:
                filter_fields[field]['field'] = field

            if 'lookups' not in filter_fields[field]:
                filter_fields[field]['lookups'] = tuple(
                    ALL_LOOKUP_FILTERS_AND_QUERIES
                )

        return filter_fields