# Generated by Django 2.2.7 on 2020-02-27 13:30

from django.db import migrations, models
import django.db.models.deletion


def delete_all_advertisements(apps, schema_editor):
    Page = apps.get_model("main", "Page")
    Advertisement = apps.get_model("advertisement", "Advertisement")
    db_alias = schema_editor.connection.alias
    Page.objects.using(db_alias).all().delete()
    Advertisement.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0061_page_page_type'),
    ]

    operations = [
        migrations.RunPython(delete_all_advertisements),
        migrations.AlterField(
            model_name='page',
            name='advertisement',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='page', to='advertisement.Advertisement', verbose_name='advertisement'),
        ),
        migrations.AlterField(
            model_name='page',
            name='page_type',
            field=models.CharField(choices=[('homepage', 'Homepage'), ('all_establishments', 'All establishments'), ('restaurants', 'Restaurants'), ('artisans', 'Artisans'), ('wineries', 'Wineries'), ('distilleries', 'Distilleries'), ('food_producers', 'Food producers'), ('all_products', 'All Products'), ('food', 'Food'), ('liquors', 'Liquors'), ('wines', 'Wines'), ('list_of_news', 'List of news'), ('news_and_recipes', 'News & recipes'), ('news', 'News'), ('recipes', 'Recipes')], max_length=32, null=True, verbose_name='Page Type'),
        ),
    ]
