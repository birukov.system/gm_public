"""SearchIndex tasks."""
import logging
from django.db import models
from celery.schedules import crontab
from celery.task import periodic_task
from django_elasticsearch_dsl.registries import registry
from django_redis import get_redis_connection
from establishment.models import Establishment
from news.models import News
from product.models import Product
from typing import List


logger = logging.getLogger(__name__)


# @periodic_task(run_every=crontab(minute='*/1'))
# def update_index():
#     """Updates ES index."""
#     cn = get_redis_connection('es_queue')
#     for model in [Establishment, News, Product]:
#         model_name = model._meta.model_name
#         while True:
#             ids = cn.spop(model_name, 500)
#             if not ids:
#                 break
#             qs = model.objects.filter(id__in=ids)
#             try:
#                 doc = registry.get_documents([model]).pop()
#             except KeyError:
#                 pass
#             else:
#                 doc().update(qs)


def es_update(key: str, related_ids: List[int]):
    """Adds an object to set of objects for indexing."""
    cn = get_redis_connection('es_queue')
    cn.sadd(key, *related_ids)
