"""Main app urls."""
from django.urls import path
from main.views import *

app = 'main'


common_urlpatterns = [
    path('awards/', AwardView.as_view(), name='awards_list'),
    path('awards/<int:pk>/', AwardRetrieveView.as_view(), name='awards_retrieve'),
    path('carousel/', CarouselListView.as_view(), name='carousel-list'),
    path('determine-location/', DetermineLocation.as_view(), name='determine-location'),
    path('content-pages/', ContentPageView.as_view(), name='content-pages-list'),
    path('content-pages/privacy-policy/', ContentPagePrivacyPolicyView.as_view()),
    path('content-pages/<int:pk>/', ContentPageIdRetrieveView.as_view(), name='content-pages-retrieve-id'),
    path('content-pages/create/', ContentPageAdminView.as_view(), name='content-pages-admin-list'),
    path('content-pages/slug/<slug:slug>/', ContentPageRetrieveView.as_view(), name='content-pages-retrieve-slug'),
    path('content-pages/update/slug/<slug:slug>/', ContentPageRetrieveAdminView.as_view(),
         name='content-pages-admin-retrieve')
]
