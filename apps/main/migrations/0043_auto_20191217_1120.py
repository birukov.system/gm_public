# Generated by Django 2.2.7 on 2019-12-17 11:20

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0042_panel'),
    ]

    operations = [
        migrations.CreateModel(
            name='FooterLinks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='Date created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('link', models.URLField(verbose_name='link')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='footer',
            name='links',
            field=models.ManyToManyField(related_name='link_footer', to='main.FooterLinks', verbose_name='links'),
        ),
    ]
