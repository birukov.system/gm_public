from rest_framework import generics

from comment import models
from comment.serializers import CommentBaseSerializer
from utils.methods import get_permission_classes
from utils.permissions import IsModerator


class CommentLstView(generics.ListCreateAPIView):
    """
    ## Comment list create view.
    ### *GET*
    #### Description
    Return paginated list of comments with ordering by `created` (new first).
    Result can be filtered by `type` or/and `object` via kwarg parameters.
    For filter by
    ##### Response
    E.g.
    ```
    {
        "count": 0,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```
    ### *POST*
    #### Description
    Create a new comment.
    ##### Request
    ##### Required
    * text (`str`) - text of comment
    ##### Available
    * mark (`int`) - comment mark (from 0 to 10)
    * status (`int`) -
    ```
    WAITING = 0
    PUBLISHED = 1
    REJECTED = 2
    DELETED = 3
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    def get_queryset(self):
        from product.models import Product
        from establishment.models import Establishment

        allowed = {
            "product": Product.__name__.lower(),
            "establishment": Establishment.__name__.lower()
        }

        qs = models.Comment.objects.with_base_related()

        if "object" not in self.kwargs and "type" not in self.kwargs:
            pass

        if "object" in self.kwargs:
            qs = qs.by_object_id(self.kwargs["object"])

        if "type" in self.kwargs and self.kwargs["type"] in allowed:
            model = allowed[self.kwargs["type"]]
            qs = qs.by_content_type(self.kwargs["type"], model)

        return qs.order_by('-created')

    serializer_class = CommentBaseSerializer
    queryset = models.Comment.objects.all()
    permission_classes = get_permission_classes(IsModerator)


class CommentRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Comment RUD view.
    ### *GET*
    #### Description
    Return serialized object of a comment by comment identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an object by an identifier.
    ##### Request
    * text (`str`) - text of comment
    * mark (`int`) - comment mark (from 0 to 10)
    * status (`int`) -
    ```
    WAITING = 0
    PUBLISHED = 1
    REJECTED = 2
    DELETED = 3
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete a comment object by an identifier
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = CommentBaseSerializer
    queryset = models.Comment.objects.all()
    permission_classes = get_permission_classes(IsModerator)
    lookup_field = 'id'
