from pprint import pprint

from account.models import User
from establishment.models import Establishment
from transfer.models import Comments
from transfer.serializers.comments import CommentSerializer


def transfer_comments():
    # В queryset исключены объекты по условию в связанные моделях
    # см. transfer_establishment() и transfer_user()
    establishments = Establishment.objects.all().values_list('old_id', flat=True)
    users = User.objects.all().values_list('old_id', flat=True)
    queryset = Comments.objects.filter(
        establishment_id__in=list(establishments),
        account_id__in=list(users),
    ).only(
        'id',
        'comment',
        'mark',
        'establishment_id',
        'account_id',
        'state',
    )

    serialized_data = CommentSerializer(data=list(queryset.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(serialized_data.errors)


data_types = {
    'comment': [
        transfer_comments
    ]
}
