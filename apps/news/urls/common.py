from django.urls import path
from news import views

common_urlpatterns = [
    path('types/', views.NewsTypeListView.as_view(), name='type'),
    path('slug/<slug:slug>/', views.NewsDetailView.as_view(), name='rud'),
    path('slug/<slug:slug>/favorites/', views.NewsFavoritesCreateDestroyView.as_view(),
         name='create-destroy-favorites'),
    path('preview/slug/<slug:slug>/', views.NewsPreviewView.as_view(), name='preview'),
]
