"""

Структура записи в card:
Название таблицы в postgresql: {
    "data_type": "тип данных в таблице (словарь, объект, дочерний объект и так далее)",
    "dependencies": кортеж с зависимостями от других таблиц в postgresql,
    "fields": список полей для таблицы postgresql, пример:
    {
        "название legacy таблицы": {
            список полей для переноса, пример структуры описан далее
        },
        "relations": список зависимостей legacy-таблицы, пример:
        {
            # имеет внешний ключ на "название legacy таблицы" из  "fields"
            "название legacy таблицы": {
                "key": ключ для связи. Строка, если тип поля в legacy таблице - ForeignKey, или кортеж из названия поля
                в дочерней таблице и названия поля в родительской таблице в ином случае
                "fields": {
                    список полей для переноса, пример структуры описан далее
                }
            }
        }
    },
    "relations": список внешних ключей таблицы postgresql, пример структуры описан далее
    {
        "Cities": [(
            (None, "region_code"),
            ("Region", "region", "code", "CharField")),
            ((None, "country_code_2"),
             ("Country", "country", "code", "CharField"))
        ]
    }
},


Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql


"""

card = {
    "Timetable": {
        "data_type": "objects",
        "dependencies": None,
        "fields": {
            "Schedules": {
                # нет аналогов для weekday, opening_at, closed_at
                # upd: запустить команду add_closed_at_timetable. она заполнит opening_at, closed_at
                "lunch_start": "lunch_start",
                "lunch_end": "lunch_end",
                "diner_start": "diner_start",
                "diner_end": "diner_end"
            }
        }
    }
}

used_apps = None