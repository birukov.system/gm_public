import factory
from django.contrib.contenttypes.models import ContentType
from faker import Factory
from unittest import TestCase

from establishment.models import Establishment
from ..models import Review, Inquiries
from ..serializers import ReviewBackCreateSerializer, ReviewBackDetailSerializer

faker = Factory.create()


class ReviewFactory(factory.DjangoModelFactory):

    class Meta:
        model = Review

    object_id = 1
    priority = 2
    vintage = 10
    content_type_id = 1
    text = {'fr-FR': 'text'}


class InquiriesFactory(factory.DjangoModelFactory):

    class Meta:
        model = Inquiries

    advertisement = factory.SubFactory(ReviewFactory)


class TestReviewCreateSerializer(TestCase):

    def setUp(self) -> None:
        self.content_type = ContentType.objects.get_for_model(Establishment).id
        self.object_id = 1
        self.vintage = 1999

        self.data = {
            'vintage': self.vintage,
            'object_id': self.object_id,
            'content_type': self.content_type,
            'text': {'fr-FR': 'text'},
        }

    def test_update_priority_null(self):
        review = ReviewFactory()
        self.data['priority'] = None
        s = ReviewBackDetailSerializer(instance=review, data=self.data, partial=True)
        s.is_valid(raise_exception=True)
        instance = s.save()
        self.assertEqual(instance.priority, None)
