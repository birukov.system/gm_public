"""Back office views for app advertisement"""
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from advertisement.models import Advertisement
from advertisement.serializers.back import AdvertisementBackOfficeSerializer
from main.models import Page
from main.serializers import PageExtendedSerializer
from utils.methods import get_permission_classes
from advertisement.filters import back as filters
from utils.permissions import IsSuperAdmin, IsNonTopLevelAdvertisement


class AdvertisementBackOfficeViewMixin(generics.GenericAPIView):
    """Base back office advertisement view."""

    queryset = Advertisement.objects.with_base_related().order_by('-start')
    permission_classes = get_permission_classes()

    def _validate_block_level(self, serializer):
        block_level = serializer.validated_data.get('block_level')
        if block_level == Advertisement.TOP and not IsSuperAdmin().has_permission(self.request, self):
            raise ValidationError({'block_level': 'Non-superusers cannot use Top block level'})

    def perform_create(self, serializer):
        self._validate_block_level(serializer)
        return super().perform_create(serializer)

    def perform_update(self, serializer):
        self._validate_block_level(serializer)
        return super().perform_update(serializer)


class AdvertisementListCreateView(AdvertisementBackOfficeViewMixin, generics.ListCreateAPIView):
    """
    ## List/Create Advertisement view.
    ### **GET**
    #### Description
    Return paginated list of advertisements.
    ##### Response
    E.g.
    ```
    {
        "count": 58,
        "next": 2,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST**
    #### Description
    Create a new advertisement
    ##### Request
    Required
    * url (`str`) - advertisement URL
    * device (`int`) - enum
    ```
    MOBILE = 0
    WEB = 1
    ALL = 2
    ```
    * block_level (`str`) - enum
    ```
    TOP = 'top'
    LEVEL_1 = '1'
    LEVEL_2 = '2'
    LEVEL_3 = '3'
    ```
    * image (`int`) - identifier of an image
    * page_type (`int`) - identifier of page type
    * target_sites (`list`) - list of identifiers of a target site
    Non-required
    * start (`str`) - datetime in a format ISO-8601
    * end (`str`) - datetime in a format ISO-8601
    ##### Response
    E.g.
    ```
    {
        "count": 3314,
        "next": 2,
        "previous": null,
        "facets": {
          ...
        },
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```
    """

    serializer_class = AdvertisementBackOfficeSerializer
    filter_class = filters.AdvertisementListFilter

    def get_queryset(self):
        qs = super().get_queryset()

        if not IsSuperAdmin().has_permission(self.request, self):
            qs = qs.exclude(block_level=Advertisement.TOP)

        return qs


class AdvertisementRUDView(AdvertisementBackOfficeViewMixin,
                           generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve/Update/Destroy Advertisement page view.
    ### **GET**
    #### Description
    Retrieve a serialized object of an advertisement
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update an advertisement object by an identifier.
    ##### Request
    * url (`str`) - advertisement URL
    * device (`int`) - enum
    ```
    MOBILE = 0
    WEB = 1
    ALL = 2
    ```
    * block_level (`str`) - enum
    ```
    TOP = 'top'
    LEVEL_1 = '1'
    LEVEL_2 = '2'
    LEVEL_3 = '3'
    ```
    * image (`int`) - identifier of an image
    * page_type (`int`) - identifier of page type
    * target_sites (`list`) - list of identifiers of a target site
    * start (`str`) - datetime in a format ISO-8601
    * end (`str`) - datetime in a format ISO-8601
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### **DELETE**
    #### Description
    Delete an advertisement by an identifier
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """

    serializer_class = AdvertisementBackOfficeSerializer

    def get_permissions(self):
        permissions = super().get_permissions()

        if not IsSuperAdmin().has_permission(self.request, self):
            permissions.append(IsNonTopLevelAdvertisement())

        return permissions


class AdvertisementPageCreateView(AdvertisementBackOfficeViewMixin,
                                  generics.CreateAPIView):
    """
    ## Create Advertisement page view.
    ### **POST**
    #### Description
    Create an advertisement page (e.g. we have an ad, then we need to create
    **pages** that have properties like image banner, height and width
    that advertisement) by an advertisement identifier.
    ##### Request
    * image_url (`str`) - image URL address
    * width (`int`) - width
    * height (`int`) - height
    * source ('int') - enum
    ```
    MOBILE = 0
    WEB = 1
    ALL = 2
    ```
    ##### Response
    ```
    No content
    ```
    """

    serializer_class = PageExtendedSerializer

    def get_object(self):
        """Returns the object the view is displaying."""
        ad_qs = Advertisement.objects.all()
        filtered_ad_qs = self.filter_queryset(ad_qs)

        ad = get_object_or_404(filtered_ad_qs, pk=self.kwargs.get('pk'))

        # May raise a permission denied
        self.check_object_permissions(self.request, ad)

        return ad

    def get_queryset(self):
        """An overridden get_queryset method."""
        return self.get_object().pages.all()

    def create(self, request, *args, **kwargs):
        """An overridden create method."""
        request.data.update({'advertisement': self.get_object().pk})
        super().create(request, *args, **kwargs)
        return Response(status=status.HTTP_200_OK)


class AdvertisementPageUDView(AdvertisementBackOfficeViewMixin,
                              generics.UpdateAPIView,
                              generics.DestroyAPIView):
    """
    ## Update/Destroy Advertisement page view.
    ### **PUT/PATCH**
    #### Description
    Completely/Partially update an advertisement page object by its identifier.
    ##### Request
    * image_url (`str`) - image URL address
    * width (`int`) - width
    * height (`int`) - height
    * source ('int') - enum
    ```
    MOBILE = 0
    WEB = 1
    ALL = 2
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### **DELETE**
    #### Description
    Delete an advertisement page by its identifier.
    ##### Response
    ```
    No body
    ```
    ##### Request
    ```
    No content
    ```
    """

    serializer_class = PageExtendedSerializer

    def get_object(self):
        """Returns the object the view is displaying."""
        ad_qs = Advertisement.objects.all()
        filtered_ad_qs = self.filter_queryset(ad_qs)

        ad = get_object_or_404(filtered_ad_qs, pk=self.kwargs.get('ad_pk'))
        page = get_object_or_404(ad.pages.all(), pk=self.kwargs.get('page_pk'))

        # May raise a permission denied
        self.check_object_permissions(self.request, page)

        return page


class AdvertisementPageTypeListView(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        data = []
        for value, title in Page.PAGE_TYPES:
            data.append({
                'value': value,
                'title': title,
            })
        return Response(data)


class AdvertisementDeviceListView(generics.GenericAPIView):

    def get(self, request):
        page_type = request.query_params.get('page_type')
        if page_type is None:
            return Response('page_type query parameter was expected',
                            status=status.HTTP_400_BAD_REQUEST)

        devices = Advertisement.get_devices_for_page_type(page_type)
        return Response(devices)


class AdvertisementBlockLevelListView(generics.GenericAPIView):

    def _levels_for_device_and_page_type(self, device, page_type):
        if device == Page.WEB:
            return Advertisement.LEVELS_FOR_PAGE_TYPE[page_type]

        return Advertisement.TOP_TO_LEVEL_1

    def get(self, request):
        page_type = request.query_params.get('page_type')
        device = request.query_params.get('device')
        if page_type is None or device is None:
            return Response('page_type, device query parameters were expected',
                            status=status.HTTP_400_BAD_REQUEST)

        if device not in map(lambda x: str(x[0]), Page.SOURCES):
            return Response('Invalid device',
                            status=status.HTTP_400_BAD_REQUEST)

        if page_type not in map(lambda x: x[0], Page.PAGE_TYPES):
            return Response('Invalid page_type',
                            status=status.HTTP_400_BAD_REQUEST)

        block_levels = self._levels_for_device_and_page_type(int(device),
                                                             page_type)
        return Response(block_levels)
