"""Notification app common urlconf."""
from django.urls import path
from notification.views import common

app_name = "notification"

urlpatterns = [
    path('subscribe/', common.CreateSubscribeView.as_view(), name='create-subscribe'),
    path('subscribe/<code>/', common.UpdateSubscribeView.as_view(), name='update-subscribe'),
    path('subscribe-info/', common.SubscribeInfoAuthUserView.as_view(), name='check-code-auth'),
    path('subscribe-info/<code>/', common.SubscribeInfoView.as_view(), name='check-code'),
    path('unsubscribe/', common.UnsubscribeAuthUserView.as_view(), name='unsubscribe-auth'),
    path('unsubscribe/<code>/', common.UnsubscribeView.as_view(), name='unsubscribe'),
    path('subscription-types/', common.SubscriptionTypesView.as_view(), name='subscription-types'),
]
