"""Serializers for app advertisements"""
from rest_framework import serializers

from advertisement import models
from translation.serializers import LanguageSerializer


class AdvertisementBaseSerializer(serializers.ModelSerializer):
    """Base serializer for model Advertisement."""
    target_language = LanguageSerializer(read_only=True)

    class Meta:
        model = models.Advertisement
        fields = [
            'id',
            'uuid',
            'url',
            'device',
            'block_level',
            'image_url',
            'start',
            'end',
            'page_type',
            'target_language',
        ]


class AdvertisementSerializer(AdvertisementBaseSerializer):
    """Serializer for model Advertisement."""
    pass
