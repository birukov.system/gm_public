"""Booking app urls."""
from django.urls import path
from booking import views

app = 'booking'

urlpatterns = [
    path('<int:establishment_id>/check/', views.CheckWhetherBookingAvailable.as_view(), name='booking-check'),
    path('<int:establishment_id>/create/', views.CreatePendingBooking.as_view(), name='create-pending-booking'),
    path('<int:pk>/', views.UpdatePendingBooking.as_view(), name='update-pending-booking'),
    path('<int:pk>/commit/', views.CommitPendingBooking.as_view(), name='update-pending-booking'),
    path('<int:pk>/cancel/', views.CancelBooking.as_view(), name='cancel-existing-booking'),
    path('last/', views.LastBooking.as_view(), name='last_booking-for-authorizer-user'),
    path('retrieve/<int:pk>/', views.GetBookingById.as_view(), name='retrieves-booking-by-id'),
]
