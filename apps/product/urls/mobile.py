"""Product mobile url patterns."""
from product.urls.common import urlpatterns as common_urlpatterns
from django.urls import path

from product.views import mobile as views

urlpatterns = [
    path('slug/<slug:slug>/', views.ProductMobileDetailView.as_view(), name='detail-mobile'),
]

urlpatterns.extend(common_urlpatterns)
