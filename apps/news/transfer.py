"""
Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql


NOTE: среди legacy таблиц совпадение для таблицы Address не найдено (Возможно для Address подходит Locations в legacy)
"""

# "from django.db import models"

# "models.PositiveIntegerField"

# "django.db.models."

card = {
    # нету аналога для NewsType
    "NewsType": {
        "data_type": "news",
        "dependencies": None,
        "fields": {
            "Pages": {
                # будет только один тип новости "News"
                # значения для поля "name" берутся из поля "type"  legacy модели "Pages", притом type="News"
                # Mysql - select distinct(type) from pages;
                "name": "type"
            }
        }
    },

    "News": {
        "data_type": "news",
        # "dependencies": ("NewsType", "MetaDataContent", "Country", "Address"),
        "dependencies": ("NewsType", ),
        "fields": {
            # нету аналогов для start, end, playlist
            "Pages": {
                "state": ("state", "django.db.models.PositiveSmallIntegerField"),
                "template": ("template", "django.db.models.PositiveIntegerField"),
                "image_url": ("attachment_file_name", "django.db.models.URLField"),
                "preview_image_url": ("attachment_file_name", "django.db.models.URLField"),

                # в NewsOlds нету аналога для поля subtitle модели News, также нет аналогов для полей start, end, playlist
                # "subtitle": ""


                # Поле "description" модели News имеет тип JSONField(где ключ - это язык, а значение - новость
                # на языке который указан ключом), а поле "body" NewsOlds имеет тип html-разметки
                # с вставками шаблонизатора Ruby
                # "description" : "body"
            },
            "relations": {
                "PageTexts": {
                    "key": "page",
                    "fields": {
                        "title": ("title", "django.db.models.JSONField"),
                        "description": ("body", "django.db.models.JSONField"),
                        "slug": ("slug", "django.db.models.SlugField")
                    }
                }
            },
        },
        # нету аналога для поля tags
        "relations": {
            "Pages": [
                ((None, "type"),
                 ("NewsType", "news_type", "name", "django.db.models.CharField"))
            ]
        }
    },
}

used_apps = ("location", "main", )
