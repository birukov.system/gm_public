# Generated by Django 2.2.4 on 2019-10-31 06:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0004_review_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='old_id',
            field=models.PositiveIntegerField(blank=True, default=None, null=True, verbose_name='old id'),
        ),
    ]
