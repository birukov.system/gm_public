from django.core.management.base import BaseCommand
from timetable.models import Timetable


class Command(BaseCommand):
    help = '''Add closed_at, opening_at Timetable fields'''

    def handle(self, *args, **options):
        for tt in Timetable.objects.all():
            end = tt.dinner_end or tt.lunch_end
            start = tt.lunch_start or tt.dinner_start
            if end or start:
                tt.closed_at = end
                tt.opening_at = start
                tt.save()
