from django.core.management.base import BaseCommand

from account.models import User
from establishment.models import Establishment
from transfer.models import Comments
from comment.models import Comment as NewComment


class Command(BaseCommand):
    help = 'Add publish values from old db to new db'

    def handle(self, *args, **kwargs):
        count = 0

        establishments = Establishment.objects.all().values_list('old_id', flat=True)
        users = User.objects.all().values_list('old_id', flat=True)
        queryset = Comments.objects.filter(
            establishment_id__in=list(establishments),
            account_id__in=list(users),
        )
        for comment in queryset:
            obj = NewComment.objects.filter(old_id=comment.id).first()
            if obj:
                count += 1
                obj.created = comment.created_at
                obj.modified = comment.updated_at
                obj.is_publish = comment.state == 'published'
                obj.save()
        self.stdout.write(self.style.WARNING(f'Updated {count} objects.'))
