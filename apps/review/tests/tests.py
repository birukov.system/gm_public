from http.cookies import SimpleCookie

from rest_framework import status
from rest_framework.test import APITestCase

from account.models import User
from location.models import Country
from review.models import Review, Inquiries, GridItems
from translation.models import Language


class BaseTestCase(APITestCase):

    def setUp(self):
        self.username = 'test_user'
        self.password = 'test_user_password'
        self.email = 'test_user@mail.com'
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
            is_staff=True,
        )

        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie({
            'access_token': tokens.get('access_token'),
            'refresh_token': tokens.get('refresh_token'),
        })

        self.lang = Language.objects.create(
            title='Russia',
            locale='ru-RU'
        )

        self.country_ru = Country.objects.create(
            name={'en-GB': 'Russian'},
            code='RU',
        )

        self.test_review = Review.objects.create(
            reviewer=self.user,
            status=Review.READY,
            vintage=2020,
            country=self.country_ru,
            text={'en-GB': 'Text review'},
            created_by=self.user,
            modified_by=self.user,
            object_id=1,
            content_type_id=1,
        )

        self.test_inquiry = Inquiries.objects.create(
            review=self.test_review,
            author=self.user,
            comment='Test comment',
        )

        self.test_grid = GridItems.objects.create(
            inquiry=self.test_inquiry,
            name='Test name',
        )


class ReviewTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_review_list(self):
        response = self.client.get('/api/back/review/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_review_post(self):
        test_review = {
            'reviewer': self.user.id,
            'status': Review.READY,
            'vintage': 2019,
            'country': self.country_ru.id,
            'object_id': 1,
            'content_type': 1,
        }
        response = self.client.post('/api/back/review/', data=test_review)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_review_detail(self):
        response = self.client.get(f'/api/back/review/{self.test_review.id}/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_review_detail_put(self):
        data = {
            'id': self.test_review.id,
            'vintage': 2018,
            'reviewer': self.user.id,
            'status': Review.READY,
            'country': self.country_ru.id,
            'object_id': 1,
            'content_type': 1,
        }

        response = self.client.put(f'/api/back/review/{self.test_review.id}/', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_review_delete(self):
        response = self.client.delete(f'/api/back/review/{self.test_review.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class InquiriesTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_inquiry_list(self):
        response = self.client.get('/api/back/review/inquiries/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_inquiry_list_by_review_id(self):
        response = self.client.get(f'/api/back/review/{self.test_review.id}/inquiries/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_inquiry_post(self):
        test_inquiry = {
            'review': self.test_review.pk,
            'author': self.user.pk,
            'comment': 'New test comment',
        }
        response = self.client.post('/api/back/review/inquiries/', data=test_inquiry)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_inquiry_detail(self):
        response = self.client.get(f'/api/back/review/inquiries/{self.test_inquiry.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_inquiry_detail_put(self):
        data = {
            'id': self.test_inquiry.id,
            'review': self.test_review.pk,
            'author': self.user.pk,
            'comment': 'New test comment 2',
        }

        response = self.client.put(f'/api/back/review/inquiries/{self.test_inquiry.id}/', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GridItemsTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_grid_list(self):
        response = self.client.get('/api/back/review/inquiries/grid/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_grid_list_by_inquiry_id(self):
        response = self.client.get(f'/api/back/review/inquiries/{self.test_inquiry.id}/grid/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_grid_post(self):
        test_grid = {
            'inquiry': self.test_inquiry.pk,
            'name': 'New test name',
        }
        response = self.client.post('/api/back/review/inquiries/grid/', data=test_grid)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_grid_detail(self):
        response = self.client.get(f'/api/back/review/inquiries/grid/{self.test_grid.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_grid_detail_put(self):
        data = {
            'id': self.test_grid.id,
            'inquiry': self.test_inquiry.pk,
            'name': 'New test name 2',
        }

        response = self.client.put(f'/api/back/review/inquiries/grid/{self.test_inquiry.id}/', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
