"""Product web url patterns."""
from product.urls.common import urlpatterns as common_urlpatterns

urlpatterns = [
]

urlpatterns.extend(common_urlpatterns)
