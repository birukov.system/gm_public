# Generated by Django 2.2.7 on 2020-01-15 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0046_auto_20200114_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feature',
            name='priority',
            field=models.IntegerField(blank=True, default=None, null=True),
        ),
    ]
