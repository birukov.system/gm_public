"""Recipe app back office urlpatterns."""
from django.urls import path

from recipe import views

app_name = 'recipe'

urlpatterns = [
    path('', views.RecipeListCreateView.as_view(), name='list-create'),
    path('slug/<str:slug>/', views.RecipeRUDView.as_view(), name='rud'),
]
