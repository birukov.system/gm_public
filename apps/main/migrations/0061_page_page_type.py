# Generated by Django 2.2.7 on 2020-02-27 09:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0060_auto_20200226_1536'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='page_type',
            field=models.CharField(choices=[('Homepage', 'Homepage'), ('All establishments', 'All establishments'), ('Restaurants', 'Restaurants'), ('Artisans', 'Artisans'), ('Wineries', 'Wineries'), ('Distilleries', 'Distilleries'), ('Food producers', 'Food producers'), ('All Products', 'All Products'), ('Food', 'Food'), ('Liquors', 'Liquors'), ('Wines', 'Wines'), ('List of news', 'List of news'), ('News & recipes', 'News & recipes'), ('News', 'News'), ('Recipes', 'Recipes')], max_length=32, null=True, verbose_name='Page Type'),
        ),
    ]
