"""Location app common serializers."""
from django.contrib.gis.geos import Point
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from gallery import models as gallery_models
from location import models
from utils.serializers import TranslatedField, ImageBaseSerializer
from tag.models import Tag


class CountrySerializer(serializers.ModelSerializer):
    """Country serializer."""

    name_translated = TranslatedField()
    display_calling_code = serializers.ListField(allow_null=True, read_only=True)
    has_sub_region = serializers.BooleanField(read_only=True)
    default_calling_code = serializers.CharField(allow_null=True, read_only=True)
    is_non_latin = serializers.BooleanField(read_only=True)

    class Meta:
        model = models.Country
        fields = [
            'id',
            'code',
            'flag_image',
            'name_translated',
            'display_calling_code',
            'default_calling_code',
            'has_sub_region',
            'is_non_latin',
        ]


class CountrySimpleSerializer(serializers.ModelSerializer):
    """Simple country serializer."""

    name_translated = TranslatedField()

    class Meta:
        """Meta class."""

        model = models.Country
        fields = ('id', 'code', 'name_translated')


class ParentRegionSerializer(serializers.ModelSerializer):
    """Region serializer"""

    country = CountrySerializer(read_only=True)
    country_id = serializers.PrimaryKeyRelatedField(
        source='country',
        queryset=models.Country.objects.all(),
        write_only=True
    )

    class Meta:
        model = models.Region
        fields = [
            'id',
            'name',
            'code',
            'country',
            'country_id'
        ]


class RegionSerializer(serializers.ModelSerializer):
    """Region serializer"""

    country = CountrySerializer(read_only=True)
    country_id = serializers.PrimaryKeyRelatedField(
        source='country',
        queryset=models.Country.objects.all(),
        write_only=True
    )
    parent_region_id = serializers.PrimaryKeyRelatedField(
        source='parent_region',
        queryset=models.Region.objects.all(),
        write_only=True
    )
    parent_region = ParentRegionSerializer(read_only=True)

    class Meta:
        model = models.Region
        fields = [
            'id',
            'name',
            'code',
            'parent_region',
            'parent_region_id',
            'country',
            'country_id'
        ]


class RegionShortSerializer(RegionSerializer):
    """Region short serializer"""

    class Meta(RegionSerializer.Meta):
        fields = [f for f in RegionSerializer.Meta.fields if f not in ['country', 'country_id']]


class CityShortSerializer(serializers.ModelSerializer):
    """Short city serializer"""
    country = CountrySerializer(read_only=True)
    name_translated = TranslatedField()

    class Meta:
        """Meta class"""
        model = models.City
        fields = (
            'id',
            'name_translated',
            'code',
            'country',
        )


class CityBaseSerializer(serializers.ModelSerializer):
    """City serializer."""
    region = RegionShortSerializer(read_only=True)
    region_id = serializers.PrimaryKeyRelatedField(
        source='region',
        queryset=models.Region.objects.all(),
        write_only=True
    )
    country_id = serializers.PrimaryKeyRelatedField(
        source='country',
        queryset=models.Country.objects.all(),
        write_only=True
    )
    image_id = serializers.PrimaryKeyRelatedField(
        source='image',
        queryset=gallery_models.Image.objects.all(),
        write_only=True,
        required=False,
    )
    country = CountrySerializer(read_only=True)
    name_translated = TranslatedField()

    class Meta:
        model = models.City
        fields = [
            'id',
            'name',
            'name_translated',
            'region',
            'region_id',
            'country_id',
            'country',
            'postal_code',
            'is_island',
            'image',
            'image_id',
        ]
        extra_fields = {
            'image': {'read_only': True}
        }


class CityMobileSerializer(CityBaseSerializer):
    name = serializers.SerializerMethodField()

    class Meta(CityBaseSerializer.Meta):
        fields = CityBaseSerializer.Meta.fields + [
            'name'
        ]

    def get_name(self, obj: models.City) -> str:
        if hasattr(obj, 'name_translated'):
            return obj.name_translated


class CityDetailSerializer(CityBaseSerializer):
    """Serializer for detail view."""
    image = ImageBaseSerializer(source='crop_image', read_only=True)

    class Meta(CityBaseSerializer.Meta):
        """Meta class."""
        fields = CityBaseSerializer.Meta.fields + [
            'image',
        ]


class AddressBaseSerializer(serializers.ModelSerializer):
    """Serializer for address obj in related objects."""

    latitude = serializers.FloatField(allow_null=True)
    longitude = serializers.FloatField(allow_null=True)

    # todo: remove this fields (backward compatibility)
    geo_lon = serializers.FloatField(source='longitude', allow_null=True,
                                     read_only=True)
    geo_lat = serializers.FloatField(source='latitude', allow_null=True,
                                     read_only=True)
    district_name = serializers.CharField(allow_blank=True, required=False, allow_null=True)

    class Meta:
        """Meta class."""

        model = models.Address
        fields = (
            'id',
            'street_name_1',
            'street_name_2',
            'number',
            'postal_code',
            'latitude',
            'longitude',
            'district_name',

            # todo: remove this fields (backward compatibility)
            'geo_lon',
            'geo_lat',
        )

    def validate_latitude(self, value):
        if value is None:
            return
        elif -90 <= value <= 90:
            return value
        raise serializers.ValidationError(_('Invalid value'))

    def validate_longitude(self, value):
        if value is None:
            return
        elif -180 <= value <= 180:
            return value
        raise serializers.ValidationError(_('Invalid value'))

    def validate(self, attrs):
        # validate coordinates
        latitude = attrs.pop('latitude', None)
        longitude = attrs.pop('longitude', None)
        if latitude is not None and longitude is not None:
            attrs['coordinates'] = Point(longitude, latitude)
        return attrs


class AddressEstablishmentSerializer(AddressBaseSerializer):
    """Address serializer."""

    id = serializers.IntegerField(required=False)
    street_name_1 = serializers.CharField(required=False, allow_blank=True, default='')
    street_name_2 = serializers.CharField(required=False, allow_blank=True, default='')
    number = serializers.IntegerField(required=False, default=0)
    postal_code = serializers.CharField(required=False, default='')
    city_id = serializers.PrimaryKeyRelatedField(
        source='city',
        queryset=models.City.objects.all(),
        write_only=True,
        required=True,
    )
    city = CityBaseSerializer(read_only=True)

    class Meta(AddressBaseSerializer.Meta):
        """Meta class."""

        fields = AddressBaseSerializer.Meta.fields + (
            'id',
            'street_name_1',
            'street_name_2',
            'number',
            'postal_code',
            'city_id',
            'city',
        )


class AddressDetailSerializer(AddressBaseSerializer):
    """Address serializer."""

    city_id = serializers.PrimaryKeyRelatedField(
        source='city', write_only=True,
        queryset=models.City.objects.all())
    city = CityBaseSerializer(read_only=True)

    class Meta(AddressBaseSerializer.Meta):
        """Meta class."""

        fields = AddressBaseSerializer.Meta.fields + (
            'city_id',
            'city',
        )


class AddressMobileDetailSerializer(AddressDetailSerializer):
    city = CityMobileSerializer(read_only=True)


class WineRegionBaseSerializer(serializers.ModelSerializer):
    """Wine region serializer."""
    country = CountrySerializer(read_only=True)
    latitude = serializers.FloatField(allow_null=True)
    longitude = serializers.FloatField(allow_null=True)
    country_id = serializers.PrimaryKeyRelatedField(
        source='country',
        queryset=models.Country.objects.all(),
        write_only=True,
        required=False,
    )
    tag_ids = serializers.PrimaryKeyRelatedField(
        source='tags',
        queryset=Tag.objects.all(),
        many=True,
        write_only=True,
        required=False,
    )

    class Meta:
        """Meta class."""
        model = models.WineRegion
        fields = [
            'id',
            'name',
            'country',
            'country_id',
            'tag_ids',
            'description',
            'coordinates',
            'latitude',
            'longitude',
        ]
        extra_kwargs = {
            'description': {'write_only': True},
            'coordinates': {'write_only': True},
        }

    def validate_latitude(self, value):
        if value is None:
            return
        elif -90 <= value <= 90:
            return value
        raise serializers.ValidationError(_('Invalid value'))

    def validate_longitude(self, value):
        if value is None:
            return
        elif -180 <= value <= 180:
            return value
        raise serializers.ValidationError(_('Invalid value'))

    def validate(self, attrs):
        # validate coordinates
        latitude = attrs.pop('latitude', None)
        longitude = attrs.pop('longitude', None)
        if latitude is not None and longitude is not None:
            attrs['coordinates'] = Point(longitude, latitude)
        return attrs


class WineSubRegionBaseSerializer(serializers.ModelSerializer):
    """Wine sub region serializer."""

    wine_region_id = serializers.PrimaryKeyRelatedField(
        source='wine_region',
        write_only=True,
        queryset=models.WineRegion.objects.all(),
    )
    wine_region_name = serializers.CharField(read_only=True, allow_null=True,
                                             source='wine_region.name')

    class Meta:
        """Meta class."""
        model = models.WineSubRegion
        fields = [
            'id',
            'name',
            'wine_region_id',
            'wine_region_name',
        ]


class EstablishmentWineRegionBaseSerializer(serializers.ModelSerializer):
    """Establishment wine region origin serializer."""

    id = serializers.IntegerField(source='wine_region.id')
    name = serializers.CharField(source='wine_region.name')
    country = CountrySerializer(source='wine_region.country')

    class Meta:
        """Meta class."""
        model = models.EstablishmentWineOriginAddress
        fields = [
            'id',
            'name',
            'country',
        ]


class EstablishmentWineOriginBaseSerializer(serializers.ModelSerializer):
    """Serializer for intermediate model EstablishmentWineOrigin."""
    wine_region = WineRegionBaseSerializer()
    wine_sub_region = WineSubRegionBaseSerializer(allow_null=True)

    class Meta:
        """Meta class."""
        model = models.EstablishmentWineOriginAddress
        fields = [
            'wine_region',
            'wine_sub_region',
        ]


class WineOriginRegionBaseSerializer(EstablishmentWineRegionBaseSerializer):
    """Product wine region origin serializer."""

    class Meta(EstablishmentWineRegionBaseSerializer.Meta):
        """Meta class."""
        model = models.WineOriginAddress


class WineOriginBaseSerializer(EstablishmentWineOriginBaseSerializer):
    """Serializer for intermediate model ProductWineOrigin."""

    class Meta(EstablishmentWineOriginBaseSerializer.Meta):
        """Meta class."""
        model = models.WineOriginAddress


class WineRegionSerializer(WineRegionBaseSerializer):
    """Wine region w/ sub region serializer"""

    wine_sub_region = WineSubRegionBaseSerializer(source='wine_region.wine_sub_region',
                                                  allow_null=True,
                                                  read_only=True,
                                                  many=True)

    class Meta(WineRegionBaseSerializer.Meta):
        fields = WineRegionBaseSerializer.Meta.fields + [
            'wine_sub_region'
        ]
