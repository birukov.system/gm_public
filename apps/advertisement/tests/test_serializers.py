import io
from unittest.mock import patch

from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework import status
from rest_framework.test import APITestCase
import factory
from faker import Factory

from advertisement.models import Advertisement
from main.models import Page
from translation.models import Language
from rest_framework.serializers import ValidationError
from ..serializers.back import AdvertisementBackOfficeSerializer

faker = Factory.create()


def generate_image():
    file = io.BytesIO()
    image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
    image.save(file, 'png')
    file.name = 'test.png'
    file.seek(0)

    return SimpleUploadedFile(name='test_image.png',
                              content=file.getvalue(),
                              content_type='image/png')


class LanguageFactory(factory.DjangoModelFactory):

    class Meta:
        model = Language


class AdvertisementFactory(factory.DjangoModelFactory):

    class Meta:
        model = Advertisement

    url = faker.word()


class PageFactory(factory.DjangoModelFactory):

    class Meta:
        model = Page

    advertisement = factory.SubFactory(AdvertisementFactory)


class TestAdvertisementBackOfficeSerializer(APITestCase):

    def setUp(self) -> None:
        self.url = 'http://example.com'
        self.device = Page.WEB
        self.block_level = Advertisement.TOP
        self.page_type = Page.HOMEPAGE_TYPE
        self.image_url = 'http://image.com'
        self.lang = LanguageFactory(title='English', locale='en-EN')

        self.data = {
            'url': self.url,
            'device': self.device,
            'block_level': self.block_level,
            'page_type': self.page_type,
            'image_url': self.image_url,
            'target_language': {'title': self.lang.title, 'locale': self.lang.locale},
        }

    def test_page_type_retrieve_format(self):
        ads = AdvertisementFactory()
        PageFactory(page_type='homepage', advertisement=ads, source=Page.WEB)

        data = AdvertisementBackOfficeSerializer(ads).data

        self.assertEqual(data['page_type']['value'],
                         Page.HOMEPAGE_TYPE)
        self.assertEqual(data['page_type']['title'],
                         'Homepage')
        self.assertEqual(data['page_type']['levels'],
                         ('top', '1', '2', '3'))

    def test_target_language_create_format(self):
        s = AdvertisementBackOfficeSerializer(data=self.data)
        s.is_valid(raise_exception=True)

        self.assertEqual(s.validated_data['target_language'], self.lang)

    def test_ads_creation(self):
        s = AdvertisementBackOfficeSerializer(data=self.data)
        s.is_valid(raise_exception=True)
        s.save()

        ads = Advertisement.objects.all().first()
        page = Page.objects.all().first()

        self.assertEqual(ads.page, page)
        self.assertEqual(ads.target_language, self.lang)
        self.assertEqual(ads.url, self.url)
        self.assertEqual(ads.block_level, self.block_level)

        self.assertEqual(page.image_url, self.image_url)
        self.assertEqual(page.source, self.device)
        self.assertEqual(page.page_type, self.page_type)

    def test_ads_partial_update(self):
        ads_instance = AdvertisementFactory(url='http://b.com')
        page = PageFactory(page_type=Page.ARTISANS_TYPE, advertisement=ads_instance, source=Page.WEB)
        data = {
            'url': 'http://a.com',
            'page_type': Page.HOMEPAGE_TYPE,
            'device': Page.MOBILE,
        }

        s = AdvertisementBackOfficeSerializer(instance=ads_instance, data=data, partial=True)
        s.is_valid(raise_exception=True)
        s.save()

        ads = Advertisement.objects.all().first()
        page = Page.objects.all().first()
        self.assertEqual(ads.url, 'http://a.com')
        self.assertEqual(page.page_type, Page.HOMEPAGE_TYPE)
        self.assertEqual(page.source, Page.MOBILE)

    def test_null_target_language(self):
        self.data['target_language'] = None
        s = AdvertisementBackOfficeSerializer(data=self.data)
        s.is_valid(raise_exception=True)

        self.assertEqual(s.validated_data['target_language'], None)

    def test_page_type_validation(self):
        data = self.data.copy().update({'page_type': 'wrong page type'})
        s = AdvertisementBackOfficeSerializer(data=data)
        with self.assertRaises(ValidationError):
            s.is_valid(raise_exception=True)
