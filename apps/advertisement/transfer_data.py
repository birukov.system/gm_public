from pprint import pprint

from transfer.models import Ads
from transfer.serializers.advertisement import AdvertisementSerializer, AdvertisementImageSerializer
from advertisement.models import Advertisement
from main.models import Page


def transfer_advertisement():
    errors = []
    queryset = Ads.objects.exclude(href__isnull=True) \
                          .exclude(attachment_suffix_url__isnull=True) \
                          .exclude(site_id__isnull=True)

    # delete old ads
    Page.objects.all().delete()
    Advertisement.objects.all().delete()

    serialized_data = AdvertisementSerializer(data=list(queryset.values()), many=True)

    if serialized_data.is_valid():
        serialized_data.save()
    else:
        for d in serialized_data.errors: errors.append(d) if d else None
        pprint(f"transfer_product errors: {errors}")


def transfer_page():
    errors = []
    queryset = Ads.objects.exclude(href__isnull=True) \
                          .exclude(attachment_suffix_url__isnull=True) \
                          .exclude(site_id__isnull=True)

    # delete old ads' pages
    Page.objects.all().delete()

    serialized_data = AdvertisementImageSerializer(data=list(queryset.values()), many=True)

    if serialized_data.is_valid():
        serialized_data.save()
    else:
        for d in serialized_data.errors: errors.append(d) if d else None
        pprint(f"transfer_page errors: {errors}")


data_types = {
    "commercial": [
        transfer_advertisement,
        transfer_page
    ]
}
