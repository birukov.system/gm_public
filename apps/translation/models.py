"""Translation app models."""
from django.apps import apps
from django.contrib.postgres.indexes import GinIndex
from django.db import models
from django.utils.translation import gettext_lazy as _
from typing import List

from utils.models import ProjectBaseMixin, LocaleManagerMixin
from utils.models import TranslatedFieldsMixin, TJSONField


class LanguageQuerySet(models.QuerySet):
    """QuerySet for model Language"""

    def active(self, switcher=True):
        """Filter only active users."""
        return self.filter(is_active=switcher)

    def by_locale(self, locale: str) -> models.QuerySet:
        """Filter by locale"""
        return self.filter(locale=locale)

    def by_title(self, title: str) -> models.QuerySet:
        """Filter by title"""
        return self.filter(title=title)


class Language(models.Model):
    """Language model."""

    title = models.CharField(max_length=255,
                             verbose_name=_('Language title'))
    locale = models.CharField(max_length=10,
                              verbose_name=_('Locale identifier'))

    old_id = models.IntegerField(null=True, blank=True, default=None)

    is_active = models.BooleanField(_('is active'), default=True)

    objects = LanguageQuerySet.as_manager()

    class Meta:
        """Meta class."""

        verbose_name = _('Language')
        verbose_name_plural = _('Languages')
        unique_together = ('title', 'locale')

    def __str__(self):
        """String method"""
        return f'{self.title} ({self.locale})'


class SiteInterfaceDictionaryManager(LocaleManagerMixin):
    """Extended manager for SiteInterfaceDictionary model."""

    def update_or_create_for_tag(self, tag, translations: dict):
        Tag = apps.get_model('tag', 'Tag')
        """Creates or updates translation for EXISTING in DB Tag"""
        if not tag.pk or not isinstance(tag, Tag):
            raise NotImplementedError()
        if tag.translation:
            tag.translation.text = translations
            tag.translation.page = 'tag'
            tag.translation.keywords = f'tag.{tag.category.index_name}.{tag.value}'
        else:
            trans = SiteInterfaceDictionary.objects.create(**{
                'text': translations,
                'page': 'tag',
                'keywords': f'tag.{tag.category.index_name}.{tag.value}'
            })
            # trans.save()
            tag.translation = trans
            tag.save()

    def update_or_create_for_tag_category(self, tag_category, translations: dict):
        """Creates or updates translation for EXISTING in DB TagCategory"""
        TagCategory = apps.get_model('tag', 'TagCategory')
        if not tag_category.pk or not isinstance(tag_category, TagCategory):
            raise NotImplementedError()
        if tag_category.translation:
            tag_category.translation.text = translations
            tag_category.translation.page = 'tag'
            tag_category.translation.keywords = f'tag.{tag_category.category.index_name}'
        else:
            trans = SiteInterfaceDictionary({
                'text': translations,
                'page': 'tag',
                'keywords':  f'tag.{tag_category.category.index_name}'
            })
            trans.save()
            tag_category.translation = trans
            tag_category.save()


class SiteInterfaceDictionary(TranslatedFieldsMixin, ProjectBaseMixin):
    """Site interface dictionary model."""

    STR_FIELD_NAME = 'text'

    page = models.CharField(max_length=255, db_index=True, verbose_name=_('Page'))
    keywords = models.CharField(max_length=255, verbose_name='Keywords')
    text = TJSONField(_('Text'), default=dict, help_text='{"en-GB":"some text"}')
    pending_text = TJSONField(_('Not applied translations'), default=dict, help_text='{"en-GB":"some text"}')

    objects = SiteInterfaceDictionaryManager()

    @property
    def text_en_searchable(self) -> str:
        """for search via ES purposes"""
        return self.text.get('en-GB', '')

    @property
    def text_not_en_searchable(self) -> List[str]:
        """for search via ES purposes"""
        new_text = self.text.copy() if self.text else {}
        if 'en-GB' in new_text:
            new_text.pop('en-GB')
        return list(new_text.values())

    @property
    def keys_translated(self) -> List[str]:
        """for search via ES purposes"""
        return [locale for locale, value in self.text.items() if value]

    @property
    def keys_pending_translated(self) -> List[str]:
        """for search via ES purposes"""
        return [locale for locale, value in self.pending_text.items() if value]

    class Meta:
        """Meta class."""

        verbose_name = _('Site interface dictionary')
        verbose_name_plural = _('Site interface dictionary')
        indexes = [
            GinIndex(fields=['text']),
            GinIndex(fields=['pending_text']),
        ]

    def __str__(self):
        return f'{self.page}: {self.keywords}'
