# Generated by Django 2.2.4 on 2019-09-04 08:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorization', '0005_auto_20190904_0821'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='jwtaccesstoken',
            name='refresh_token',
        ),
    ]
