from rest_framework import serializers

from translation import models
from utils.serializers import TranslatedField


class LanguageSerializer(serializers.ModelSerializer):
    """Serializer for model Language"""

    class Meta:
        model = models.Language
        fields = [
            'id',
            'title',
            'locale',
            'is_active',
        ]


class SiteInterfaceDictionarySerializer(serializers.ModelSerializer):
    """Serializer for model SiteInterfaceDictionary."""

    text_trans = TranslatedField(source='text_translated')
    pending_text_trans = TranslatedField(source='pending_text_translated')

    class Meta:
        """Meta class."""
        model = models.SiteInterfaceDictionary
        fields = [
            'id',
            'page',
            'keywords',
            'text_trans',
            'pending_text_trans',
        ]
