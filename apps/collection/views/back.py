from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _
from rest_framework import generics, mixins, permissions, status, viewsets
from rest_framework.response import Response

from collection import filters, models, serializers, tasks
from utils.methods import get_permission_classes
from utils.views import BindObjectMixin


class CollectionViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """ViewSet for Collection model."""

    # pagination_class = None
    serializer_class = serializers.CollectionBackOfficeSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        """Overridden method 'get_queryset'."""
        qs = models.Collection.objects.all().order_by('-created').with_base_related()
        if self.request.country_code:
            qs = qs.by_country_code(self.request.country_code)
        return qs


class GuideBaseView(generics.GenericAPIView):
    """ViewSet for Guide model."""
    serializer_class = serializers.GuideBaseSerializer
    permission_classes = get_permission_classes()

    def get_queryset(self):
        """An overridden get_queryset method."""
        queryset = models.Guide.objects.with_base_related
        if hasattr(self.request, 'country_code'):
            return queryset().by_country_code(self.request.country_code).order_by('-created')
        return queryset().none()


class GuideFilterBaseView(generics.GenericAPIView):
    """ViewSet for GuideFilter model."""
    pagination_class = None
    queryset = models.GuideFilter.objects.all()
    serializer_class = serializers.GuideFilterBaseSerializer
    permission_classes = get_permission_classes()


class GuideElementBaseView(generics.GenericAPIView):
    """Base view for GuideElement model."""
    pagination_class = None
    queryset = models.GuideElement.objects.all()
    serializer_class = serializers.GuideElementBaseSerializer
    permission_classes = get_permission_classes()


class AdvertorialBaseView(generics.GenericAPIView):
    """Base view for Advertorial model."""
    pagination_class = None
    queryset = models.Advertorial.objects.all()
    serializer_class = serializers.AdvertorialBaseSerializer
    permission_classes = get_permission_classes()


class CollectionBackOfficeViewSet(mixins.CreateModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.DestroyModelMixin,
                                  mixins.RetrieveModelMixin,
                                  BindObjectMixin,
                                  CollectionViewSet):
    """
    ## ViewSet for Collections list for BackOffice users and Collection create.

    ### **GET (/)**
    #### Description
    Return paginated list of collections with descending default ordering by created field.
    #### Available filters
    * establishment_id (`int`) - Allows to filter list of collections by choosen estblishment.
    Use for Establishment detail\'s sheet to content display within "Collections & Guides" tab.
    #### Available ordering
    * rank
    * start
    In order to use the sorting feature, you need to pass the parameter - `ordering`,
    e.g.: ?ordering=-rank
    ##### Response
    E.g.
    ```
    {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST (/)**
    #### Description
    Create a new collection.
    ##### Request
    Required
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * country_id (`int`) - identifier of country
    Non-required
    * collection_type (`int`) - enum
    ```
    ORDINARY = 0  # Ordinary collection as default
    POP = 1  # POP collection
    ```
    * is_publish (`bool`) - publish status
    * on_top (`bool`) - position on top
    * description (`str`) - description
    * rank (`int`) - rank
    * image_url (`str`) - image URL address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **GET (/<int:pk>/)**
    #### Description
    Return a serialized object of a collection.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH (/<int:pk>/)**
    #### Description
    Completely/Partially update a collection object by an identifier.
    ##### Request
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * collection_type (`int`) - enum
    ```
    ORDINARY = 0  # Ordinary collection as default
    POP = 1  # POP collection
    ```
    * is_publish (`bool`) - publish status
    * on_top (`bool`) - position on top
    * country_id (`int`) - identifier of country
    * description (`str`) - description
    * rank (`int`) - rank
    * image_url (`str`) - image URL address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE** (/<int:pk>/)
    #### Description
    Delete an instance of a collection by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```

    ### **POST** (/<int:pk>/bind-object/)
    #### Description
    Method that allows to bind an object to collection by its identifier.
    ##### Request
    * type (`str`) - available types
    ```
    establishment
    product
    ```
    * object_id (`int`) - identifier of an object of type entity
    ##### Response
    E.g.
    ```
    {
        "type": "establishment",
        "object_id": 869861212
    }
    ```
    ### **DELETE** (/<int:pk>/bind-object/)
    #### Description
    Method that allows to unbind an object from collection by its identifier.
    ##### Request
    * type (`str`) - available types
    ```
    establishment
    product
    ```
    * object_id (`int`) - identifier of an object of type entity
    ##### Response
    ```
    No content
    ```
    """

    queryset = models.Collection.objects.with_base_related().order_by('-created')
    filter_class = filters.CollectionFilterSet
    serializer_class = serializers.CollectionBackOfficeSerializer
    bind_object_serializer_class = serializers.CollectionBindObjectSerializer
    permission_classes = (permissions.AllowAny, )

    def perform_binding(self, serializer):
        data = serializer.validated_data
        collection = data.pop('collection')
        obj_type = data.get('type')
        related_object = data.get('related_object')
        if obj_type == self.bind_object_serializer_class.ESTABLISHMENT:
            collection.establishments.add(related_object)
        elif obj_type == self.bind_object_serializer_class.PRODUCT:
            collection.products.add(related_object)

    def perform_unbinding(self, serializer):
        data = serializer.validated_data
        collection = data.pop('collection')
        obj_type = data.get('type')
        related_object = data.get('related_object')
        if obj_type == self.bind_object_serializer_class.ESTABLISHMENT:
            collection.establishments.remove(related_object)
        elif obj_type == self.bind_object_serializer_class.PRODUCT:
            collection.products.remove(related_object)


class CollectionBackOfficeList(CollectionBackOfficeViewSet):
    """
    ## ViewSet for Collections list for BackOffice users and Collection create.

    ### **GET (/)**
    #### Description
    Return non-paginated list of collections with descending default ordering by created field.
    #### Available filters
    * establishment_id (`int`) - Allows to filter list of collections by choosen estblishment.
    Use for Establishment detail\'s sheet to content display within "Collections & Guides" tab.
    #### Available ordering
    * rank
    * start
    In order to use the sorting feature, you need to pass the parameter - `ordering`,
    e.g.: ?ordering=-rank
    ##### Response
    E.g.
    ```
    {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST (/)**
    #### Description
    Create a new collection.
    ##### Request
    Required
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * country_id (`int`) - identifier of country
    Non-required
    * collection_type (`int`) - enum
    ```
    ORDINARY = 0  # Ordinary collection as default
    POP = 1  # POP collection
    ```
    * is_publish (`bool`) - publish status
    * on_top (`bool`) - position on top
    * description (`str`) - description
    * rank (`int`) - rank
    * image_url (`str`) - image URL address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **GET (/<int:pk>/)**
    #### Description
    Return a serialized object of a collection.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH (/<int:pk>/)**
    #### Description
    Completely/Partially update a collection object by an identifier.
    ##### Request
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * collection_type (`int`) - enum
    ```
    ORDINARY = 0  # Ordinary collection as default
    POP = 1  # POP collection
    ```
    * is_publish (`bool`) - publish status
    * on_top (`bool`) - position on top
    * country_id (`int`) - identifier of country
    * description (`str`) - description
    * rank (`int`) - rank
    * image_url (`str`) - image URL address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE** (/<int:pk>/)
    #### Description
    Delete an instance of a collection by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```

    ### **POST** (/<int:pk>/bind-object/)
    #### Description
    Method that allows to bind an object to collection by its identifier.
    ##### Request
    * type (`str`) - available types
    ```
    establishment
    product
    ```
    * object_id (`int`) - identifier of an object of type entity
    ##### Response
    E.g.
    ```
    {
        "type": "establishment",
        "object_id": 869861212
    }
    ```
    ### **DELETE** (/<int:pk>/bind-object/)
    #### Description
    Method that allows to unbind an object from collection by its identifier.
    ##### Request
    * type (`str`) - available types
    ```
    establishment
    product
    ```
    * object_id (`int`) - identifier of an object of type entity
    ##### Response
    ```
    No content
    ```
    """
    pagination_class = None


class GuideListCreateView(GuideBaseView, generics.ListCreateAPIView):
    """
    ## Guide list/create view.
    ### Request
    For creating new instance of Guide model, need to pass the following data in the request:
    required fields:
    * name (str) - guide name
    * start (str) - guide start datetime (datetime in a format `ISO-8601`)
    * vintage (str) - valid year
    * guide_type (int) - guide type enum: `0 (Restaurant), 1 (Artisan), 2 (Wine)`
    * site (int) - identifier of site
    non-required fields:
    * slug - generated automatically if not provided
    * state - state enum`: `0 (Built), 1 (Waiting), 2 (Removing), 3 (Building)` (service states)
    * end - guide end date (datetime in a format `ISO-8601`)

    ### Response
    Return paginated list of guides.
    E.g.:
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```

    ### Description
    *GET*
    Return paginated list of guides with the opportunity of filtering by next fields:
    * establishment_id (int) - identifier of establishment,
    * guide_type (int) - guide type enum: `0 (Restaurant), 1 (Artisan), 2 (Wine)`

    *POST*
    Create a new instance of guide.
    """
    filter_class = filters.GuideFilterSet

    def create(self, request, *args, **kwargs):
        """Overridden create method."""
        super(GuideListCreateView, self).create(request, *args, **kwargs)
        return Response(status=status.HTTP_201_CREATED)


class GuideListView(generics.ListAPIView):
    """View for Guides list.
    ### *GET*
    Return guides list all or return guides list by  country_code param in request.
    Ordered by Guide.created.
    Available filters for fields establishment_id, guide_type
    Values for guide_type: 0 (RESTAURANT), 1 (ARTISAN), 2 (WINE)
    """
    pagination_class = None
    serializer_class = serializers.GuideShortSerializer
    filter_class = filters.GuideFilterSet

    def get_queryset(self):
        """An overridden get_queryset method."""
        if hasattr(self.request, 'country_code'):
            return (
                models.Guide.objects.by_country_code(self.request.country_code)
                                    .order_by('-created')
            )
        return models.Guide.objects.none()


class GuideBackOfficeList(BindObjectMixin, GuideBaseView):
    """ViewSet for Guides list for BackOffice users"""
    pagination_class = None
    filter_class = filters.GuideFilterSet
    bind_object_serializer_class = serializers.GuideBindObjectSerializer

    def perform_binding(self, serializer):
        data = serializer.validated_data
        guide = data.pop('guide')
        obj_type = data.get('type')
        related_object = data.get('related_object')

        if obj_type == self.bind_object_serializer_class.GUIDE:
            guide.establishments.add(related_object)

        elif obj_type == self.bind_object_serializer_class.PRODUCT:
            guide.products.add(related_object)

    def perform_unbinding(self, serializer):
        data = serializer.validated_data
        guide = data.pop('guide')
        obj_type = data.get('type')
        related_object = data.get('related_object')

        if obj_type == self.bind_object_serializer_class.GUIDE:
            guide.establishments.remove(related_object)

        elif obj_type == self.bind_object_serializer_class.PRODUCT:
            guide.products.remove(related_object)


class GuideFilterCreateView(GuideFilterBaseView,
                            generics.CreateAPIView):
    """View for GuideFilter model for BackOffice users."""

    def post(self, request, *args, **kwargs):
        """View for GuideFilter model for BackOffice users.
        ### *POST*
        Create Guide filter for guide instance
        """
        super().create(request, *args, **kwargs)
        return Response(status=status.HTTP_200_OK)


class GuideElementListView(GuideElementBaseView,
                           generics.ListAPIView):
    """TreeView for model GuideElement for back office users.
    ### GET
    Gets the root element and its children. If guide object is not exists then return empty list.
    Available for roles Country-Admin, Admin-User.
    """

    def get_queryset(self):
        """Overridden get_queryset method."""
        guide = get_object_or_404(models.Guide.objects.all(), pk=self.kwargs.get('pk'))
        node = models.GuideElement.objects.get_root_node(guide)
        if node:
            return node.get_children()
        else:
            return models.GuideElement.objects.none()


class GuideUpdateView(GuideBaseView):
    """View for model GuideElement for back office users."""

    def get(self, request, *args, **kwargs):
        """ View for model GuideElement for back office users.
            *GET* method to regenerate elements of guide instance.
        """
        guide = get_object_or_404(models.Guide.objects.all(),
                                  pk=self.kwargs.get('pk'))
        guide.regenerate_elements()
        return Response(status=status.HTTP_200_OK)


class AdvertorialCreateView(AdvertorialBaseView,
                            generics.CreateAPIView):
    """View for Advertorial for back office users."""

    def post(self, request, *args, **kwargs):
        """ View for Advertorial for back office users.
            ### *POST*
            Add Advertorial to guide element
        """
        super(AdvertorialCreateView, self).create(request, *args, **kwargs)
        return Response(status=status.HTTP_200_OK)


class AdvertorialDestroyView(AdvertorialBaseView,
                             generics.DestroyAPIView):
    """View for model Advertorial for back office users."""
    lookup_url_kwarg = 'advertorial_pk'

    def delete(self, request, *args, **kwargs):
        """
            View for model Advertorial for back office users.
            ### *DELETE*
            Delete Advertorial by pk.
        """
        return self.destroy(request, *args, **kwargs)


class GuideElementExportCSVView(generics.ListAPIView):
    """Guide element export csv view."""
    pagination_class = None
    queryset = models.GuideElement.objects.all()
    serializer_class = serializers.GuideElementBaseSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        """Guide element export to csv by pk.
            * .csv file will be sent to the user's mail
        """
        guide = get_object_or_404(
            models.Guide.objects.all(), pk=self.kwargs.get('pk'))
        tasks.export_guide(guide_id=guide.id, user_id=request.user.id)
        return Response({"success": _('The file will be sent to your email.')},
                        status=status.HTTP_200_OK)


class GuideElementExportXMLView(generics.ListAPIView):
    """Guide element export xml view."""
    pagination_class = None
    queryset = models.GuideElement.objects.all()
    serializer_class = serializers.GuideElementBaseSerializer
    permission_classes = get_permission_classes()

    def get(self, request, *args, **kwargs):
        """Guide element export to xml by pk.
            * .xml file will be sent to the user's mail
        """
        guide = get_object_or_404(
            models.Guide.objects.all(), pk=self.kwargs.get('pk'))
        tasks.export_guide(guide_id=guide.id, user_id=request.user.id, file_type='xml')
        return Response({"success": _('The file will be sent to your email.')},
                        status=status.HTTP_200_OK)


class GuideElementExportDOCView(generics.ListAPIView):
    """Guide element export doc view."""
    pagination_class = None
    queryset = models.GuideElement.objects.all()
    serializer_class = serializers.GuideElementBaseSerializer
    permission_classes = get_permission_classes()

    def get(self, request, *args, **kwargs):
        """Guide element export to doc by pk.
            * .doc file will be sent to the user's mail
        """
        guide = get_object_or_404(
            models.Guide.objects.all(), pk=self.kwargs.get('pk'))
        guide.export_to_file(user_id=request.user.id, file_type='doc')
        return Response({"success": _('The file will be sent to your email.')},
                        status=status.HTTP_200_OK)
