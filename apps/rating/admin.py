from django.contrib import admin
from rating import models
from rating import tasks

@admin.register(models.Rating)
class RatingAdmin(admin.ModelAdmin):
    """Rating type admin conf."""
    list_display = ['name', 'ip']
    list_display_links = ['name']


