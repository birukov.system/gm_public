# Generated by Django 2.2.4 on 2019-10-24 09:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0025_merge_20191023_1317'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='image_url',
        ),
        migrations.RemoveField(
            model_name='news',
            name='preview_image_url',
        ),
    ]
