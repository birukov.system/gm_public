"""Recipe app common views."""
from django.utils import translation
from rest_framework import generics

from recipe import models
from recipe.serializers import common as serializers
from rest_framework.permissions import AllowAny


class RecipeViewMixin(generics.GenericAPIView):
    """Recipe view mixin."""

    pagination_class = None
    lookup_field = 'slug'
    permission_classes = (AllowAny, )

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        qs = models.Recipe.objects.published().annotate_in_favorites(user)

        locale = kwargs.get('locale')
        if locale:
            qs = qs.by_locale(locale)

        return qs


class RecipeListView(RecipeViewMixin, generics.ListAPIView):
    """Resource for obtaining a list of recipes."""

    serializer_class = serializers.RecipeListSerializer

    def get_queryset(self, *args, **kwargs):
        locale = translation.get_language()
        kwargs.update({'locale': locale})
        return super().get_queryset(*args, **kwargs)


class RecipeDetailView(RecipeViewMixin, generics.RetrieveAPIView):
    """Resource for detailed recipe information."""

    serializer_class = serializers.RecipeDetailSerializer
