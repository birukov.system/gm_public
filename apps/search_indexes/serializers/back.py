from django_elasticsearch_dsl_drf.serializers import serializers as es_serializers, DocumentSerializer
from search_indexes.documents import (UserDocument, TranslationDocument, EmployeeDocument, EstablishmentDocument,
                                      NewsDocument)
from search_indexes.serializers import SerializedField, SerializedTranslatedField
from establishment.models import Establishment
from news.models import News
from review.models import Review


class LastCountrySerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    subdomain = es_serializers.CharField()
    country_code = es_serializers.CharField()


class UserEsBackRoleSerializer(es_serializers.Serializer):
    role_display = es_serializers.CharField()
    role = es_serializers.IntegerField()
    country_code = es_serializers.CharField(allow_null=True)


class UserDocumentBackSerializer(DocumentSerializer):
    last_login = es_serializers.DateTimeField(allow_null=True)
    first_name = es_serializers.CharField(allow_null=True)
    last_name = es_serializers.CharField(allow_null=True)
    locale = es_serializers.CharField(allow_null=True)
    roles = UserEsBackRoleSerializer(source='roles_visible', many=True)

    class Meta:
        document = UserDocument
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'last_login',
            'is_superuser',
            'last_country',
            'roles',
        )


class TranslationESSerializer(DocumentSerializer):
    text = SerializedField()
    pending_text = SerializedField()

    class Meta:
        document = TranslationDocument
        fields = (
            'id',
            'text',
            'pending_text',
            'page',
            'keywords',
        )


class PositionsEsSerizer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    priority = es_serializers.IntegerField()
    index_name = es_serializers.CharField()
    name_translated = es_serializers.CharField()


class EmployeeAwardTypeEsSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    name = es_serializers.CharField()
    created = es_serializers.DateTimeField()
    modified = es_serializers.DateTimeField()


class EmployeeAwardEsSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    vintage_year = es_serializers.CharField()
    title_translated = SerializedTranslatedField(source='title')
    award_type = EmployeeAwardTypeEsSerializer()


class EmployeeEsSerializer(DocumentSerializer):
    positions = PositionsEsSerizer(many=True, source='positions_indexing')
    awards = EmployeeAwardEsSerializer(many=True)

    class Meta:
        document = EmployeeDocument
        fields = (
            'id',
            'name',
            'last_name',
            'sex',
            'toque_number',
            'public_mark',
            'positions',
            'awards',
            'modified',
        )


class EstablishmentBOReviewSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    vintage = es_serializers.IntegerField()
    status = es_serializers.IntegerField()
    status_display = es_serializers.SerializerMethodField()
    text_translated = SerializedTranslatedField(source='text')

    def get_status_display(self, obj):
        for st, tr in Review.REVIEW_STATUSES:
            if obj.status == st:
                return tr
        return None

    def get_attribute(self, instance):
        return getattr(instance, self.source) if instance and hasattr(instance, self.source) \
                                                 and getattr(instance, self.source) else None


class EstablishmentBOShortCountrySerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    code = es_serializers.CharField()
    name_translated = SerializedTranslatedField(source='name')


class EstablishmentMetaTagsSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    index_name = es_serializers.CharField(source='value')
    label_translated = SerializedTranslatedField(source='label')

    # def get_attribute(self, instance):
    #     return getattr(instance, self.source) if instance and hasattr(instance, self.source) else []


class EstablishmentTypeBOESSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    name_translated = SerializedTranslatedField(source='name')
    index_name = es_serializers.CharField()


class EstablishmentSubTypeBOESSerializer(EstablishmentTypeBOESSerializer):
    id = es_serializers.IntegerField()
    name_translated = SerializedTranslatedField(source='name_serialized')
    index_name = es_serializers.CharField()


class EstablishmentBOESParentRegionSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    name = es_serializers.CharField()
    code = es_serializers.CharField()
    country = EstablishmentBOShortCountrySerializer(allow_null=True)

    def get_attribute(self, instance):
        return instance.parent_region if instance and instance.parent_region else None


class EstablishmentBOESRegionSerializer(es_serializers.Serializer):
    id = es_serializers.IntegerField()
    name = es_serializers.CharField()
    code = es_serializers.CharField()
    country = EstablishmentBOShortCountrySerializer(allow_null=True)
    parent_region = EstablishmentBOESParentRegionSerializer(allow_null=True)

    def get_attribute(self, instance):
        return instance.region if instance and instance.region else None


class EstablishmentBackCityDocumentShortSerializer(es_serializers.Serializer):
    """City serializer for ES Document,"""
    id = es_serializers.IntegerField()
    code = es_serializers.CharField(allow_null=True)
    name_translated = SerializedTranslatedField(source='name')
    region = EstablishmentBOESRegionSerializer(allow_null=True)
    country = EstablishmentBOShortCountrySerializer(allow_null=True)


class AddressEstablishmentDocumentSerializer(es_serializers.Serializer):
    """Address serializer for ES Document."""

    id = es_serializers.IntegerField()
    city = EstablishmentBackCityDocumentShortSerializer()
    street_name_1 = es_serializers.CharField()
    street_name_2 = es_serializers.CharField()
    number = es_serializers.IntegerField()
    postal_code = es_serializers.CharField()
    latitude = es_serializers.FloatField(allow_null=True, source='coordinates.lat')
    longitude = es_serializers.FloatField(allow_null=True, source='coordinates.lon')
    geo_lon = es_serializers.FloatField(allow_null=True, source='coordinates.lon')
    geo_lat = es_serializers.FloatField(allow_null=True, source='coordinates.lat')
    district_name = es_serializers.CharField(allow_null=True)


class EstablishmentBackOfficeDocumentSerializer(DocumentSerializer):
    type = EstablishmentTypeBOESSerializer(source='establishment_type')
    subtypes = EstablishmentSubTypeBOESSerializer(many=True, source='establishment_subtypes')
    status_display = es_serializers.SerializerMethodField()
    restaurant_category = EstablishmentMetaTagsSerializer(many=True, allow_null=True)
    restaurant_cuisine = EstablishmentMetaTagsSerializer(many=True, allow_null=True)
    artisan_category = EstablishmentMetaTagsSerializer(many=True, allow_null=True)
    distillery_type = EstablishmentMetaTagsSerializer(many=True, allow_null=True)
    food_producer = EstablishmentMetaTagsSerializer(many=True, allow_null=True)
    address = AddressEstablishmentDocumentSerializer(allow_null=True)
    last_published_review = EstablishmentBOReviewSerializer(allow_null=True)
    most_recent_review = EstablishmentBOReviewSerializer(allow_null=True)
    vintage_year = es_serializers.IntegerField(allow_null=True)
    phones = es_serializers.ListField(source='contact_phones')
    emails = es_serializers.ListField(source='contact_emails')

    class Meta:
        document = EstablishmentDocument
        fields = (
            'id',
            'name',
            'slug',
            'transliterated_name',
            'index_name',
            'website',
            'toque_number',
            'price_level',
            'public_mark',
            'vintage_year',
            'type',
            'subtypes',
            'status',
            'status_display',
            'restaurant_category',
            'restaurant_cuisine',
            'artisan_category',
            'distillery_type',
            'food_producer',
            'address',
            'last_published_review',
            'most_recent_review',
            'phones',
            'emails',
        )

    def get_status_display(self, obj):
        for st, tr in Establishment.STATUS_CHOICES:
            if obj.status == st:
                return tr
        return None

class NewsTypeBackOfficeSerializer(es_serializers.Serializer):
    """News type serializer"""
    id = es_serializers.IntegerField()
    name = es_serializers.CharField()


class NewsDocumentBackSerializer(DocumentSerializer):
    """News document serializer."""
    title = SerializedField()
    subtitle = SerializedField()
    slugs = SerializedField()
    slug = SerializedTranslatedField(source='slugs')
    title_translated = SerializedTranslatedField(source='title')
    subtitle_translated = SerializedTranslatedField(source='subtitle')
    description_translated = SerializedTranslatedField(source='description')
    news_type = NewsTypeBackOfficeSerializer(allow_null=True)
    tags = EstablishmentMetaTagsSerializer(many=True)
    publication_date = es_serializers.DateField(source='start', format='%d/%m/%Y', allow_null=True)
    locale_to_description_is_active = SerializedField(source='locale_to_description_is_active_serialized')
    state_display = es_serializers.SerializerMethodField()

    class Meta:
        """Meta class."""
        document = NewsDocument
        fields = [
            'id',
            'state',
            'state_display',
            'title',
            'title_translated',
            'subtitle_translated',
            'description_translated',
            'slug',
            'news_type',
            'tags',
            'subtitle',
            'slugs',
            'publication_date',
            'publication_time',
        ]

    def get_state_display(self, obj):
        for st, tr in News.STATE_CHOICES:
            if obj.state == st:
                return tr
        return None
