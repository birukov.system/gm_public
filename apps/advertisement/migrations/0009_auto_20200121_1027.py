# Generated by Django 2.2.7 on 2020-01-21 10:27

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('advertisement', '0008_auto_20191116_1135'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertisement',
            name='duration',
            field=models.FloatField(default=1.0, validators=[django.core.validators.MinValueValidator(0.01)], verbose_name='display duration in seconds'),
        ),
        migrations.AddField(
            model_name='advertisement',
            name='frequency_percentage',
            field=models.FloatField(default=100, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Probability to show'),
        ),
        migrations.AddField(
            model_name='advertisement',
            name='views_count',
            field=models.IntegerField(default=0, verbose_name='How many times shown'),
        ),
    ]
