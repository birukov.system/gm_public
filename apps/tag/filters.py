"""Tag app filters."""
from django_filters import rest_framework as filters
from establishment.models import EstablishmentType, EstablishmentSubType
from django.conf import settings
from tag import models
from product import models as product_models


class TagsBaseFilterSet(filters.FilterSet):

    # Object type choices
    NEWS = 'news'
    ESTABLISHMENT = 'establishment'
    RECIPES = 'recipe'

    TYPE_CHOICES = (
        (NEWS, 'News'),
        (ESTABLISHMENT, 'Establishment'),
        (RECIPES, 'Recipe'),
    )

    type = filters.MultipleChoiceFilter(choices=TYPE_CHOICES,
                                        method='filter_by_type')

    def filter_by_type(self, queryset, name, value):
        if self.NEWS in value:
            queryset = queryset.for_news()
        if self.ESTABLISHMENT in value:
            queryset = queryset.for_establishments()
        if self.RECIPES in value:
            queryset = queryset.by_news_type(self.RECIPES)

        return queryset


class TagCategoryFilterSet(TagsBaseFilterSet):
    """TagCategory filterset."""

    establishment_type = filters.CharFilter(method='by_establishment_type')
    product_type = filters.CharFilter(method='by_product_type')

    class Meta:
        """Meta class."""

        model = models.TagCategory
        fields = ('type',
                  'establishment_type',
                  'product_type', )

    def by_product_type(self, queryset, name, value):
        if value == product_models.ProductType.WINE:
            return queryset.wine_tags_category().filter(tags__products__isnull=False)
        queryset = queryset.by_product_type(value)
        return queryset

    # todo: filter by establishment type
    def by_establishment_type(self, queryset, name, value):
        if value == EstablishmentType.ARTISAN:
            qs = models.TagCategory.objects.with_base_related().filter(index_name='shop_category')
        else:
            qs = queryset.by_establishment_type(value).exclude(index_name__in=['guide', 'collection', 'shop_category'])
        return qs


class FiltersTagCategoryFilterSet(TagCategoryFilterSet):

    generic_type = filters.CharFilter(method='by_generic_type')

    def by_generic_type(self, queryset, name, value):
        # it is type
        if value in map(lambda x: x[0], EstablishmentType.INDEX_NAME_CHOICES):
            return self.by_establishment_type(queryset, name, value)

        # it is subtype
        qs = queryset.by_establishment_subtype(value)
        return qs


class TagsFilterSet(TagsBaseFilterSet):
    """Chosen tags filterset."""

    establishment_type = filters.CharFilter(method='by_establishment_type')

    class Meta:
        """Meta class."""

        model = models.Tag
        fields = (
            'type',
            'establishment_type',
        )

    def by_establishment_type(self, queryset, name, value):
        if value == EstablishmentType.ARTISAN:
            qs = models.Tag.objects.filter(value__in=settings.ARTISANS_CHOSEN_TAGS)
            if self.request.country_code and self.request.country_code not in settings.INTERNATIONAL_COUNTRY_CODES:
                qs = qs.filter(establishments__address__city__country__code=self.request.country_code).distinct('id')
            return qs.exclude(establishments__isnull=True)
        return queryset.by_establishment_type(value)

    # TMP TODO remove it later
    # Временный хардкод для демонстрации 4 ноября, потом удалить!
    def filter_by_type(self, queryset, name, value):
        """ Overrides base filter. Temporary decision"""
        if not (settings.NEWS_CHOSEN_TAGS and settings.ESTABLISHMENT_CHOSEN_TAGS):
            return super().filter_by_type(queryset, name, value)
        queryset = models.Tag.objects
        if self.NEWS in value:
            queryset = queryset.for_news().filter(value__in=settings.NEWS_CHOSEN_TAGS).distinct('value')
        if self.ESTABLISHMENT in value:
            queryset = queryset.for_establishments().filter(category__value_type='list').filter(value__in=settings.ESTABLISHMENT_CHOSEN_TAGS).distinct(
                'value')
        if self.RECIPES in value:
            queryset = queryset.for_news().filter(value__in=settings.RECIPES_CHOSEN_TAGS).distinct('value')
        return queryset

