# Generated by Django 2.2.7 on 2020-03-03 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0043_auto_20200228_1355'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='is_popular',
            field=models.BooleanField(default=False, verbose_name='is city popular'),
        ),
    ]
