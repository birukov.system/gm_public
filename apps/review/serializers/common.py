from rest_framework import serializers

from account.serializers import UserBaseSerializer
from review.models import Review, Inquiries, GridItems
from account.models import User
from django.shortcuts import get_object_or_404


class ReviewBaseSerializer(serializers.ModelSerializer):
    text_translated = serializers.CharField(read_only=True)
    status_display = serializers.CharField(read_only=True)
    reviewer = UserBaseSerializer(read_only=True)

    class Meta:
        model = Review
        fields = [
            'id',
            'reviewer',
            'text',
            'text_translated',
            'priority',
            'status',
            'status_display',
            'child',
            'published_at',
            'vintage',
            'country',
            'content_type',
            'object_id',
            'visited_at',
        ]


class InquiriesBaseSerializer(serializers.ModelSerializer):
    """Serializer for model Inquiries."""

    author = UserBaseSerializer(read_only=True, allow_null=True)
    author_id = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        source='author',
        write_only=True,
        required=True,
    )

    class Meta:
        model = Inquiries
        fields = (
            'id',
            'review',
            'comment',
            'final_comment',
            'mark',
            'attachment_file',
            'author',
            'author_id',
            'bill_file',
            'price',
            'moment',
            'gallery',
            'decibels',
            'nomination',
            'nominee',
            'created',
            'published',
        )
        read_only_fields = (
            'created',
        )
        extra_kwargs = {
            'review': {'required': False},
        }

    @property
    def url_kwargs(self):
        return self.context['request'].parser_context['kwargs']

    def create(self, validated_data):
        validated_review = validated_data.get('review')
        if not validated_review:
            validated_data['review'] = get_object_or_404(
                Review.objects.all(),
                pk=self.url_kwargs.get('review_id'))
        return super(InquiriesBaseSerializer, self).create(validated_data)


class ReviewWithInquiriesSerializer(ReviewBaseSerializer):

    inquiries = InquiriesBaseSerializer(read_only=True, many=True)

    class Meta(ReviewBaseSerializer.Meta):
        fields = ReviewBaseSerializer.Meta.fields + [
            'inquiries',
        ]


class ReviewShortSerializer(ReviewBaseSerializer):
    """Serializer for model Review."""
    text_translated = serializers.CharField(read_only=True)

    class Meta(ReviewBaseSerializer.Meta):
        """Meta class."""
        fields = (
            'id',
            'text_translated',
            'status_display',
            'status',
            'vintage',
        )


class GridItemsBaseSerializer(serializers.ModelSerializer):
    """Serializer for model GridItems."""

    class Meta:
        model = GridItems
        fields = (
            'id',
            'inquiry',
            'sub_name',
            'name',
            'value',
            'desc',
            'dish_title',
        )
        extra_kwargs = {
            'inquiry': {'required': False}
        }

    @property
    def url_kwargs(self):
        return self.context['request'].parser_context['kwargs']

    def create(self, validated_data):
        validated_inquiry = validated_data.get('inquiry')
        if not validated_inquiry:
            validated_data['inquiry'] = get_object_or_404(
                Inquiries.objects.all(),
                pk=self.url_kwargs.get('inquiry_id'))
        return super(GridItemsBaseSerializer, self).create(validated_data)
