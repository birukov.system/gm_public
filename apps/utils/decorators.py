from django.contrib.auth.models import AbstractUser


def with_base_attributes(cls):

    def validate(self, data):
        user = None
        request = self.context.get("request")

        if request and hasattr(request, "user"):
            user = request.user

        if user is not None and isinstance(user, AbstractUser):
            data.update({'modified_by': user})

            if not self.instance:
                data.update({'created_by': user})

        return data

    setattr(cls, "validate", validate)

    return cls
