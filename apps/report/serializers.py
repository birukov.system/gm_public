"""DRF-serializers for application report."""
from django.utils.functional import cached_property
from rest_framework import serializers

from report.models import Report


class ReportBaseSerializer(serializers.ModelSerializer):
    """Serializer for model Report."""

    category_display = serializers.CharField(source='get_category_display', read_only=True)

    class Meta:
        """Meta class."""
        model = Report
        fields = [
            'id',
            'category',
            'category_display',
            'url',
            'description',
            'locale',
        ]
        extra_kwargs = {
            'source': {'required': False},
            'category': {'write_only': True},
            'locale': {'write_only': True}
        }

    @cached_property
    def locale(self) -> str:
        """Return locale from request."""
        request = self.context.get('request')
        if hasattr(request, 'locale'):
            return request.locale

    def validate(self, attrs):
        """An overridden validate method."""
        attrs['source'] = self.context.get('view').get_source()
        attrs['locale'] = self.locale
        return attrs

    def create(self, validated_data):
        """An overridden create method."""
        return self.Meta.model.objects.make(**validated_data)
