from django.core.management.base import BaseCommand
from tqdm import tqdm

from review.models import Review
from transfer.models import Reviews


class Command(BaseCommand):
    help = '''Add review priority from old db to new db.'''

    def handle(self, *args, **kwargs):
        reviews = Review.objects.all().values_list('old_id', flat=True)
        queryset = Reviews.objects.exclude(product_id__isnull=False).filter(
            id__in=list(reviews),
        ).values_list('id', 'priority')

        for old_id, priority in tqdm(queryset, desc='Add priority to reviews'):
            review = Review.objects.filter(old_id=old_id).first()
            if review:
                review.priority = priority
                review.save()

        self.stdout.write(self.style.WARNING(f'Priority added to review objects.'))
