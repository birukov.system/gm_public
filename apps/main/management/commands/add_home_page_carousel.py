from django.core.management.base import BaseCommand
from tqdm import tqdm

from establishment.models import Establishment
from main.models import Carousel
from transfer.models import HomePages
from location.models import Country
from django.db.models import F


class Command(BaseCommand):
    help = '''Add establishment form HomePage to Carousel!'''

    @staticmethod
    def get_country(country_code):
        return Country.objects.filter(code__iexact=country_code).first()

    def handle(self, *args, **kwargs):
        objects = []
        deleted = 0
        hp_list = HomePages.objects.annotate(
            country=F('site__country_code_2'),
        ).all()
        for hm in tqdm(hp_list, desc='Add home_page.establishments to carousel'):
            est = Establishment.objects.filter(old_id=hm.selection_of_week).first()
            if est:
                if est.carousels.exists():
                    est.carousels.all().delete()
                    deleted += 1
                carousel = Carousel(
                    content_object=est,
                    country=self.get_country(hm.country)
                )
                objects.append(carousel)
        Carousel.objects.bulk_create(objects)
        self.stdout.write(
            self.style.WARNING(f'Created {len(objects)}/Deleted {deleted} carousel objects.'))