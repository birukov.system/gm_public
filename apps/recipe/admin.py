"""Recipe app admin conf."""
from django.contrib import admin
from recipe import models
from utils.admin import BaseModelAdminMixin


@admin.register(models.Recipe)
class RecipeAdminModel(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin for model Recipe model."""

