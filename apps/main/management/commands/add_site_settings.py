from django.core.management.base import BaseCommand
from django.db import connections
from tqdm import tqdm

from location.models import Country
from main.models import SiteSettings
from utils.methods import namedtuplefetchall
from translation.models import Language
import json


class Command(BaseCommand):
    help = '''Add site settings from old db to new db.
              Run after country migrate!!!'''

    def site_sql(self):
        with connections['legacy'].cursor() as cursor:
            cursor.execute('''
                            select
                                distinct 
                                id, 
                                country_code_2 as code, 
                                pinterest_page_url, 
                                twitter_page_url, 
                                facebook_page_url, 
                                contact_email, 
                                config, 
                                released, 
                                instagram_page_url, 
                                ad_config
                            from sites as s                         
                         ''')
            return namedtuplefetchall(cursor)

    def add_site_settings(self):

        def get_default_language(config: dict):
            try:
                config = json.loads(config)
                locale = config.get('default_locale')
                if locale:
                    language_qs = Language.objects.filter(locale=locale)
                    if language_qs.exists():
                        return language_qs.first()
            except:
                return None

        objects = []
        for s in tqdm(self.site_sql(), desc='Add site settings'):
            country = Country.objects.filter(code=s.code).first()
            sites = SiteSettings.objects.filter(subdomain=s.code)
            default_language = get_default_language(s.config)
            if not sites.exists():
                objects.append(
                    SiteSettings(
                        subdomain=s.code,
                        country=country,
                        pinterest_page_url=s.pinterest_page_url,
                        twitter_page_url=s.twitter_page_url,
                        facebook_page_url=s.facebook_page_url,
                        instagram_page_url=s.instagram_page_url,
                        contact_email=s.contact_email,
                        config=s.config,
                        ad_config=s.ad_config,
                        old_id=s.id,
                        default_language=default_language,
                    )
                )
        SiteSettings.objects.bulk_create(objects)
        self.stdout.write(self.style.WARNING(f'Add SiteSettings.'))

    def handle(self, *args, **kwargs):
        self.add_site_settings()
