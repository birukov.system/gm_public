"""Favorites urlpaths."""
from django.urls import path

from . import views

app_name = 'favorites'

urlpatterns = [
    path('establishments/', views.FavoritesEstablishmentListView.as_view(),
         name='establishment-list'),
    path('products/', views.FavoritesProductListView.as_view(),
         name='product-list'),
    path('news/', views.FavoritesNewsListView.as_view(),
         name='news-list'),
]
