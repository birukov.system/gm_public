# Generated by Django 2.2.7 on 2020-02-21 11:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0011_image_cropbox'),
        ('advertisement', '0012_advertisement_source'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertisement',
            name='image',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='advertisement', to='gallery.Image'),
        ),
        migrations.AddField(
            model_name='advertisement',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(1, 'ON'), (0, 'OFF')], default=0, verbose_name='Status'),
        ),
    ]
