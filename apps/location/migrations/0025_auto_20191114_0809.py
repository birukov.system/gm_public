# Generated by Django 2.2.7 on 2019-11-14 08:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tag', '0014_tag_old_id_meta_product'),
        ('location', '0024_wineregion_tag_categories'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wineregion',
            name='tag_categories',
        ),
        migrations.AddField(
            model_name='wineregion',
            name='tags',
            field=models.ManyToManyField(blank=True, help_text='attribute from legacy db', related_name='wine_regions', to='tag.Tag'),
        ),
    ]
