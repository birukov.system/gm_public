"""Translation app views."""
from rest_framework import generics

from translation import models, serializers
from utils.methods import get_permission_classes
from utils.permissions import IsTranslator, IsReadOnly


class LanguageViewMixin:
    """Mixin for Language views"""

    queryset = models.Language.objects.all()
    serializer_class = serializers.LanguageSerializer
    pagination_class = None
    permission_classes = get_permission_classes(
        IsTranslator,
        IsReadOnly,
    )


class SiteInterfaceDictionaryMixin:
    """Mixin for view SiteInterfaceDictionary model."""

    queryset = models.SiteInterfaceDictionary.objects.all()
    serializer_class = serializers.SiteInterfaceDictionarySerializer
    pagination_class = None
    permission_classes = get_permission_classes(IsReadOnly)


class LanguageView(LanguageViewMixin, generics.RetrieveAPIView):
    """
    ## Retrieve view for model Language
    ### *GET*
    #### Description
    Return a serialized language object
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a language object.
    ##### Request
    Available
    * title (`str`) - Title of language
    * locale (`str`) - Locale identifier (length=10)
    * is_active (`bool`) - Status of language (default=True)
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE**
    #### Description
    Delete an object by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """


class SiteInterfaceDictionaryListView(SiteInterfaceDictionaryMixin, generics.ListAPIView):
    """
    ## Site interface dictionary List/Create view.
    ### *GET*
    #### Description
    Return non-paginated list of languages.
    Available filters
    * page (`str`) - filtered by page name
    * keywords (`str`) - filtered by a keyword
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    """

    filter_fields = ['page', 'keywords']


class SiteInterfaceDictionaryView(SiteInterfaceDictionaryMixin, generics.RetrieveAPIView):
    """
    ## Site interface dictionary RUD view.
    ### *GET*
    #### Description
    Return serialized object by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    """


class LanguageListView(LanguageViewMixin, generics.ListAPIView):
    """
    ## List/Create view for model Language
    ### *GET*
    #### Description
    Return non-paginated list of languages.
    Available filters
    * is_active (`bool`) - filtered by language status
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    ### *POST*
    #### Description
    Create a new language.
    ##### Request
    Required
    * title (`str`) - Title of language
    * locale(`str`) - Locale identifier (length=10)
    Available
    * is_active (`bool`) - Status of language (default=True)
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    """

    filter_fields = ['is_active', ]
