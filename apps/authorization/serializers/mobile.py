from rest_framework import serializers

from utils.serializers import SourceSerializerMixin


class MobileCodeConfirmationSerializer(SourceSerializerMixin):
    email = serializers.EmailField(write_only=True)
    code = serializers.RegexField('^[0-9]{4}$')
