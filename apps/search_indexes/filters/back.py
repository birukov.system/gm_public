from django_elasticsearch_dsl_drf.filter_backends import DefaultOrderingFilterBackend


class CustomDefaultOrderingFilterBackend(DefaultOrderingFilterBackend):

    @classmethod
    def get_default_ordering_params(cls, view):
        """Overriden method"""
        if ['search', 'ordering'] & view.request.query_params.keys():
            return None
        return super().get_default_ordering_params(view)