from django.conf import settings
from rest_framework import serializers

from main.serializers.common import SiteSettingsBackOfficeSerializer


class _SiteSettingsMobileSerializer(SiteSettingsBackOfficeSerializer):
    """Site settings serializer for mobiles."""
    country_capital_longitude = serializers.FloatField(source='country.capital_longitude', read_only=True)
    country_capital_latitude = serializers.FloatField(source='country.capital_latitude', read_only=True)
    country_flag_url = serializers.URLField(source='country.flag_image.url', read_only=True)
    subregions = serializers.SerializerMethodField(read_only=True)

    def get_subregions(self, obj):
        return settings.COUNTRY_CODE_ALIASES_ANTILLES_GUYANE_WEST_INDIES if obj.country.code == 'aa' else []

    class Meta(SiteSettingsBackOfficeSerializer.Meta):
        fields = SiteSettingsBackOfficeSerializer.Meta.fields + [
            'country_capital_longitude',
            'country_capital_latitude',
            'country_flag_url',
            'subregions',
        ]


class SiteSettingsMobileSerializer(serializers.Serializer):
    site_settings = _SiteSettingsMobileSerializer(many=True)
    password_regexp = serializers.SerializerMethodField()

    def get_password_regexp(self, obj):
        return settings.MOBILE_PASSWORD_REGEXP if settings.MOBILE_PASSWORD_REGEXP else None
