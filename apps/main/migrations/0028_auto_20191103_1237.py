# Generated by Django 2.2.4 on 2019-11-03 12:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0027_carousel_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currency',
            name='sign',
            field=models.CharField(max_length=3, verbose_name='sign'),
        ),
    ]
