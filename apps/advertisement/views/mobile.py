"""Mobile views for app advertisement"""
import random

from django.db.models import F
from advertisement.serializers import AdvertisementPageTypeMobileListSerializer
from .common import AdvertisementPageTypeListView


class AdvertisementPageTypeMobileListView(AdvertisementPageTypeListView):
    """Advertisement mobile list view."""

    serializer_class = AdvertisementPageTypeMobileListSerializer
    pagination_class = None

    def get_queryset(self):
        percentage = random.randrange(0, 100)
        qs = super().get_queryset().exclude(frequency_percentage__lte=percentage)
        qs.update(views_count=F('views_count') + 1)
        return qs
