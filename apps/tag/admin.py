from django.contrib import admin
from .models import Tag, TagCategory


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    """Admin model for model Tag."""
    list_display = ['id', 'value', 'category']
    list_display_links = ['id', 'value',]
    list_filter = ['value']


@admin.register(TagCategory)
class TagCategoryAdmin(admin.ModelAdmin):
    """Admin model for model TagCategory."""
    list_display = ['id', 'index_name', ]
    list_display_links = ['id', 'index_name', ]