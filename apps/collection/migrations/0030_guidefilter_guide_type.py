# Generated by Django 2.2.7 on 2019-12-25 18:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0029_merge_20191225_1819'),
    ]

    operations = [
        migrations.AddField(
            model_name='guidefilter',
            name='guide_type',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Restaurant'), (1, 'Artisan'), (2, 'Wine')], default=0, verbose_name='guide type'),
        ),
    ]
