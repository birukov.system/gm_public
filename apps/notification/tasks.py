from datetime import datetime

from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import get_template, render_to_string

from main.models import SiteSettings
from notification import models
from django.utils.translation import gettext_lazy as _


@shared_task
def send_subscribes_update_email(subscriber_id):
    subscriber = models.Subscriber.objects.get(pk=subscriber_id)

    if subscriber is None:
        return

    country_code = subscriber.country_code

    html_template = get_template(settings.NOTIFICATION_SUBSCRIBE_TEMPLATE)
    year = datetime.now().year

    socials = list(SiteSettings.objects.with_country().select_related('country'))
    socials = dict(zip(map(lambda social: social.country.code, socials), socials))

    socials_for_subscriber = socials.get(country_code)

    context = {
        "title": _("You have subscribed on news G&M"),
        "description": _("<br>".join([
            name.get(subscriber.locale) if subscriber.locale in name else name.get(next(iter(name.keys())))
            for name in subscriber.subscription_types.values_list('name', flat=True)
        ])),
        "code": subscriber.update_code,
        "link_to_unsubscribe": subscriber.link_to_unsubscribe,
        "twitter_page_url": socials_for_subscriber.twitter_page_url if socials_for_subscriber else '#',
        "instagram_page_url": socials_for_subscriber.instagram_page_url if socials_for_subscriber else '#',
        "facebook_page_url": socials_for_subscriber.facebook_page_url if socials_for_subscriber else '#',
        "send_to": subscriber.send_to,
        "year": year
    }

    send_mail(
        subject=_("G&M Subscriptions"),
        message=render_to_string(settings.NOTIFICATION_SUBSCRIBE_TEMPLATE, context),
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[subscriber.send_to],
        fail_silently=False,
        html_message=html_template.render(context)
    )


@shared_task
def send_unsubscribe_email(subscriber_id):
    subscriber = models.Subscriber.objects.get(pk=subscriber_id)

    if subscriber is None:
        return

    country_code = subscriber.country_code

    html_template = get_template(settings.NOTIFICATION_SUBSCRIBE_TEMPLATE)
    year = datetime.now().year

    socials = list(SiteSettings.objects.with_country().select_related('country'))
    socials = dict(zip(map(lambda social: social.country.code, socials), socials))

    socials_for_subscriber = socials.get(country_code)

    context = {
        "title": _("You have successfully unsubscribed from G&M news"),
        "description": "",
        "code": subscriber.update_code,
        "link_to_unsubscribe": subscriber.link_to_unsubscribe,
        "twitter_page_url": socials_for_subscriber.twitter_page_url if socials_for_subscriber else '#',
        "instagram_page_url": socials_for_subscriber.instagram_page_url if socials_for_subscriber else '#',
        "facebook_page_url": socials_for_subscriber.facebook_page_url if socials_for_subscriber else '#',
        "send_to": subscriber.send_to,
        "year": year
    }

    send_mail(
        subject="G&M Subscriptions",
        message=render_to_string(settings.NOTIFICATION_SUBSCRIBE_TEMPLATE, context),
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[subscriber.send_to],
        fail_silently=False,
        html_message=html_template.render(context)
    )
