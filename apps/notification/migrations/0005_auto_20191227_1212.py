# Generated by Django 2.2.7 on 2019-12-27 12:12

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0004_auto_20191118_1307'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscriber',
            name='subscription_type',
        ),
        migrations.CreateModel(
            name='Subscribe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='Date created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('subscriber', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='notification.Subscriber')),
                ('subscription_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='notification.SubscriptionType')),
            ],
            options={
                'verbose_name': 'Subscribe',
                'verbose_name_plural': 'Subscribes',
            },
        ),
        migrations.AddField(
            model_name='subscriber',
            name='subscription_types',
            field=models.ManyToManyField(through='notification.Subscribe', to='notification.SubscriptionType'),
        ),
    ]
