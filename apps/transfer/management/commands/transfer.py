from django.core.management.base import BaseCommand

from transfer.utils import transfer_objects


class Command(BaseCommand):
    help = 'Transfer data between databases'

    """Типы данных для трансфера
    ВНИМАНИЕ: первые буквы типов данных должны быть уникальны!
    """
    SHORT_DATA_TYPES = [
        'dictionaries',  # №2 - перенос стран, регионов, городов, адресов
        'news',  # перенос новостей (после №2)
        # 'account',  # №1 - перенос пользователей - нет, см make_data_migrations.sh !!!
        'subscriber',
        # 'recipe',  # №2 - рецепты - удалить приложение recipe вообще, все в news !!!
        'partner',
        'establishment',  # №3 - перенос заведений
        'gallery',
        'commercial',  # перенос рекламмы (очередность не важна)
        'overlook',  # №5 - перенос языков, отзывов
        'tmp',
        'menu',
        'location_establishment',  # №4 - добавление адресов для перенесенных заведений
        'whirligig',
        'identities',
    ]

    LONG_DATA_TYPES = [
        'update_country_flag',
        'comment',
        'inquiries',  # №6 - перенос запросов оценок
        'wine_characteristics',  # №5 - перенос характиристик вин
        'product',  # №5 - перенос продуктов
        'product_note',  # №6 - перенос заметок продуктов
        'souvenir',  # №5 - перенос продуктов типа - сувениры
        'establishment_note',  # №5 - перенос заметок заведений
        'assemblage',  # №6 - перенос тегов для типа продуктов - вино
        'rating_count',
        'product_review',
        'newsletter_subscriber',  # подписчики на рассылку - переносить после переноса пользователей №1
        'purchased_plaques',  # №6 - перенос купленных тарелок
        'guides',
        'guide_filters',
        'guide_element_sections',
        'guide_wine_color_sections',
        'guide_element_types',
        'guide_elements_bulk',
        'guide_element_advertorials',
        'guide_element_label_photo',
        'guide_complete',
        'update_city_info',
        'fix_location',
        'remove_old_locations',
        'add_fake_country',
        'setup_clean_db',
        'languages',  # №4 - перенос языков
        'set_unused_regions',
        'update_fake_country_flag',
        'transfer_text_review',  # переводы для review с их авторами - запускать после overlook и product_review
        'set_country_phone_code',
        'a_la_cartes',
        'employees',
        'collections',
    ]

    def handle(self, *args, **options):
        """
        Находим включённую опцию путём пересечения множества типов данных и множества включённых опций"""
        data_type = list(set(option for option in options.keys() if options[option]) & set(self.LONG_DATA_TYPES))
        if len(data_type) != 1:
            data_type = list(set(option for option in options.keys() if options[option]) & set(self.SHORT_DATA_TYPES))
            if len(data_type) != 1:
                print(
                    "You must set correct option!\r\nYou can get options list with \r\n\r\n\tmanage.py help transfer\r\n")
                exit(1)
            else:
                data_type = data_type[0]
        else:
            data_type = data_type[0]

        transfer_objects(data_type)

    def add_arguments(self, parser):
        for option_type in self.LONG_DATA_TYPES:
            parser.add_argument(
                f'--{option_type}',
                action='store_true',
                default=False,
                help=f'Transfer {option_type} objects'
            )

        """Добавляем опции к команде, основываясь на типах данных, определённых в DATA_TYPES"""
        for option_type in self.SHORT_DATA_TYPES:
            parser.add_argument(
                f'-{option_type[:1]}',
                f'--{option_type}',
                action='store_true',
                default=False,
                help=f'Transfer {option_type} objects'
            )
