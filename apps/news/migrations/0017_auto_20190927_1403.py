# Generated by Django 2.2.4 on 2019-09-27 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0016_news_template'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='is_publish',
        ),
        migrations.AddField(
            model_name='news',
            name='state',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Waiting'), (1, 'Hidden'), (2, 'Published'), (3, 'Published exclusive')], default=0, verbose_name='State'),
        ),
    ]
