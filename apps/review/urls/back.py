"""Back review URLs"""
from django.urls import path

from review.views import back as views

app_name = 'review'

urlpatterns = [
    path('', views.ReviewListView.as_view(), name='review-list-create'),
    path('<int:id>/', views.ReviewRUDView.as_view(), name='review-crud'),
    path('<int:review_id>/inquiries/', views.InquiriesListView.as_view(), name='inquiries-list'),
    path('inquiries/', views.InquiriesListView.as_view(), name='inquiries-list-create'),
    path('inquiries/<int:id>/', views.InquiriesRUDView.as_view(), name='inquiries-crud'),
    path('inquiries/<int:inquiry_id>/grid/', views.GridItemsListView.as_view(), name='grid-list-create'),
    path('inquiries/grid/', views.GridItemsListView.as_view(), name='grid-list-create'),
    path('inquiries/grid/<int:id>/', views.GridItemsRUDView.as_view(), name='grid-crud'),
    path('inquiries/inspectors/', views.InquiriesInspectorsListView.as_view(), name='inspector-list'),
    path('vintage-list/', views.ReviewVintageListView.as_view(), name='vintage-list'),
    path('author-list/', views.ReviewAuthorListView.as_view(), name='author-list'),
    path('available_statuses/', views.ReviewAvailableStatusesSetView.as_view(), name='available-statuses'),
    path('csv/', views.ReviewCSVListView.as_view(), name='review-csv-list'),
]
