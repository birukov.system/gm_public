"""
Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql

"""

card = {
    # как работать с GenericForeignKey(content_type) - ?
    "Comment": {
        "data_type": "objects",
        "dependencies": ("User",),
        "fields": {
            "Comments": {
                "text": "comment",
                "mark": ("mark", "django.db.models.PositiveIntegerField")
                # как работать с GenericForeignKey - ?
                # "content_object" : ""
            },
        },
        "relations": {
            "Accounts": [
                (("account", None),
                 ("User", "user", None, None))
            ]
        }
    }
}

used_apps = ("account", )