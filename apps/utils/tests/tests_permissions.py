from rest_framework.test import APITestCase
from location.models import Country
from translation.models import Language


class BasePermissionTests(APITestCase):
    def setUp(self):
        self.lang, created = Language.objects.get_or_create(
            title='Russia',
            locale='ru-RU'
        )

        self.country_ru, created = Country.objects.get_or_create(
            name={"en-GB": "Russian"}
        )


