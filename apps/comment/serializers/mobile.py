from comment.models import Comment
from .common import CommentBaseSerializer


class MobileCommentSerializer(CommentBaseSerializer):
    class Meta(CommentBaseSerializer.Meta):
        model = Comment
        fields = [f for f in CommentBaseSerializer.Meta.fields if f != 'user_email']