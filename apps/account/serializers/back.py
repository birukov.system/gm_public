"""Back account serializers"""
from rest_framework import serializers
from django.db import transaction
from django.db.models.query import Prefetch, prefetch_related_objects

from account import models
from account.serializers import RoleBaseSerializer, UserSerializer, subscriptions_handler
from main.models import SiteSettings


class _SiteSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteSettings
        fields = (
            'id',
            'subdomain',
            'country_code',
        )


class BackUserSerializer(UserSerializer):
    last_country = _SiteSettingsSerializer(read_only=True)
    roles = RoleBaseSerializer(read_only=True, many=True)

    class Meta(UserSerializer.Meta):
        fields = (
            'id',
            'last_login',
            'is_superuser',
            'username',
            'last_name',
            'first_name',
            'is_active',
            'date_joined',
            'image_url',
            'cropped_image_url',
            'email',
            'email_confirmed',
            'unconfirmed_email',
            'email_confirmed',
            'newsletter',
            'password',
            'city',
            'locale',
            'last_ip',
            'last_country',
            'roles',
            'subscriptions',
            'phone',
            'country_calling_code',
            'national_calling_number',

        )
        extra_kwargs = {
            'password': {'write_only': True},
            'email': {'required': True},
        }
        read_only_fields = (
            'old_password',
            'last_login',
            'date_joined',
            'city',
            'locale',
            'last_ip',
            'last_country',
        )

    def create(self, validated_data):
        subscriptions_list = []
        if 'subscription_types' in validated_data:
            subscriptions_list = validated_data.pop('subscription_types')

        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        subscriptions_handler(subscriptions_list, user)
        return user


class BackDetailUserSerializer(BackUserSerializer):
    class Meta(BackUserSerializer.Meta):
        fields = (
            'id',
            'last_country',
            'roles',
            'last_login',
            'is_superuser',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'date_joined',
            'username',
            'image_url',
            'cropped_image_url',
            'email',
            'unconfirmed_email',
            'email_confirmed',
            'newsletter',
            'old_id',
            'locale',
            'city',
            'last_ip',
            'groups',
            'user_permissions',
            'subscriptions',
            'phone',
            'country_calling_code',
            'national_calling_number',
        )
        read_only_fields = (
            'old_password',
            'last_login',
            'date_joined',
            'last_ip',
            'last_country',
        )

    def create(self, validated_data):
        subscriptions_list = []

        if 'subscription_types' in validated_data:
            subscriptions_list = validated_data.pop('subscription_types')

        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        subscriptions_handler(subscriptions_list, user)
        return user

    def update(self, instance, validated_data):
        subscriptions_list = []

        if 'subscription_types' in validated_data:
            subscriptions_list = validated_data.pop('subscription_types')

        if 'roles' in validated_data:
            roles_ids = [role['id'] for role in validated_data.pop('roles') if 'id' in role]
            instance.set_roles(roles_ids)

        instance = super().update(instance, validated_data)
        subscriptions_handler(subscriptions_list, instance)
        prefetch_related_objects([instance], Prefetch('roles', queryset=models.Role.objects.
                                                      order_by('userrole__created')))
        return instance


class UserRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserRole
        fields = [
            'role',
            'user',
            'establishment'
        ]
        extra_kwargs = {
            'role': {'required': True}
        }


class UserCSVSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True, source='date_joined')
    last_seen_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True, source='last_login')
    confirmed_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True,)
    country = serializers.CharField(allow_blank=True, allow_null=True, source='country_name')
    roles_list = serializers.CharField(allow_blank=True, allow_null=True, source='locale_roles')

    class Meta:
        model = models.User
        fields = [
            'id',
            'username',
            'email',
            'locale',
            'country',
            'created_at',
            'confirmed_at',
            'last_seen_at',
            'last_ip',
            'roles_list',
        ]


class InviteTeamSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, allow_blank=False, allow_null=False)

    class Meta:
        model = models.User
        fields = (
            'id',
            'email',
        )

    def create(self, validated_data):
        with transaction.atomic():
            establishment = self.context['view'].kwargs['establishment']
            country_code = self.context['request'].country_code
            return models.User.objects.invite_for_team(validated_data['email'], establishment, country_code)
