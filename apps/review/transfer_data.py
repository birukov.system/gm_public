from pprint import pprint

from establishment.models import Establishment
from product.models import Product
from review.models import Inquiries as NewInquiries, Review
from transfer.models import Reviews, ReviewTexts, Inquiries, GridItems, InquiryPhotos
from transfer.serializers.grid import GridItemsSerializer
from transfer.serializers.inquiries import InquiriesSerializer
from transfer.serializers.inquiry_gallery import InquiryGallerySerializer
from transfer.serializers.reviews import (
    LanguageSerializer, ReviewSerializer, ReviewTextSerializer, ProductReviewSerializer

)


def transfer_languages():
    queryset = ReviewTexts.objects.raw("""SELECT id, locale 
                                        FROM review_texts 
                                        WHERE CHAR_LENGTH(locale) = 5
                                        AND review_texts.locale IS NOT NULL GROUP BY locale""")

    queryset = [vars(query) for query in queryset]

    serialized_data = LanguageSerializer(data=queryset, many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"Language serializer errors: {serialized_data.errors}")


def transfer_reviews():
    establishments = Establishment.objects.filter(old_id__isnull=False).values_list('old_id', flat=True)
    queryset = Reviews.objects.exclude(product_id__isnull=False).filter(
        establishment_id__in=list(establishments),
    ).values('id', 'reviewer_id', 'aasm_state', 'created_at', 'establishment_id', 'mark', 'vintage')

    serialized_data = ReviewSerializer(data=list(queryset), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"ReviewSerializer serializer errors: {serialized_data.errors}")


def transfer_text_review():
    reviews = Review.objects.filter(old_id__isnull=False).values_list('old_id', flat=True)
    queryset = ReviewTexts.objects.filter(
        review_id__in=list(reviews)
    ).exclude(
        id__in=(23183, 25348, 43930, 23199, 26226, 34006)  # пробелы вместо текста
    ).exclude(
        text__isnull=True
    ).exclude(
        text__iexact=''
    ).values('review_id', 'locale', 'text', 'updated_by', 'created_at', 'updated_at')

    serialized_data = ReviewTextSerializer(data=list(queryset), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"ReviewTextSerializer serializer errors: {serialized_data.errors}")


def make_en_text_review():
    for review in Review.objects.filter(old_id__isnull=False):
        text = review.text
        if text and 'en-GB' not in text:
            text.update({
                'en-GB': next(iter(text.values()))
            })
            review.text = text
            review.save()


def transfer_inquiries():
    reviews = Review.objects.all().values_list('old_id', flat=True)
    inquiries = Inquiries.objects.exclude(account__confirmed_at__isnull=True).filter(review_id__in=list(reviews))

    serialized_data = InquiriesSerializer(data=list(inquiries.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"Inquiries serializer errors: {serialized_data.errors}")


def transfer_grid():
    inquiries = NewInquiries.objects.all().values_list('old_id', flat=True)
    grids = GridItems.objects.filter(inquiry_id__in=list(inquiries))

    serialized_data = GridItemsSerializer(data=list(grids.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"GridItems serializer errors: {serialized_data.errors}")


def transfer_inquiry_photos():
    inquiries = NewInquiries.objects.all().values_list('old_id', flat=True)
    grids = InquiryPhotos.objects.filter(inquiry_id__in=list(inquiries), attachment_suffix_url__isnull=False)

    serialized_data = InquiryGallerySerializer(data=list(grids.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"InquiryGallery serializer errors: {serialized_data.errors}")


def transfer_product_reviews():
    products = Product.objects.filter(
        old_id__isnull=False).values_list('old_id', flat=True)

    queryset = Reviews.objects.filter(
        product_id__in=list(products),
    ).values('id', 'reviewer_id', 'aasm_state', 'created_at', 'product_id', 'mark', 'vintage')

    serialized_data = ProductReviewSerializer(data=list(queryset.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"ProductReviewSerializer serializer errors: {serialized_data.errors}")


data_types = {
    "languages": [
        transfer_languages,
    ],
    "overlook": [
        transfer_reviews,
        transfer_text_review,
        make_en_text_review,
    ],
    'inquiries': [
        transfer_inquiries,
        transfer_grid,
        transfer_inquiry_photos,
    ],
    "product_review": [
        transfer_product_reviews,
    ],
    "transfer_text_review": [
        transfer_text_review,
    ]
}
