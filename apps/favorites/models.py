from django.contrib.contenttypes import fields as generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _
from utils.querysets import ContentTypeQuerySetMixin
from utils.models import ProjectBaseMixin


class FavoritesQuerySet(ContentTypeQuerySetMixin):
    """QuerySet for model Favorites"""

    def by_user(self, user):
        """Filter by user"""
        return self.filter(user=user)


class Favorites(ProjectBaseMixin):
    """Favorites model."""

    user = models.ForeignKey('account.User',
                             on_delete=models.CASCADE,
                             related_name='favorites',
                             verbose_name=_('User'))
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    objects = FavoritesQuerySet.as_manager()

    class Meta:
        """Meta class."""
        verbose_name = _('Favorites')
        verbose_name_plural = _('Favorites')

    def __str__(self):
        """String representation."""
        return f'{self.id}'
