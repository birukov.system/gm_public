# Generated by Django 2.2.7 on 2019-11-13 09:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0014_auto_20191112_0538'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='mark',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='mark'),
        ),
    ]
