from django.contrib import admin

from partner import models


@admin.register(models.Partner)
class PartnerModelAdmin(admin.ModelAdmin):
    """Model admin for Partner model."""
    raw_id_fields = ('establishment',)
