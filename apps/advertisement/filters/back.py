from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters
from advertisement import models
from main import models as main_models


class AdvertisementListFilter(filters.FilterSet):
    """Role filter set."""
    page_type = filters.MultipleChoiceFilter(choices=main_models.Page.PAGE_TYPES, method='by_page_type')
    device = filters.MultipleChoiceFilter(
        choices=main_models.Page.SOURCES,
        method='by_devices'
    )
    state = filters.BooleanFilter(method='by_state')

    class Meta:
        """Meta class."""
        model = models.Advertisement
        fields = [
            'page_type',
            'device',
            'state',
        ]

    @staticmethod
    def by_page_type(queryset, _, value):
        if value not in EMPTY_VALUES:
            '''apply filter.'''
            return queryset.filter_by_page_type(value)

        return queryset

    @staticmethod
    def by_devices(queryset, _, value):
        if value not in EMPTY_VALUES:
            '''apply filters.'''
            return queryset.filter_by_device(value)

        return queryset

    @staticmethod
    def by_state(queryset, _, value):
        if value not in EMPTY_VALUES:
            '''apply filters.'''
            return queryset.filter_by_state(value)

        return queryset
