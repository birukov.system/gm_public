"""News app urlpatterns for backoffice"""
from django.urls import path

from news import views
from search_indexes.views import BackOfficeNewsDocumentViewSet

app_name = 'news'

urlpatterns = [
    path('', views.NewsBackOfficeLCView.as_view(), name='list-create'),
    path('list/', BackOfficeNewsDocumentViewSet.as_view({'get': 'list'}), name='search-news'),
    path('states/', views.NewsStatesView.as_view(), name='possible-news-states-list'),
    path('<int:pk>/', views.NewsBackOfficeRUDView.as_view(), name='retrieve-update-destroy'),
    path('<int:pk>/gallery/', views.NewsBackOfficeGalleryListView.as_view(), name='gallery-list'),
    path('<int:pk>/gallery/<int:image_id>/', views.NewsBackOfficeGalleryCreateDestroyView.as_view(),
         name='gallery-create-destroy'),
    path('<int:pk>/clone/<str:country_code>', views.NewsCloneView.as_view(), name='clone-news-item'),
]
