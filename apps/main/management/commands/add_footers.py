from django.core.management.base import BaseCommand
from tqdm import tqdm

from main.models import SiteSettings, Footer
from transfer.models import Footers


class Command(BaseCommand):
    help = '''Add footers from legacy DB.'''

    def handle(self, *args, **kwargs):
        objects = []
        deleted = 0
        footers_list = Footers.objects.all()

        for old_footer in tqdm(footers_list, desc='Add footers'):
            site = SiteSettings.objects.filter(old_id=old_footer.site_id).first()
            if site:
                if site.footers.exists():
                    site.footers.all().delete()
                    deleted += 1
                footer = Footer(
                    site=site,
                    about_us=old_footer.about_us,
                    copyright=old_footer.copyright,
                    created=old_footer.created_at,
                    modified=old_footer.updated_at
                )
                objects.append(footer)
        Footer.objects.bulk_create(objects)
        self.stdout.write(
            self.style.WARNING(f'Created {len(objects)}/Deleted {deleted} footer objects.'))
