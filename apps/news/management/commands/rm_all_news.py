from django.core.management.base import BaseCommand

from news.models import News


class Command(BaseCommand):
    help = 'Remove all news from new bd'

    def handle(self, *args, **kwargs):
        old_news = News.objects.all()
        count = old_news.count()
        old_news.delete()
        self.stdout.write(self.style.WARNING(f'Deleted {count} objects.'))
