"""Common serializer for application authorization"""
from django.conf import settings
from django.contrib.auth import password_validation as password_validators
from django.db.models import Q
from rest_framework import serializers

from account import models as account_models
from authorization import tasks
from authorization.models import JWTRefreshToken
from notification.models import Subscriber
from utils import exceptions as utils_exceptions
from utils import methods as utils_methods
from utils.authentication import send_confirmation_email
from utils.methods import get_user_ip
from utils.models import PlatformMixin
from utils.serializers import SourceSerializerMixin
from utils.tokens import GMRefreshToken


# Serializers
class SignupSerializer(serializers.ModelSerializer, SourceSerializerMixin):
    """Signup serializer serializer mixin"""
    email = serializers.EmailField(write_only=True)

    class Meta:
        model = account_models.User
        fields = (
            'username',
            'password',
            'email',
            'newsletter',
            'source'
        )
        extra_kwargs = {
            'username': {'write_only': True},
            'password': {'write_only': True},
            # 'email': {'write_only': True},
            'newsletter': {'write_only': True}
        }

    def validate_username(self, value):
        """Custom username validation"""
        if value:
            valid = utils_methods.username_validator(username=value)
            if not valid:
                raise utils_exceptions.NotValidUsernameError()
            if account_models.User.objects.filter(username__iexact=value).exists():
                raise serializers.ValidationError()

        return value

    def validate_password(self, value):
        """Custom password validation"""
        try:
            password_validators.validate_password(password=value)
        except serializers.ValidationError as e:
            raise serializers.ValidationError(str(e))
        else:
            return value

    def create(self, validated_data):
        request = self.context.get('request')

        """Overridden create method"""

        username = validated_data.get('username')
        if not username:
            username = utils_methods.string_random()
            while account_models.User.objects.filter(username__iexact=username).exists():
                username = utils_methods.string_random()

        obj = account_models.User.objects.make(
            username=username,
            password=validated_data.get('password'),
            email=validated_data.get('email'),
            newsletter=validated_data.get('newsletter'))

        source = validated_data['source']
        is_mobile = source == PlatformMixin.MOBILE

        send_confirmation_email(obj.id, request.country_code, is_mobile=is_mobile)

        # Make subscriber or associate user
        subscriber = Subscriber.objects.associate_user(user=obj)

        if subscriber is None:
            Subscriber.objects.make_subscriber(
                email=obj.unconfirmed_email, user=obj, ip_address=get_user_ip(request),
                country_code=request.country_code, locale=request.locale
            )

        return obj


class LoginByUsernameOrEmailSerializer(SourceSerializerMixin,
                                       serializers.ModelSerializer):
    """Serializer for login user"""
    # REQUEST
    username_or_email = serializers.CharField(write_only=True)

    # For cookie properties (Max-Age)
    remember = serializers.BooleanField(write_only=True)

    # RESPONSE
    refresh_token = serializers.CharField(read_only=True)
    access_token = serializers.CharField(read_only=True)

    class Meta:
        """Meta-class"""
        model = account_models.User
        fields = (
            'username_or_email',
            'password',
            'remember',
            'source',
            'refresh_token',
            'access_token',
        )
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def validate(self, attrs):
        """Override validate method"""
        username_or_email = attrs.pop('username_or_email')
        password = attrs.pop('password')
        user_qs = account_models.User.objects.filter(Q(username=username_or_email) |
                                                     Q(email=username_or_email))

        if not user_qs.exists():
            raise utils_exceptions.WrongAuthCredentials()

        user = user_qs.first()
        password_is_valid = user.check_password(password)

        if not password_is_valid:
            raise utils_exceptions.WrongAuthCredentials()

        self.instance = user
        return attrs

    def to_representation(self, instance):
        """Override to_representation method"""
        tokens = instance.create_jwt_tokens(source=self.validated_data.get('source'))
        setattr(instance, 'access_token', tokens.get('access_token'))
        setattr(instance, 'refresh_token', tokens.get('refresh_token'))
        return super().to_representation(instance)


class RefreshTokenSerializer(SourceSerializerMixin):
    """Serializer for refresh token view"""
    refresh_token = serializers.CharField(read_only=True)
    access_token = serializers.CharField(read_only=True)

    def validate(self, attrs):
        """Override validate method"""

        cookie_refresh_token = self.context.get('request').COOKIES.get('refresh_token')
        # Check if refresh_token in COOKIES
        if not cookie_refresh_token:
            raise utils_exceptions.NotValidRefreshTokenError()

        refresh_token = GMRefreshToken(cookie_refresh_token)
        refresh_token_qs = JWTRefreshToken.objects.valid() \
                                                  .by_jti(jti=refresh_token.payload.get('jti'))
        # Check if the user has refresh token
        if not refresh_token_qs.exists():
            raise utils_exceptions.NotValidRefreshTokenError()

        old_refresh_token = refresh_token_qs.first()
        source = old_refresh_token.source
        user = old_refresh_token.user

        # Expire existing tokens
        old_refresh_token.expire()
        old_refresh_token.access_token.expire()

        # Create new one for user
        response = user.create_jwt_tokens(source=source)

        return response


# OAuth
class OAuth2Serialzier(SourceSerializerMixin):
    """Serializer OAuth2 authorization"""
    token = serializers.CharField(max_length=255)


class SignUpResendConfirmationEmailSerializer(SourceSerializerMixin, serializers.Serializer):
    email = serializers.EmailField()
