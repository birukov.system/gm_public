from rest_framework import generics, permissions
from rest_framework.views import Response

from main import models, serializers
from main.serializers.mobile import SiteSettingsMobileSerializer
from utils.models import PlatformMixin
from utils.querysets import JSONF


class FeaturesView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.SiteFeatureSerializer

    def get_queryset(self):
        return models.SiteFeature.objects\
            .prefetch_related('feature', 'feature__route') \
            .by_country_code(self.request.country_code) \
            .by_sources([PlatformMixin.ALL, PlatformMixin.MOBILE])


class SiteSettingsMobileView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = models.SiteSettings.objects.filter(country__is_active__exact=True)
    serializer_class = SiteSettingsMobileSerializer

    def get_queryset(self):
        # Sort by translated country name
        locale = self.request.locale
        field_name = f'country__name__{locale}'
        qs = super().get_queryset().annotate(country_translated_name=JSONF(field_name)) \
            .order_by('country_translated_name')

        return qs

    def get(self, request):
        site_settings = self.get_serializer({'site_settings': self.get_queryset()})
        return Response(site_settings.data)
