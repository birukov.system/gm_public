# Generated by Django 2.2.4 on 2019-08-20 12:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_resetpasswordtoken'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='email_confirmed',
            field=models.BooleanField(default=False, verbose_name='email status'),
        ),
    ]
