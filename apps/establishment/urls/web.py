"""Establishment app web urlconf."""
from establishment.urls.common import urlpatterns as common_urlpatterns
from django.urls import path
from establishment import views


urlpatterns = [
    path('slug/<slug:slug>/', views.EstablishmentRetrieveView.as_view(), name='web-detail'),
]

urlpatterns.extend(common_urlpatterns)
