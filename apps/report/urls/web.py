"""Web URL patterns for application report."""
from django.urls import path

from report.urls.common import urlpatterns as common_urlpatterns
from report.views import web as views

app_name = 'report'

urlpatterns = common_urlpatterns + [
    path('', views.ReportListCreateView.as_view(), name='report-list-create'),
]
