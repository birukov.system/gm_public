from celery import shared_task
from rating.models import Rating
from django.contrib.contenttypes.models import ContentType


def add_rating(remote_addr, pk, model, app_label):
    add.apply_async(
        (remote_addr, pk, model, app_label), countdown=60 * 60
    )


@shared_task
def add(remote_addr, pk, model, app_label):
    content_type = ContentType.objects.get(app_label=app_label, model=model)
    Rating.objects.get_or_create(
            ip=remote_addr, object_id=pk, content_type=content_type)


