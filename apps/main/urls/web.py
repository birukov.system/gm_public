from django.urls import path

from main.urls.common import common_urlpatterns
from main.views.web import DetermineSiteView, SiteListView, SiteSettingsView

urlpatterns = [
    path('determine-site/', DetermineSiteView.as_view(), name='determine-site'),
    path('sites/', SiteListView.as_view(), name='site-list'),
    path('site-settings/<subdomain>/', SiteSettingsView.as_view(), name='site-settings'),
]

urlpatterns.extend(common_urlpatterns)
