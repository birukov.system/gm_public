from rest_framework import generics
from rest_framework import permissions

from partner import models
from partner.serializers import common as serializers


# Mixins
class PartnerViewMixin(generics.GenericAPIView):
    """View mixin for Partner views"""
    queryset = models.PartnerToEstablishment.objects.with_base_related()


# Views
class PartnerListView(PartnerViewMixin, generics.ListAPIView):
    """List Partner view"""
    pagination_class = None
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.PartnersForEstablishmentWebSerializer
