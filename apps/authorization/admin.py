from django.contrib import admin
from oauth2_provider import models as oauth2_models
from rest_framework.authtoken.models import Token
from rest_framework_simplejwt.token_blacklist import models as jwt_models
from social_django import models as social_models

from authorization import models


# Unregister unused models
admin.site.unregister(jwt_models.OutstandingToken)
admin.site.unregister(jwt_models.BlacklistedToken)
admin.site.unregister(oauth2_models.AccessToken)
admin.site.unregister(oauth2_models.RefreshToken)
admin.site.unregister(oauth2_models.Grant)
admin.site.unregister(social_models.Association)
admin.site.unregister(social_models.Nonce)
admin.site.unregister(Token)
