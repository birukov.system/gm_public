# Generated by Django 2.2.4 on 2019-09-27 08:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0014_auto_20190927_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='News slug'),
        ),
    ]
