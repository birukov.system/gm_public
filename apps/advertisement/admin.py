"""Admin models for app advertisement"""
from django.contrib import admin

from advertisement import models
from main.models import Page


class PageInline(admin.TabularInline):
    model = Page
    extra = 0


@admin.register(models.Advertisement)
class AdvertisementModelAdmin(admin.ModelAdmin):
    """Admin model for model Advertisement"""
    inlines = (PageInline, )
    list_display = ('id', '__str__', 'block_level',
                    'start', 'end',)
    list_filter = ('url', 'block_level', 'start', 'end',
                   'page__source')
    date_hierarchy = 'created'
