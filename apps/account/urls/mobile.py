"""Mobile account URLs"""
from django.urls import path

from account.urls import common as common_views
from account.views import web as views

app_name = 'account'

urlpatterns_api = [

]

urlpatterns = urlpatterns_api + \
              common_views.urlpatterns
