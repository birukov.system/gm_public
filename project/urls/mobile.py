from django.urls import path, include

app_name = 'mobile'

urlpatterns = [
    path('account/', include('account.urls.mobile')),
    path('booking/', include('booking.urls.web')),
    path('establishments/', include('establishment.urls.mobile')),
    path('products/', include('product.urls.mobile')),
    path('location/', include('location.urls.mobile')),
    path('main/', include('main.urls.mobile')),
    path('tags/', include('tag.urls.mobile')),
    path('timetables/', include('timetable.urls.mobile')),
    # path('account/', include('account.urls.web')),
    path('re_blocks/', include('advertisement.urls.mobile')),
    # path('collection/', include('collection.urls.mobile')),
    # path('establishments/', include('establishment.urls.web')),
    path('news/', include('news.urls.mobile')),
    path('auth/', include('authorization.urls.mobile')),
    # path('partner/', include('partner.urls.web')),
    path('translations/', include('translation.urls.mobile')),
]
