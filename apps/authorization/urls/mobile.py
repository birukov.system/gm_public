from django.urls import path

from authorization.views import mobile as views

app_name = 'auth'

urlpatterns = [
    path('signup/confirm/', views.MobileCodeConfirmationView.as_view(), name='mobile-code-confirmation'),
    path('signup/resend/', views.SignUpResendConfirmationEmailView.as_view(), name='signup-resend'),
]