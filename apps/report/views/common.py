"""Common views for application report."""
from rest_framework import generics

from report.filters import ReportFilterSet
from report.models import Report
from report.serializers import ReportBaseSerializer
from utils.methods import get_permission_classes
from utils.permissions import (
    IsEstablishmentManager, IsContentPageManager, IsReviewManager,
    IsModerator, IsRestaurantInspector, IsArtisanInspector,
    IsWineryWineInspector, IsDistilleryLiquorInspector, IsProducerFoodInspector,
    IsEstablishmentAdministrator
)


class ReportBaseView(generics.GenericAPIView):
    """
    ## Report base view.
    """
    queryset = Report.objects.all()
    serializer_class = ReportBaseSerializer
    filter_class = ReportFilterSet
    permission_classes = get_permission_classes(
        IsEstablishmentManager, IsContentPageManager, IsReviewManager,
        IsModerator, IsRestaurantInspector, IsArtisanInspector,
        IsWineryWineInspector, IsDistilleryLiquorInspector, IsProducerFoodInspector,
        IsEstablishmentAdministrator
    )

    @staticmethod
    def get_source():
        """Return a source for view."""
        return NotImplementedError('You must implement "get_source" method')


class ReportRetrieveView(ReportBaseView, generics.RetrieveAPIView):
    """
    ## View for retrieving serialized instance.
    ### Response
    Return serialized object.
    E.g.:
    ```
    {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### Description
    Method that allows retrieving serialized report object.
    """
