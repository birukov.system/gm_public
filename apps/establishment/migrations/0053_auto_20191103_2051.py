# Generated by Django 2.2.4 on 2019-11-03 20:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('establishment', '0052_plate_currency_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plate',
            name='currency',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.Currency', verbose_name='currency'),
        ),
    ]
