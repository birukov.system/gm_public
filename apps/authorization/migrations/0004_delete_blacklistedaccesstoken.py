# Generated by Django 2.2.4 on 2019-09-03 11:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authorization', '0003_jwtaccesstoken_jwtrefreshtoken'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BlacklistedAccessToken',
        ),
    ]
