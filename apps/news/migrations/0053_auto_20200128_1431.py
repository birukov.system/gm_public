# Generated by Django 2.2.7 on 2020-01-28 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0052_auto_20200121_0940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='state',
            field=models.PositiveSmallIntegerField(choices=[(0, 'remove'), (1, 'hidden'), (2, 'published'), (3, 'not published')], default=3, verbose_name='State'),
        ),
    ]
