"""Authorization app celery tasks."""
import logging

from celery import shared_task
from django.utils.translation import gettext_lazy as _

from account import models as account_models

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


@shared_task
def send_confirm_email(user_id: int, country_code: str, is_mobile: bool = False):
    """Send verification email to user."""
    try:
        obj = account_models.User.objects.get(id=user_id)
        obj.send_email(subject=_('Email confirmation'),
                       message=obj.confirm_email_template(country_code,
                                                          obj.username,
                                                          _('Email confirmation'),
                                                          is_mobile))
    except Exception as e:
        from celery import current_task
        if not current_task:
            logger.error(f'METHOD_NAME: {send_confirm_email.__name__}\n'
                         f'DETAIL: user {user_id},  - {e}')
        else:
            raise e
