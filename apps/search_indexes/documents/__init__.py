from search_indexes.documents.establishment import EstablishmentDocument
from search_indexes.documents.news import NewsDocument
from search_indexes.documents.product import ProductDocument
from search_indexes.documents.tag_category import TagCategoryDocument
from search_indexes.documents.user import UserDocument
from search_indexes.documents.translation import TranslationDocument
from search_indexes.documents.employee import EmployeeDocument
from search_indexes.tasks import es_update

# todo: make signal to update documents on related fields
__all__ = [
    'EstablishmentDocument',
    'NewsDocument',
    'ProductDocument',
    'TagCategoryDocument',
    'UserDocument',
    'TranslationDocument',
    'EmployeeDocument',
    'es_update',
]