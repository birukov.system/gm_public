# Generated by Django 2.2.4 on 2019-10-25 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0003_auto_20191025_1613'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='stripe_token',
            field=models.TextField(default=None, null=True, verbose_name='stripe service pre-payed booking token'),
        ),
    ]
