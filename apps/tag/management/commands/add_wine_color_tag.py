from django.core.management.base import BaseCommand

from transfer import models as transfer_models
from transfer.serializers.product import WineColorTagSerializer


class Command(BaseCommand):
    help = 'Fix wine color tag'

    def handle(self, *args, **kwarg):
        errors = []
        legacy_products = transfer_models.Products.objects.filter(wine_color__isnull=False)
        serialized_data = WineColorTagSerializer(
            data=list(legacy_products.values()),
            many=True)
        if serialized_data.is_valid():
            serialized_data.save()
        else:
            for d in serialized_data.errors: errors.append(d) if d else None

        self.stdout.write(self.style.WARNING(f'Error count: {len(errors)}\nErrors: {errors}'))
