"""Recipe app web urlconf."""
from recipe.urls.common import urlpatterns as common_urlpatterns


urlpatterns = []
urlpatterns.extend(common_urlpatterns)
