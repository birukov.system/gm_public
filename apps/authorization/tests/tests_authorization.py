from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from rest_framework.test import APITestCase
from rest_framework import status

from account.models import User
from utils.methods import string_random


def get_tokens_for_user(
        username='sedragurda', password='sedragurdaredips19', email='sedragurda@desoz.com'):

    user = User.objects.create_user(username=username, email=email, password=password)
    tokens = User.create_jwt_tokens(user)

    return {
        "username": username,
        "password": password,
        "email": email,
        "newsletter": True,
        "user": user,
        "tokens": tokens
    }


class AuthorizationTests(APITestCase):

    def setUp(self):
        data = get_tokens_for_user()
        self.username = data["username"]
        self.password = data["password"]

    def LoginTests(self):
        data = {
            'username_or_email': self.username,
            'password': self.password,
            'remember': True
        }
        response = self.client.post(reverse('auth:authorization:login'), data=data)
        self.assertEqual(response.data['access_token'], self.tokens.get('access_token'))
        self.assertEqual(response.data['refresh_token'], self.tokens.get('refresh_token'))


class SignupTests(APITestCase):
    def setUp(self):
        username = string_random()

        self.data = {
            'username': username,
            'password': 'stringpass1',
            'email': f'{username}@gmail.com'
        }

        # Check if no such user before test
        user = User.objects.filter(email__iexact=self.data['email']).first()
        self.assertEqual(user, None)

    def test_signup(self):
        # Check user creation
        response = self.client.post('/api/auth/signup/', data=self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Change username in request, but keep email the same
        self.data['username'] = string_random()

        # Check creation of user with same email (unconfirmed email)
        response = self.client.post('/api/auth/signup/', data=self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check creation of user with same email (confirmed email)
        user = User.objects.filter(unconfirmed_email__iexact=self.data['email']).first()
        user.confirm_email()
        user.save()

        response = self.client.post('/api/auth/signup/', data=self.data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, [_('User is already registered.')])

    def test_resend_confirm(self):
        # Create user
        response = self.client.post('/api/auth/signup/', data=self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Если почта еще не подтверждена: 200 ( сервер отправляет письмо с новым кодом подтверждения)
        user = User.objects.filter(unconfirmed_email__iexact=self.data['email']).first()

        response = self.client.post('/api/auth/signup/resend/', data=self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Если email не найден или почта уже подтвеждена: 401 User not found
        self.assertNotEqual(user, None)
        user.confirm_email()
        user.save()

        response = self.client.post('/api/auth/signup/resend/', data=self.data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
