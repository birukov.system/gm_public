from django.core.management.base import BaseCommand

from transfer.models import Assemblages
from transfer.serializers.product import AssemblageProductTagSerializer


class Command(BaseCommand):
    help = 'Add assemblage tag to product'

    def handle(self, *args, **kwarg):
        errors = []
        legacy_products = Assemblages.objects.filter(product_id__isnull=False)
        serialized_data = AssemblageProductTagSerializer(
            data=list(legacy_products.values()),
            many=True)
        if serialized_data.is_valid():
            serialized_data.save()
        else:
            for d in serialized_data.errors: errors.append(d) if d else None

        self.stdout.write(self.style.WARNING(f'Error count: {len(errors)}\nErrors: {errors}'))
