from django.contrib import admin
from solo.admin import SingletonModelAdmin
from configuration.models import TranslationSettings


@admin.register(TranslationSettings)
class TranslationAdmin(SingletonModelAdmin):
    """Translation admin"""
