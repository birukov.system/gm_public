"""Location app views."""
from rest_framework import generics
from rest_framework import permissions
from django.db.models.expressions import RawSQL
from location import models, serializers
from utils.models import get_current_locale


# Mixins
class CountryViewMixin(generics.GenericAPIView):
    """View Mixin for model Country"""

    serializer_class = serializers.CountrySerializer
    permission_classes = (permissions.AllowAny,)
    queryset = models.Country.objects.active().annotate_has_sub_region()


class RegionViewMixin(generics.GenericAPIView):
    """View Mixin for model Region"""
    model = models.Region
    queryset = models.Region.objects.all().order_by('name', 'code')


class CityViewMixin(generics.GenericAPIView):
    """View Mixin for model City"""
    model = models.City
    queryset = models.City.objects.with_base_related()


class CityViewMixinWith(generics.GenericAPIView):
    """View Mixin for model City"""
    model = models.City
    queryset = models.City.objects.with_base_related()


class AddressViewMixin(generics.GenericAPIView):
    """View Mixin for model Address"""
    model = models.Address
    queryset = models.Address.objects.all()


# Country
class CountryListView(CountryViewMixin, generics.ListAPIView):
    """List view for model Country."""

    pagination_class = None

    def get_queryset(self):
        return super().get_queryset()


class CountryRetrieveView(CountryViewMixin, generics.RetrieveAPIView):
    """Retrieve view for model Country."""


# Region
class RegionCreateView(RegionViewMixin, generics.CreateAPIView):
    """Create view for model Region"""
    serializer_class = serializers.RegionSerializer


class RegionRetrieveView(RegionViewMixin, generics.RetrieveAPIView):
    """Retrieve view for model Region"""
    serializer_class = serializers.RegionSerializer


class RegionListView(RegionViewMixin, generics.ListAPIView):
    """List view for model Country"""
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.CountrySerializer


class WineRegionListView(generics.ListAPIView):
    """List view for model WineRegion"""
    pagination_class = None
    model = models.WineRegion
    permission_classes = (permissions.AllowAny,)
    queryset = models.WineRegion.objects.with_sub_region_related().having_wines()
    serializer_class = serializers.WineRegionSerializer


class RegionDestroyView(RegionViewMixin, generics.DestroyAPIView):
    """Destroy view for model Country"""
    serializer_class = serializers.CountrySerializer


class RegionUpdateView(RegionViewMixin, generics.UpdateAPIView):
    """Update view for model Country"""
    serializer_class = serializers.CountrySerializer


# City
class CityCreateView(CityViewMixin, generics.CreateAPIView):
    """Create view for model City"""
    serializer_class = serializers.CityBaseSerializer


class CityRetrieveView(CityViewMixin, generics.RetrieveAPIView):
    """Retrieve view for model City"""
    serializer_class = serializers.CityDetailSerializer


class CityListView(CityViewMixin, generics.ListAPIView):
    """List view for model City"""
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.CityBaseSerializer

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.country_code:
            qs = qs.by_country_code(self.request.country_code).with_base_related()
        return qs


class CityDestroyView(CityViewMixin, generics.DestroyAPIView):
    """Destroy view for model City"""
    serializer_class = serializers.CityBaseSerializer


class CityUpdateView(CityViewMixin, generics.UpdateAPIView):
    """Update view for model City"""
    serializer_class = serializers.CityBaseSerializer


# Address
class AddressCreateView(AddressViewMixin, generics.CreateAPIView):
    """Create view for model Address"""
    serializer_class = serializers.AddressDetailSerializer


class AddressRetrieveView(AddressViewMixin, generics.RetrieveAPIView):
    """Retrieve view for model Address"""
    serializer_class = serializers.AddressDetailSerializer


class AddressListView(AddressViewMixin, generics.ListAPIView):
    """List view for model Address"""
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.AddressDetailSerializer
