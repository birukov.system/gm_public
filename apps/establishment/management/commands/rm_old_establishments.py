from django.core.management.base import BaseCommand

from establishment.models import Establishment


class Command(BaseCommand):
    help = 'Remove old establishments from new bd'

    def handle(self, *args, **kwargs):
        old_establishments = Establishment.objects.exclude(old_id__isnull=True)
        count = old_establishments.count()
        old_establishments.delete()
        self.stdout.write(self.style.WARNING(f'Deleted {count} objects.'))
