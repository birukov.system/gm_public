import re

from django.core.management.base import BaseCommand
from tqdm import tqdm

from establishment.models import Establishment
from transfer.models import Establishments


class Command(BaseCommand):
    help = 'Fix establishment transliterated name'

    @staticmethod
    def get_transliterated_name(country, name: str,) -> str:
        available_country_codes = {'ru', 'jp'}
        if name and country.code and country.code.lower() in available_country_codes:
            pattern = r'\((.*?)\)'
            result = re.findall(pattern, name)
            if result:
                return result[0].rstrip()

    @staticmethod
    def get_name(name: str) -> str:
        if name:
            pattern = r'\([^\}]*\)'
            return re.sub(pattern, '', name).rstrip()

    def handle(self, *args, **kwargs):
        to_update = []
        establishments = Establishment.objects.filter(
            old_id__in=set(Establishments.objects.values_list('id', flat=True))
        )
        for establishment in tqdm(establishments):
            country = (
                getattr(getattr(getattr(establishment, 'address', None),
                                'city', None),
                        'country', None)
            )
            if country:
                is_changed = False  # default condition
                old_establishment = Establishments.objects.get(id=establishment.old_id)
                new_name = self.get_name(old_establishment.name)
                new_transliterated_name = self.get_transliterated_name(
                    country=country,
                    name=old_establishment.name
                )

                if establishment.transliterated_name != new_transliterated_name:
                    establishment.transliterated_name = new_transliterated_name
                    is_changed = True

                if establishment.name != new_name:
                    establishment.name = new_name
                    is_changed = True

                if is_changed:
                    to_update.append(establishment)

        Establishment.objects.bulk_update(to_update, ['transliterated_name', 'name', ])
        self.stdout.write(self.style.WARNING(f'Updated {len(to_update)} objects.\n'))
