# Generated by Django 2.2.7 on 2019-12-27 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0006_auto_20191227_1216'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscriber',
            name='state',
        ),
        migrations.AddField(
            model_name='subscribe',
            name='state',
            field=models.PositiveIntegerField(choices=[(0, 'Unusable'), (1, 'Usable')], default=1, verbose_name='State'),
        ),
    ]
