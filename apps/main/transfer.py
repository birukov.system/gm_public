"""

Структура записи в card:
Название таблицы в postgresql: {
    "data_type": "тип данных в таблице (словарь, объект, дочерний объект и так далее)",
    "dependencies": кортеж с зависимостями от других таблиц в postgresql,
    "fields": список полей для таблицы postgresql, пример:
    {
        "название legacy таблицы": {
            список полей для переноса, пример структуры описан далее
        },
        "relations": список зависимостей legacy-таблицы, пример:
        {
            # имеет внешний ключ на "название legacy таблицы" из  "fields"
            "название legacy таблицы": {
                "key": ключ для связи. Строка, если тип поля в legacy таблице - ForeignKey, или кортеж из названия поля
                в дочерней таблице и названия поля в родительской таблице в ином случае
                "fields": {
                    список полей для переноса, пример структуры описан далее
                }
            }
        }
    },
    "relations": список внешних ключей таблицы postgresql, пример структуры описан далее
    {
        "Cities": [(
            (None, "region_code"),
            ("Region", "region", "code", "CharField")),
            ((None, "country_code_2"),
             ("Country", "country", "code", "CharField"))
        ]
    }
},


Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql


"""

card = {
    "SiteSettings": {
        "data_type": "objects",
        "dependencies": ("Country",),
        "fields": {
            "Sites": {
                "subdomain": "country_code_2",
                "pinterest_page_url": "pinterest_page_url",
                "twitter_page_url": "twitter_page_url",
                "facebook_page_url": "facebook_page_url",
                "instagram_page_url": "instagram_page_url",
                "contact_email": "contact_email",
                "config": ("config", "django.db.models.JSONField")
                # TODO в качесте ключа использовать  country_code_2 из legacy - ?
            },
            # "relations": {
            #     # Как работать c отношение OneToOneField
            # }
        }

    },
    "Feature": {
        "data_type": "objects",
        "dependencies": ("SiteSettings",),
        "fields": {
            "Features": {
                "slug": "slug"
            }
        },
        # поле "site_settings" ManyToManyField  имеет through='SiteFeature' в postgres
        # "relations": { # как работать с ManyToManyField
        #
        # }
    },
    "SiteFeature": {
        "data_type": "objects",
        "dependencies": ("SiteSettings", "Feature"),
        "fields": {
            "SiteFeatures": {
                "published": ("state", "django.db.models.BooleanField")
            }
        },
        "relations": {
            "Sites": [(
                ("site", None),
                ("SiteSettings", "site_settings", None, None))
            ],
            "Features": [(
                ("feature", None),
                ("Feature", "feature", None, None))
            ]
        }
    },
    "AwardType": {
        "data_type": "objects",
        "dependencies": ("Country",),
        "fields": {
            "AwardTypes": {
                "name": "title"
            }
        },
        #  вопрос с ForeignKey на Country
        # "relations": {
        #
        # }
    },
    "Award": {
        "data_type": "objects",
        "dependencies": ("AwardType", "ContentType"),
        "fields": {
            "Awards": {
                "title": ("title", "django.db.models.JSONField"),
                "vintage_year": "year",
            }
        },
        # TODO вопрос с content_type
        "relations": {
            "AwardTypes": [(
                ("award_type", None),
                ("AwardType", "award_type", None, None))
            ]
        }
    }
}

used_apps = ("locations",)
