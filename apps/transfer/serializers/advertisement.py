from rest_framework import serializers
from main.models import SiteSettings

from transfer.models import Sites, Ads
from advertisement.models import Advertisement
from main.models import Page
import requests
from rest_framework import status

from translation.models import Language


class AdvertisementSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    href = serializers.CharField()
    site_id = serializers.PrimaryKeyRelatedField(queryset=Sites.objects.all())

    class Meta:
        """Meta class."""
        model = Advertisement
        fields = [
            'id',
            'href',
            'site_id',
        ]

    def validate(self, data):
        site_instance = data.pop('site_id')
        data.update({
            'old_id': data.pop('id'),
            'url': data.pop('href'),
            'status': Advertisement.STATUS_ON,
            'block_level': Advertisement.TOP,
            'target_language': self.get_target_language(site_instance)
        })
        return data

    def get_target_language(self, site_instance):
        old_country_code = site_instance.country_code_2
        site_settings = SiteSettings.objects.filter(subdomain=old_country_code).first()
        country_code = site_settings.country.code
        language = Language.objects.filter(locale__istartswith=country_code).first()
        return language


class AdvertisementImageSerializer(AdvertisementSerializer):
    id = serializers.IntegerField()
    attachment_suffix_url = serializers.CharField()

    class Meta(AdvertisementSerializer.Meta):
        model = Page
        fields = [
            'id',
            'attachment_suffix_url',
        ]

    def validate(self, data):
        data.update({
            'image_url': self.get_absolute_image_url(data.pop('attachment_suffix_url')),
            'advertisement': self.get_advertisement(data.pop('id')),
            'source': Page.WEB,
            'page_type': Page.HOMEPAGE_TYPE,
            'width': None,
            'height': None,
        })
        return data

    def get_advertisement(self, old_id):
        return Advertisement.objects.filter(old_id=old_id).first()

    def get_absolute_image_url(self, relative_path):
        if relative_path:
            absolute_image_url = f'https://1dc3f33f6d-3.optimicdn.com/gaultmillau.com/' \
                                 f'{relative_path}'
            response = requests.head(absolute_image_url)
            if response.status_code == status.HTTP_200_OK:
                return absolute_image_url

    def create(self, validated_data):
        advertisement = validated_data.get('advertisement')
        image_url = validated_data.get('image_url')

        if image_url is None:
            advertisement.delete()
            return

        return super().create(validated_data)
