from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters

from location import models


class CityBackFilter(filters.FilterSet):
    """Employee filter set."""

    search = filters.CharFilter(method='search_by_name')
    country_code = filters.CharFilter(method='filter_by_country_code')

    class Meta:
        """Meta class."""

        model = models.City
        fields = (
            'search',
            'country_code',
        )

    def filter_by_country_code(self, queryset, name, value):
        """Filter by country code."""
        if value not in EMPTY_VALUES:
            return queryset.by_country_code(value)
        return queryset

    def search_by_name(self, queryset, name, value):
        """Search by name or last name."""
        if value not in EMPTY_VALUES:
            return queryset.search_by_name(value)
        return queryset


class RegionFilter(filters.FilterSet):
    """Region filter set."""

    country_id = filters.CharFilter()
    sub_regions_by_region_id = filters.CharFilter(method='by_region')
    without_parent_region = filters.BooleanFilter(method='by_parent_region')

    class Meta:
        """Meta class."""
        model = models.Region
        fields = (
            'country_id',
            'sub_regions_by_region_id',
            'without_parent_region',
        )

    def by_region(self, queryset, name, value):
        """Search regions by sub region id."""
        if value not in EMPTY_VALUES:
            return queryset.sub_regions_by_region_id(value)

    def by_parent_region(self, queryset, name, value):
        """
        Search if region instance has a parent region..
        If True then show only Regions
        Otherwise show only Sub regions.
        """
        if value not in EMPTY_VALUES:
            return queryset.without_parent_region(value)


class WineSubRegionFilterSet(filters.FilterSet):
    """Wine sub region FilterSet."""

    wine_region_name = filters.CharFilter(field_name='wine_region__name')

    class Meta:
        """Meta class."""
        model = models.WineSubRegion
        fields = (
            'wine_region_name',
        )
