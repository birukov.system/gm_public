"""Advertisement common urlpaths."""
from django.urls import path
from advertisement.views import common as views

app_name = 'advertisements'

common_urlpatterns = [
    path('filters/available_devices/', views.AdvertisementAvailableDevicesList.as_view(),
         name='available-ads-devices-list'),
    path('filters/available_page_types/', views.AdvertisementAvailablePageTypesList.as_view(),
         name='available-ads-page-types-list'),
]
