# Generated by Django 2.2.7 on 2020-02-20 13:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('establishment', '0104_establishment_update_by'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='establishment',
            name='schedule',
        ),
    ]
