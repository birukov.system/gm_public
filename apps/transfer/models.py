# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
import yaml
from django.contrib.gis.db import models

from transfer.mixins import MigrateMixin


def convert_entry(loader, node):
    return {e[0]: e[1] for e in loader.construct_pairs(node)}


yaml.add_constructor('!ruby/hash:ActiveSupport::HashWithIndifferentAccess', convert_entry)


# models.ForeignKey(ForeignModel, models.DO_NOTHING, blank=True, null=True)

class Sites(MigrateMixin):
    using = 'legacy'

    country_code_2 = models.CharField(max_length=255, blank=True, null=True)
    pinterest_page_url = models.CharField(max_length=255, blank=True, null=True)
    twitter_page_url = models.CharField(max_length=255, blank=True, null=True)
    facebook_page_url = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    contact_email = models.CharField(max_length=255, blank=True, null=True)
    config = models.CharField(max_length=3000, blank=True, null=True)
    released = models.IntegerField(blank=True, null=True)
    instagram_page_url = models.CharField(max_length=255, blank=True, null=True)
    ad_config = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sites'


class Features(MigrateMixin):
    using = 'legacy'

    slug = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'features'


class SiteFeatures(MigrateMixin):
    using = 'legacy'

    site = models.ForeignKey('Sites', models.DO_NOTHING, blank=True, null=True)
    feature = models.ForeignKey(Features, models.DO_NOTHING, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'site_features'


class AwardTypes(MigrateMixin):
    using = 'legacy'

    site = models.ForeignKey('Sites', models.DO_NOTHING, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    region = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'award_types'


class Awards(MigrateMixin):
    using = 'legacy'

    profile_id = models.IntegerField(blank=True, null=True)
    award_type = models.ForeignKey(AwardTypes, models.DO_NOTHING, blank=True, null=True)
    award = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    region_code = models.CharField(max_length=255, blank=True, null=True)
    year = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'awards'


class Ezuser(MigrateMixin):
    using = 'legacy'

    contentobject_id = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=150)
    login = models.CharField(max_length=150)
    password_hash = models.CharField(max_length=50, blank=True, null=True)
    password_hash_type = models.IntegerField()
    facebook_id = models.BigIntegerField()
    # TODO: в legacy нету таблицы 'CadLevel'

    # level = models.ForeignKey('CadLevel', models.DO_NOTHING)
    points = models.IntegerField()
    publish_fb_activity = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'ezuser'


class Accounts(MigrateMixin):
    using = 'legacy'

    uuid = models.CharField(max_length=24)
    nickname = models.CharField(max_length=128, blank=True, null=True)
    locale = models.CharField(max_length=5, blank=True, null=True)
    country_code = models.CharField(max_length=3, blank=True, null=True)
    city = models.CharField(max_length=32, blank=True, null=True)
    role = models.CharField(max_length=16, blank=True, null=True)
    consent_purpose = models.CharField(max_length=255, blank=True, null=True)
    consent_at = models.DateTimeField(blank=True, null=True)
    last_seen_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    email = models.CharField(unique=True, max_length=255)
    is_admin = models.IntegerField(blank=True, null=True)
    ezuser_id = models.IntegerField(blank=True, null=True)
    ez_user_id = models.IntegerField(blank=True, null=True)
    encrypted_password = models.CharField(max_length=255)
    reset_password_token = models.CharField(unique=True, max_length=255, blank=True, null=True)
    reset_password_sent_at = models.DateTimeField(blank=True, null=True)
    remember_created_at = models.DateTimeField(blank=True, null=True)
    sign_in_count = models.IntegerField()
    current_sign_in_at = models.DateTimeField(blank=True, null=True)
    last_sign_in_at = models.DateTimeField(blank=True, null=True)
    current_sign_in_ip = models.CharField(max_length=255, blank=True, null=True)
    last_sign_in_ip = models.CharField(max_length=255, blank=True, null=True)
    confirmation_token = models.CharField(max_length=255, blank=True, null=True)
    confirmed_at = models.DateTimeField(blank=True, null=True)
    confirmation_sent_at = models.DateTimeField(blank=True, null=True)
    unconfirmed_email = models.CharField(max_length=255, blank=True, null=True)
    webpush_subscription = models.CharField(max_length=5000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accounts'


class Profiles(MigrateMixin):
    using = 'legacy'

    firstname = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    gender = models.CharField(max_length=255, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    phone_country_code = models.TextField(blank=True, null=True)
    phone_local_number = models.TextField(blank=True, null=True)
    site_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    unique_key = models.CharField(max_length=255, blank=True, null=True)
    account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    requester_id = models.IntegerField(blank=True, null=True)
    available_for_events = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'profiles'


class ProfilePictures(MigrateMixin):
    using = 'legacy'

    profile_id = models.IntegerField(blank=True, null=True)
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.CharField(max_length=255)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'profile_pictures'


class Affiliations(MigrateMixin):
    using = 'legacy'

    establishment_id = models.IntegerField(blank=True, null=True)
    profile_id = models.IntegerField(blank=True, null=True)
    role = models.CharField(max_length=255, blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=255, blank=True, null=True)
    requester_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'affiliations'


class Cities(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255, blank=True, null=True)
    country_code = models.CharField(max_length=3, blank=True, null=True)
    country_code_2 = models.CharField(max_length=2, blank=True, null=True)
    region_code = models.CharField(max_length=255, blank=True, null=True)
    subregion_code = models.CharField(max_length=255, blank=True, null=True)
    is_island = models.IntegerField(blank=True, null=True)
    zip_code = models.CharField(max_length=9, blank=True, null=True)
    situation = models.CharField(max_length=255, blank=True, null=True)
    map_ref = models.CharField(max_length=255, blank=True, null=True)
    map1 = models.CharField(max_length=255, blank=True, null=True)
    map2 = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    encima_id = models.IntegerField(blank=True, null=True)
    related_city_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    index_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cities'
        unique_together = (('name', 'region_code', 'country_code'),)


class CityNames(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=100, blank=True, null=True)
    locale = models.CharField(max_length=5, blank=True, null=True)
    city = models.ForeignKey(Cities, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'city_names'
        unique_together = (('city', 'name', 'locale'),)


class CityPhotos(MigrateMixin):
    using = 'legacy'

    city = models.ForeignKey(Cities, models.DO_NOTHING, blank=True, null=True)
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_suffix_url = models.CharField(max_length=255)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'city_photos'


class Locations(MigrateMixin):
    using = 'legacy'

    country_code = models.CharField(max_length=3)
    region_code = models.CharField(max_length=3, blank=True, null=True)
    subregion_code = models.CharField(max_length=3, blank=True, null=True)
    zip_code = models.CharField(max_length=9, blank=True, null=True)
    district_name = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    timezone = models.CharField(max_length=32, blank=True, null=True)
    transportation = models.CharField(max_length=255, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    city = models.ForeignKey(Cities, models.DO_NOTHING, blank=True, null=True)
    map_ref = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'locations'


class Collections(MigrateMixin):
    using = 'legacy'

    title = models.CharField(max_length=255, blank=True, null=True)
    tag_name = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(max_length=255, blank=True, null=True)
    site_id = models.IntegerField(blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    aasm_state = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'collections'


# class CollectionEvents(MigrateMixin):
#     using = 'legacy'
#
#     account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
#     account_collection = models.ForeignKey(AccountCollections, models.DO_NOTHING, blank=True, null=True)
#     possible_dates = models.CharField(max_length=255, blank=True, null=True)
#     final_date = models.DateTimeField(blank=True, null=True)
#     establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
#     created_at = models.DateTimeField()
#     updated_at = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'collection_events'


# class CollectionEventAvailabilities(MigrateMixin):
#     using = 'legacy'
# TODO: collection_event - внешний ключ к CollectionEvents, которая имеет внешний ключ к Accounts

#     collection_event = models.ForeignKey('CollectionEvents', models.DO_NOTHING, blank=True, null=True)
#     establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
#     email_address = models.ForeignKey('EmailAddresses', models.DO_NOTHING, blank=True, null=True)
#     date = models.DateTimeField(blank=True, null=True)
#     state = models.CharField(max_length=255, blank=True, null=True)
#     created_at = models.DateTimeField()
#     updated_at = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'collection_event_availabilities'


class Guides(MigrateMixin):
    using = 'legacy'

    title = models.CharField(max_length=255, blank=True, null=True)
    vintage = models.IntegerField(blank=True, null=True)
    slug = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    site_id = models.IntegerField(blank=True, null=True)
    inserter_field = models.CharField(max_length=255, blank=True, null=True)
    items_count = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guides'


class GuideAds(MigrateMixin):
    using = 'legacy'

    nb_pages = models.IntegerField(blank=True, null=True)
    nb_right_pages = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    guide_ad_node = models.ForeignKey('GuideElements', on_delete=models.DO_NOTHING, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guide_ads'


class GuideFilters(MigrateMixin):
    using = 'legacy'

    year = models.TextField(blank=True, null=True)
    establishment_type = models.CharField(max_length=255, blank=True, null=True)
    countries = models.TextField(blank=True, null=True)
    regions = models.TextField(blank=True, null=True)
    subregions = models.TextField(blank=True, null=True)
    wine_regions = models.TextField(blank=True, null=True)
    wine_classifications = models.TextField(blank=True, null=True)
    wine_colors = models.TextField(blank=True, null=True)
    wine_types = models.TextField(blank=True, null=True)
    max_mark = models.FloatField(blank=True, null=True)
    min_mark = models.FloatField(blank=True, null=True)
    marks_only = models.IntegerField(blank=True, null=True)
    locales = models.CharField(max_length=255, blank=True, null=True)
    states = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    guide = models.ForeignKey(Guides, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'guide_filters'


class GuideSections(MigrateMixin):
    using = 'legacy'

    type = models.CharField(max_length=255)
    key_name = models.CharField(max_length=255, blank=True, null=True)
    value_name = models.CharField(max_length=255, blank=True, null=True)
    right = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'guide_sections'


class GuideElements(MigrateMixin):
    using = 'legacy'

    type = models.CharField(max_length=255)
    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
    review = models.ForeignKey('Reviews', models.DO_NOTHING, blank=True, null=True)
    review_text = models.ForeignKey('ReviewTexts', models.DO_NOTHING, blank=True, null=True)
    wine_region = models.ForeignKey('WineLocations', models.DO_NOTHING, blank=True, null=True)
    wine = models.ForeignKey('Products', models.DO_NOTHING, blank=True, null=True)
    color = models.CharField(max_length=255, blank=True, null=True)
    order_number = models.IntegerField(blank=True, null=True)
    guide_ad = models.ForeignKey(GuideAds, models.DO_NOTHING, blank=True, null=True)
    city = models.ForeignKey(Cities, models.DO_NOTHING, blank=True, null=True)
    section = models.ForeignKey('GuideSections', models.DO_NOTHING, blank=True, null=True)
    guide = models.ForeignKey('Guides', models.DO_NOTHING, blank=True, null=True)
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    lft = models.IntegerField()
    rgt = models.IntegerField()
    depth = models.IntegerField()
    children_count = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'guide_elements'


class Establishments(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)
    index_name = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    phone_country_code = models.TextField(blank=True, null=True)
    phone_local_number = models.TextField(blank=True, null=True)
    fax = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    location = models.ForeignKey('Locations', models.DO_NOTHING, blank=True, null=True)
    unique_key = models.CharField(max_length=255, blank=True, null=True)
    filemaker_id = models.IntegerField(unique=True, blank=True, null=True)
    aut_mysql_id = models.IntegerField(unique=True, blank=True, null=True)
    fra_encima_id = models.IntegerField(blank=True, null=True)
    ca_import_id = models.IntegerField(blank=True, null=True)
    ch_import_id = models.IntegerField(blank=True, null=True)
    be_ezpublish_id = models.IntegerField(blank=True, null=True)
    au_import_id = models.IntegerField(blank=True, null=True)
    lux_import_id = models.IntegerField(blank=True, null=True)
    hun_import_id = models.IntegerField(blank=True, null=True)
    deu_import_id = models.IntegerField(blank=True, null=True)
    win_import_id = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    state = models.CharField(max_length=255, blank=True, null=True)
    manager_revised_at = models.DateTimeField(blank=True, null=True)
    cover_id = models.IntegerField(blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)
    admin_updated_at = models.DateTimeField(blank=True, null=True)
    admin_updated_by = models.IntegerField(blank=True, null=True)
    company_id = models.IntegerField(blank=True, null=True)
    production_type = models.CharField(max_length=3000, blank=True, null=True)
    metal_plate_name = models.CharField(max_length=255, blank=True, null=True)
    metal_plate_name_validated_at = models.DateTimeField(blank=True, null=True)
    metal_plate_name_validated_by = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'establishments'


class EstablishmentNotes(MigrateMixin):
    using = 'legacy'

    establishment_id = models.IntegerField(null=True, blank=True)
    account_id = models.IntegerField(null=True, blank=True)
    text = models.TextField(null=True)

    class Meta:
        managed = False
        db_table = 'notes'


class Descriptions(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
    locale = models.CharField(max_length=5, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'descriptions'


class Dishes(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    currency = models.CharField(max_length=255, blank=True, null=True)
    dish_type = models.CharField(max_length=255, blank=True, null=True)
    signature = models.IntegerField(blank=True, null=True)
    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'dishes'


class EstablishmentAssets(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey('Establishments', models.DO_NOTHING)
    account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
    menu_id = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=64)
    scope = models.CharField(max_length=32)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'establishment_assets'


class EstablishmentBacklinks(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
    partnership_name = models.CharField(max_length=255, blank=True, null=True)
    partnership_icon = models.CharField(max_length=255, blank=True, null=True)
    backlink_url = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    type = models.CharField(max_length=255, blank=True, null=True)
    starting_date = models.DateField(blank=True, null=True)
    expiry_date = models.DateField(blank=True, null=True)
    price_per_month = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'establishment_backlinks'


# class EstablishmentCollections(MigrateMixin):
#     using = 'legacy'
#
#     establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
#     account_collection = models.ForeignKey(AccountCollections, models.DO_NOTHING, blank=True, null=True)
#     created_at = models.DateTimeField()
#     updated_at = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'establishment_collections'

class EstablishmentHolidays(MigrateMixin):
    using = 'legacy'

    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    establishment_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'establishment_holidays'


class EstablishmentInfos(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    website = models.CharField(max_length=255, blank=True, null=True)
    facebook = models.CharField(max_length=255, blank=True, null=True)
    twitter = models.CharField(max_length=255, blank=True, null=True)
    instagram = models.TextField(blank=True, null=True)
    lafourchette = models.CharField(max_length=255, blank=True, null=True)
    pub = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    booking_url = models.CharField(max_length=255, blank=True, null=True)
    nb_slot = models.IntegerField(blank=True, null=True)
    booking_enabled = models.IntegerField(blank=True, null=True)
    guestonline_id = models.IntegerField(blank=True, null=True)
    guestonline_auth_token = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'establishment_infos'


class EstablishmentMerchandises(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, blank=True, null=True)
    merchandise = models.ForeignKey('Merchandise', models.DO_NOTHING, blank=True, null=True)
    gifted = models.NullBooleanField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'establishment_merchandises'


class Menus(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey(Establishments, models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    timing = models.CharField(max_length=255, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    currency = models.CharField(max_length=255, blank=True, null=True)
    drinks = models.CharField(max_length=255, blank=True, null=True)
    served_on_offdays = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'menus'


class Schedules(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey(Establishments, models.DO_NOTHING, blank=True, null=True)
    continuous_service = models.CharField(max_length=255, blank=True, null=True)
    open_august = models.CharField(max_length=255, blank=True, null=True)
    lunch_start = models.TimeField(blank=True, null=True)
    lunch_end = models.TimeField(blank=True, null=True)
    diner_start = models.TimeField(blank=True, null=True)
    diner_end = models.TimeField(blank=True, null=True)
    opening_hours = models.CharField(max_length=255, blank=True, null=True)
    opening_dates = models.CharField(max_length=255, blank=True, null=True)
    timetable = models.CharField(max_length=10000, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'schedules'


class MercuryImages(MigrateMixin):
    using = 'legacy'

    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mercury_images'


class NewsOlds(MigrateMixin):
    using = 'legacy'

    title = models.CharField(max_length=255, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    slug = models.CharField(max_length=255, blank=True, null=True)
    template = models.CharField(max_length=255, blank=True, null=True)
    account = models.ForeignKey("Accounts", models.DO_NOTHING, blank=True, null=True)
    # site = models.ForeignKey('Sites', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    image_file_name = models.CharField(max_length=255, blank=True, null=True)
    image_content_type = models.CharField(max_length=255, blank=True, null=True)
    image_file_size = models.IntegerField(blank=True, null=True)
    image_updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'news_olds'


class EmailAddresses(MigrateMixin):
    using = 'legacy'

    account = models.ForeignKey("Accounts", models.DO_NOTHING, blank=True, null=True)
    partner_notification = models.IntegerField(blank=True, null=True)
    ip = models.CharField(max_length=255, blank=True, null=True)
    country_code = models.CharField(max_length=3, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    locale = models.CharField(max_length=5, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    email = models.CharField(max_length=255, blank=True, null=True)
    site_id = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'email_addresses'


class Reviews(MigrateMixin):
    using = 'legacy'

    vintage = models.PositiveIntegerField()
    mark = models.FloatField(blank=True, null=True)
    favorite = models.IntegerField(blank=True, null=True)
    account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True, related_name="account_reviews")
    # account_id = models.IntegerField(blank=True, null=True)
    establishment = models.ForeignKey(Establishments, models.DO_NOTHING, blank=True, null=True)
    visited_at = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField()
    published_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField()
    aasm_state = models.CharField(max_length=255, blank=True, null=True)
    reviewer = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    # TODO: модель Products в postgres закомментирована
    product = models.ForeignKey("Products", models.DO_NOTHING, blank=True, null=True)
    received_at = models.DateTimeField(blank=True, null=True)
    reviewer_name = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    locked = models.IntegerField(blank=True, null=True)
    temporary = models.IntegerField(blank=True, null=True)
    last_state_change_at = models.DateTimeField(blank=True, null=True)
    editor = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True, related_name="editor_reviews")

    class Meta:
        managed = False
        db_table = 'reviews'


class ReviewTexts(MigrateMixin):
    using = 'legacy'

    review = models.ForeignKey('Reviews', models.DO_NOTHING, blank=True, null=True)
    # review_id = models.IntegerField(blank=True, null=True)
    locale = models.CharField(max_length=5, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    updated_by = models.ForeignKey(Accounts, models.DO_NOTHING, db_column='updated_by', blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'review_texts'


class Comments(MigrateMixin):
    using = 'legacy'

    account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
    establishment = models.ForeignKey('Establishments', models.DO_NOTHING)
    parent_id = models.IntegerField(blank=True, null=True)
    main_parent_id = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    mark = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    locale = models.CharField(max_length=5)
    ip = models.CharField(max_length=40)
    state = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comments'


class Pages(MigrateMixin):
    using = 'legacy'

    root_title = models.CharField(max_length=255, blank=True, null=True)
    site = models.ForeignKey(Sites, models.DO_NOTHING, blank=True, null=True)
    account_id = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    template = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.TextField(blank=True, null=True)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    scheduled_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    published_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField()
    type = models.CharField(max_length=255, blank=True, null=True)
    is_main = models.IntegerField(blank=True, null=True)
    related_pages_id = models.TextField(blank=True, null=True)
    config = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages'


class PageTexts(MigrateMixin):
    using = 'legacy'

    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(max_length=255, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    locale = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    page = models.ForeignKey(Pages, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    summary = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'page_texts'


class PageCounters(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255, blank=True, null=True)
    count = models.IntegerField(blank=True, null=True)
    page = models.ForeignKey('Pages', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'page_counters'


class PageMetadata(MigrateMixin):
    using = 'legacy'

    key = models.CharField(max_length=255, blank=True, null=True)
    value = models.CharField(max_length=255, blank=True, null=True)
    page = models.ForeignKey('Pages', models.DO_NOTHING, blank=True, null=True, related_name='tags')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'page_metadata'


class Ads(MigrateMixin):
    using = 'legacy'

    site = models.ForeignKey('Sites', on_delete=models.DO_NOTHING)
    href = models.CharField(max_length=255, blank=True, null=True)
    start_at = models.DateTimeField(blank=True, null=True)
    expire_at = models.DateTimeField(blank=True, null=True)
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.TextField(blank=True, null=True)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'ads'


class KeyValueMetadata(MigrateMixin):
    using = 'legacy'

    key_name = models.CharField(max_length=255, blank=True, null=True)
    value_type = models.CharField(max_length=255, blank=True, null=True)
    value_list = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    public = models.IntegerField(blank=True, null=True)
    site_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'key_value_metadata'


class Metadata(MigrateMixin):
    using = 'legacy'

    key = models.CharField(max_length=255, blank=True, null=True)
    value = models.CharField(max_length=255, blank=True, null=True)
    establishment = models.ForeignKey('transfer.Establishments', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    key_value_metadatum = models.ForeignKey('transfer.KeyValueMetadata', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'metadata'


class KeyValueMetadatumEstablishments(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'key_value_metadatum_establishments'


class KeyValueMetadatumKeyValueMetadatumEstablishments(MigrateMixin):
    using = 'legacy'

    key_value_metadatum_id = models.IntegerField(blank=True, null=True)
    key_value_metadatum_establishment_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'key_value_metadatum_key_value_metadatum_establishments'


class WineColor(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)
    order_number = models.IntegerField(null=True, blank=True)

    class Meta:
        managed = False
        db_table = 'wine_colors'


class WineType(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wine_types'


class ProductClassification(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)
    desc = models.TextField()
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    type = models.CharField(max_length=255)
    parent_id = models.IntegerField()
    possible_type_id = models.IntegerField(null=True, blank=True)
    possible_color_id = models.IntegerField(null=True, blank=True)
    fra_encima_id = models.IntegerField(null=True, blank=True)

    class Meta:
        managed = False
        db_table = 'wine_classifications'


class Products(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey('Establishments', models.DO_NOTHING, null=True)
    brand = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=255, null=True)
    vintage = models.CharField(max_length=255, null=True)
    type = models.CharField(max_length=255, null=True)
    price = models.FloatField(null=True)
    average_price_in_shops = models.FloatField(null=True)
    wine_sub_region_id = models.IntegerField(null=True)
    classification = models.ForeignKey('ProductClassification', models.DO_NOTHING, null=True,
                                       related_name='product_classification')
    wine_region = models.ForeignKey('WineLocations', models.DO_NOTHING, null=True)
    wine_type = models.ForeignKey('WineType', models.DO_NOTHING, null=True)
    wine_color = models.ForeignKey('WineColor', models.DO_NOTHING, null=True)
    appellation = models.ForeignKey('ProductClassification', models.DO_NOTHING, null=True)
    state = models.CharField(max_length=255)
    village = models.ForeignKey('WineLocations', models.DO_NOTHING, null=True,
                                related_name='product_village')
    vineyard = models.ForeignKey('WineLocations', models.DO_NOTHING, null=True,
                                 related_name='product_vineyard')
    wine_quality = models.ForeignKey('ProductClassification', models.DO_NOTHING, null=True,
                                     related_name='product_wine_quality')
    bottles_produced = models.CharField(max_length=3000, null=True)
    unique_key = models.CharField(max_length=255, null=True)

    class Meta:
        managed = False
        db_table = 'products'


class ProductNotes(MigrateMixin):
    using = 'legacy'

    product_id = models.IntegerField(null=True, blank=True)
    text = models.CharField(max_length=255)
    win_import_id = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'product_notes'


class HomePages(MigrateMixin):
    using = 'legacy'

    site = models.ForeignKey(Sites, models.DO_NOTHING, blank=True, null=True)
    selection_of_week = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'home_pages'


class CarouselElements(MigrateMixin):
    using = 'legacy'

    title = models.CharField(max_length=255, blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    home_page = models.ForeignKey(HomePages, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.TextField(blank=True, null=True)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    link_title = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'carousel_elements'


class MetadatumAliases(MigrateMixin):
    """MetadatumAliases model."""
    using = 'legacy'

    meta_alias = models.CharField(max_length=255, blank=True, null=True)
    value = models.CharField(max_length=255, blank=True, null=True)
    locale = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'metadatum_aliases'


class Identities(MigrateMixin):
    using = 'legacy'

    account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
    provider = models.CharField(max_length=255, blank=True, null=True)
    uid = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'identities'


class WineLocations(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)
    desc = models.TextField(null=True)
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    type = models.CharField(max_length=255)
    parent_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wine_locations'


class Merchandise(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)
    vintage = models.CharField(max_length=255)
    highlighted = models.CharField(max_length=255)
    site = models.ForeignKey('Sites', models.DO_NOTHING)
    attachment_suffix_url = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'merchandises'


class Inquiries(MigrateMixin):
    using = 'legacy'

    visited_at = models.DateField()
    comment = models.TextField(blank=True, null=True)
    mark = models.FloatField(blank=True, null=True)
    review = models.ForeignKey(Reviews, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.TextField(blank=True, null=True)
    account = models.ForeignKey(Accounts, models.DO_NOTHING, blank=True, null=True)
    geometries = models.CharField(max_length=1024, blank=True, null=True)
    bill_file_name = models.CharField(max_length=255, blank=True, null=True)
    bill_content_type = models.CharField(max_length=255, blank=True, null=True)
    bill_file_size = models.IntegerField(blank=True, null=True)
    bill_updated_at = models.DateTimeField(blank=True, null=True)
    bill_suffix_url = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    moment = models.CharField(max_length=255, blank=True, null=True)
    published = models.PositiveSmallIntegerField(blank=True, null=True)
    menu_id = models.IntegerField(blank=True, null=True)
    final_comment = models.TextField(blank=True, null=True)
    decibels = models.CharField(max_length=255, blank=True, null=True)
    nomination = models.CharField(max_length=255, blank=True, null=True)
    nominee = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inquiries'


class InquiryPhotos(MigrateMixin):
    using = 'legacy'

    inquiry = models.ForeignKey(Inquiries, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    attachment_file_name = models.CharField(max_length=255, blank=True, null=True)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    attachment_file_size = models.IntegerField(blank=True, null=True)
    attachment_updated_at = models.DateTimeField(blank=True, null=True)
    attachment_suffix_url = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inquiry_photos'


class GridItems(MigrateMixin):
    using = 'legacy'

    inquiry = models.ForeignKey(Inquiries, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    sub_item_name = models.CharField(max_length=255, blank=True, null=True)
    item_name = models.CharField(max_length=255, blank=True, null=True)
    value = models.FloatField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    dish_title = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'grid_items'


class Assemblages(MigrateMixin):
    using = 'legacy'

    percent = models.FloatField()
    cepage = models.ForeignKey('Cepages', on_delete=models.DO_NOTHING)
    product_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'assemblages'


class CepageRegions(MigrateMixin):
    using = 'legacy'

    cepage = models.ForeignKey('Cepages', on_delete=models.DO_NOTHING)
    wine_region_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'cepage_regions'


class Cepages(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'cepages'


class NewsletterSubscriber(MigrateMixin):
    using = 'legacy'

    site = models.ForeignKey(Sites, models.DO_NOTHING, blank=True, null=True)
    email_address = models.ForeignKey(EmailAddresses, models.DO_NOTHING, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    consent_purpose = models.CharField(max_length=255, blank=True, null=True)
    consent_at = models.DateTimeField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'newsletter_subscriptions'


class Footers(MigrateMixin):
    using = 'legacy'

    about_us = models.TextField(blank=True, null=True)
    copyright = models.TextField(blank=True, null=True)
    site = models.ForeignKey('Sites', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'footers'


class LabelPhotos(MigrateMixin):
    using = 'legacy'

    guide_ad = models.ForeignKey(GuideAds, models.DO_NOTHING, blank=True, null=True)
    attachment_file_name = models.CharField(max_length=255)
    attachment_content_type = models.CharField(max_length=255)
    attachment_file_size = models.IntegerField()
    attachment_updated_at = models.DateTimeField()
    attachment_suffix_url = models.CharField(max_length=255)
    geometries = models.CharField(max_length=1024)

    class Meta:
        managed = False
        db_table = 'label_photos'


class OwnershipAffs(MigrateMixin):
    using = 'legacy'

    role = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    account_id = models.IntegerField(blank=True, null=True)
    establishment_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    requester_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ownership_affs'


class Panels(MigrateMixin):
    using = 'legacy'

    name = models.CharField(max_length=255, blank=True, null=True)
    display = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    query = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    account_id = models.IntegerField(blank=True, null=True)
    site_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'panels'


class ALaCartes(MigrateMixin):
    using = 'legacy'

    establishment = models.ForeignKey(Establishments, on_delete=models.DO_NOTHING, null=True)
    renewal_per_year = models.IntegerField(null=True)
    nb_wine = models.IntegerField(null=True)
    lowest_price = models.FloatField(null=True)
    highest_price = models.FloatField(null=True)
    by_glass = models.CharField(null=True, max_length=25)
    price_min_by_glass = models.FloatField(null=True)
    price_max_by_glass = models.FloatField(null=True)

    class Meta:
        managed = False
        db_table = 'a_la_cartes'
