# Generated by Django 2.2.7 on 2020-03-05 13:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0025_auto_20200303_1458'),
    ]

    operations = [
        migrations.AddField(
            model_name='inquiries',
            name='visited_at',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='visited at'),
        ),
        migrations.AlterField(
            model_name='review',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(0, 'ToInvestigate'), (1, 'Pending reception'), (2, 'To validate'), (3, 'To translate'), (4, 'Ready'), (5, 'Published'), (6, 'Abandoned'), (7, 'Hidden')], default=0),
        ),
    ]
