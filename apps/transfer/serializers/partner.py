from rest_framework import serializers

from establishment.models import Establishment
from partner.models import Partner, PartnerToEstablishment


class PartnerSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    establishment_id = serializers.IntegerField()
    partnership_name = serializers.CharField(allow_null=True)
    partnership_icon = serializers.CharField(allow_null=True)
    backlink_url = serializers.CharField(allow_null=True)
    created_at = serializers.DateTimeField(format='%m-%d-%Y %H:%M:%S')
    type = serializers.CharField(allow_null=True)
    starting_date = serializers.DateField(allow_null=True)
    expiry_date = serializers.DateField(allow_null=True)
    price_per_month = serializers.DecimalField(max_digits=10, decimal_places=2, allow_null=True)

    def validate(self, data):
        data.update({
            'old_id': data.pop('id'),
            'name': data['partnership_name'],
            'url': data.pop('backlink_url'),
            'image': self.get_image(data),
            'establishment': self.get_establishment(data),
            'type': Partner.PARTNER if data['type'] == 'Partner' else Partner.SPONSOR,
            'created': data.pop('created_at'),
        })
        data.pop('partnership_icon')
        data.pop('partnership_name')
        data.pop('establishment_id')
        return data

    @staticmethod
    def get_image(data):
        image = None
        try:
            image = partnership_to_image_url.get(data['partnership_name']).get(data['partnership_icon'])
        except AttributeError:
            pass
        return image

    @staticmethod
    def get_establishment(data):
        establishment = Establishment.objects.filter(old_id=data['establishment_id']).first()
        if not establishment:
            raise ValueError(f"Establishment not found with old_id {data['establishment_id']}: ")
        return establishment

    def create(self, validated_data):
        establishment = validated_data.pop('establishment')
        url = validated_data.pop('url')
        image = validated_data.pop('image')

        old_id = validated_data.pop('old_id')
        created = validated_data.pop('created')

        expiry_date = validated_data.pop('expiry_date')
        price_per_month = validated_data.pop('price_per_month')
        starting_date = validated_data.pop('starting_date')

        obj, is_created = Partner.objects.update_or_create(
            # old_id=validated_data['old_id'],
            **validated_data
        )

        obj.old_id = old_id
        obj.created = created

        obj.establishment.add(establishment)
        if is_created:
            obj.images = [image]
        elif image not in obj.images:
            obj.images.append(image)
        obj.save()

        p_t_e = PartnerToEstablishment.objects.filter(establishment=establishment, partner=obj).first()
        p_t_e.url = url
        p_t_e.image = image
        p_t_e.expiry_date = expiry_date
        p_t_e.price_per_month = price_per_month
        p_t_e.starting_date = starting_date
        p_t_e.save()
        return obj


partnership_to_image_url = {
    # partnership_name
    "tables-auberges": {
        # partnership_icon
        "Table Gastronomique":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/tables-auberges/Table Gastronomique-c1908231742bc0d0909c03cdac723c1daa3ee620134e8a6fbfae78efdaab8263.png",
        "Bistrot Gourmand":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/tables-auberges/Bistrot Gourmand-fdf0527d8ed05d9c7d26456e531301325582f04e1f8e5d4c283ee4eac367dba1.png",
        "Auberges de Village":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/tables-auberges/Auberges de Village-39aab1bbc678a6a5f7e0a2c9b8565bb6d21b01294371533580cdff0d9aa80da0.png",
        "Table de Prestige":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/tables-auberges/Table de Prestige-6bf4ab0fcc56d89ffc97cc71553f0b1d21b92f4b5ef7656b832ab03e5c6ff31c.png",
        "Table de Terroir":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/tables-auberges/Table de Terroir-e72a35a60c4a4575c162398d7355fc77787f368ff240ff8725dc24a37a497be7.png"
    },
    "riedel": {
        "riedel":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/riedel/riedel-1fe0fa8298ae76d0a2de6e00316d2dd4357cace659cc0398629ae4c117bd114e.png"
    },
    "lavazza": {
        "lavazza":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/lavazza/lavazza-9a013b9f954dd63ba871019a1fcd55ea5cb84b38ab2dbff47ee9464f671195bb.png"
    },
    "kaviari": {
        "Kaviari Paris":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/kaviari/Kaviari Paris-e6c4fe81a1d5257854f9b8e28cb244b01a9129ed01056cb98e809f70ec7b5fcf.png"
    },
    "mutti": {
        "mutti":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/mutti/mutti-86f8514b03e096b8e6843e8885756d311486c461a5b8c4f2f10f75995b146814.png"
    },
    "nespresso": {
        "nespresso":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/nespresso/nespresso-c401e553731c68a364754fe512f25889398d42f98bf8edbe1233695b546d1c5d.png"
    },
    "le-manoir": {
        "le-manoir":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/le-manoir/le-manoir-d6a616bebca2948c147ed7c58d9883682ee2d4a3779dea98e9d1a94f17e14771.png"
    },
    "discovery-cheque": {
        "discovery-cheque":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/discovery-cheque/discovery-cheque-665a3c60d3d6302897863b90245f405979760a27bea06c98aa4130b2c61276fb.png"
    },
    "mineralwater-selters": {
        "Mineral Water Selters":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/mineralwater-selters/Mineral Water Selters-2bac82327955c4357b51feff50bd6a2466705f85cb8e59875cb0192a161fd1d4.png"
    },
    "mineral-water-acquamorelli": {
        "Mineral Water Acqua Morelli":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/mineral-water-acquamorelli/Mineral Water Acqua Morelli-4032ba6b4da7f79dcf005809423fc663cfb49199d6ce95f3cfe99a7705359ac4.png"
    },
    "mastercard-maestro": {
        "MastercardMaestro":
            "https://1dc3f33f6d.optimicdn.com/assets/establishment-backlinks/mastercard-maestro/MastercardMaestro-718680bc48d98c4a7417a14a62e4ba2d3af980361256739d980e7b453552b5d8.png"
    }
}
