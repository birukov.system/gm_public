from http.cookies import SimpleCookie

from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from rest_framework import status

from account.models import Role, User, UserRole
from authorization.tests.tests_authorization import get_tokens_for_user
from comment.models import Comment
from utils.tests.tests_permissions import BasePermissionTests
from main.models import SiteSettings


class CommentModeratorPermissionTests(BasePermissionTests):
    def setUp(self):
        super().setUp()

        self.site_ru, created = SiteSettings.objects.get_or_create(
            subdomain='ru'
        )

        self.role = Role.objects.create(
            role=2,
            site=self.site_ru
        )
        self.role.save()

        self.moderator = User.objects.create_user(username='moderator',
                                                  email='moderator@mail.com',
                                                  password='passwordmoderator')

        self.userRole = UserRole.objects.create(
            user=self.moderator,
            role=self.role
        )
        self.userRole.save()

        self.content_type = ContentType.objects.get(app_label='location', model='country')

        self.user_test = get_tokens_for_user()

        self.comment = Comment.objects.create(text='Test comment', mark=1,
                                              user=self.user_test["user"],
                                              object_id=self.country_ru.pk,
                                              content_type_id=self.content_type.id,
                                              site=self.site_ru
                                              )
        self.comment.save()
        self.url = reverse('back:comment:comment-crud', kwargs={"id": self.comment.id})

    def test_post(self):
        self.url = reverse('back:comment:comment-list-create')

        comment = {
            "text": "Test comment POST",
            "user": self.user_test["user"].id,
            "object_id": self.country_ru.pk,
            "content_type": self.content_type.id,
            "site_id": self.site_ru.id
        }

        response = self.client.post(self.url, format='json', data=comment)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        comment = {
            "text": "Test comment POST moder",
            "user": self.moderator.id,
            "object_id": self.country_ru.id,
            "content_type": self.content_type.id,
            "site_id": self.site_ru.id
        }

        tokens = User.create_jwt_tokens(self.moderator)
        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('access_token')})

        response = self.client.post(self.url, format='json', data=comment)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_put_moderator(self):
        tokens = User.create_jwt_tokens(self.moderator)
        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('access_token')})

        data = {
            "id": self.comment.id,
            "text": "test text moderator",
            "mark": 1,
            "user": self.moderator.id,
            "object_id": self.country_ru.id,
            "content_type": self.content_type.id,
            'site_id': self.site_ru.id
        }

        response = self.client.put(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get(self):
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_put_other_user(self):
        other_user = User.objects.create_user(username='test',
                                              email='test@mail.com',
                                              password='passwordtest')

        tokens = User.create_jwt_tokens(other_user)

        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('access_token')})

        data = {
            "id": self.comment.id,
            "text": "test text moderator",
            "mark": 1,
            "user": other_user.id
        }

        response = self.client.put(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put_super_user(self):
        super_user = User.objects.create_user(username='super',
                                              email='super@mail.com',
                                              password='passwordtestsuper',
                                              is_superuser=True)

        tokens = User.create_jwt_tokens(super_user)

        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('access_token')})

        data = {
            "id": self.comment.id,
            "text": "test text moderator",
            "mark": 1,
            "user": super_user.id,
            "object_id": self.country_ru.id,
            "content_type": self.content_type.id,
        }
        response = self.client.put(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


