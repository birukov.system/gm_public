# Generated by Django 2.2.7 on 2019-12-11 15:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0006_merge_20191027_1758'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='image',
            options={'ordering': ['-modified'], 'verbose_name': 'Image', 'verbose_name_plural': 'Images'},
        ),
    ]
