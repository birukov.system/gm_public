"""
Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql


"""

card = {
    # как работать с GenericForeignKey(content_type) - ?
    # как работать с ForeignKey на самого себя(self), поле "child"
    # вопрос с внешним ключом language на таблицу Language
    "Review": {
        "data_type": "objects",
        "dependencies": ("User", "Language", "Review"),
        "fields": {
            "Reviews": {
                "published_at": "published_at",
                "vintage": "vintage",
                "status": ("aasm_state", "django.db.models.PositiveSmallIntegerField")

                # "content_object": ""
            },
            "relations": {
                "ReviewTexts": {
                    "key": "review",
                    "fields": {
                        # полу text в модели Review имеет тип TJSONField, а поле text в модели ReviewTexts имеет тип TextField
                        # при их сопоставлении использовать поле locale модели ReviewTexts
                        "text": ("text", "django.db.models.JSONField")
                    }
                }
            },
        },
        "relations": {
            "Accounts": [
                (("account", None),
                 ("User", "reviewer", None, None))
            ]
        }
    }

}

used_apps = ("account", "translation")