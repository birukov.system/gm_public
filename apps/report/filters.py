"""Filters for application report."""
from django_filters import rest_framework as filters

from report.models import Report


class ReportFilterSet(filters.FilterSet):
    """Report filter set."""
    source = filters.ChoiceFilter(
        choices=Report.SOURCE_CHOICES,
        help_text='Filter allow filtering QuerySet by a field - source.'
                  'Choices - 0 (Back office), 1 (Web), 2 (Mobile)'
    )
    category = filters.ChoiceFilter(
        choices=Report.CATEGORY_CHOICES,
        help_text='Filter allow filtering QuerySet by a field - category.'
                  'Choices - 0 (Bug), 1 (Suggestion/improvement)'
    )

    class Meta:
        """Meta class."""
        model = Report
        fields = (
            'source',
            'category',
        )
