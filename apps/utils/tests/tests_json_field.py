from django.test import TestCase
from translation.models import Language
from django.core import exceptions
from utils.serializers import validate_tjson


class ValidJSONTest(TestCase):

    def test_valid_json(self):
        lang = Language.objects.create(title='English', locale='en-GB')
        lang.save()

        data = 'str'

        with self.assertRaises(exceptions.ValidationError) as err:
            validate_tjson(data)

        self.assertEqual(err.exception.code, 'invalid_json')

        data = {
            "string": "value"
        }

        with self.assertRaises(exceptions.ValidationError) as err:
            validate_tjson(data)

        self.assertEqual(err.exception.code, 'invalid_translated_keys')

        data = {
            "en-GB": "English"
        }

        try:
            validate_tjson(data)
            self.assertTrue(True)
        except exceptions.ValidationError:
            self.assert_(False, "Test json translated FAILED")