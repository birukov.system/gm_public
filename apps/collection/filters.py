"""Collection app filters."""
from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters

from collection import models


class CollectionFilterSet(filters.FilterSet):
    """Collection filter set."""
    establishment_id = filters.NumberFilter(
        field_name='establishments__id',
        help_text='Establishment id. Allows to filter list of collections by choosen estblishment. '
                  'Use for Establishment detail\'s sheet to content display within '
                  '"Collections & Guides" tab.'
    )

    # "ordering" instead of "o" is for backward compatibility
    ordering = filters.OrderingFilter(
        # tuple-mapping retains order
        fields=(
            ('rank', 'rank'),
            ('start', 'start'),
        ),
        help_text='Ordering by fields - rank, start',
    )

    class Meta:
        """Meta class."""
        model = models.Collection
        fields = (
            'ordering',
            'establishment_id',
        )


class GuideFilterSet(filters.FilterSet):
    """Guide filter set."""
    establishment_id = filters.NumberFilter(
        method='by_establishment_id',
        help_text='Establishment id. It allows filtering list of guides by choosen establishment. '
                  'Use for Establishment detail\'s sheet to content display within '
                  '"Collections & Guides" tab.'
    )
    guide_type = filters.ChoiceFilter(
        choices=models.Guide.GUIDE_TYPE_CHOICES,
        help_text=f'It allows filtering guide list by type.'
                  f'Enum: {dict(models.Guide.GUIDE_TYPE_CHOICES)}'
    )

    class Meta:
        """Meta class."""
        model = models.Guide
        fields = (
            'establishment_id',
            'guide_type',
        )

    def by_establishment_id(self, queryset, name, value):
        """Filter by establishment id."""
        if value not in EMPTY_VALUES:
            return queryset.by_establishment_id(value)
        return queryset
