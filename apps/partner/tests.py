# Create your tests here.
from http.cookies import SimpleCookie

from rest_framework import status
from rest_framework.test import APITestCase

from account.models import User, Role, UserRole
from establishment.models import EstablishmentType, Establishment
from location.models import Country, Region, City, Address
from partner.models import Partner
from translation.models import Language


class BaseTestCase(APITestCase):

    def setUp(self):
        self.username = 'test_user'
        self.password = 'test_user_password'
        self.email = 'test_user@mail.com'
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
            is_staff=True,
        )

        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie({
            'access_token': tokens.get('access_token'),
            'refresh_token': tokens.get('refresh_token'),
        })

        self.establishment_type = EstablishmentType.objects.create(name="Test establishment type")
        self.role = Role.objects.create(role=Role.ESTABLISHMENT_MANAGER)

        self.establishment = Establishment.objects.create(
            name="Test establishment",
            establishment_type_id=self.establishment_type.id,
            is_publish=True,
            slug="test",
        )

        self.user_role = UserRole.objects.create(
            user=self.user,
            role=self.role,
            establishment=self.establishment,
        )

        self.partner = Partner.objects.create(
            url='www.ya.ru',
            establishment=self.establishment,
        )


class PartnerWebTestCase(BaseTestCase):

    def test_partner_list(self):
        response = self.client.get("/api/web/partner/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class PartnerBackTestCase(BaseTestCase):

    def test_partner_list(self):
        response = self.client.get('/api/back/partner/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partner_post(self):
        test_partner = {
            'url': 'http://google.com',
        }
        response = self.client.post('/api/back/partner/', data=test_partner, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_partner_detail(self):
        response = self.client.get(f'/api/back/partner/{self.partner.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partner_detail_put(self):
        data = {
            'url': 'http://yandex.com',
            'name': 'Yandex',
        }

        response = self.client.put(f'/api/back/partner/{self.partner.id}/', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partner_delete(self):
        response = self.client.delete(f'/api/back/partner/{self.partner.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
