from django.contrib import admin
from timetable import models


@admin.register(models.Timetable)
class TimetableModelAdmin(admin.ModelAdmin):
    """Timetable model admin"""
