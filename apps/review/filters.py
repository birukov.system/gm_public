from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters

from review import models


class ReviewFilter(filters.FilterSet):
    """Review filter set."""

    establishment_id = filters.NumberFilter(field_name='object_id', )
    product_id = filters.NumberFilter(field_name='object_id', )

    class Meta:
        """Meta class."""

        model = models.Review
        fields = (
            'establishment_id',
            'product_id',
        )

    def by_establishment_id(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_establishment_id(value, content_type='establishment')
        return queryset

    def by_product_id(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_product_id(value, content_type='product')
        return queryset
