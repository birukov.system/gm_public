"""Collection common urlpaths."""
from django.urls import path
from rest_framework.routers import SimpleRouter

from collection.views import back as views

app_name = 'collection'

router = SimpleRouter()
router.register(r'collections\/all', views.CollectionBackOfficeList)
router.register(r'collections', views.CollectionBackOfficeViewSet)
urlpatterns = [
    path('guides/', views.GuideListCreateView.as_view(),
         name='guide-list-create'),
    path('guides/all/', views.GuideListView.as_view(),
         name='guide-list'),
    path('guides/<int:pk>/', views.GuideElementListView.as_view(),
         name='guide-element-list'),
    path('guides/<int:pk>/regenerate/', views.GuideUpdateView.as_view(),
         name='guide-regenerate'),
    path('guides/<int:pk>/element/<int:element_pk>/advertorial/',
         views.AdvertorialCreateView.as_view(),
         name='guide-advertorial-create'),
    path('guides/<int:pk>/element/<int:element_pk>/advertorial/<int:advertorial_pk>/',
         views.AdvertorialDestroyView.as_view(),
         name='guide-advertorial-destroy'),
    path('guides/<int:pk>/filters/', views.GuideFilterCreateView.as_view(),
         name='guide-filter-list-create'),
    path('guides/<int:pk>/export-csv/', views.GuideElementExportCSVView.as_view(),
         name='guide-export-csv'),
    path('guides/<int:pk>/export-xml/', views.GuideElementExportXMLView.as_view(),
         name='guide-export-xml'),
    path('guides/<int:pk>/export-doc/', views.GuideElementExportDOCView.as_view(),
         name='guide-export-doc'),
    path('guides/<int:pk>/bind-object/', views.GuideBackOfficeList.as_view(),
         name='guide-bind-object')
] + router.urls
