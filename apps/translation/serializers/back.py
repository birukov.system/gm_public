from rest_framework.validators import UniqueTogetherValidator
from django.http import Http404
from rest_framework import serializers


from translation.serializers import common as serializers_common
from translation import models


class SiteInterfaceDictionaryBackSerializer(serializers_common.SiteInterfaceDictionarySerializer):
    class Meta(serializers_common.SiteInterfaceDictionarySerializer.Meta):
        fields = serializers_common.SiteInterfaceDictionarySerializer.Meta.fields + [
            'text',
            'pending_text',
        ]

        extra_kwargs = {
            'text': {'required': True},
        }
        validators = [
            UniqueTogetherValidator(queryset=models.SiteInterfaceDictionary.objects.all(), fields=['page', 'keywords'])
        ]

    @staticmethod
    def mutate_validated_data(validated_data):
        """mutate validated data"""
        if 'text' in validated_data:
            validated_data['pending_text'] = validated_data.pop('text')

        return validated_data

    def create(self, validated_data):
        return super().create(validated_data=self.mutate_validated_data(validated_data))

    def update(self, instance, validated_data):
        return super().update(instance=instance, validated_data=self.mutate_validated_data(validated_data))


class SiteInterfaceDictionaryUpdateBackSerializer(SiteInterfaceDictionaryBackSerializer):
    validators = []


class SiteInterfaceDictionaryConfirmBackSerializer(serializers.ModelSerializer):
    locale = serializers.CharField(required=True, write_only=True)
    ids = serializers.ListField(child=serializers.IntegerField(), write_only=True)

    class Meta:
        model = models.SiteInterfaceDictionary
        fields = (
            'ids',
            'locale',
            'id',
            'text',
            'pending_text',
            'page',
            'keywords',
        )
        extra_kwargs = {
            'id': {'read_only': True},
            'text': {'read_only': True},
            'pending_text': {'read_only': True},
            'page': {'read_only': True},
            'keywords': {'read_only': True},
        }

    def update(self, instance, validated_data):
        if 'locale' in validated_data:
            locale = validated_data['locale']
            if locale in instance.pending_text:
                instance.text[locale] = instance.pending_text.pop(locale)
                instance.save()
        return instance

    def create(self, validated_data):
        pass
