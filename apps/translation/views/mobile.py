from translation.views import common as views_common
from translation.views import back as views_back
from translation.serializers import mobile as serializers_mobile
from utils.methods import get_permission_classes
from utils.permissions import IsTranslator, IsSuperAdmin, IsReadOnly


class MobileSiteInterfaceDictionaryListCreateView(views_back.SiteInterfaceDictionaryListCreateView):
    permission_classes = get_permission_classes(
        IsTranslator,
        IsSuperAdmin,
        IsReadOnly,
    )

    serializer_class = serializers_mobile.MobileSiteInterfaceDictionarySerializer
