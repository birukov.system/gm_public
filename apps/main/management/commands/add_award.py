from django.core.management.base import BaseCommand
from django.db import connections
from tqdm import tqdm

from establishment.models import Employee
from main.models import Award, AwardType
from utils.methods import namedtuplefetchall


class Command(BaseCommand):
    help = '''Add award from old db to new db.
    Run after command add_award_type!'''

    def award_sql(self):
        with connections['legacy'].cursor() as cursor:
            cursor.execute('''
                            select 
                                DISTINCT
                                a.id, a.profile_id, a.title, 
                                a.`year` as vintage_year, a.state,
                                t.id as award_type 
                            from awards as a
                            join award_types t on t.id = a.award_type_id
                            join profiles p on p.id = a.profile_id               
                           ''')
            return namedtuplefetchall(cursor)

    def handle(self, *args, **kwargs):
        objects = []
        Award.objects.all().delete()
        for a in tqdm(self.award_sql(), desc='Add award to profile'):
            profiles = Employee.objects.filter(old_id=a.profile_id)
            type = AwardType.objects.filter(old_id=a.award_type)
            state = Award.PUBLISHED if a.state == 'published' else Award.WAITING
            if profiles.exists() and type.exists():
                for profile in profiles:
                    award = Award(award_type=type.first(), vintage_year=a.vintage_year,
                                  title={"en-GB": a.title}, state=state,
                                  content_object=profile, old_id=a.id)

                    objects.append(award)
        awards = Award.objects.bulk_create(objects)
        self.stdout.write(self.style.WARNING(f'Created awards objects.'))
