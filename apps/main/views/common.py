"""Main app views."""
from django.http import Http404
from rest_framework import generics, permissions, status
from rest_framework.response import Response

from main import methods, models, serializers
from main.models import SiteSettings
from news.models import News
from news.serializers import NewsDetailSerializer, NewsListSerializer, ContentPagePrivacyPolicySerializer
from news.views import NewsMixinView
from utils.serializers import EmptySerializer


#
# class FeatureViewMixin:
#     """Feature view mixin."""
#
#     queryset = models.Feature.objects.all()
#     serializer_class = serializers.FeatureSerializer
#     permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
#
#
# class FeaturesLCView(FeatureViewMixin, generics.ListCreateAPIView):
#     """Site features LC View."""
#
#     pagination_class = None
#
#
# class FeaturesRUDView(FeatureViewMixin, generics.RetrieveUpdateDestroyAPIView):
#     """Site features RUD View."""
#
#
# class SiteFeaturesViewMixin:
#     """Site feature view mixin."""
#
#     queryset = models.SiteFeature.objects.all()
#     serializer_class = serializers.SiteFeatureSerializer
#     permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
#
#
# class SiteFeaturesLCView(SiteFeaturesViewMixin, generics.ListCreateAPIView):
#     """Site features LC."""
#
#     pagination_class = None
#
#
# class SiteFeaturesRUDView(SiteFeaturesViewMixin,
#                           generics.RetrieveUpdateDestroyAPIView):
#     """Site features RUD."""


class AwardView(generics.ListAPIView):
    """Awards list view."""
    serializer_class = serializers.AwardSerializer
    queryset = models.Award.objects.all().with_base_related()
    permission_classes = (permissions.AllowAny,)


class AwardRetrieveView(generics.RetrieveAPIView):
    """Award retrieve view."""
    serializer_class = serializers.AwardSerializer
    queryset = models.Award.objects.all().with_base_related()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class CarouselListView(generics.ListAPIView):
    """Return list of carousel items."""

    queryset = models.Carousel.objects.is_parsed().active()
    serializer_class = serializers.CarouselListSerializer
    permission_classes = (permissions.AllowAny,)
    pagination_class = None

    def get_queryset(self):
        country_code = self.request.country_code
        qs = models.Carousel.objects.is_parsed().active()
        if country_code:
            qs = qs.by_country_code(country_code)
        return qs.order_by('-modified')


class DetermineLocation(generics.GenericAPIView):
    """Determine user's location."""

    permission_classes = (permissions.AllowAny,)
    serializer_class = EmptySerializer

    def get(self, request, *args, **kwargs):
        longitude, latitude = methods.determine_coordinates(request)
        city = methods.determine_user_city(request)
        country_name = methods.determine_country_name(request)
        country_code = methods.determine_country_code(request)
        if longitude and latitude and city and country_name:
            return Response(data={
                'latitude': latitude,
                'longitude': longitude,
                'city': city,
                'country_name': country_name,
                'country_code': country_code,
            })
        raise Http404


class ContentPageBaseView(generics.GenericAPIView):

    @property
    def static_page_category(self):
        return 'static'

    def get_queryset(self):
        return News.objects.with_base_related() \
            .order_by('-is_highlighted', '-publication_date', '-publication_time') \
            .filter(news_type__name=self.static_page_category)


class ContentPageView(ContentPageBaseView, generics.ListAPIView):
    """Method to get content pages"""

    permission_classes = (permissions.AllowAny,)
    serializer_class = NewsListSerializer
    queryset = News.objects.all()


class ContentPageAdminView(ContentPageBaseView, generics.ListCreateAPIView):
    """Method to get content pages"""

    permission_classes = (permissions.IsAdminUser,)
    serializer_class = NewsListSerializer
    queryset = News.objects.all()


class ContentPageRetrieveView(ContentPageBaseView, NewsMixinView, generics.RetrieveAPIView):
    """Retrieve method to get content pages"""

    lookup_field = None
    permission_classes = (permissions.AllowAny,)
    serializer_class = NewsDetailSerializer
    queryset = News.objects.all()


class ContentPageIdRetrieveView(ContentPageBaseView, generics.RetrieveAPIView):
    """Retrieve method to get content pages"""

    permission_classes = (permissions.AllowAny,)
    serializer_class = NewsDetailSerializer
    queryset = News.objects.all()


class ContentPageRetrieveAdminView(ContentPageBaseView, NewsMixinView, generics.RetrieveUpdateDestroyAPIView):
    """Retrieve method to get content pages"""

    lookup_field = None
    permission_classes = (permissions.IsAdminUser,)
    serializer_class = NewsDetailSerializer
    queryset = News.objects.all()


class ContentPagePrivacyPolicyView(generics.RetrieveAPIView):
    serializer_class = ContentPagePrivacyPolicySerializer
    pagination_class = None
    permission_classes = (permissions.AllowAny,)

    def get_object(self):
        settings = SiteSettings.objects.filter(subdomain='www').first()
        return {
            'privacy_policy': settings.privacy_policy,
            'privacy_policy_version': settings.privacy_policy_version,
        }
