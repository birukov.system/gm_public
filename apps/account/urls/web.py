"""Web account URLs"""
from django.urls import path

from account.urls import common as common_views
from account.views import web as views

app_name = 'account'

urlpatterns_api = [
    path('reset-password/', views.PasswordResetView.as_view(), name='password-reset'),
    path('reset-password/confirm/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(),
         name='password-reset-confirm'),
    path('join-establishment-team/<uidb64>/<token>/<int:role_id>', views.ApplyUserEstablishmentRole.as_view(),
         name='join-establishment-team'),
]

urlpatterns = urlpatterns_api + \
              common_views.urlpatterns
