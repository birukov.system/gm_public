from django.core.management.base import BaseCommand
from tqdm import tqdm

from establishment.models import Establishment, Menu, Plate
from transfer.models import Menus


class Command(BaseCommand):
    help = 'Add menus from old db to new db'

    def handle(self, *args, **kwargs):
        count = 0
        menus = Menus.objects.filter(name__isnull=False).exclude(name='')
        for old_menu in tqdm(menus, desc='Add formulas menu'):
            est = Establishment.objects.filter(
                old_id=old_menu.establishment_id).first()
            if est:

                menu, _ = Menu.objects.get_or_create(
                    category={'en-GB': 'formulas'},
                    establishment=est,
                    old_id=old_menu.id,
                    is_drinks_included=True if old_menu.drinks == 'included' else False,
                    created=old_menu.created_at,
                )
                plate, created = Plate.objects.get_or_create(
                    name={"en-GB": old_menu.name},
                    menu=menu,
                    price=old_menu.price or 0
                )
                if created:
                    plate.currency_code = old_menu.currency
                    plate.currency = est.currency
                    plate.save()
                    count += 1

        self.stdout.write(self.style.WARNING(f'Updated/created {count} objects.'))
