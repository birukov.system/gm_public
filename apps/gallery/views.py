from django.conf import settings
from django.shortcuts import get_object_or_404
from django.db.transaction import on_commit
from django.http import FileResponse
from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from establishment.views import EstablishmentLastUpdatedMixin
from utils.methods import get_permission_classes
from utils.permissions import IsContentPageManager
from utils.serializers import EmptySerializer

from . import tasks, models, serializers


class ImageBaseView(generics.GenericAPIView):
    """Base Image view."""
    model = models.Image
    queryset = models.Image.objects.all()
    serializer_class = serializers.ImageSerializer
    permission_classes = (AllowAny, )


class ImageListCreateView(ImageBaseView, generics.ListCreateAPIView):
    """
    ## List/Create view
    ### *GET*
    #### Description
    Get paginated list of images, with ordering by field `modified` (descending)
    #### Response
    E.g.:
    ```
    {
      "count": 40595,
      "next": 2,
      "previous": null,
      "results": [
        {
          "id": 47336,
          ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Upload an image on a server.
    ##### Request
    ###### Required
    * file (`file`) - download file
    ###### Available
    * orientation (`enum`) - default: `null`
    ```
    0 (Horizontal)
    1 (Vertical)
    ```
    * title (`str`) - title of image file (default - `''`)
    * is_public (`bool`) - flag that responds for availability
    for displaying (default - `True`)
    * preview (`file`) - download preview file (default - `null`)
    * link (`str`) - mp4 or youtube video link (default - `null`)
    * order (`int`) - order number (default - `0`)
    ##### Response
    E.g.:
    ```
    {
      "id": 47336,
      ...
    }

    ```
    """


class MediaForEstablishmentView(EstablishmentLastUpdatedMixin,
                                ImageBaseView,
                                generics.ListCreateAPIView):
    """View for creating and retrieving certain establishment media."""
    pagination_class = None
    serializer_class = serializers.EstablishmentGallerySerializer
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'

    def get_queryset(self):
        return super().get_queryset().filter(establishment__pk=self.kwargs['establishment_id'])\
            .order_by('-establishment_gallery__is_main', '-order').prefetch_related('created_by',
                                                                                    'establishment_gallery')


class MediaUpdateView(EstablishmentLastUpdatedMixin,
                      ImageBaseView,
                      generics.UpdateAPIView,
                      generics.DestroyAPIView):
    """View for updating media data"""
    serializer_class = serializers.EstablishmentGallerySerializer
    permission_classes = get_permission_classes(
        IsContentPageManager
    )

    def delete(self, request, *args, **kwargs):
        """Override destroy view"""
        instance = self.get_object()
        tasks.delete_image(image_id=instance.id)
        # if settings.USE_CELERY:
        #     on_commit(lambda: tasks.delete_image.delay(image_id=instance.id))
        # else:
        #     on_commit(lambda: tasks.delete_image(image_id=instance.id))
        return Response(status=status.HTTP_204_NO_CONTENT)

    def should_control_last_updating(self):
        establishment_instance = self.get_object().establishment_set.first()

        # if establishment has this image
        if establishment_instance is not None:
            self._establishment_for_last_updated_mixin = establishment_instance
            return True

        return False

    def get_establishment_for_last_updated_mixin(self):
        return self._establishment_for_last_updated_mixin


class MediaPictureDownloadView(generics.RetrieveAPIView):
    """Get image as file"""
    permission_classes = get_permission_classes(
        IsContentPageManager
    )
    serializer_class = EmptySerializer

    def get(self, request, *args, **kwargs):
        image_instance = get_object_or_404(klass=models.Image, pk=kwargs['pk'])
        if not image_instance.image:
            from utils.exceptions import UnprocessableEntityError
            raise UnprocessableEntityError()
        response = FileResponse(image_instance.image)
        response['Content-Length'] = image_instance.image.size
        response['Content-Disposition'] = f'attachment; filename="{image_instance.image.name}"'
        return response


class ImageRetrieveDestroyView(EstablishmentLastUpdatedMixin, ImageBaseView, generics.RetrieveDestroyAPIView):
    """
    ## Retrieve/Destroy view.
    ### *GET*
    #### Description
    Retrieve serialized instance of Image model by image identifier.
    #### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an image (DB record and storage object) by image identifier.
    ##### Response
    ```
    No content
    ```
    """
    permission_classes = get_permission_classes(
        IsContentPageManager
    )

    def delete(self, request, *args, **kwargs):
        """Override destroy view"""
        instance = self.get_object()
        tasks.delete_image(image_id=instance.id)
        # if settings.USE_CELERY:
        #     on_commit(lambda: tasks.delete_image.delay(image_id=instance.id))
        # else:
        #     on_commit(lambda: tasks.delete_image(image_id=instance.id))
        return Response(status=status.HTTP_204_NO_CONTENT)

    def should_control_last_updating(self):
        establishment_instance = self.get_object().establishment_set.first()

        # if establishment has this image
        if establishment_instance is not None:
            self._establishment_for_last_updated_mixin = establishment_instance
            return True

        return False

    def get_establishment_for_last_updated_mixin(self):
        return self._establishment_for_last_updated_mixin


class CropImageCreateView(ImageBaseView, generics.CreateAPIView):
    """
    ## Create view
    ### *POST*
    #### Description
    Create crop of an existing image by an identifier of image and parameters in request data.
    ##### Request
    Required
    * width (`int`) - width of cropped image
    * height (`int`) - height of cropped image
    * crop (`str`) - crop coordinates (e.g.: `100px 200px`)

    Non-required
    * quality (`int`) - image quality, by default - `settings.THUMBNAIL_QUALITY` (85%)
    * certain_aspect (`str`) - certain aspect, by default -
    ```
    (x2 - x1)
        x
    (y2 - y1)
    ```
    > where x, y is coordinates parsed from `crop` parameter
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    """

    serializer_class = serializers.CropImageSerializer

    def should_control_last_updating(self):
        establishment_instance = self.get_object().establishment_set.first()

        # if establishment has this image
        if establishment_instance is not None:
            self._establishment_for_last_updated_mixin = establishment_instance
            return True

        return False

    def get_establishment_for_last_updated_mixin(self):
        return self._establishment_for_last_updated_mixin


class ResizeImageCreateView(ImageBaseView, generics.CreateAPIView):
    serializer_class = serializers.ResizeImageSerializer
