from datetime import datetime

from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import generics, permissions, status
from rest_framework.response import Response

from account import models as account_models
from authorization.serializers import MobileCodeConfirmationSerializer, SignUpResendConfirmationEmailSerializer
from utils import exceptions as utils_exceptions
from utils.authentication import send_confirmation_email
from utils.models import PlatformMixin
from utils.views import JWTGenericViewMixin


class MobileCodeConfirmationView(JWTGenericViewMixin, generics.GenericAPIView):
    """
    ## Confirm signup by mobile code view.
    ### POST-request data
    ```
    {
    "source": <int> # 0 - Mobile, 1 - Web, 2 - All (by default used: 1),
    "email": <str>,
    "code": <str> # Numeric string exactly 4 digits long
    }
    ```
    ### Response
    If confirmation code provided in request is right for given unconfirmed user email and is not expired -> 200 OK

    ### Errors
    - 401 UNAUTHORIZED: User not found -> if user with such unconfirmed email was not found
    - 410 GONE: Code is expired -> if confirmation code was created more than 7 days ago
    - 400 BAD_REQUEST: Not valid token -> if provided confirmation code is not the right one
    - 400 BAD_REQUEST {some validation error} -> code format is invalid
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = MobileCodeConfirmationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data['email']
        code = serializer.validated_data['code']
        source = serializer.validated_data['source']

        user_qs = account_models.User.objects.filter(unconfirmed_email=email)
        if user_qs.exists():
            user = user_qs.first()

            # TODO compare hashes
            if user.confirmation_code != code:
                raise utils_exceptions.NotValidTokenError()

            days_since_code_creation = timezone.now() - user.confirmation_code_created_at
            if days_since_code_creation.days > 7:
                raise utils_exceptions.ConfirmationCodeExpiredError()

            # Cleanup & confirm email
            user.confirmation_code = None
            user.confirmation_code_created_at = None
            user.confirm_email()

            # Create tokens
            tokens = user.create_jwt_tokens()
            return self._put_cookies_in_response(
                cookies=self._put_data_in_cookies(
                    access_token=tokens.get('access_token'),
                    refresh_token=tokens.get('refresh_token')),
                response=Response(status=status.HTTP_200_OK))

        else:
            raise utils_exceptions.UserNotFoundError()


class SignUpResendConfirmationEmailView(generics.GenericAPIView):
    """
        ## Resend signup confirmation email view
        ### POST-request data
        ```
        {
        "email": <str>,
        "source": <int> # 0 - Mobile, 1 - Web, 2 - All (by default used: 1),
        }
        ```

        ### Description
        Resend signup confirmation email for given user email. If source=0 (mobile), confirmation email contains 4-digit
        code, else link.

        ### Response
        - If there is no unconfirmed user with such email -> [401 UNAUTHORIZED] User not found
        - Validation errors -> [400 BAD REQUEST]
        - Unconfirmed user with such email was found -> resend confirmation email -> [200 OK]

        """

    permission_classes = (permissions.AllowAny,)
    serializer_class = SignUpResendConfirmationEmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data['email']
        user = account_models.User.objects.filter(unconfirmed_email__iexact=email).first()
        if not user:
            raise utils_exceptions.UserNotFoundError()

        source = serializer.validated_data['source']
        is_mobile = source == PlatformMixin.MOBILE

        send_confirmation_email(user.id, request.country_code, is_mobile=is_mobile)

        return Response(status=status.HTTP_200_OK)
