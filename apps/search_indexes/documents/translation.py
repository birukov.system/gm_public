"""User model index."""
from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from search_indexes.utils import trigram_analyzer
from translation.models import SiteInterfaceDictionary

TranslationsIndex = Index(settings.ELASTICSEARCH_INDEX_NAMES.get(__name__, 'translation'))
TranslationsIndex.settings(number_of_shards=2, number_of_replicas=1, max_result_window=500000)


@TranslationsIndex.doc_type
class TranslationDocument(Document):

    text_en_searchable = fields.TextField(analyzer=trigram_analyzer)
    text_not_en_searchable = fields.ListField(fields.TextField(analyzer=trigram_analyzer))
    text = fields.KeywordField(attr='text_serialized')
    keys_translated = fields.ListField(fields.KeywordField(attr='keys_translated'))
    keys_pending_translated = fields.ListField(fields.KeywordField(attr='keys_pending_translated'))
    pending_text = fields.KeywordField(attr='pending_text_serialized')
    keywords = fields.TextField(analyzer=trigram_analyzer)
    page = fields.TextField(analyzer=trigram_analyzer)

    class Django:
        model = SiteInterfaceDictionary
        fields = (
            'id',
            'created',
            'modified',
        )

    def get_queryset(self):
        return super().get_queryset()
