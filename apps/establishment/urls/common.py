"""Establishment url patterns."""
from django.urls import path

from establishment import views

app_name = 'establishment'

urlpatterns = [
    path('', views.EstablishmentListView.as_view(), name='list'),
    path('recent-reviews/', views.EstablishmentRecentReviewListView.as_view(),
         name='recent-reviews'),
    path('slug/<slug:slug>/comments/', views.EstablishmentCommentListView.as_view(),
         name='list-comments'),
    path('slug/<slug:slug>/comments/create/', views.EstablishmentCommentCreateView.as_view(),
         name='create-comment'),
    path('slug/<slug:slug>/comments/<int:comment_id>/', views.EstablishmentCommentRUDView.as_view(),
         name='rud-comment'),
    path('slug/<slug:slug>/favorites/', views.EstablishmentFavoritesCreateDestroyView.as_view(),
         name='create-destroy-favorites'),

    # similar establishments by type/subtype
    path('slug/<slug:slug>/similar/', views.RestaurantSimilarListView.as_view(),
         name='similar-restaurants'),
    path('slug/<slug:slug>/similar/wineries/', views.WinerySimilarListView.as_view(),
         name='similar-wineries'),

    path('slug/<slug:slug>/similar/artisans/', views.ArtisanProducerSimilarListView.as_view(),
         name='similar-artisans'),

    path('slug/<slug:slug>/similar/producers/', views.ArtisanProducerSimilarListView.as_view(),
         name='similar-producers'),
    path('slug/<slug:slug>/similar/producers/food/', views.FoodProducerSimilarListView.as_view(),
         name='similar-food-producers'),
    path('slug/<slug:slug>/similar/producers/distilleries/',
         views.DistillerySimilarListView.as_view(),
         name='similar-distilleries'),
]
