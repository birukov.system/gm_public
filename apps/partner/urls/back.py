"""Back account URLs"""
from django.urls import path

from partner.views import back as views

app_name = 'partner'

urlpatterns = [
    path('', views.PartnerLstView.as_view(), name='partner-list-create'),
    path('<int:id>/', views.PartnerRUDView.as_view(), name='partner-rud'),
    path('for_establishment/<int:establishment_id>/', views.EstablishmentPartners.as_view(),
         name='partners-for-establishment'),
    path('pictures/<int:id>/', views.PartnerPicturesListView.as_view(), name='partner-pictures-get'),
    path('bind/<int:partner_id>/<int:establishment_id>/', views.BindPartnerToEstablishmentView.as_view(),
         name='bind-partner-to-establishment'),
    path('unbind/<int:bind_id>/', views.UnbindPartnerFromEstablishmentView.as_view(),
         name='unbind-partner-from-establishment'),
]
