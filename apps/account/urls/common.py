"""Common account URLs"""
from django.urls import path

from account.views import common as views

app_name = 'account'

urlpatterns = [
    path('user/', views.UserRetrieveUpdateView.as_view(), name='user-retrieve-update'),
    path('change-password/', views.ChangePasswordView.as_view(), name='change-password'),
    path('email/confirm/', views.SendConfirmationEmailView.as_view(), name='send-confirm-email'),
    path('email/confirm/<uidb64>/<token>/', views.ConfirmEmailView.as_view(), name='confirm-email'),
]
