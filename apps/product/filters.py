"""Filters for app Product."""
from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters

from product import models


class ProductBackFilterSet(filters.FilterSet):
    """Product filter set for back."""
    product_type = filters.CharFilter(method='by_product_type')
    product_subtype = filters.CharFilter(method='by_product_subtype')
    product_state = filters.CharFilter(method='by_product_state')
    all_states = filters.BooleanFilter(method='by_all_states')
    deleted = filters.BooleanFilter(method='by_deleted')

    class Meta:
        """Meta class."""
        model = models.Product
        fields = [
            'product_type',
            'product_subtype',
            'product_state',
        ]

    def by_product_type(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_product_type(value)
        return queryset

    def by_product_subtype(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_product_subtype(value)
        return queryset

    def by_product_state(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            value_list = [int(val) for val in value.split(',')]
            return queryset.by_product_states(value_list)
        return queryset

    def by_all_states(self, queryset, name, value):
        if value:
            return queryset.by_all_states(value)
        return queryset

    def by_deleted(self, queryset, name, value):
        if value:
            return queryset.by_deleted(value)
        return queryset


class ProductFilterSet(filters.FilterSet):
    """Product filter set."""

    establishment_id = filters.NumberFilter()
    current_product = filters.CharFilter(method='without_current_product')
    product_type = filters.CharFilter(method='by_product_type')
    product_subtype = filters.CharFilter(method='by_product_subtype')

    class Meta:
        """Meta class."""
        model = models.Product
        fields = [
            'establishment_id',
            'product_type',
            'product_subtype',
        ]

    def without_current_product(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.without_current_product(value)
        return queryset

    def by_product_type(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_product_type(value)
        return queryset

    def by_product_subtype(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_product_subtype(value)
        return queryset
