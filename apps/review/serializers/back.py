"""Review app back serializers."""

from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers

from account.models import User
from location.serializers import CityBaseSerializer, WineRegionBaseSerializer, RegionSerializer
from review.models import Review, ReviewTextAuthor, Inquiries
from review.serializers import UserBaseSerializer


class _ReviewerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
        )


class _ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = (
            'id',
            'app_label',
            'model',
        )


class _ReviewTextAuthorSerializer(serializers.ModelSerializer):
    author = _ReviewerSerializer(read_only=True)

    class Meta:
        model = ReviewTextAuthor
        fields = (
            'id',
            'author',
            'locale',
            'modified',
        )


class ReviewLastInquiryReadSerializer(serializers.ModelSerializer):
    author = UserBaseSerializer(read_only=True, allow_null=True)

    class Meta:
        model = Inquiries
        fields = (
            'id',
            'final_comment',
            'mark',
            'status',
            'author',
            'updated_on',
            'visited_at',
        )


class ReviewCityReadSerializer(CityBaseSerializer):

    class Meta(CityBaseSerializer.Meta):
        fields = [field for field in CityBaseSerializer.Meta.fields
                  if field not in ('region', 'country')]


class ReviewBackBaseSerializer(serializers.ModelSerializer):
    reviewer_data = _ReviewerSerializer(read_only=True, source='reviewer')
    content_type_data = _ContentTypeSerializer(read_only=True, source='content_type')
    status_display = serializers.CharField(read_only=True, source='get_status_display')
    authors = _ReviewTextAuthorSerializer(read_only=True, many=True, source='text_authors')

    class Meta:
        model = Review
        fields = [
            'id',
            'reviewer',
            'reviewer_data',
            'text',
            'status',
            'status_display',
            'mark',
            'priority',
            # 'child',
            'published_at',
            'vintage',
            # 'country',
            'content_type',
            'content_type_data',
            'object_id',
            'authors',
        ]


class ReviewBackCreateSerializer(ReviewBackBaseSerializer):

    def create(self, validated_data):
        obj = super().create(validated_data)
        new_text = validated_data['text']

        # если нет переводов для review - то ничего не делаем
        if not isinstance(new_text, dict):
            return obj

        # если пользователь анонимный - то ничего не делаем
        user = self.context['request'].user
        if user.is_anonymous:
            return obj

        for locale, text in new_text.items():
            ReviewTextAuthor.objects.create(
                author=user,
                review=obj,
                locale=locale,
            )

        return obj


class ReviewBackDetailSerializer(ReviewBackBaseSerializer):

    def update(self, instance, validated_data):
        old_text = instance.text
        new_text = validated_data['text']

        # если поле text не менялось - то ничего не делаем
        if new_text == old_text:
            return super().update(instance, validated_data)

        # если пользователь анонимный - то ничего не делаем
        user = self.context['request'].user
        if user.is_anonymous:
            return super().update(instance, validated_data)

        for locale, text in new_text.items():

            # если поменяли имеющийся текст
            if locale in old_text and text != old_text[locale]:
                review_author, _ = ReviewTextAuthor.objects.update_or_create(
                    review=instance,
                    locale=locale,
                    defaults={
                        'author': user,
                    }
                )

            # если добавили новый перевод
            elif locale not in old_text:
                ReviewTextAuthor.objects.create(
                    author=user,
                    review=instance,
                    locale=locale,
                )

        for locale in old_text:

            # Если удалили перевод
            if locale not in new_text:
                ReviewTextAuthor.objects.filter(
                    author=user,
                    review=instance,
                    locale=locale,
                ).delete()

        return super().update(instance, validated_data)


class ReviewBackListSerializer(ReviewBackBaseSerializer):
    establishment_city = ReviewCityReadSerializer(read_only=True)
    establishment_region = RegionSerializer(read_only=True)
    establishment_sub_region = RegionSerializer(read_only=True)
    wine_region = WineRegionBaseSerializer(read_only=True)
    last_inquiry = ReviewLastInquiryReadSerializer(read_only=True)

    class Meta(ReviewBackBaseSerializer.Meta):
        fields = ReviewBackBaseSerializer.Meta.fields + [
            'object_slug',
            'object_type',
            'producer_name',
            'wine_region',
            'establishment_city',
            'establishment_region',
            'establishment_sub_region',
            'last_inquiry',
        ]


class ReviewBackCSVSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = (
            'id',
            'vintage',
            'mark',
        )
