"""

Структура записи в card:
Название таблицы в postgresql: {
    "data_type": "тип данных в таблице (словарь, объект, дочерний объект и так далее)",
    "dependencies": кортеж с зависимостями от других таблиц в postgresql,
    "fields": список полей для таблицы postgresql, пример:
    {
        "название legacy таблицы": {
            список полей для переноса, пример структуры описан далее
        },
        "relations": список зависимостей legacy-таблицы, пример:
        {
            # имеет внешний ключ на "название legacy таблицы" из  "fields"
            "название legacy таблицы": {
                "key": ключ для связи. Строка, если тип поля в legacy таблице - ForeignKey, или кортеж из названия поля
                в дочерней таблице и названия поля в родительской таблице в ином случае
                "fields": {
                    список полей для переноса, пример структуры описан далее
                }
            }
        }
    },
    "relations": список внешних ключей таблицы postgresql, пример структуры описан далее
    {
        "Cities": [(
            (None, "region_code"),
            ("Region", "region", "code", "CharField")),
            ((None, "country_code_2"),
             ("Country", "country", "code", "CharField"))
        ]
    }
},


Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

Структура внешних ключей:
"legacy_table" - спикок кортежей для сопоставления полей
"legacy_table": [
    (("legacy_key", "legacy_field"),
    ("psql_table", "psql_key", "psql_field", "psql_field_type"))
], где:
legacy_table - название модели legacy
legacy_key - ForeignKey в legacy
legacy_field - уникальное поле в модели legacy для сопоставления с postgresql
psql_table - название модели psql
psql_key - ForeignKey в postgresql
psql_field - уникальное поле в модели postgresql для сопоставления с legacy
psql_field_type - тип уникального поля в postgresql


NOTE: среди legacy таблиц совпадение для таблицы Address не найдено (Возможно для Address подходит Locations в legacy)
"""

card = {
    "Country": {
        "data_type": "dictionaries",
        "dependencies": None,
        "fields": {
            "Cities": {
                "code": "country_code_2",
            }
        }
    },

    "Region": {
        # subregion_code -> code
        # region_code -> parent_region
        # MySQL: select distinct(country_code_2) from cities where subregion_code is not NULL;
        # только у пяти стран есть понятие "subregion_code"(не NULL): fr, be, ma, aa, gr (country_code_2)
        # Возможно получиться обойтись без изменений модели Region
        "data_type": "dictionaries",
        "dependencies": ("Country", "Region"),
        "fields": {
            # нету аналога для поля name
            "Cities": {
                "code": "region_code",
            },
        },
        "relations": {
            "Cities": [(
                (None, "country_code_2"),
                ("Country", "country", "code", "django.db.models.CharField")),
                # ((None, "subregion_code"), #TODO: как сопоставлять parent_region из postgres с subregion_code из legacy ?
                #  ("Region", "parent_region", "code", "CharField"))

            ]
        }
    },

    "City": {
        "data_type": "dictionaries",
        "dependencies": ("Country", "Region"),
        "fields": {
            "Cities": {
                "name": "name",
                "code": "country_code_2",
                "postal_code": "zip_code",
                "is_island": ("is_island", "is_island", "django.db.models.Boolean")
            }
        },
        "relations": {
            "Cities": [(
                (None, "region_code"),
                ("Region", "region", "code", "django.db.models.CharField")),
                ((None, "country_code_2"),
                 ("Country", "country", "code", "django.db.models.CharField"))
            ]
        }
    },


    "Address": {
        "data_type": "dictionaries",
        "dependencies": ("City",),
        "fields": {
            # нету аналога для поля number
            "Locations": {
                "postal_code": "zip_code",
                "coordinates": (("lat", "latitude"), ("long", "longitude"))
            },
        },
        "relations": {
            "Cities": [ #TODO: Locations ссылается внешним ключом на Cities
                (("city", None),
                 ("City", "city", None, None))
            ]
        }
    }
}


used_apps = None
