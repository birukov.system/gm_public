from datetime import datetime, timedelta
from http.cookies import SimpleCookie

from rest_framework import status
from rest_framework.test import APITestCase

from account.models import Role, User, UserRole
from location.models import Country
from main.models import SiteSettings
from news.models import News, NewsType
from notification.models import Subscriber, SubscriptionType
from translation.models import Language


class BaseTestCase(APITestCase):

    def setUp(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)
        # get tokens
        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie({
            'access_token': tokens.get('access_token'),
            'refresh_token': tokens.get('refresh_token')
        })


class NotificationAnonSubscribeTestCase(APITestCase):

    def test_subscribe(self):
        test_data = {
            "email": "test@email.com"
        }

        response = self.client.post("/api/web/notifications/subscribe/", data=test_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["email"], test_data["email"])


class NotificationSubscribeTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()

        self.test_data = {
            "email": self.email
        }

    def test_subscribe(self):
        response = self.client.post("/api/web/notifications/subscribe/", data=self.test_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["email"], self.email)

    def test_subscribe_info_auth_user(self):
        Subscriber.objects.create(user=self.user, email=self.email)

        response = self.client.get("/api/web/notifications/subscribe-info/", data=self.test_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NotificationSubscribeInfoTestCase(APITestCase):

    def test_subscribe_info(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)

        test_subscriber = Subscriber.objects.create(user=self.user, email=self.email)

        response = self.client.get(f"/api/web/notifications/subscribe-info/{test_subscriber.update_code}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NotificationUnsubscribeAuthUserTestCase(BaseTestCase):

    def test_unsubscribe_auth_user(self):
        Subscriber.objects.create(user=self.user, email=self.email)

        self.test_data = {
            "email": self.email
        }

        response = self.client.patch("/api/web/notifications/unsubscribe/", data=self.test_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NotificationUnsubscribeTestCase(APITestCase):

    def test_unsubscribe(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)

        self.test_data = {
            "email": self.email
        }

        test_subscriber = Subscriber.objects.create(user=self.user, email=self.email)

        response = self.client.patch(f"/api/web/notifications/unsubscribe/{test_subscriber.update_code}/",
                                     data=self.test_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class NotificationManySubscribeTestCase(APITestCase):

    def test_subscribe(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)

        # get tokens
        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie(
            {
                'access_token': tokens.get('access_token'),
                'refresh_token': tokens.get('refresh_token')
            })
        self.test_news_type = NewsType.objects.create(name="Test news type")

        self.lang, created = Language.objects.get_or_create(
            title='Russia',
            locale='ru-RU'
        )

        self.country_ru, created = Country.objects.get_or_create(
            name={"en-GB": "Russian"}
        )

        self.site_ru, created = SiteSettings.objects.get_or_create(
            subdomain='ru'
        )

        role = Role.objects.create(
            role=Role.CONTENT_PAGE_MANAGER,
            site_id=self.site_ru.id
        )
        role.save()

        user_role = UserRole.objects.create(
            user=self.user,
            role=role
        )
        user_role.save()

        self.test_news = News.objects.create(
            created_by=self.user, modified_by=self.user,
            title={"ru-RU": "Test news"},
            news_type=self.test_news_type,
            description={"ru-RU": "Description test news"},
            end=datetime.now() + timedelta(hours=2),
            state=News.PUBLISHED,
            slugs={'en-GB': 'test-news-slug'},
            country=self.country_ru,
            site=self.site_ru
        )

        self.test_subscribe_type = SubscriptionType.objects.create(
            index_name='test_index_name',
            name={"ru-RU": "Test subscription type"}
        )

        test_data = {
            'email': self.email,
            'subscription_types_pk': [
                self.test_subscribe_type.id
            ]
        }

        response = self.client.post("/api/web/notifications/subscribe/", data=test_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()["email"], test_data["email"])

    def test_unsubscribe(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)

        test_data = {
            "email": self.email
        }

        test_subscriber = Subscriber.objects.create(user=self.user, email=self.email)

        response = self.client.patch(f"/api/web/notifications/unsubscribe/{test_subscriber.update_code}/",
                                     data=test_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
