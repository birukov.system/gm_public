# Generated by Django 2.2.7 on 2019-11-12 15:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comment', '0005_remove_comment_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='is_publish',
            field=models.BooleanField(default=False, verbose_name='Publish status'),
        ),
    ]
