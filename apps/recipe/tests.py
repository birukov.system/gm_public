from http.cookies import SimpleCookie


from rest_framework.test import APITestCase
from rest_framework import status

from account.models import User
from recipe.models import Recipe


class BaseTestCase(APITestCase):

    def setUp(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)
        tokkens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie({'access_token': tokkens.get('access_token'),
                                            'refresh_token': tokkens.get('refresh_token')})
        self.test_recipe = Recipe.objects.create(title={"en-GB": "test title"}, description={"en-GB": "test description"},
                                                 state=2, author="Test Author", created_by=self.user,
                                                 modified_by=self.user)

    def test_recipe_list(self):
        response = self.client.get("/api/web/recipes/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recipe_detail(self):
        response = self.client.get(f"/api/web/recipes/{self.test_recipe.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
