"""Establishment app documents."""
from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from establishment import models
from gallery.models import Image as GalleryImage
from search_indexes.utils import trigram_analyzer
from account import models as account_models
from timetable import models as timetable_models
from location import models as location_models
from tag import models as tag_models
from product import models as product_models
from review import models as review_models


EstablishmentIndex = Index(settings.ELASTICSEARCH_INDEX_NAMES.get(__name__,
                                                                  'establishment'))
EstablishmentIndex.settings(number_of_shards=5, number_of_replicas=2, max_result_window=500000)


@EstablishmentIndex.doc_type
class EstablishmentDocument(Document):
    """Establishment document."""

    preview_image = fields.KeywordField(attr='preview_image_url')
    description = fields.KeywordField(attr='description_serialized')
    description_searchable = fields.ListField(fields.TextField(analyzer=trigram_analyzer))
    last_updated = fields.DateField()  # for ordering via bo. see prop declared on establishment model
    establishment_type = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.KeywordField(attr='name_serialized'),
            'index_name': fields.KeywordField(attr='index_name'),
            'default_image': fields.KeywordField(attr='default_image_url'),
            'preview_image_url': fields.KeywordField(),
        })
    establishment_subtypes = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.ObjectField(attr='name_indexing'),
            'name_serialized': fields.KeywordField(),
            'index_name': fields.KeywordField(attr='index_name'),
            'default_image': fields.KeywordField(attr='default_image_url'),
            'preview_image_url': fields.KeywordField(),
        },
        multi=True)
    works_evening = fields.ListField(fields.IntegerField(
        attr='works_evening'
    ))
    works_noon = fields.ListField(fields.IntegerField(
        attr='works_noon'
    ))
    works_at_weekday = fields.ListField(fields.IntegerField(
        attr='works_at_weekday'
    ))
    works_now = fields.BooleanField(attr='works_now')
    tags = fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        multi=True)
    restaurant_category = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        attr='restaurant_category_indexing'))
    restaurant_cuisine = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        attr='restaurant_cuisine_indexing'))
    artisan_category = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        }, attr='artisan_category_indexing'))
    distillery_type = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        }, attr='distillery_type_indexing'))
    food_producer = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        }, attr='food_producer_indexing'))
    visible_tags = fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        multi=True)
    products = fields.ObjectField(
        properties={
            'wine_origins': fields.ListField(
                fields.ObjectField(
                    properties={
                        'wine_region': fields.ObjectField(properties={
                            'id': fields.IntegerField(),
                            'name': fields.KeywordField(),
                            'country': fields.ObjectField(properties={
                                'id': fields.IntegerField(),
                                'name': fields.KeywordField(attr='name_serialized'),
                                'code': fields.KeywordField(),
                            }),
                            # 'coordinates': fields.GeoPointField(),
                            'description': fields.KeywordField(attr='description_serialized'),
                        }),
                        'wine_sub_region': fields.ObjectField(properties={
                            'id': fields.IntegerField(),
                            'name': fields.KeywordField(),
                        }),
                    })),
            'wine_colors': fields.ObjectField(
                properties={
                    'id': fields.IntegerField(),
                    'label': fields.KeywordField(attr='label_serialized'),
                    'value': fields.KeywordField(),
                },
                multi=True,)},
        multi=True
    )
    actual_employees_indexing = fields.ListField(
        fields.ObjectField(properties={
            'id': fields.IntegerField()
        })
    )

    wine_origins = fields.ListField(
        fields.ObjectField(
            properties={
                'wine_region': fields.ObjectField(properties={
                    'id': fields.IntegerField(),
                    'name': fields.TextField(analyzer=trigram_analyzer),
                    'country': fields.ObjectField(properties={
                        'id': fields.IntegerField(),
                        'name': fields.KeywordField(attr='name_serialized'),
                        'code': fields.KeywordField(),
                    }),
                    # 'coordinates': fields.GeoPointField(),
                    'description': fields.KeywordField(attr='description_serialized'),

                }),
                'wine_sub_region': fields.ObjectField(properties={
                    'id': fields.IntegerField(),
                    'name': fields.KeywordField(),
                })})
    )
    schedule = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'weekday': fields.IntegerField(attr='weekday'),
            'weekday_display': fields.KeywordField(attr='get_weekday_display'),
            'weekday_display_short': fields.KeywordField(attr='weekday_display_short'),
            'closed_at': fields.KeywordField(attr='closed_at_str'),
            'opening_at': fields.KeywordField(attr='opening_at_str'),
            'closed_at_indexing': fields.DateField(),
            'opening_at_indexing': fields.DateField(),
        }
    ))
    team_members = fields.ListField(fields.ObjectField(
        properties={
            'username': fields.TextField(analyzer=trigram_analyzer)
        }
    ))
    contact_phones = fields.ListField(
        fields.TextField(analyzer=trigram_analyzer)
    )
    contact_emails = fields.ListField(
        fields.TextField(analyzer=trigram_analyzer)
    )
    address = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'street_name_1': fields.TextField(analyzer=trigram_analyzer),
            'street_name_2': fields.TextField(analyzer=trigram_analyzer),
            'number': fields.IntegerField(),
            'postal_code': fields.TextField(analyzer=trigram_analyzer),
            'coordinates': fields.GeoPointField(attr='location_field_indexing'),
            'district_name': fields.TextField(analyzer=trigram_analyzer),
            'city': fields.ObjectField(
                properties={
                    'id': fields.IntegerField(),
                    'name': fields.KeywordField(attr='name_dumped'),
                    'name_searchable': fields.TextField(analyzer=trigram_analyzer),
                    'name_translated': fields.KeywordField(),
                    'is_island': fields.BooleanField(),
                    'country': fields.ObjectField(
                        properties={
                            'id': fields.IntegerField(),
                            'name': fields.KeywordField(attr='name_serialized'),
                            'name_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
                            'code': fields.KeywordField(),
                        }
                    ),
                    'region': fields.ObjectField(
                        properties={
                            'id': fields.IntegerField(),
                            'code': fields.KeywordField(),
                            'name': fields.KeywordField(),
                            'parent_region': fields.ObjectField(
                                properties={
                                    'id': fields.IntegerField(),
                                    'code': fields.KeywordField(),
                                    'name': fields.KeywordField(),
                                    'country': fields.ObjectField(
                                        properties={
                                            'id': fields.IntegerField(),
                                            'name': fields.KeywordField(attr='name_serialized'),
                                            'code': fields.KeywordField(),
                                        }
                                    ),
                                }
                            ),
                            'country': fields.ObjectField(
                                properties={
                                    'id': fields.IntegerField(),
                                    'name': fields.KeywordField(attr='name_serialized'),
                                    'code': fields.KeywordField(),
                                }
                            ),
                        }
                    )
                }
            ),
        },
    )
    favorites_for_users = fields.ListField(field=fields.IntegerField())
    tz = fields.KeywordField(attr='timezone_as_str')
    created = fields.DateField(attr='created')
    transliterated_name = fields.TextField(analyzer=trigram_analyzer)
    _main_image = fields.ObjectField(
        properties={
            'type': fields.KeywordField(),
            'id': fields.IntegerField(),
            'image': fields.KeywordField(attr='image_indexing'),
            'link': fields.KeywordField(),
            'preview': fields.KeywordField(attr='preview_indexing'),
            'cropped_image': fields.KeywordField(attr='cropped_image_indexing'),
        }
    )
    # awards = fields.ListField(fields.ObjectField(
    #     properties={
    #         'id': fields.IntegerField(),
    #         'vintage_year': fields.KeywordField(),
    #         'title': fields.KeywordField(attr='title_serialized'),
    #         'title_searchable': fields.TextField(analyzer=trigram_analyzer),
    #         'award_type': fields.ObjectField(
    #             properties={
    #                 'id': fields.IntegerField(),
    #                 'name': fields.TextField(analyzer=trigram_analyzer),
    #                 'created': fields.DateField(),
    #                 'modified': fields.DateField(),
    #             }
    #         )
    #     }
    # ))

    reviews = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'text': fields.KeywordField(attr='text_serialized'),
            'text_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'status': fields.IntegerField(),
            'vintage': fields.IntegerField(),
            'mark': fields.FloatField(),
            'toque_number': fields.IntegerField(),
        }
    ))
    last_published_review = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'text': fields.KeywordField(attr='text_serialized'),
            'status': fields.IntegerField(),
            'vintage': fields.IntegerField(),
            'mark': fields.FloatField(),
            'toque_number': fields.IntegerField(),
        }
    )
    most_recent_review = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'text': fields.KeywordField(attr='text_serialized'),
            'status': fields.IntegerField(),
            'vintage': fields.IntegerField(),
            'mark': fields.FloatField(),
            'toque_number': fields.IntegerField(),
        }
    )
    last_published_and_most_recent_reviews = fields.ListField(
        fields.ObjectField(
            properties={
                'id': fields.IntegerField(),
                'text': fields.KeywordField(attr='text_serialized'),
                'status': fields.IntegerField(),
                'vintage': fields.IntegerField(),
                'mark': fields.FloatField(),
                'toque_number': fields.IntegerField(),
            }
        )
    )
    vintage_year = fields.IntegerField(attr='vintage_year_indexing')
    bo_type = fields.KeywordField()
    last_inquiry_author = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'username': fields.KeywordField(),
        }
    )
    transportation = fields.TextField(analyzer=trigram_analyzer)
    # emails = fields.ListField(fields.)
    generic_types = fields.ListField(field=fields.KeywordField())
    is_pop = fields.BooleanField()
    name = fields.TextField(analyzer=trigram_analyzer)

    class Django:

        model = models.Establishment
        fields = (
            'id',
            'index_name',
            'is_publish',
            'price_level',
            'toque_number',
            'public_mark',
            'slug',
            'status',
        )
        related_models = (
            GalleryImage, models.EstablishmentGallery,  # gallery
            account_models.UserRole,  # account
            timetable_models.Timetable, timetable_models.Holiday,  # working schedule
            location_models.Address,  # establishment address
            models.ContactEmail, models.ContactPhone,  # contacts
            models.EstablishmentSubType, models.EstablishmentType, tag_models.Tag,  # classification
            location_models.WineOriginAddress,
            review_models.Review, review_models.Inquiries,
            models.EstablishmentEmployee, models.Employee,
        )

    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, GalleryImage):
            return related_instance.establishment_set.first()
        if isinstance(related_instance, models.EstablishmentGallery):
            return related_instance.establishment
        if isinstance(related_instance, account_models.UserRole):
            return related_instance.establishment
        if isinstance(related_instance, timetable_models.Timetable):
            return related_instance.schedule
        if isinstance(related_instance, timetable_models.Holiday):
            return related_instance.establishment
        if isinstance(related_instance, location_models.Address):
            return related_instance.establishment_set.all()
        if isinstance(related_instance, models.ContactPhone):
            return related_instance.establishment
        if isinstance(related_instance, models.ContactEmail):
            return related_instance.establishment
        if isinstance(related_instance, models.EstablishmentType):
            return related_instance.establishment.all()
        if isinstance(related_instance, models.EstablishmentSubType):
            return related_instance.establishment_type.establishment.all()
        # if isinstance(related_instance, tag_models.Tag):
        #     return related_instance.establishments.all()
        if isinstance(related_instance, product_models.Product):
            return related_instance.establishment
        if isinstance(related_instance, location_models.WineOriginAddress):
            return related_instance.product.establishment
        if isinstance(related_instance, review_models.Review):
            if related_instance.is_establishment_review:
                return related_instance.content_object
        if isinstance(related_instance, review_models.Inquiries):
            review = related_instance.review
            if review.is_establishment_review:
                return review.content_object
        if isinstance(related_instance, models.Employee):
            return related_instance.establishments.all()
        if isinstance(related_instance, models.EstablishmentEmployee):
            return related_instance.establishment

    def get_queryset(self):
        return super().get_queryset().with_es_related()
