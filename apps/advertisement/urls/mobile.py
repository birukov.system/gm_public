"""Advertisement common urlpaths."""
from django.urls import path

from advertisement.views import mobile as views
from .common import common_urlpatterns


app_name = 'advertisements'

urlpatterns = [
    path('', views.AdvertisementPageTypeMobileListView.as_view(), name='list'),
    path('<page_type>/', views.AdvertisementPageTypeMobileListView.as_view(), name='list'),
]

urlpatterns += common_urlpatterns
