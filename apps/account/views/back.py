import csv
from datetime import datetime

from django.http import HttpResponse, HttpResponseNotFound
from django.db.models.query import prefetch_related_objects, Prefetch
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework import permissions
from rest_framework.authtoken.models import Token
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from account import models, filters
from account.models import User, Role
from account.serializers import back as serializers
from account.serializers.common import RoleBaseSerializer
from establishment.models import Establishment
from utils.methods import get_permission_classes
from utils.permissions import IsReviewManager


class RoleListView(generics.ListCreateAPIView):
    """
    ## Role List/Create view.
    ### **GET**
    #### Description
    Return non-paginated list of roles in order to **ROLE_ORDER**.
    #### Available filters
    * country_code (`str`) - country code
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```

    ### **POST**
    #### Description
    Create a new role.
    ##### Request
    Required
    * site_id (`int`) - identifier of a site settings
    * role (`int`) - enum
    ```
    STANDARD_USER = 1
    MODERATOR = 2
    COUNTRY_ADMIN = 3
    CONTENT_PAGE_MANAGER = 4
    ESTABLISHMENT_MANAGER = 5
    REVIEW_MANAGER = 6
    RESTAURANT_INSPECTOR = 7
    SALES_MAN = 8
    WINERY_WINE_INSPECTOR = 9
    SELLER = 10
    DISTILLERY_LIQUOR_INSPECTOR = 11
    PRODUCER_FOOD_INSPECTOR = 12
    ESTABLISHMENT_ADMINISTRATOR = 13
    ARTISAN_INSPECTOR = 14
    TRANSLATOR = 15
    SUPERADMIN = 16
    ```
    Non-required
    * country_id (`int`) - identifier of a country
    * establishment_subtype_id (`int`) - identifier of an establishment subtype
    * navigation_bar_permission_id (`int`) - identifier of a navigation bar permission
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = RoleBaseSerializer
    filter_class = filters.RoleListFilter
    permission_classes = get_permission_classes()
    pagination_class = None

    def get_queryset(self):
        qs = models.Role.objects\
            .annotate_order_pos(models.Role.ROLE_ORDER)

        return qs.exclude(
            role__in=[
                models.Role.ESTABLISHMENT_ADMINISTRATOR,
                models.Role.SALES_MAN,
            ]
        ).order_by('role_order_pos')


class RoleTypeRetrieveView(generics.GenericAPIView):
    """
    ## Generic list view for user roles.
    ### **GET**
    #### Description
    Return non-paginated list of roles with user counts
    ##### Response
    E.g.
    ```
    [
        {
          "role": 4,
          "role_name": "Content page manager",
          "count": 6
        },
        ...
    ]
    ```
    """
    permission_classes = get_permission_classes()

    def get_queryset(self):
        return models.UserRole.objects.all()

    def get(self, request, *args, **kwargs):
        """
        ## Generic list view for user roles.
        ### **GET**
        #### Description
        Return non-paginated list of roles with user counts
        ##### Response
        E.g.
        ```
        [
            {
              "role": 4,
              "role_name": "Content page manager",
              "count": 6
            },
            ...
        ]
        ```
        """
        country_code = None

        if self.request.user.is_country_admin and hasattr(self.request, 'country_code'):
            country_code = self.request.country_code

        data = self.get_queryset().aggregate_role_counter(country_code)
        return Response(data, status=status.HTTP_200_OK)


class UserRoleListView(generics.ListCreateAPIView):
    """
    ## User role List/Create
    ### **GET**
    #### Description
    Return paginated list of a user roles.
    ##### Response
    E.g.
    ```
    {
        "count": 0,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST**
    #### Description
    Create a new link between user and role.
    ##### Request
    * role (`int`) - identifier of a role
    * user (`int`) - identifier of a user
    Non-required
    * establishment (`int`) - identifier of an establishment
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.UserRoleSerializer
    queryset = models.UserRole.objects.all()
    permission_classes = get_permission_classes()


class UserListView(generics.ListCreateAPIView):
    """User list create view with 'roles' and 'subscriber'
    ### **GET**
    #### Description
    Returning list of users with roles and subscriber
    #### Ordering fields:
    email_confirmed, is_staff, is_active, is_superuser, last_login, date_joined
    #### Filter fields:
    role, email_confirmed, is_staff, is_active, is_superuser, search, role_country_code
    ##### Response
    Available for a role ReviewManager
    ```
    {
      "count": 58,
      "next": null,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```

    ### **POST**
    #### Description
    Create a new user.
    ##### Request
    Required
    * password (`str`)
    * email (`str`)
    Non-required
    * is_superuser (`bool`) - flag that responds for type of account
    * username (`str`)
    * last_name (`str`)
    * first_name (`str`)
    * is_active (`bool`) - flag that responds for account activity state
    * image_url (`str`) - user image URL address
    * cropped_image_url (`str`) - user cropped image URL address
    * email_confirmed (`bool`) - flag that responds for state of email
    * unconfirmed_email (`str`)
    * newsletter (`bool`) - flag that responds for state of newsletter subscription
    * subscriptions (`list`) - list of subscription_type identifiers
    * phone (`str`)
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """

    serializer_class = serializers.BackUserSerializer
    queryset = User.objects.with_extend_related()
    filter_class = filters.AccountBackOfficeFilter
    filter_backends = (OrderingFilter, DjangoFilterBackend)
    permission_classes = get_permission_classes(IsReviewManager)

    ordering_fields = (
        'email_confirmed',
        'is_staff',
        'is_active',
        'is_superuser',
        'last_login',
        'date_joined',
    )


class UserRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## User RUD view.
    ### **GET**
    #### Description
    Return serialized instance of a user by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a user object by an identifier.
    ##### Request
    * is_superuser (`bool`) - flag that responds for type of account
    * username (`str`)
    * last_name (`str`)
    * first_name (`str`)
    * is_active (`bool`) - flag that responds for account activity state
    * image_url (`str`) - user image URL address
    * cropped_image_url (`str`) - user cropped image URL address
    * email (`str`)
    * email_confirmed (`bool`) - flag that responds for state of email
    * unconfirmed_email (`str`)
    * newsletter (`bool`) - flag that responds for state of newsletter subscription
    * subscriptions (`list`) - list of subscription_type identifiers
    * phone (`str`)
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete a user instance by an identifier
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    queryset = User.objects.all()
    serializer_class = serializers.BackDetailUserSerializer
    lookup_field = 'id'
    permission_classes = get_permission_classes()

    def get_object(self):
        obj = super().get_object()
        prefetch_related_objects([obj], Prefetch('roles', queryset=Role.objects.order_by('userrole__created')))
        return obj

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        serializer = self.get_serializer(self.get_object())  # crunch for ordering roles
        return Response(serializer.data)


def get_user_csv(request, id):
    """User CSV file download"""
    # fields = ["id", "uuid", "nickname", "locale", "country_code", "city", "role", "consent_purpose", "consent_at",
    #           "last_seen_at", "created_at", "updated_at", "email", "is_admin", "ezuser_id", "ez_user_id",
    #           "encrypted_password", "reset_password_token", "reset_password_sent_at", "remember_created_at",
    #           "sign_in_count", "current_sign_in_at", "last_sign_in_at", "current_sign_in_ip", "last_sign_in_ip",
    #           "confirmation_token", "confirmed_at", "confirmation_sent_at", "unconfirmed_email", "webpush_subscription"]

    # uuid == id
    #
    # Не найдены:
    # consent_purpose
    # consent_at
    # ezuser_id
    # ez_user_id
    # remember_created_at
    # sign_in_count
    # current_sign_in_at
    # current_sign_in_ip
    # last_sign_in_ip
    # confirmed_at
    # confirmation_sent_at
    # webpush_subscription
    #
    # country_code не получить - клиент не привязан к стране

    try:
        user = User.objects.get(id=id)
    except User.DoesNotExist:
        return HttpResponseNotFound("User not found")

    try:
        roles = " ".join([role for role in user.roles])
    except:
        roles = ""

    token, _ = Token.objects.get_or_create(user=user)

    fields = {
        "id": user.id,
        "uuid": user.id,
        "username": getattr(user, "username", ""),
        "locale": getattr(user, "locale", ""),
        "city": getattr(user, "city", ""),
        "role": roles,
        "created_at": getattr(user, "date_joined", ""),
        "updated_at": user.last_login,
        "email": user.email,
        "is_admin": user.is_superuser,
        "encrypted_password": user.password,
        "reset_password_token": token.key,
        "reset_password_sent_at": token.created,  # TODO: не уверен в назначении поля, лучше проверить
        "last_sign_in_at": user.last_login,  # Повтор?
        "confirmation_token": user.confirm_email_token,
        "unconfirmed_email": 1 if user.unconfirmed_email else 0
    }

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = f'attachment; filename="{user.email}.csv"'

    writer = csv.writer(response)
    writer.writerow(fields.keys())
    writer.writerow(fields.values())

    return response


class UserCSVViewSet(ModelViewSet):
    """
    ## User CSV-viewset.
    ### **GET**
    #### Description
    Method that allows getting information about user (by user identifier)
    in `csv` format.
    Response content-type: 'text/csv'
    ##### Response
    E.g.
    ```
    id,uuid,username,locale,city,role,created_at,updated_at,email,is_admin,encrypted_password,reset_password_token,reset_password_sent_at,last_sign_in_at,confirmation_token,unconfirmed_email
    1,1,admin,en-GB,,,2019-08-12 07:37:25.967348+00:00,2020-02-26 14:13:15.608696+00:00,admin@mail.com,True,pbkdf2_sha256$150000$LocNKmAbafIy$NWWD1XDl563rtpZ+n2emWJVQLnnlPoadiMla6fH8mKg=,2b004ca74e8e58139a85dff2638f8fdab7505659,2020-02-26 14:40:57.393723+00:00,2020-02-26 14:13:15.608696+00:00,5eb-efb0b38e03211b8d74e1,0

    ```
    """
    http_method_names = ['get', ]
    queryset = User.objects.all().with_base_related().with_extend_related()
    serializer_class = serializers.UserCSVSerializer
    permission_classes = (permissions.IsAdminUser,)
    filter_class = filters.AccountBackOfficeFilter
    filter_backends = (OrderingFilter, DjangoFilterBackend)
    pagination_class = None

    ordering_fields = (
        'email_confirmed',
        'is_staff',
        'is_active',
        'is_superuser',
        'last_login',
        'date_joined',
    )

    def to_csv(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = serializers.UserCSVSerializer(queryset, many=True)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="users_{datetime.now().date()}.csv"'

        writer = csv.writer(response)
        writer.writerow((
            'id',
            'nickname',
            'email',
            'locale',
            'last_country',
            'created_at',
            'confirmed_at',
            'last_seen_at',
            'last_ip',
            'role',
        ))

        for item in serializer.data:
            writer.writerow(item.values())

        return response


class EstablishmentTeamInviteView(generics.CreateAPIView):
    """View to invite new team member by email"""
    queryset = User.objects.all()
    # permission_classes = get_permission_classes()
    from rest_framework.permissions import AllowAny
    permission_classes = (AllowAny, )
    serializer_class = serializers.InviteTeamSerializer

    def create(self, request, *args, **kwargs):
        self.kwargs['establishment'] = get_object_or_404(klass=Establishment, pk=self.kwargs['establishment_id'])
        return super().create(request, *args, **kwargs)
