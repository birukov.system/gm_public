from pprint import pprint

from django.db.models import F

from transfer.models import CarouselElements
from transfer.serializers.carousel import CarouselSerializer


def transfer_carousel():
    queryset = CarouselElements.objects.all().annotate(
        country=F('home_page__site__country_code_2'),
    )

    serialized_data = CarouselSerializer(data=list(queryset.values()), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f'Carousel serializer errors: {serialized_data.errors}')


data_types = {
    'whirligig': [transfer_carousel]
}
