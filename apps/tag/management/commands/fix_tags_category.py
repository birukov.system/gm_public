from django.core.management.base import BaseCommand
from tqdm import tqdm

from location.models import Country
from django.db import models
from tag.models import Tag, TagCategory
from translation.models import SiteInterfaceDictionary


class Command(BaseCommand):
    help = 'Fix tags category'

    to_delete = ('collection', 'guide')

    # old_category -> old_name: (public(bool), new_name(str), rank(int), country(str), tags(str))
    to_refactor = {
        'service': (True, 'restaurant_service', 4, None,
                    'private_parking, valet_parking, air_conditioning, handicap_access, pet_friendly, terrace, byow, byow_free, continue, delivery, take_away, cooking_lessons, smoking_area, kid_meal, live_music, live_show, wifi, food_drinks_pairing, accomodation, child_friendly, credit_cards_accepted, garden, with_a_view, rooftop, cigar_lounge'),
        'category': (True, 'restaurant_category', 1, None,
                     'gastronomic, canteen, bistro_brasserie, wine_bar, tea_shop, coffee_shop, bar_cocktail, snacking, food_truck, classic, pub, luxury, tavern, todays_food, fine_dining, beer_bar, veggie_friendly'),
        'moment': (True, 'restaurant_moment', 2, None,
                   'for_lovers, gangs, with_kids, alone, brunch, tea_time, lunch, apertizer, en_vogue, business, outside, work_friendly, casual, mountain_restaurant, breakfast, afterwork, aperitif, romantic_charming, beach, family, fine_dining'),
        'tag': (False, 'restaurant_advantage', 5, None,
                'pop, great_cave, great_view, favorite, guide_ad, microbrewery, good_deal, great_beer, rss, rss_selection, private_room, outside_sits, new'),
        'cuisine': (True, 'restaurant_cuisine', 3, None,
                    'african, american, arab, argentinian, asian, australian, author_cuisine, belgian, brazilian, bucarican, burger, caribbean, chinese, churrascaria, creole, crepes, dessert, eastern, european, fish, french, fusion, german, greek, guyanese, halal, healthy, hungarian, indian, indian_ocean, israeli, italian, japanese, korean, kosher, latino, lebanese, local, mediterranean, mexican, middle-eastern, modern, moroccan, nordic, no_glu, organic, palestinian, persian, peruvian, pizza, portuguese, romanian, russian, sausage_stand, sea_food, smokehouse, spanish, steakhouse, streetfood, tapas, thai, traditional_style, turkish, vegan, vegetarian, vietnamese, international, balkan, corsican, georgian, jewish, polish, sushi, ukrainian'),
        'purchased_item': (False, 'restaurant_purchased_item', 0, None,
                           'plaque_2018, plaque_2019, region_guide_2019_ad'),
        'accepted_payments_hr': (False, 'restaurant_accepted_payments_hr', 0, 'HR',
                                 'Mastercard, Maestro'),
        'drinks': (False, 'restaurant_drinks', 0, None,
                   'wine, beer, local, spirits, cocktails, tea, coffee, juice_smoothies, sake, rhum'),
        'shop_category': (True, 'artisan_type', 1, None,
                          'butchery, delicatessen, bakery, patisserie, coffe_shop, chocolate_shop, confectioner, cheese_shop, market, fish_shop, rotisserie, tea_shop, coffee_shop, caterer, fine_grocery, oil-mill, jam_shop, cookie_factory, distillery, beer_cellar, breeder, ice-cream_maker, frozen_food, wine_merchant, greengrocer, organic, producer, bar_cocktail, winegrower'),
        'shop_service': (True, 'artisan_service', 2, None,
                         'on_site_tasting, wholesame, webstore, tailored, on_order, bulk_sale, travel_packaging, private_parking, handicap_access, cooking_lessons, delivery'),
        'activity': (True, 'artisan_activity', 0, 'FR',
                     'roaster, refiner, beekeeper, breeder, brewer, jam_maker, caterer'),
        'mono_product': (False, 'mono_product', 0, 'FR',
                         'sea_food, jam, biscuits, caviar, oil, honey, rhum, whisky, liquor, armagnac, cognac, calvados'),
        'certification': (True, 'artisan_certification', 3, 'FR',
                          'organic, gluten_free, vegetarian, organization, 100_local'),
        'wineyard_tag': (True, 'winery_certification', 2, None,
                         'cooperative, dealer, independant, conventional, reasoned, organic, organic_conversion, biodynamics, biodynamics_conversion, terra_vitis, hve1, hve2, hve3, champagne_vdc'),
        'business_tag': (False, 'business_tag', 0, 'FR',
                         'target_welcomebox_2017, welcomebox_2017, not_responding, address_change, no_business_contact, cholat'),
        'guide_settings': (False, 'guide_settings', 0, 'FR',
                           'itineraire_gourmand'),
        'winery_service': (False, 'winery_service', 1, None,
                           'accomodation, catering, visit, on_site_sell'),
        'distillery_type': (True, 'distillery_type', 1, None,
                            'whishy, rum, vodka, cognac, armagnac, porto, sherry, brandy'),
        'distillery_service': (True, 'distillery_service', 2, None,
                               'accomodation, catering, visit, on_site_sell'),
        'distillery_certification': (True, 'distillery_certification', 3, None, ''),
        'producer_type': (True, 'producer_type', 1, None, ''),
        'producer_service': (True, 'producer_service', 2, None, 'accomodation, catering, visit, on_site_sell'),
        'producer_certification': (True, 'producer_certification', 3, None, ''),
    }

    @staticmethod
    def get_country(code):
        if code:
            return Country.objects.filter(code__iexact=code).first()

    def handle(self, *args, **kwargs):
        for tag_category in tqdm(self.to_delete, desc='delete not needed categories'):
            TagCategory.objects.filter(index_name__iexact=tag_category).delete()

        for tag_category, data in tqdm(self.to_refactor.items(), desc='refactor categories'):
            # delete old tag category with new name
            old_category = TagCategory.objects.filter(index_name__iexact=data[1]).delete()
            category, _ = TagCategory.objects.update_or_create(
                index_name__iexact=tag_category,
                defaults={
                    'public': data[0],
                    'index_name': data[1],
                    'priority': data[2],
                    'country': self.get_country(data[3]),
                },
            )

            if not data[4]:
                continue

            tags = (val.strip() for val in data[4].split(','))
            for item in tqdm(tags, desc=f'check tags for category {data[1]}'):
                tags_qs = Tag.objects.filter(value__iexact=item)
                if tags_qs:
                    for tag in tags_qs:
                        tag.category = category
                        tag.save()
                else:
                    new_tag = Tag.objects.create(
                        value=item,
                        category=category,
                    )
                    text_value = ' '.join(item.split('_'))
                    translation = SiteInterfaceDictionary(
                        text={'en-GB': text_value},
                        keywords=f'tag.{new_tag.category}.{new_tag.value}',
                    )
                    translation.save()
                    new_tag.translation = translation
                    category.save()
        self.stdout.write(self.style.WARNING(f'Fix tags categories for Establishment'))
