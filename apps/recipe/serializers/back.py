"""Recipe app back serializers."""
from rest_framework import serializers

from recipe.serializers.common import RecipeDetailSerializer


class RecipeRUDSerializer(RecipeDetailSerializer):
    """Serializer for more information about the recipe."""
    state_display = serializers.CharField(read_only=True, source='get_state_display')
    must_of_the_week = serializers.BooleanField(required=False)

    class Meta(RecipeDetailSerializer.Meta):
        """Meta class."""
        fields = [
            'id',
            'title',
            'subtitle',
            'description',
            'state',
            'author',
            'published_at',
            'published_scheduled_at',
            'slug',
            'title_translated',
            'subtitle_translated',
            'in_favorites',
            'must_of_the_week',
            'state_display',
        ]
        extra_kwargs = {
            'title': {'write_only': True, 'required': True},
            'subtitle': {'write_only': True},
            'state': {'write_only': True},
            'description': {'write_only': True},
            'author': {'required': True},
            'slug': {'required': True},
        }
