"""Advertisement app models."""
import uuid

from django.apps import apps
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from location.models import Country
from main.models import Page
from translation.models import Language
from utils.models import ProjectBaseMixin, ImageMixin, PlatformMixin, URLImageMixin


class AdvertisementQuerySet(models.QuerySet):
    """QuerySet for model Advertisement."""

    def active(self):
        return self.filter(
            models.Q(start__lte=timezone.now(), end__gte=timezone.now()) |
            models.Q(start__lte=timezone.now(), end__isnull=True),
        )

    def inactive(self):
        return self.filter(
            models.Q(start__gt=timezone.now())
            | models.Q(end__lt=timezone.now(), end__isnull=False)
        )

    def with_base_related(self):
        """Return QuerySet with base related"""
        return self.select_related('target_language', 'page')

    def by_page_type(self, page_type: str):
        """Filter Advertisement by page type."""
        return self.filter(page__page_type=page_type)

    def by_country(self, code: str):
        """Filter Advertisement by country code."""
        return self.filter(target_language__country__code=code)

    def by_locale(self, locale):
        """Filter by locale."""
        return self.filter(models.Q(target_language__locale=locale) | models.Q(target_language__isnull=True))

    def valid(self):
        """Return only valid advertisements."""
        return self.filter(end__lte=timezone.now())

    def filter_by_page_type(self, filter_types):
        """Return filtered values by page types"""
        found_types = [t[0] for t in Page.PAGE_TYPES if t[0] in filter_types]

        if len(found_types):
            return self.filter(page__page_type__in=found_types)

        return self

    def filter_by_device(self, devices):
        """Return filtered values by device."""
        found_devices = [d[0] for d in Page.SOURCES if d[0] in [int(device) for device in devices]]

        if len(found_devices):
            return self.filter(page__source__in=found_devices)

        return self

    def filter_by_state(self, state: bool):
        """Return filtered values by state."""
        return self.active() if state else self.inactive()


class Advertisement(ProjectBaseMixin):
    """Advertisement model."""

    TOP = 'top'
    LEVEL_1 = '1'
    LEVEL_2 = '2'
    LEVEL_3 = '3'

    BLOCK_LEVEL_CHOICES = (
        (TOP, _('top')),
        (LEVEL_1, _('level 1')),
        (LEVEL_2, _('level 2')),
        (LEVEL_3, _('level 3')),
    )

    STATUS_ON = 1
    STATUS_OFF = 0

    STATUS_CHOICES = (
        (STATUS_ON, _('ON')),
        (STATUS_OFF, _('OFF')),
    )

    ONLY_TOP_LEVELS = (TOP,)
    TOP_TO_LEVEL_1 = (TOP, LEVEL_1)
    TOP_TO_LEVEL_2 = (TOP, LEVEL_1, LEVEL_2)
    TOP_TO_LEVEL_3 = (TOP, LEVEL_1, LEVEL_2, LEVEL_3)

    LEVELS_FOR_PAGE_TYPE = {
        Page.HOMEPAGE_TYPE: TOP_TO_LEVEL_3,
        Page.ALL_ESTABLISHMENTS_TYPE: TOP_TO_LEVEL_2,
        Page.RESTAURANTS_TYPE: TOP_TO_LEVEL_2,
        Page.ARTISANS_TYPE: TOP_TO_LEVEL_2,
        Page.WINERIES_TYPE: TOP_TO_LEVEL_2,
        Page.DISTILLERIES_TYPE: TOP_TO_LEVEL_2,
        Page.FOOD_PRODUCERS_TYPE: TOP_TO_LEVEL_2,
        Page.ALL_PRODUCTS_TYPE: TOP_TO_LEVEL_2,
        Page.FOOD_TYPE: TOP_TO_LEVEL_2,
        Page.LIQUORS_TYPE: TOP_TO_LEVEL_2,
        Page.WINES_TYPE: TOP_TO_LEVEL_2,
        Page.LIST_OF_NEWS_TYPE: TOP_TO_LEVEL_2,
        Page.NEWS_AND_RECIPES_TYPE: TOP_TO_LEVEL_1,
        Page.NEWS_TYPE: TOP_TO_LEVEL_1,
        Page.RECIPES_TYPE: TOP_TO_LEVEL_1,
    }

    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    url = models.URLField(verbose_name=_('Ad URL'))
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_OFF,
                                              verbose_name=_('Status'))
    block_level = models.CharField(verbose_name=_('Block level'),
                                   max_length=10,
                                   blank=True,
                                   null=True,
                                   choices=BLOCK_LEVEL_CHOICES)
    target_language = models.ForeignKey(Language, null=True, on_delete=models.SET_NULL)
    start = models.DateTimeField(null=True, verbose_name=_('start'))
    end = models.DateTimeField(blank=True, null=True, default=None, verbose_name=_('end'))
    duration = models.FloatField(default=5.0, verbose_name=_('display duration in seconds'),
                                 validators=[MinValueValidator(0.01)])
    frequency_percentage = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(100)], default=100,
                                             verbose_name=_('Probability to show'))
    views_count = models.IntegerField(default=0, verbose_name=_('How many times shown'))
    objects = AdvertisementQuerySet.as_manager()

    class Meta:
        verbose_name = _('Advertisement')
        verbose_name_plural = _('Advertisements')

    def __str__(self):
        return str(self.url)

    @property
    def page_type(self):
        return self.page.page_type

    @property
    def page_type_detail(self):
        return {
            'value': self.page_type,
            'title': self.get_page_type_title(),
            'levels': self.get_available_block_levels(),
        }

    @property
    def image_url(self):
        return self.page.image_url

    @property
    def device(self):
        return self.page.source

    def get_page_type_title(self):
        page_type_choice = next(iter(filter(lambda x: x[0] == self.page_type, self.page.PAGE_TYPES)))
        page_type_title = page_type_choice[1]
        return page_type_title

    def get_available_block_levels(self):
        if self.device in (self.page.MOBILE, self.page.ALL):
            return self.TOP_TO_LEVEL_1

        return self.LEVELS_FOR_PAGE_TYPE[self.page_type]

    @classmethod
    def get_devices_for_page_type(cls, page_type):
        Page = apps.get_model(app_label='main', model_name='page')

        if page_type not in map(lambda x: x[0], Page.PAGE_TYPES):
            return []

        # devices for any page type are the same, it can be changed in the future
        return list(map(lambda x: {'value': x[0], 'title': x[1]}, Page.SOURCES))
