from django.core.management.base import BaseCommand

from establishment.models import Establishment
from transfer.models import Establishments


class Command(BaseCommand):
    help = 'Add publish values from old db to new db'

    def handle(self, *args, **kwargs):
        old_establishments = Establishments.objects.all()
        count = 0
        for item in old_establishments:
            obj = Establishment.objects.filter(old_id=item.id).first()
            if obj:
                count += 1
                obj.is_publish = item.state == 'published'
                obj.save()
        self.stdout.write(self.style.WARNING(f'Updated {count} objects.'))
