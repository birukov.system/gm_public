from django.conf import settings
from django.core.management.base import BaseCommand
from tqdm import tqdm

from main.models import FooterLink


class Command(BaseCommand):
    help = "Move value of title to translated field with default locale."

    def handle(self, *args, **kwargs):
        objects = []
        model_field_names = [field.name for field in FooterLink._meta.fields]
        if 'title' in model_field_names and 'link_label' in model_field_names:
            for footer_link in tqdm(FooterLink.objects.all()):
                footer_link.link_label = {settings.FALLBACK_LOCALE: footer_link.title}
                objects.append(footer_link)
            FooterLink.objects.bulk_update(objects, ['link_label', ])
        self.stdout.write(self.style.WARNING(f'Updated {len(objects)} FooterLink.'))
