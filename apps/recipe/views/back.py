"""Recipe app back views."""
from rest_framework import generics
from utils.methods import get_permission_classes
from utils.permissions import IsContentPageManager
from recipe.serializers import RecipeRUDSerializer
from recipe.models import Recipe
from recipe.filters import RecipeBackOfficeFilterSet
from recipe.views import RecipeViewMixin


class RecipeBackOfficeMixin(RecipeViewMixin):
    """Mixin for a back office views."""

    serializer_class = RecipeRUDSerializer
    permission_classes = get_permission_classes(
        IsContentPageManager
    )

    def get_queryset(self, *args, **kwargs):
        """An overridden get_queryset method."""
        return Recipe.objects.all()


class RecipeListCreateView(RecipeBackOfficeMixin, generics.ListCreateAPIView):
    """
    ## List/Create view.
    ### *GET*
    #### Description
    Return non-paginated list of recipes.
    Available filters:
    * state (`enum`) - filter by state
    ```
    WAITING = 0
    HIDDEN = 1
    PUBLISHED = 2
    PUBLISHED_EXCLUSIVE = 3
    ```
    > For multiple filtering, use next syntax for query string,
    e.g.: ?state=1&state=2
    ##### Response
    E.g.:
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    ### *POST*
    #### Description
    Create a new recipe
    ##### Request
    ###### Required
    * title ('JSON') - title of recipe
    * subtitle ('JSON') - subtitle of recipe
    * description ('JSON') - recipe description
    * slug ('str') - recipe slug
    ###### Non-required
    * state ('enum') -
    ```
    WAITING = 0
    HIDDEN = 1
    PUBLISHED = 2
    PUBLISHED_EXCLUSIVE = 3
    ```
    * author ('str') - author recipe
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    """

    filter_class = RecipeBackOfficeFilterSet


class RecipeRUDView(RecipeBackOfficeMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve/Update/Destroy view.
    ### *GET*
    #### Description
    Retrieve serialized object of Recipe model by a slug.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a recipe object by a slug.
    ##### Request
    Available
    * title ('str') - title of recipe
    * subtitle ('str') - subtitle of recipe
    * description ('str') - recipe description
    * state ('enum') -
    ```
    WAITING = 0
    HIDDEN = 1
    PUBLISHED = 2
    PUBLISHED_EXCLUSIVE = 3
    ```
    * author ('str') - author recipe
    * slug ('str') - recipe slug
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of recipe by a slug.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
