"""Serializers for mobile app advertisements"""
from advertisement.serializers import AdvertisementSerializer
from main.serializers import PageBaseSerializer


class AdvertisementPageTypeMobileListSerializer(AdvertisementSerializer):
    """Serializer for AdvertisementPageTypeMobileView."""

    pages = PageBaseSerializer(many=True, source='mobile_pages', read_only=True)

    class Meta(AdvertisementSerializer.Meta):
        """Meta class."""
        fields = AdvertisementSerializer.Meta.fields + [
            'pages',
            'duration',
            'views_count',
        ]
