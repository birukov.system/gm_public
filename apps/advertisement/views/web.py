"""Web views for app advertisement"""
from advertisement.serializers import AdvertisementPageTypeWebListSerializer
from .common import AdvertisementPageTypeListView


class AdvertisementPageTypeWebListView(AdvertisementPageTypeListView):
    """Advertisement mobile list view."""
    pagination_class = None
    serializer_class = AdvertisementPageTypeWebListSerializer
