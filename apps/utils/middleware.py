"""Custom middlewares."""
import logging

from django.conf import settings
from django.utils import translation, timezone
from django.db import connection
from oauth2_provider.middleware import OAuth2TokenMiddleware

from account.models import User
from configuration.models import TranslationSettings
from main.methods import determine_user_city
from main.models import SiteSettings
from translation.models import Language
from utils.views import JWTGenericViewMixin

logger = logging.getLogger(__name__)


def get_locale(cookie_dict):
    return cookie_dict.get('locale')


def get_country_code(cookie_dict):
    return cookie_dict.get('country_code')


def parse_cookies(get_response):
    """Parse cookies."""

    def middleware(request):
        cookie_dict = request.COOKIES
        # processing locale cookie
        locale = get_locale(cookie_dict)
        # todo: don't use DB!!! Use cache
        if not Language.objects.filter(locale=locale).exists():
            locale = TranslationSettings.get_solo().default_language
        translation.activate(locale)
        request.locale = locale

        # processing country country cookie
        country_code = get_country_code(cookie_dict)
        if country_code and country_code != 'None':
            # Custom mapping for Antilles country codes
            if country_code.lower() in settings.COUNTRY_CODE_ALIASES_ANTILLES_GUYANE_WEST_INDIES:
                country_code = 'aa'

            request.country_code = country_code
        else:
            request.country_code = None

        response = get_response(request)
        return response

    return middleware


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def user_last_ip(get_response):
    """Update user last ip address and last country"""

    def middleware(request):
        response = get_response(request)
        current_ip = get_client_ip(request)

        country_code = request.COOKIES.get('country_code')
        current_site = SiteSettings.objects.by_country_code(country_code).first()
        if not current_site:
            current_site = SiteSettings.objects.filter(subdomain='www').first()

        if request.user.is_authenticated:
            request.user.last_login = timezone.now()
            request.locale = request.locale
            request.city = determine_user_city(request)
            request.user.last_ip = current_ip
            request.user.last_country = current_site
            request.user.save()
        return response

    return middleware


def log_db_queries_per_API_request(get_response):
    """Middleware-helper to optimize requests performance"""

    def middleware(request):
        total_time = 0
        response = get_response(request)
        for query in connection.queries:
            query_time = query.get('time')
            if query_time is None:
                query_time = query.get('duration', 0) / 1000
            total_time += float(query_time)

        total_queries = len(connection.queries)
        if total_queries > 10:
            logger.error(
                f'\t{len(connection.queries)} queries run, total {total_time} seconds \t'
                f'URL: "{request.method} {request.get_full_path_info()}"'
            )
        return response

    return middleware


class CustomOAuth2TokenMiddleware(OAuth2TokenMiddleware, JWTGenericViewMixin):
    def process_response(self, request, response):
        updated_access_token = request.COOKIES.get('updated_access_token', None)
        if updated_access_token:
            access_token_cookie = self.COOKIE(key='access_token',
                                              value=updated_access_token,
                                              http_only=self.ACCESS_TOKEN_HTTP_ONLY,
                                              max_age=None,
                                              secure=self.ACCESS_TOKEN_SECURE)

            response = self._put_cookies_in_response([access_token_cookie], response)

        return super().process_response(request, response)
