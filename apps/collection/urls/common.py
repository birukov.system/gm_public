"""Collection common urlpaths."""
from django.urls import path

from collection.views import common as views

app_name = 'collection'

urlpatterns = [
    path('', views.CollectionHomePageView.as_view(), name='list'),
    path('slug/<slug:slug>/', views.CollectionDetailView.as_view(), name='detail'),
    path('slug/<slug:slug>/establishments/', views.CollectionEstablishmentListView.as_view(),
         name='detail'),
]
