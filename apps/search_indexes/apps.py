from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SearchIndexesConfig(AppConfig):
    name = 'search_indexes'
    verbose_name = _('Search indexes')
