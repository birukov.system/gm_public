"""Search indexes utils."""
from utils.models import get_current_locale, get_default_locale
from elasticsearch_dsl import analyzer, tokenizer

FACET_MAX_RESPONSE = 9999999  # Unlimited

trigram_analyzer = analyzer('trigram_analyzer',
                            tokenizer=tokenizer('trigram', 'nGram', min_gram=3, max_gram=4),
                            filter=['lowercase', 'elision', 'kstem']
                            )


# todo: refactor serializer
def get_translated_value(value):
    if value is None:
        return None
    elif not isinstance(value, dict):
        field_dict = value.to_dict()
    else:
        field_dict = value
    field_dict = {k: v for k, v in field_dict.items() if v is not None}
    result = field_dict.get(get_current_locale(), None)
    # fallback
    if result is None:
        result = field_dict.get(get_default_locale(), None)
        if result is None:
            values = list(field_dict.values())
            result = values[0] if values else None
    return result
