from django.core.management.base import BaseCommand
from django.utils.text import slugify

from tag.models import TagCategory
from transfer import models as transfer_models


class Command(BaseCommand):
    help = 'Fix wine color tag'

    def handle(self, *args, **kwarg):
        queryset = transfer_models.Cepages.objects.all()
        cepage_list = [slugify(i) for i in queryset.values_list('name', flat=True)]
        tag_categories = TagCategory.objects.filter(index_name__in=cepage_list)
        deleted_tag_categories = tag_categories.count()
        tag_categories.delete()
        self.stdout.write(self.style.WARNING(f"Deleted tag categories: {deleted_tag_categories}"))
