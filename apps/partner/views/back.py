from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import OrderingFilter

from establishment.views import EstablishmentLastUpdatedMixin
from partner import filters
from partner.models import Partner, PartnerToEstablishment
from partner.serializers import back as serializers
from utils.methods import get_permission_classes
from utils.permissions import IsEstablishmentManager, IsEstablishmentAdministrator


class PartnerLstView(generics.ListCreateAPIView):
    """
    ## List/Create view
    ### *GET*
    #### Description
    Return non-paginated queryset. Available filters:
    * establishment () - filter by establishment identifier
    * type (`enum`) - filter by partner type
    ```
    Partner - 0
    Sponsor - 1
    ```
    * ordering (`str`) - set ordering by `partner_bind_date` field
    ##### Response
    E.g.:
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    ### *POST*
    #### Description
    Create new partner
    ##### Request
    Available:
    * name (`str`) - partner name
    * images (`list`) - array of an images
    * type (`int`) - partner type
    ```
    Partner - 0
    Sponsor - 1
    ```
    * country_id (`int`) - identifier of country
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    queryset = Partner.objects.with_base_related()
    serializer_class = serializers.BackPartnerSerializer
    pagination_class = None
    filter_class = filters.PartnerFilterSet
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )


class EstablishmentPartners(generics.ListAPIView):
    """
    ## List view
    ### *GET*
    #### Description
    Return non-paginated list of establishment partners
    by `establishment slug`.
    Ordering by a field - `partner_bind_date` (descending).
    Available ordering fields: `__all__`
    Available filters:
    * type (`int`) - partner type
    ```
    Partner - 0
    Sponsor - 1
    ```
    #### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    queryset = PartnerToEstablishment.objects.with_base_related()
    serializer_class = serializers.PartnersForEstablishmentSerializer
    pagination_class = None
    filter_class = filters.PartnerToEstablishmentFilterSet
    filter_backends = (OrderingFilter, DjangoFilterBackend)
    ordering_fields = '__all__'
    ordering = '-partner_bind_date'
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        return super().get_queryset().filter(establishment=self.kwargs['establishment_id'])


class PartnerRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve/Update/Destroy view
    ### *GET*
    #### Description
    Return serialized object of partner
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a partner object.
    ##### Request
    Available:
    * name (`str`) - partner name
    * images (`list`) - array of images
    * type (`int`) - partner type
    ```
    Partner - 0
    Sponsor - 1
    ```
    * country_id (`int`) - identifier of country
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete a partner
    ##### Response
    E.g.:
    ```
    No content
    ```
    """
    queryset = Partner.objects.with_base_related()
    serializer_class = serializers.BackPartnerSerializer
    lookup_field = 'id'
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )


class PartnerPicturesListView(generics.RetrieveAPIView):
    """
    ## List view
    ### *GET*
    #### Description
    Return serialized object of Partner model by `partner identifier`,
    only with a parameter `images`.
    ##### Response
    E.g.:
    ```
    {
        "images": ["http://google.com"],
        ...
    }
    ```
    """
    lookup_field = 'id'
    serializer_class = serializers.PartnerPicturesSerializer
    queryset = Partner.objects.with_base_related()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )


class BindPartnerToEstablishmentView(EstablishmentLastUpdatedMixin, generics.CreateAPIView):
    """
    ## Binding a partner to establishment
    ### *POST*
    #### Description
    Bind existing partner to an establishment.
    ##### Request
    In `URL kwargs`:
    * partner_id (`int`) - identifier of a partner
    * establishment_id (`int`) - identifier of an establishment
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.PartnersForEstablishmentSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'


class UnbindPartnerFromEstablishmentView(EstablishmentLastUpdatedMixin, generics.DestroyAPIView):
    """
    ## Unbinding a partner from establishment
    ### *DELETE*
    #### Description
    Unbind an existing partner from an establishment.
    ##### Request
    In `URL kwargs`:
    * partner_id (`int`) - identifier of a partner
    * establishment_id (`int`) - identifier of an establishment
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.PartnersForEstablishmentSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_object(self):
        ret = get_object_or_404(PartnerToEstablishment, pk=self.kwargs['bind_id'])
        establishment = ret.establishment
        setattr(self, 'cached_establishment', establishment)
        return ret

    def get_establishment_for_last_updated_mixin(self):
        return getattr(self, 'cached_establishment')
