from rest_framework.test import APITestCase
from rest_framework import status
from account.models import User
from http.cookies import SimpleCookie
from django.urls import reverse

# Create your tests here.
from translation.models import Language
from account.models import Role, UserRole
from location.models import Country, Address, City, Region
from main.models import SiteSettings
from product.models import Product, ProductType

class BaseTestCase(APITestCase):
    def setUp(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.newsletter = True
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
            is_staff=True,
        )
        # get tokens
        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('refresh_token')})


        self.lang = Language.objects.create(
            title='Russia',
            locale='ru-RU'
        )

        self.country_ru = Country.objects.create(
            name={'en-GB': 'Russian'},
            code='RU',
        )

        self.region = Region.objects.create(name='Moscow area', code='01',
                                            country=self.country_ru)
        self.region.save()

        self.city = City.objects.create(
            name='Mosocow', code='01',
            region=self.region,
            country=self.country_ru)
        self.city.save()

        self.address = Address.objects.create(
            city=self.city, street_name_1='Krasnaya',
            number=2, postal_code='010100')
        self.address.save()

        self.site = SiteSettings.objects.create(
            subdomain='ru',
            country=self.country_ru
        )

        self.site.save()

        self.role = Role.objects.create(role=Role.LIQUOR_REVIEWER,
                                        site=self.site)
        self.role.save()

        self.user_role = UserRole.objects.create(
             user=self.user, role=self.role)

        self.user_role.save()

        self.product_type = ProductType.objects.create(index_name=ProductType.LIQUOR)
        self.product_type.save()

        self.product = Product.objects.create(name='Product')
        self.product.save()



class LiquorReviewerTests(BaseTestCase):
    def test_get(self):
        self.product.product_type = self.product_type
        self.product.save()

        url = reverse("back:product:list-create")
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse("back:product:rud", kwargs={'pk': self.product.id})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_patch_put_delete(self):
        data_post = {
                "slug": None,
                "public_mark": None,
                "vintage": None,
                "average_price": None,
                "description": None,
                "available": False,
                "establishment": None,
                "wine_village": None,
                "state": Product.PUBLISHED,
                "site_id": self.site.id,
                "product_type_id": self.product_type.id
        }
        url = reverse("back:product:list-create")
        response = self.client.post(url, data=data_post, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data_patch = {
            'name': 'Test product'
        }
        url = reverse("back:product:rud", kwargs={'pk': self.product.id})
        response = self.client.patch(url, data=data_patch, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)



