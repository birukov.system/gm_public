"""Serializers for app favorites."""
from .models import Favorites
from rest_framework import serializers
from establishment.serializers import EstablishmentBaseSerializer
