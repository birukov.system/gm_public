"""Employee model index."""
from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from search_indexes.utils import trigram_analyzer
from establishment.models import Employee, Establishment, EstablishmentEmployee, Position
from main.models import Award, AwardType
from django.db.models.query import Prefetch, prefetch_related_objects

EmployeesIndex = Index(settings.ELASTICSEARCH_INDEX_NAMES.get(__name__, 'employee'))
EmployeesIndex.settings(number_of_shards=2, number_of_replicas=1, max_result_window=500000)


@EmployeesIndex.doc_type
class EmployeeDocument(Document):

    name = fields.TextField(analyzer=trigram_analyzer)
    last_name = fields.TextField(analyzer=trigram_analyzer)
    toque_number_indexing = fields.ListField(fields.IntegerField())
    public_mark_indexing = fields.ListField(fields.FloatField())
    toque_number = fields.IntegerField(attr='toque_number_max')
    public_mark = fields.IntegerField(attr='public_mark_max')
    positions_indexing = fields.ListField(
        fields.ObjectField(properties={
            'id': fields.IntegerField(),
            'priority': fields.IntegerField(),
            'index_name': fields.KeywordField(),
            'name_translated': fields.KeywordField(),
        })
    )
    awards = fields.ListField(fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'vintage_year': fields.KeywordField(),
            'title': fields.KeywordField(attr='title_serialized'),
            'title_searchable': fields.TextField(analyzer=trigram_analyzer),
            'award_type': fields.ObjectField(
                properties={
                    'id': fields.IntegerField(),
                    'name': fields.TextField(analyzer=trigram_analyzer),
                    'created': fields.DateField(),
                    'modified': fields.DateField(),
                }
            )
        }
    ))

    class Django:
        model = Employee
        fields = (
            'id',
            'modified',
            'sex',
            'available_for_events',
        )
        related_models = [
            Establishment, EstablishmentEmployee, Position, Award
        ]

    def get_instances_from_related(self, related_instance):
        # todo retrieve employees related to certain AwardType
        if isinstance(related_instance, Award):
            establishments = list(related_instance.establishment.all())
            if len(establishments):
                prefetch_related_objects(establishments, Prefetch('employees', queryset=Employee.objects.all()))
                employees = [list(est.employees.all()) for est in establishments]
                return [item for sublist in employees for item in sublist]
        if isinstance(related_instance, Establishment):
            return related_instance.employees.all()
        if isinstance(related_instance, EstablishmentEmployee):
            return related_instance.employee
        if isinstance(related_instance, Position):
            return [ee.employee for ee in related_instance.establishmentemployee_set.all()]

    def get_queryset(self):
        return super().get_queryset().with_back_office_related()
