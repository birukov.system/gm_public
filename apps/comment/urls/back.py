"""Back comment URLs"""
from django.urls import path

from comment.views import back as views

app_name = 'comment'

urlpatterns = [
    path('', views.CommentLstView.as_view(), name='comment-list-create'),
    path('<int:id>/', views.CommentRUDView.as_view(), name='comment-crud'),
    path('<str:type>/', views.CommentLstView.as_view(), name='comment-list-by-type-create'),
    path('<str:type>/<int:object>', views.CommentLstView.as_view(), name='comment-list-by-type-object-create'),
]
