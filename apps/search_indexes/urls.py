"""Search indexes app urlconf."""
from rest_framework import routers
from search_indexes import views


router = routers.SimpleRouter()

router.register(r'establishments', views.EstablishmentDocumentViewSet, basename='establishment')
router.register(r'mobile/establishments', views.MobileEstablishmentDocumentViewSet, basename='establishment-mobile')
router.register(r'news', views.NewsDocumentViewSet, basename='news')
router.register(r'mobile/news', views.MobileNewsDocumentViewSet, basename='news-mobile')
router.register(r'products', views.ProductDocumentViewSet, basename='product')
router.register(r'mobile/products', views.MobileProductDocumentViewSet, basename='product-mobile')

urlpatterns = router.urls

