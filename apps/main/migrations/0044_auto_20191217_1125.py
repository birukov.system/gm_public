# Generated by Django 2.2.7 on 2019-12-17 11:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0043_auto_20191217_1120'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='FooterLinks',
            new_name='FooterLink',
        ),
    ]
