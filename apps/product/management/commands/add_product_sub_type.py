from django.conf import settings
from django.core.management.base import BaseCommand

from product.models import ProductType, ProductSubType


class Command(BaseCommand):
    help = 'Add product sub type data'

    def handle(self, *args, **kwarg):
        error_counter = 0
        create_counter = 0

        product_subtypes = {
            'plate': {
                'product_type_index_name': 'souvenir',
                'name': 'plate',
                'index_name': 'plate',
            }
        }
        for product_subtype in product_subtypes.values():
            product_type_qs = ProductType.objects.filter(
                index_name=product_subtype.get('product_type_index_name'))
            if product_type_qs:
                product_type = product_type_qs.first()
                subtype, status = ProductSubType.objects.get_or_create(**{
                    'name': {settings.FALLBACK_LOCALE: product_subtype.get('name')},
                    'index_name': product_subtype.get('index_name'),
                    'product_type': product_type
                })
                create_counter += 1 if status else 0
            else:
                error_counter += 1
        self.stdout.write(self.style.WARNING(f'Errors occurred: {error_counter}\nCreated: {create_counter}'))
