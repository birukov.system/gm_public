CODE_LOCALES = {
    'AAA': 'en-GB',
    'AUS': 'en-AU',  # Австралия
    'AUT': 'de-AT',  # Австрия
    'BEL': 'de-BE',  # Бельгия
    'BRA': 'pt-BR',  # Бразилия
    'CAN': 'fr-CA',  # Канада
    'DEU': 'da-DE',  # Германия
    'FRA': 'fr-FR',  # Франция
    'GEO': 'ka',  # Грузия
    'GRC': 'el-GR',  # Греция
    'HRV': 'hr-HR',  # Хорватия
    'HUN': 'hu-HU',  # Венгрия
    'ISR': 'en-IL',  # Израиль
    'ITA': 'it-IT',  # Италия
    'JPN': 'ja',  # Япония
    'LUX': 'fr-LU',  # Люксембург
    'MAR': 'ar-MA',  # Марокко
    'MDV': 'dv',  # Мальдивы
    'NLD': 'nl-NL',  # Нидерланды
    'POL': 'pl',  # Польша
    'ROU': 'ro',  # Румыния
    'RUS': 'ru-RU',  # Россия
    'SVN': 'hu-SI',  # Словения
    'USA': 'en-US',  # США
}
