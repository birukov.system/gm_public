# Generated by Django 2.2.7 on 2019-12-10 13:49

import django.contrib.postgres.fields.hstore
from django.db import migrations
from django.contrib.postgres.operations import HStoreExtension

def migrate_slugs(apps, schemaeditor):
    News = apps.get_model('news', 'News')
    for news in News.objects.all():
        if news.slug:
            news.slugs = {'en-GB': news.slug}
            news.save()


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0038_news_backoffice_title'),
    ]

    operations = [
        HStoreExtension(),
        migrations.AddField(
            model_name='news',
            name='slugs',
            field=django.contrib.postgres.fields.hstore.HStoreField(blank=True, default=None, help_text='{"en-GB":"some slug"}', null=True, verbose_name='Slugs for current news obj'),
        ),
        migrations.RunPython(migrate_slugs, migrations.RunPython.noop)
    ]
