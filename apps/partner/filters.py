"""Partner app filters."""
from django_filters import rest_framework as filters

from partner.models import Partner, PartnerToEstablishment
from django.core.validators import EMPTY_VALUES


class PartnerFilterSet(filters.FilterSet):
    """Establishment filter set."""

    establishment = filters.NumberFilter(
        help_text='Allows to get partner list by establishment ID.')
    type = filters.ChoiceFilter(
        choices=Partner.MODEL_TYPES,
        help_text=f'Allows to filter partner list by partner type. '
                  f'Enum: {dict(Partner.MODEL_TYPES)}')
    ordering = filters.CharFilter(method='sort_partner')

    class Meta:
        """Meta class."""
        model = Partner
        fields = (
            'establishment',
            'type',
            'ordering',
        )

    @staticmethod
    def sort_partner(queryset, name, value):
        if value not in EMPTY_VALUES:
            if 'date_bind' in value:
                value = value.replace('date_bind', 'partnertoestablishment__partner_bind_date')
            queryset = queryset.order_by(value)
        return queryset


class PartnerToEstablishmentFilterSet(filters.FilterSet):
    """Partner to establishment filter set."""

    type = filters.ChoiceFilter(
        field_name='partner__type',
        choices=Partner.MODEL_TYPES,
        help_text=f'Allows to filter partner list by partner type. '
                  f'Enum: {dict(Partner.MODEL_TYPES)}')

    class Meta:
        model = PartnerToEstablishment
        fields = (
            'type',
        )
