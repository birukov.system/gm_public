"""Gallery urlpaths."""
from django.urls import path

from . import views

app_name = 'gallery'

urlpatterns = [
    path('', views.ImageListCreateView.as_view(), name='list-create'),
    path('for_establishment/<int:establishment_id>/', views.MediaForEstablishmentView.as_view(),
         name='establishment-media'),
    path('media/<int:pk>/', views.MediaUpdateView.as_view(), name='media-update'),
    path('media/as-file/<int:pk>/', views.MediaPictureDownloadView.as_view(), name='media-file-get'),
    path('<int:pk>/', views.ImageRetrieveDestroyView.as_view(), name='retrieve-destroy'),
    path('<int:pk>/crop/', views.CropImageCreateView.as_view(), name='create-crop'),
    path('<int:pk>/resize/', views.ResizeImageCreateView.as_view(), name='create-resize'),
]
