from rest_framework import generics
from rest_framework.response import Response

from translation.views import common as views_common
from translation.serializers import back as serializers_back
from utils.methods import get_permission_classes
from utils.permissions import IsTranslator, IsSuperAdmin


class SiteInterfaceDictionaryBackMixin(views_common.SiteInterfaceDictionaryMixin):
    serializer_class = serializers_back.SiteInterfaceDictionaryBackSerializer
    permission_classes = get_permission_classes(
        IsTranslator,
        IsSuperAdmin,
    )


class LanguageViewBackMixin(views_common.LanguageViewMixin):
    permission_classes = get_permission_classes(IsTranslator)


class SiteInterfaceDictionaryConfirmView(SiteInterfaceDictionaryBackMixin, generics.CreateAPIView):
    serializer_class = serializers_back.SiteInterfaceDictionaryConfirmBackSerializer

    def create(self, request, *args, **kwargs):
        response = []
        for instance in self.get_queryset().filter(pk__in=request.data['ids']):
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            response.append(serializer.data)

        return Response(response)


class SiteInterfaceDictionaryListCreateView(SiteInterfaceDictionaryBackMixin, generics.ListCreateAPIView):
    """
    ## Site interface dictionary List/Create view.
    ### *POST*
    #### Description
    Create a new record in table SiteInterfaceDictionary.
    ##### Request
    Required
    * page (`str`) - Page name
    * keywords (`str`) - Keyword
    * text (`JSON`) - text dictionary, e.g.: {"en-GB": "Text"}
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    """

    filter_fields = ['page', 'keywords']


class SiteInterfaceDictionaryRUDView(SiteInterfaceDictionaryBackMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    ## Site interface dictionary UD view.
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a recipe object by an identifier.
    ##### Request
    Available
    * page (`str`) - Page name
    * keywords (`str`) - Keyword
    * text (`JSON`) - text dictionary, e.g.: {"en-GB": "Text"}
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ### *DELETE*
    #### Description
    Delete an object by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """

    serializer_class = serializers_back.SiteInterfaceDictionaryUpdateBackSerializer


class LanguageListCreateView(LanguageViewBackMixin, generics.ListCreateAPIView):
    """
    ## Create view for model Language
    ### *POST*
    #### Description
    Create a new language.
    ##### Request
    Required
    * title (`str`) - Title of language
    * locale(`str`) - Locale identifier (length=10)
    Available
    * is_active (`bool`) - Status of language (default=True)
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    """

    filter_fields = ['is_active']


class LanguageRUDView(LanguageViewBackMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    ## Create/Destroy view for model Language
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a language object.
    ##### Request
    Available
    * title (`str`) - Title of language
    * locale (`str`) - Locale identifier (length=10)
    * is_active (`bool`) - Status of language (default=True)
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE**
    #### Description
    Delete an object by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
