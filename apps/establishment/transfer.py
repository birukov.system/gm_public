"""
Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

"""

card = {
    "EstablishmentType": {
        "data_type": "dictionaries",
        "dependencies": None,
        "fields": {
            "Establishments": {
                # значения для поля "name" берутся из поля "type"  legacy модели "establishments"
                # https://jira.spider.ru/browse/GM-199
                "name": ("name", "django.db.models.TJSONField")
            }
        }
    },
    "Establishment": {
        "data_type": "dictionaries",
        "dependencies": ("Address", "Collection", "EstablishmentType"),
        # нету аналогов для establishment_type, establishment_subtypes, schedule, comments, tags
        "fields": {
            "Establishments": {
                "name": "name",
                "slug": "slug",
                "is_publish": ("state", "django.db.models.BooleanField")
            },
            "relations": {
                "EstablishmentInfos": {
                    "key": "establishment",
                    "fields": {
                        "website": "website",
                        "facebook": "facebook",
                        "twitter": "twitter",
                        "lafourchette": "lafourchette",
                        "booking": "booking_url"
                    }
                },
            },
        },
        # как работать с GenericRelation - ?
        # как работать с ManyToManyField - ? "EstablishmentSubType", "schedule"
        "relations": {
            "Locations": [
                (("location", None),
                 ("Address", "address", None, None)),
            ],
            "Establishments": [  # TODO правильно ли заполнена связь с EstablishmentType - ?
                (("type", "type"),
                 ("EstablishmentType", "establishment_type", "id", "django.db.models.PositiveIntegerField"))
            ]
            # # "establishment_subtypes": "EstablishmentSubType",
            # "collections": "Collection",
            # # TODO: нашел schedules в legacy
            # # "schedule": "Timetable",
            # "award": "Award",
            # # "tags": "MetaDataContent",
            # "reviews": "Review",
            # # "comments": "Comment",
            # # "favorites": "Favorites" # В legacy  этой таблицы не было

        }
    },
    "Menu": {
        "data_type": "objects",
        "dependencies": ("Establishment",),
        "fields": {
            "Menus": {
                "category": ("name", "django.db.models.TJSONField")
            }
        },
        "relations": {
            "Establishments": [
                (("establishment", None),
                 ("Establishment", "establishment", None, None))
            ]
        }
    },
    "ContactPhone": {
        "data_type": "objects",
        "dependencies": ("Establishment",),
        "fields": {
            "Establishments": {
                "phone": "phone"
            },
            "relations": {  # TODO правильно ли заполнена связь с Establishment - ?
                "Establishments": [
                    (("id", "id"),
                     ("Establishment", "establishment", "id", "django.db.models.PositiveIntegerField"))
                ]
            }
        }
    },

    "ContactEmail": {
        "data_type": "objects",
        "dependencies": ("Establishment",),
        "fields": {
            "EstablishmentInfos": {
                "email": "email"
            },
        },
        "relations": {
            "Establishments": [
                (("establishment", None),
                 ("Establishment", "establishment", None, None))
            ]
        }
    }
}

used_apps = ("review", "location", "collection", "main", "timetable", "favorites", "comment",)
