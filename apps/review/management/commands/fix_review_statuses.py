from django.core.management.base import BaseCommand

from establishment.models import Establishment
from review.models import Review
from transfer.models import Reviews


class Command(BaseCommand):
    help = """Fix reviews status"""

    def handle(self, *args, **kwargs):

        def get_status(old_status):
            if old_status == 'published':
                return Review.PUBLISHED
            elif old_status == 'hidden':
                return Review.HIDDEN
            elif old_status == 'to_investigate':
                return Review.TO_INVESTIGATE
            elif old_status == 'duplicated':
                return Review.READY
            elif old_status == 'to_validate':
                return Review.TO_VALIDATE
            elif old_status == 'to_translate':
                return Review.TO_TRANSLATE
            elif old_status == 'ready':
                return Review.READY
            elif old_status == 'to_taste':
                return Review.TO_INVESTIGATE
            elif old_status == 'receiving':
                return Review.PENDING_RECEPTION
            else:
                return Review.TO_INVESTIGATE

        establishments = Establishment.objects.filter(old_id__isnull=False).values_list('old_id',
                                                                                        flat=True)
        old_reviews = Reviews.objects.exclude(product_id__isnull=False).filter(
            establishment_id__in=list(establishments),
        ).values_list('id', 'aasm_state')

        to_update = []
        for old_id, old_state in old_reviews:
            review_qs = Review.objects.filter(old_id=old_id)
            if review_qs.exists():
                review = review_qs.first()

                old_status = review.status
                new_status = get_status(old_state)

                if old_status != new_status:
                    review.status = new_status
                    to_update.append(review)
            print(f'Current: {old_id}\nTo update: {len(to_update)}')

        Review.objects.bulk_update(to_update, ['status', ])
        self.stdout.write(self.style.WARNING(f'Fixed {len(to_update)} reviews status.'))
