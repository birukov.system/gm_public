from django.urls import reverse
from http.cookies import SimpleCookie

from rest_framework.test import APITestCase
from rest_framework import status
from datetime import datetime, timedelta

from main.models import SiteSettings
from news.models import NewsType, News
from account.models import User, Role, UserRole
from translation.models import Language
from location.models import Country


# Create your tests here.


class BaseTestCase(APITestCase):

    def setUp(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)

        # get tokens
        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('refresh_token')})
        self.test_news_type = NewsType.objects.create(name="Test news type")

        self.lang, created = Language.objects.get_or_create(
            title='Russia',
            locale='ru-RU'
        )

        self.country_ru, created = Country.objects.get_or_create(
            name={"en-GB": "Russian"}
        )

        self.site_ru, created = SiteSettings.objects.get_or_create(
            subdomain='ru'
        )

        role = Role.objects.create(
            role=Role.CONTENT_PAGE_MANAGER,
            site_id=self.site_ru.id
        )
        role.save()

        user_role = UserRole.objects.create(
            user=self.user,
            role=role
        )
        user_role.save()

        self.test_news = News.objects.create(
            created_by=self.user, modified_by=self.user,
            title={"ru-RU": "Test news"},
            news_type=self.test_news_type,
            description={"ru-RU": "Description test news"},
            end=datetime.now() + timedelta(hours=2),
            state=News.PUBLISHED,
            slugs={'en-GB': 'test-news-slug'},
            country=self.country_ru,
            site=self.site_ru
        )
        self.slug = next(iter(self.test_news.slugs.values()))


class NewsTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_news_post(self):
        test_news = {
            "title": {"ru-RU": "Test news POST"},
            "news_type_id": self.test_news_type.id,
            "description": {"ru-RU": "Description test news"},
            "end": datetime.now() + timedelta(hours=2),
            "state": News.PUBLISHED,
            "slugs": {'en-GB': 'test-news-slug_post'},
            "country_id": self.country_ru.id,
            "site_id": self.site_ru.id
        }

        url = reverse("back:news:list-create")
        response = self.client.post(url, data=test_news, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_web_news(self):
        response = self.client.get(reverse('web:news:list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(f"/api/web/news/slug/{self.slug}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get("/api/web/news/types/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_news_back_detail(self):
        response = self.client.get(f"/api/back/news/{self.test_news.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_news_list_back(self):
        response = self.client.get("/api/back/news/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_news_back_detail_put(self):
        # retrieve-update-destroy
        url = reverse('back:news:retrieve-update-destroy', kwargs={'pk': self.test_news.id})
        data = {
            'id': self.test_news.id,
            'description': {"ru-RU": "Description test news!"},
            'slugs': self.test_news.slugs,
            'news_type_id': self.test_news.news_type_id,
            'country_id': self.country_ru.id,
            "site_id": self.site_ru.id
        }

        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_web_favorite_create_delete(self):
        data = {
            "user": self.user.id,
            "object_id": self.test_news.id
        }

        response = self.client.post(f'/api/web/news/slug/{self.slug}/favorites/', data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(f'/api/web/news/slug/{self.slug}/favorites/', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class NewsCarouselTests(BaseTestCase):

    def test_back_carousel_CR(self):
        data = {
            "object_id": self.test_news.id
        }

        response = self.client.post(f'/api/back/news/{self.test_news.id}/carousels/', data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(f'/api/back/news/{self.test_news.id}/carousels/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
