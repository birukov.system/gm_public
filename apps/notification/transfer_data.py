from pprint import pprint

from transfer.models import EmailAddresses, NewsletterSubscriber
from transfer.serializers.notification import SubscriberSerializer, NewsletterSubscriberSerializer


def transfer_subscriber():
    queryset = EmailAddresses.objects.filter(state='usable')

    serialized_data = SubscriberSerializer(data=list(queryset.values()), many=True)

    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f'News serializer errors: {serialized_data.errors}')


def transfer_newsletter_subscriber():
    queryset = NewsletterSubscriber.objects.all().values(
        'id',
        'email_address__email',
        'email_address__account_id',
        'email_address__ip',
        'email_address__country_code',
        'email_address__locale',
        'updated_at',
    )

    serialized_data = NewsletterSubscriberSerializer(data=list(queryset), many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f'NewsletterSubscriber serializer errors: {serialized_data.errors}')


data_types = {
    'subscriber': [transfer_subscriber],
    'newsletter_subscriber': [transfer_newsletter_subscriber],
}
