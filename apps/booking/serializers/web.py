from rest_framework import serializers
from booking.models import models


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Booking
        fields = (
            'id',
            'type',
        )


class CheckBookingSerializer(serializers.ModelSerializer):
    available = serializers.BooleanField()
    type = serializers.ChoiceField(choices=models.Booking.AVAILABLE_SERVICES, allow_null=True)
    details = serializers.DictField()

    class Meta:
        model = models.Booking
        fields = (
            'available',
            'type',
            'details',
        )


class PendingBookingSerializer(serializers.ModelSerializer):
    restaurant_id = serializers.CharField()
    booking_id = serializers.CharField(allow_null=True, allow_blank=True)
    id = serializers.ReadOnlyField()
    user = serializers.ReadOnlyField()

    class Meta:
        model = models.Booking
        fields = (
            'id',
            'type',
            'restaurant_id',
            'booking_id',
            'pending_booking_id',
            'user',
        )

class CommitBookingSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Booking
        fields = (
            'stripe_token',
        )

    def update(self, instance, validated_data):
        """Override update method"""
        # Update user password from instance
        service = instance.get_service_by_type(instance.type)
        service.commit_booking(instance.pending_booking_id, validated_data.get('stripe_token'))
        instance.stripe_token = validated_data.get('stripe_token')
        instance.save()
        return instance

class UpdateBookingSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = models.Booking
        fields = ('booking_id', 'id', 'stripe_key', 'amount')


class GetBookingSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    def get_details(self, obj):
        booking = self.instance
        service = booking.get_service_by_type(booking.type)
        return service.get_booking_details(booking.booking_id)

    class Meta:
        model = models.Booking
        fields = '__all__'
