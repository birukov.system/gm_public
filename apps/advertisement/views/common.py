"""Views for app advertisement"""
from rest_framework import generics
from rest_framework import permissions

from advertisement.models import Advertisement
from main import models as main_models
from advertisement.serializers import AdvertisementBaseSerializer
from utils.views import ChoseToListMixin


class AdvertisementBaseView(generics.GenericAPIView):
    """Advertisement list view."""

    queryset = Advertisement.objects.with_base_related()
    permission_classes = (permissions.AllowAny,)
    serializer_class = AdvertisementBaseSerializer


class AdvertisementFilterBaseView:
    permission_classes = (permissions.AllowAny,)


class AdvertisementPageTypeListView(AdvertisementBaseView, generics.ListAPIView):
    """Advertisement list view by page type."""

    def get_queryset(self):
        """Overridden get queryset method."""
        page_type = self.kwargs.get('page_type')
        qs = super().get_queryset() \
            .by_page_type(page_type)

        if self.request.country_code is not None:
            country_code = self.request.country_code
            qs = qs.by_country(country_code)

        if self.request.locale is not None:
            qs = qs.by_locale(self.request.locale)

        return qs


class AdvertisementAvailableDevicesList(AdvertisementFilterBaseView, ChoseToListMixin, generics.ListAPIView):
    """Gets a list of available devices ​​for the ads."""
    chose_field = main_models.Page.SOURCES


class AdvertisementAvailablePageTypesList(AdvertisementFilterBaseView, ChoseToListMixin, generics.ListAPIView):
    """Gets a list of available page types ​​for the ads."""
    chose_field = main_models.Page.PAGE_TYPES
