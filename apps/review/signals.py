from django.db.models.signals import post_save
from django.dispatch import receiver

from review import models
from establishment import tasks
from establishment.models import Establishment

# TODO: вернуть после миграции!
# @receiver(post_save, sender=models.Review)
# def create_order(sender, instance=None, created=False, **kwargs):
#     """Recalculation public mark for establishment."""
#     if isinstance(instance.content_object, Establishment):
#         tasks.recalculation_public_mark.delay(
#             establishment_id=instance.content_object.id)
