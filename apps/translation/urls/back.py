from django.urls import path

from .common import common_urlpatterns
from translation.views import back as views

app_name = 'translation'

urlpatterns = [
    path('site/', views.SiteInterfaceDictionaryListCreateView.as_view(), name='site-interface-list-create'),
    path('site/<int:pk>/', views.SiteInterfaceDictionaryRUDView.as_view(), name='site-interface-rud'),
    path('languages/', views.LanguageListCreateView.as_view(), name='languages-create-list'),
    path('languages/<int:pk>/', views.LanguageRUDView.as_view(), name='language-rud'),
]


urlpatterns.extend(common_urlpatterns)
