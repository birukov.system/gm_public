"""Task methods for main app."""

from celery import shared_task

from account.models import User
from main.models import Panel
from utils.export import SendExport


@shared_task
def send_export_to_email(panel_id, user_id, file_type='csv'):
    panel = Panel.objects.get(id=panel_id)
    user = User.objects.get(id=user_id)
    SendExport(user, panel=panel, file_type=file_type).send()
