"""Account app filters."""
from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters

from account import models


class AccountBackOfficeFilter(filters.FilterSet):
    """Account filter set."""

    search = filters.CharFilter(method='search_text')
    role_country_code = filters.CharFilter(method='by_role_country_code')
    role = filters.MultipleChoiceFilter(choices=models.Role.ROLE_CHOICES,
                                        method='by_roles')

    class Meta:
        """Meta class."""

        model = models.User
        fields = (
            'role',
            'email_confirmed',
            'is_staff',
            'is_active',
            'is_superuser',
            'search',
            'role_country_code',
        )

    def by_roles(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_roles(value)
        return queryset

    def search_text(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.full_text_search(value)
        return queryset

    def by_role_country_code(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_role_country_code(value)
        return queryset


class RoleListFilter(filters.FilterSet):
    """Role filter set."""
    country_code = filters.CharFilter(field_name='country__code')

    class Meta:
        """Meta class."""
        model = models.Role
        fields = [
            'country_code',
        ]
