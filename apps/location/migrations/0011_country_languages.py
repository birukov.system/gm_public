# Generated by Django 2.2.4 on 2019-10-10 12:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('translation', '0003_auto_20190901_1032'),
        ('location', '0010_auto_20190904_0711'),
    ]

    operations = [
        migrations.AddField(
            model_name='country',
            name='languages',
            field=models.ManyToManyField(to='translation.Language', verbose_name='Languages'),
        ),
    ]
