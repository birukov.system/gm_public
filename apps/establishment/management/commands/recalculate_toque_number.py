"""Run recalculating toque number for establishments."""
from django.core.management.base import BaseCommand
from establishment.models import Establishment


class Command(BaseCommand):

    help = 'Recalculation toque number for all establishments.'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def handle(self, *args, **options):
        for establishment in Establishment.objects.select_related('address__city__country'):
            print(f'Recalculate for {establishment.name} ({establishment.id})')
            establishment.recalculate_toque_number()
