"""Tag app urlpatterns web users."""
from rest_framework.routers import SimpleRouter
from tag import views


app_name = 'tag'

router = SimpleRouter()
router.register(r'categories', views.TagCategoryViewSet)
router.register(r'filters', views.FiltersTagCategoryViewSet)
router.register(r'chosen_tags', views.ChosenTagsView)

urlpatterns = [

]

urlpatterns += router.urls

