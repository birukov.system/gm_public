from django.core.management.base import BaseCommand
from tag.models import Tag, TagCategory
from tqdm import tqdm


class Command(BaseCommand):
    help = '''Check product serial number from old db to new db.
              Run after add_product_tag!!!'''

    def check_serial_number(self):
        category = TagCategory.objects.get(index_name='serial_number')
        tags = Tag.objects.filter(category=category, products__isnull=False)
        for tag in tqdm(tags, desc='Update serial number for product'):
            tag.products.all().update(serial_number=tag.value)
            tag.products.clear()

        self.stdout.write(self.style.WARNING(f'Check serial number product end.'))

    def handle(self, *args, **kwargs):
        self.check_serial_number()
