"""Back account URLs"""
from django.urls import path

from account.views import back as views
from search_indexes.views import UsersBackDocumentViewSet

app_name = 'account'

urlpatterns = [
    path('role/', views.RoleListView.as_view(), name='role-list-create'),
    path('role/types/', views.RoleTypeRetrieveView.as_view(), name='role-types'),
    path('user-role/', views.UserRoleListView.as_view(), name='user-role-list-create'),
    path('user/', views.UserListView.as_view(), name='user-create-list'),
    path('user/list/', UsersBackDocumentViewSet.as_view({'get': 'list'}), name='user-elasticsearch-list'),
    path('user/<int:id>/', views.UserRUDView.as_view(), name='user-rud'),
    path('user/<int:id>/csv/', views.get_user_csv, name='user-csv'),
    path('user/csv/', views.UserCSVViewSet.as_view({'get': 'to_csv'}), name='user-csv-list'),
    path('team/invite/<int:establishment_id>', views.EstablishmentTeamInviteView.as_view(),
         name='invite-to-establishment-team'),
]
