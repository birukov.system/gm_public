"""Filters from application Recipe"""
from django_filters import rest_framework as filters

from recipe import models


class RecipeBackOfficeFilterSet(filters.FilterSet):

    state = filters.MultipleChoiceFilter(
        choices=models.Recipe.STATE_CHOICES,
        help_text='Filter by state'
    )

    class Meta:
        """Meta class."""
        model = models.Recipe
        fields = [
            'state'
        ]
