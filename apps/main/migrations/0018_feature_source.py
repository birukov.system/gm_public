# Generated by Django 2.2.4 on 2019-10-07 14:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0017_feature_route'),
    ]

    operations = [
        migrations.AddField(
            model_name='feature',
            name='source',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Mobile'), (1, 'Web'), (2, 'All')], default=0, verbose_name='Source'),
        ),
    ]
