from translation.serializers import common as serializers_common
from translation.serializers import back as serializers_back


class MobileSiteInterfaceDictionarySerializer(serializers_common.SiteInterfaceDictionarySerializer):
    class Meta(serializers_common.SiteInterfaceDictionarySerializer.Meta):
        fields = [
            'id',
            'page',
            'keywords',
            'text_trans',
        ]

        extra_kwargs = {**serializers_back.SiteInterfaceDictionaryBackSerializer.Meta.extra_kwargs}
        validators = serializers_back.SiteInterfaceDictionaryBackSerializer.Meta.validators
