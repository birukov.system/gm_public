# Generated by Django 2.2.4 on 2019-09-24 08:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
        ('news', '0012_auto_20190923_1416'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='slug',
            field=models.SlugField(null=True, unique=True, verbose_name='News slug'),
        ),
    ]
