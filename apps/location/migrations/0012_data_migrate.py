from django.db import migrations, connection
import os


class Migration(migrations.Migration):
    # Check migration
    def load_data_from_sql(apps, schema_editor):
        file_path = os.path.join(os.path.dirname(__file__), 'migrate_lang.sql')
        sql_statement = open(file_path).read()
        with connection.cursor() as c:
            c.execute(sql_statement)

    def revert_data(apps, schema_editor):
        file_path = os.path.join(os.path.dirname(__file__), 'remigrate_lang.sql')
        sql_statement = open(file_path).read()
        with connection.cursor() as c:
            c.execute(sql_statement)

    dependencies = [
        ('location', '0011_country_languages'),
    ]

    operations = [
         # migrations.RunPython(load_data_from_sql, revert_data),
    ]
