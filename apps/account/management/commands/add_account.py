from django.core.management.base import BaseCommand
from django.db import connections
from django.db.models import Q, F, Value
from django.db.models.functions import ConcatPair
from tqdm import tqdm

from account.models import User
from utils.methods import namedtuplefetchall


class Command(BaseCommand):
    help = 'Add account from old db to new db'

    def account_sql(self):
        with connections['legacy'].cursor() as cursor:
            cursor.execute('''
                            select a.email, a.id as account_id, a.encrypted_password, a.locale, a.city,
                            a.confirmed_at as cfd, a.unconfirmed_email,
                            case when a.confirmed_at is not null then true else false end as confirmed_at,                          
                            nickname  
                            from accounts as a 
                            where a.email is not null and a.nickname!='admin'                                                               
            ''')
            return namedtuplefetchall(cursor)

    def handle(self, *args, **kwargs):
        objects = []
        for a in tqdm(self.account_sql(), desc='find users'):
            users = User.objects.filter(Q(email=a.email) | Q(old_id=a.account_id))
            if not users.exists():
                objects.append(User(
                    email=a.email,
                    unconfirmed_email=a.unconfirmed_email,
                    email_confirmed=a.confirmed_at,
                    old_id=a.account_id,
                    password=a.encrypted_password,
                    username=a.nickname,
                    locale=a.locale,
                    city=a.city,
                    confirmed_at=a.cfd,
                ))

        User.objects.bulk_create(objects)
        user = User.objects.filter(old_id__isnull=False)
        user.update(password=ConcatPair(Value('bcrypt$'), F('password')))
        self.stdout.write(self.style.WARNING(f'Created {len(objects)} accounts objects.'))
