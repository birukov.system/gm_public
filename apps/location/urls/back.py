"""Location app back-office urlconf."""
from django.urls import path

from location import views

app_name = 'location'

urlpatterns = [
    path('addresses/', views.AddressListCreateView.as_view(), name='address-list-create'),
    path('addresses/<int:pk>/', views.AddressRUDView.as_view(), name='address-RUD'),

    path('cities/', views.CityListCreateView.as_view(), name='city-list-create'),
    path('cities/all/', views.CityListSearchView.as_view(), name='city-list-create'),
    path('cities/<int:pk>/', views.CityRUDView.as_view(), name='city-retrieve'),
    path('cities/popular/', views.PopularCityListView.as_view(), name='popular-city-list'),
    path('cities/<int:pk>/popular/', views.PopularCityCreateDestroyView.as_view(),
         name='popular-city-create-destroy'),

    path('countries/', views.CountryListCreateView.as_view(), name='country-list-create'),
    path('countries/calling-codes/', views.CountryCallingCodeListView.as_view(),
         name='country-calling-code-list'),
    path('countries/<int:pk>/', views.CountryRUDView.as_view(), name='country-retrieve'),

    path('regions/', views.RegionListCreateView.as_view(), name='region-list-create'),
    path('regions/<int:pk>/', views.RegionRUDView.as_view(), name='region-retrieve'),

    path('wine-regions/', views.WineRegionListCreateView.as_view(), name='wine-region-list-create'),
    path('wine-regions/<int:pk>/', views.WineRegionRUDView.as_view(), name='wine-region-rud'),

    path('wine-sub-regions/', views.WineSubRegionListCreateView.as_view(),
         name='wine-sub-region-list-create'),
    path('wine-sub-regions/<int:pk>/', views.WineSubRegionRUDView.as_view(),
         name='wine-sub-region-rud'),
]
