from django.conf import settings
from django.core.management.base import BaseCommand
from tqdm import tqdm

from collection.models import Collection


class Command(BaseCommand):
    help = """Fix existed collections."""

    def handle(self, *args, **kwarg):
        update_collections = []
        collections = Collection.objects.values_list('id', 'collection_type', 'description')
        for id, collection_type, description in tqdm(collections):
            collection = Collection.objects.get(id=id)
            description = collection.description
            collection_updated = False

            if isinstance(description, str):
                if description.lower().find('pop') != -1:
                    collection.collection_type = Collection.POP
                    collection_updated = True

            if not isinstance(description, dict):
                collection.description = {settings.FALLBACK_LOCALE: collection.description}
                collection_updated = True

            if collection_updated:
                update_collections.append(collection)

        Collection.objects.bulk_update(update_collections, ['collection_type', 'description', ])
        self.stdout.write(self.style.WARNING(f'Updated collections: {len(update_collections)}'))
