from django.contrib.gis import admin
from mptt.admin import DraggableMPTTAdmin
from utils.admin import BaseModelAdminMixin

from collection import models


class GuideFilterInline(admin.TabularInline):
    """Tabular inline for GuideFilter model."""
    extra = 0
    model = models.GuideFilter


class GuideElementInline(admin.TabularInline):
    """Tabular inline for GuideElement model."""
    extra = 0
    model = models.GuideElement
    raw_id_fields = [
        'guide_element_type',
        'establishment',
        'review',
        'wine_region',
        'product',
        'city',
        'wine_color_section',
        'section',
        'guide',
        'parent',
        'label_photo',
    ]


@admin.register(models.Collection)
class CollectionAdmin(admin.ModelAdmin):
    """Collection admin."""


@admin.register(models.Guide)
class GuideAdmin(admin.ModelAdmin):
    """Guide admin."""
    inlines = [GuideFilterInline, ]


@admin.register(models.GuideElementType)
class GuideElementType(admin.ModelAdmin):
    """Guide element admin."""


@admin.register(models.GuideElement)
class GuideElementAdmin(DraggableMPTTAdmin, BaseModelAdminMixin, admin.ModelAdmin):
    """Guide element admin."""
    raw_id_fields = [
        'guide_element_type', 'establishment', 'review',
        'wine_region', 'product', 'city',
        'wine_color_section', 'section', 'guide',
        'parent',
    ]
