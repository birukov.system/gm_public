"""Admin page for app Review"""
from django.contrib import admin

from utils.admin import BaseModelAdminMixin
from . import models


@admin.register(models.Review)
class ReviewAdminModel(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin model for model Review."""

    raw_id_fields = ('reviewer', 'child', 'country')


@admin.register(models.ReviewTextAuthor)
class ReviewTextAuthorAdminModel(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin model for model ReviewTextAuthor."""

    list_display = ('author', 'review', 'locale', 'modified')
    raw_id_fields = ('author', 'review')
