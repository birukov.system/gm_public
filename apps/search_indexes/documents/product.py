"""Product app documents."""
from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from search_indexes.utils import trigram_analyzer
from product import models

ProductIndex = Index(settings.ELASTICSEARCH_INDEX_NAMES.get(__name__, 'product'))
ProductIndex.settings(number_of_shards=5, number_of_replicas=2, max_result_window=500000)


@ProductIndex.doc_type
class ProductDocument(Document):
    """Product document."""

    description = fields.KeywordField(attr='description_serialized')
    description_searchable = fields.ListField(fields.TextField(analyzer=trigram_analyzer))
    product_type = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.KeywordField(attr='name_serialized'),
            'name_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'index_name': fields.KeywordField(),
            'default_image': fields.KeywordField(attr='default_image_url'),
            'preview_image_url': fields.KeywordField(),
        },
    )
    subtypes = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.KeywordField(attr='name_serialized'),
            'name_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'index_name': fields.KeywordField(),
            'default_image': fields.KeywordField(attr='default_image_url'),
            'preview_image_url': fields.KeywordField(),
        },
        multi=True
    )
    preview_image_url = fields.KeywordField(attr='preview_image_url')
    establishment = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.KeywordField(),
            'index_name': fields.KeywordField(),
            'slug': fields.KeywordField(),
            'city': fields.ObjectField(
                attr='address.city',
                properties={
                    'id': fields.IntegerField(),
                    'name': fields.KeywordField(attr='name_dumped'),
                    'name_translated': fields.KeywordField(),
                    'code': fields.KeywordField(),
                    'country': fields.ObjectField(
                        properties={
                            'id': fields.IntegerField(),
                            'name': fields.KeywordField(attr='name_serialized'),
                            'name_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
                            'code': fields.KeywordField(),
                            'flag_image': fields.KeywordField(attr='flag_image_indexing')
                        }
                    ),
                }
            ),
            'address': fields.ObjectField(
                properties={
                    'id': fields.IntegerField(),
                    'street_name_1': fields.TextField(
                        fields={'raw': fields.KeywordField()}
                    ),
                    'postal_code': fields.KeywordField(),
                    'coordinates': fields.GeoPointField(attr='location_field_indexing'),
                }
            )
        }
    )
    wine_colors = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        multi=True,
    )
    grape_variety = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        multi=True,
    )
    wine_origins = fields.ListField(
        fields.ObjectField(
            properties={
                'wine_region': fields.ObjectField(properties={
                    'id': fields.IntegerField(),
                    'name': fields.KeywordField(),
                    'country': fields.ObjectField(properties={
                        'id': fields.IntegerField(),
                        'name': fields.KeywordField(attr='name_serialized'),
                        'name_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
                        'code': fields.KeywordField(),
                    }),
                    # 'coordinates': fields.GeoPointField(),
                    'description': fields.KeywordField(attr='description_serialized'),
                    'description_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),

                }),
                'wine_sub_region': fields.ObjectField(properties={
                    'id': fields.IntegerField(),
                    'name': fields.KeywordField(),
                })})
    )
    classifications = fields.ObjectField(  # TODO
        properties={
            'classification_type': fields.ObjectField(properties={}),
            'standard': fields.ObjectField(properties={
                'name': fields.KeywordField(),
                'standard_type': fields.IntegerField(),
                # 'coordinates': fields.GeoPointField(),
            }),
            'tags': fields.ObjectField(
                properties={
                    'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
                    'label': fields.KeywordField(attr='label_serialized'),
                    'value': fields.KeywordField(),
                },
                multi=True
            ),
        },
        multi=True
    )
    standards = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.KeywordField(),
            'standard_type': fields.IntegerField(),
            # 'coordinates': fields.GeoPointField(),
        },
        multi=True
    )
    wine_village = fields.ObjectField(properties={
        'name': fields.KeywordField(),
    })
    tags = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        multi=True
    )
    related_tags = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField(),
        },
        multi=True
    )
    name = fields.TextField(attr='display_name', analyzer=trigram_analyzer)
    favorites_for_users = fields.ListField(field=fields.IntegerField())
    created = fields.DateField(attr='created')  # publishing date (?)

    class Django:
        model = models.Product
        fields = (
            'id',
            'category',
            'available',
            'public_mark',
            'slug',
            'old_id',
            'state',
            'old_unique_key',
            'vintage',
            'average_price',
        )
        related_models = [models.ProductType]

    def get_queryset(self):
        return super().get_queryset().published().with_base_related()

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Product instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, models.ProductType) and hasattr(related_instance, 'product_set'):
            return related_instance.product_set().all()

