"""News app documents."""
from django.conf import settings
from django_elasticsearch_dsl import Document, Index, fields
from search_indexes.utils import trigram_analyzer
from news import models
from json import dumps


NewsIndex = Index(settings.ELASTICSEARCH_INDEX_NAMES.get(__name__, 'news'))
NewsIndex.settings(number_of_shards=10, number_of_replicas=1, max_result_window=500000)


@NewsIndex.doc_type
class NewsDocument(Document):
    """News document."""

    news_type = fields.ObjectField(properties={'id': fields.IntegerField(),
                                               'name': fields.KeywordField()})
    title = fields.KeywordField(attr='title_serialized')
    title_searchable = fields.ListField(fields.TextField(analyzer=trigram_analyzer))
    slugs = fields.KeywordField()
    backoffice_title = fields.TextField(analyzer='english')
    subtitle = fields.KeywordField(attr='subtitle_serialized')
    subtitle_searchable = fields.ListField(fields.TextField(analyzer=trigram_analyzer))
    description = fields.KeywordField(attr='description_serialized', ignore_above=10000)
    description_searchable = fields.ListField(fields.TextField(analyzer=trigram_analyzer))
    country = fields.ObjectField(properties={'id': fields.IntegerField(),
                                             'code': fields.KeywordField()})
    web_url = fields.KeywordField(attr='web_url')
    image_url = fields.KeywordField(attr='image_url')
    preview_image_url = fields.KeywordField(attr='preview_image_url')
    tags = fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
            'value': fields.KeywordField()
        },
        multi=True)
    visible_tags = fields.ObjectField(
        properties={
            'id': fields.IntegerField(attr='id'),
            'label_searchable': fields.ListField(fields.TextField(analyzer=trigram_analyzer)),
            'label': fields.KeywordField(attr='label_serialized'),
        },
        multi=True)
    favorites_for_users = fields.ListField(field=fields.IntegerField())
    start = fields.DateField(attr='publication_datetime')
    has_any_desc_active = fields.BooleanField()
    locale_to_description_is_active_serialized = fields.KeywordField()
    created = fields.DateField()
    must_of_the_week = fields.BooleanField(attr='must_of_the_week')
    modified = fields.DateField()
    # descriptions = fields.ListField(
    #     fields.ObjectField(
    #         properties={
    #             'locale': fields.KeywordField(),
    #             'slug': fields.KeywordField(),
    #             'active': fields.KeywordField(),
    #             'title': fields.KeywordField(),
    #             'subtitle': fields.KeywordField(),
    #             'text': fields.KeywordField(),
    #         },
    #         multi=True,
    #     )
    # )
    is_published = fields.BooleanField(attr='is_publish')


    def prepare_slugs(self, instance):
        return dumps(instance.slugs or {})

    class Django:

        model = models.News
        fields = (
            'id',
            'end',
            'state',
            'is_highlighted',
            'template',
        )
        related_models = [models.NewsType]

    def get_queryset(self):
        return super().get_queryset().with_base_related()

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, models.NewsType) and hasattr(related_instance, 'news'):
            return related_instance.news.all()
