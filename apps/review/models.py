"""Review app models."""

from django.contrib.contenttypes import fields as generic
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.functional import cached_property

from location.models import Region
from utils.models import (BaseAttributes, TranslatedFieldsMixin,
                          ProjectBaseMixin, GalleryMixin,
                          TJSONField, IntermediateGalleryModelMixin)


class ReviewQuerySet(models.QuerySet):
    """QuerySets for model Review"""

    def with_base_related(self):
        """Return QuerySet with base related."""
        return self.select_related(
            'reviewer',
            'country',
            'child',
            'content_type',
        )#.prefetch_related('content_object')

    def by_reviewer(self, user):
        """Return reviews by user"""
        return self.filter(reviewer=user)

    def by_vintage(self, year: int):
        """Return reviews by year"""
        return self.filter(vintage=year)

    def by_status(self, status):
        """Filter by status"""
        return self.filter(status=status)

    def published(self):
        """Return published reviews"""
        return self.filter(status=Review.PUBLISHED)

    def recent(self):
        """Return recent review"""
        return self.annotate(is_ready=models.Case(
            models.When(
                models.Q(status=Review.READY),
                then=True
            ),
            default=False,
            output_field=models.BooleanField()
        )).order_by('-is_ready', '-modified')


class Review(BaseAttributes, TranslatedFieldsMixin):
    """Review model"""
    TO_INVESTIGATE = 0
    PENDING_RECEPTION = 1
    TO_VALIDATE = 2
    TO_TRANSLATE = 3
    READY = 4
    PUBLISHED = 5
    ABANDONED = 6
    HIDDEN = 7

    REVIEW_STATUSES = (
        (TO_INVESTIGATE, _('ToInvestigate')),
        (PENDING_RECEPTION, _('Pending reception')),
        (TO_VALIDATE, _('To validate')),
        (TO_TRANSLATE, _('To translate')),
        (READY, _('Ready')),
        (PUBLISHED, _('Published')),
        (ABANDONED, _('Abandoned')),
        (HIDDEN, _('Hidden')),
    )
    reviewer = models.ForeignKey(
        'account.User',
        related_name='reviews',
        on_delete=models.CASCADE,
        verbose_name=_('Reviewer'),
        null=True, default=None, blank=True
    )
    country = models.ForeignKey(
        'location.Country',
        on_delete=models.CASCADE,
        related_name='country',
        verbose_name=_('Country'),
        null=True,
    )
    child = models.ForeignKey(
        'self',
        blank=True,
        default=None,
        null=True,
        on_delete=models.CASCADE,
        verbose_name=_('Child review'),
    )
    text = TJSONField(
        _('text'),
        null=True,
        blank=True,
        default=None,
        help_text='{"en-GB":"Text review"}',
    )
    content_type = models.ForeignKey(generic.ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    status = models.PositiveSmallIntegerField(choices=REVIEW_STATUSES, default=TO_INVESTIGATE)
    published_at = models.DateTimeField(
        _('Publish datetime'),
        blank=True,
        default=None,
        null=True,
        help_text=_('Review published datetime'),
    )
    vintage = models.IntegerField(_('Year of review'), validators=[MinValueValidator(1900), MaxValueValidator(2100)])
    mark = models.FloatField(verbose_name=_('mark'), blank=True, null=True, default=None)
    priority = models.PositiveSmallIntegerField(_('Priority'), blank=True, null=True, default=None)
    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)
    visited_at = models.DateField(blank=True, null=True)

    objects = ReviewQuerySet.as_manager()

    @property
    def toque_number(self):
        """for ES purposes"""
        from establishment.models import RatingStrategy
        toque_number = 0
        if self.establishment_city and self.mark:
            toque_number = RatingStrategy.objects. \
                get_toque_number(country=self.establishment_city.country,
                                 public_mark=self.mark)
        return toque_number

    @property
    def status_display(self):
        return self.REVIEW_STATUSES[self.status][1]

    class Meta:
        """Meta class."""
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')

    @property
    def is_product_review(self):
        return self.content_object.__class__.__name__ == 'Product'

    @property
    def is_establishment_review(self):
        return self.content_object.__class__.__name__ == 'Establishment'

    @property
    def object_type(self):
        if self.is_establishment_review:
            generic_types = self.content_object.generic_types
            if not generic_types:
                return None

            return generic_types[0]

        if self.is_product_review:
            return self.content_object.product_type.index_name

        return None

    @property
    def object_slug(self):
        return self.content_object.slug

    @property
    def producer_name(self):
        if not self.is_product_review:
            return None

        return self.content_object.establishment.name

    @cached_property
    def establishment_city(self):
        if not self.is_establishment_review or self.content_object.address is None or \
                self.content_object.address.city is None:
            return None

        return self.content_object.address.city

    @property
    def establishment_region(self):
        if self.is_establishment_review:
            return self.content_object.address.city.region

        if self.is_product_review:
            return self.content_object.establishment.address.city.region

        return None

    @property
    def establishment_sub_region(self):
        if self.is_establishment_review:
            region_id = self.content_object.address.city.region.id
            return Region.objects.sub_regions_by_region_id(region_id).first()

        if self.is_product_review:
            region_id = self.content_object.establishment.address.city.region.id
            return Region.objects.sub_regions_by_region_id(region_id).first()

        return None

    @property
    def wine_region(self):
        if not self.is_product_review:
            return None

        return self.content_object.wine_region

    @property
    def last_inquiry(self):
        return self.inquiries.order_by('-created').first()


class ReviewTextAuthor(ProjectBaseMixin):
    author = models.ForeignKey(
        'account.User',
        verbose_name=_('author'),
        on_delete=models.CASCADE,
        related_name='review_authors',
    )
    review = models.ForeignKey(
        'review.Review',
        verbose_name=_('review'),
        on_delete=models.CASCADE,
        related_name='text_authors',
    )
    locale = models.CharField(_('locale'), max_length=10)

    class Meta:
        verbose_name = _('Text author')
        verbose_name_plural = _('Text authors')
        unique_together = ('locale', 'review')


class Inquiries(GalleryMixin, ProjectBaseMixin):
    NONE = 0
    DINER = 1
    LUNCH = 2

    MOMENTS = (
        (NONE, _('none')),
        (DINER, _('diner')),
        (LUNCH, _('lanch')),
    )

    STATUS_NONE = 0
    STATUS_STARTED = 1
    STATUS_DELIVERED = 2
    STATUS_ABANDONED = 3
    STATUS_ACCEPTED = 4

    INQUIRY_STATUSES = (
        (STATUS_NONE, _('None')),
        (STATUS_STARTED, _('Started')),
        (STATUS_DELIVERED, _('Delivered')),
        (STATUS_ABANDONED, _('Abandoned')),
        (STATUS_ACCEPTED, _('Accepted')),
    )

    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)
    review = models.ForeignKey(Review, verbose_name=_('review'), related_name='inquiries', on_delete=models.CASCADE)
    comment = models.TextField(_('comment'), blank=True, null=True)
    final_comment = models.TextField(_('final comment'), blank=True, null=True)
    mark = models.FloatField(verbose_name=_('mark'), blank=True, null=True, default=None)
    status = models.PositiveSmallIntegerField(choices=INQUIRY_STATUSES, default=STATUS_NONE)
    attachment_file = models.URLField(verbose_name=_('attachment'), max_length=255, blank=True, null=True, default=None)
    attachment_content_type = models.CharField(max_length=255, blank=True, null=True)
    author = models.ForeignKey('account.User', related_name='inquiries', on_delete=models.CASCADE,
                               verbose_name=_('author'))
    bill_file = models.URLField(verbose_name=_('bill'), max_length=255, blank=True, null=True, default=None)
    bill_content_type = models.CharField(max_length=255, blank=True, null=True)
    price = models.DecimalField(_('price'), max_digits=7, decimal_places=2, blank=True, null=True)
    moment = models.PositiveSmallIntegerField(choices=MOMENTS, default=NONE)
    gallery = models.ManyToManyField('gallery.Image', through='review.InquiriesGallery')
    decibels = models.CharField(max_length=255, blank=True, null=True)
    nomination = models.CharField(max_length=255, blank=True, null=True)
    nominee = models.CharField(max_length=255, blank=True, null=True)
    published = models.BooleanField(_('is published'), default=False)
    visited_at = models.DateField(_('visited at'), blank=True, null=True, default=None)

    class Meta:
        verbose_name = _('Inquiry')
        verbose_name_plural = _('Inquiries')

    def __str__(self):
        return f'id: {self.id}, review: {self.review.id}, author: {self.author.id}'

    @property
    def updated_on(self):
        grid = self.grids.all().first()
        if grid is None:
            return None

        return grid.modified


class GridItems(ProjectBaseMixin):
    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)
    inquiry = models.ForeignKey(Inquiries, verbose_name=_('inquiry'), on_delete=models.CASCADE, related_name='grids')
    sub_name = models.CharField(_('sub name'), max_length=255, blank=True, null=True)
    name = models.CharField(_('name'), max_length=255, blank=True, null=True)
    value = models.FloatField(_('value'), blank=True, null=True)
    desc = models.TextField(_('description'), blank=True, null=True)
    dish_title = models.CharField(_('dish title'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('inquiry grid')
        verbose_name_plural = _('inquiry grids')

    def __str__(self):
        return f'inquiry: {self.inquiry.id}, grid id: {self.id}'


class InquiriesGallery(IntermediateGalleryModelMixin):
    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)
    inquiry = models.ForeignKey(
        Inquiries,
        null=True,
        related_name='inquiries_gallery',
        on_delete=models.CASCADE,
        verbose_name=_('inquiry'),
    )
    image = models.ForeignKey(
        'gallery.Image',
        null=True,
        related_name='inquiries_gallery',
        on_delete=models.CASCADE,
        verbose_name=_('image'),
    )

    class Meta:
        verbose_name = _('inquiry gallery')
        verbose_name_plural = _('inquiry galleries')
