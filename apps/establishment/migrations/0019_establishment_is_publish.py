# Generated by Django 2.2.4 on 2019-09-16 11:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('establishment', '0018_socialnetwork'),
    ]

    operations = [
        migrations.AddField(
            model_name='establishment',
            name='is_publish',
            field=models.BooleanField(default=False, verbose_name='Publish status'),
        ),
    ]
