from rest_framework.test import APITestCase
from rest_framework import status
from authorization.tests.tests_authorization import get_tokens_for_user
from django.urls import reverse
from account.models import User


class AccountResetPassWordTests(APITestCase):

    def setUp(self):
        self.data = get_tokens_for_user()

    def test_reset_password(self):
        url = reverse('web:account:password-reset')
        data = {
            "username_or_email": self.data["email"]
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = {
            "username_or_email": self.data["username"]
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AccountResetPassWordTests(APITestCase):

    def setUp(self):
        self.data = get_tokens_for_user()

    def test_reset_password_confirm(self):
        data ={
            "password": "newpasswordnewpassword"
        }
        user = User.objects.get(email=self.data["email"])
        token = user.reset_password_token
        uidb64 = user.get_user_uidb64

        url = reverse('web:account:password-reset-confirm', kwargs={
            'uidb64': uidb64,
            'token': token
        })
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)



