"""Location app mobile urlconf."""
from location.urls.common import urlpatterns as common_urlpatterns

urlpatterns = []

urlpatterns.extend(common_urlpatterns)
