from django.conf import settings
from django.core.management.base import BaseCommand

from product.models import ProductType


class Command(BaseCommand):
    help = 'Add product type data'

    def handle(self, *args, **kwarg):
        product_types = ['wine', 'souvenir']
        create_counter = 0
        for product_type in product_types:
            subtype, created = ProductType.objects.get_or_create(**{
                'index_name': product_type
            })
            subtype.name = {settings.FALLBACK_LOCALE: product_type}
            subtype.save()
            if created:
                create_counter += 1
        self.stdout.write(self.style.WARNING(f'Created: {create_counter}'))
