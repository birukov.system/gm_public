from pprint import pprint

from establishment.models import Establishment
from partner.models import Partner, PartnerToEstablishment
from transfer.models import EstablishmentBacklinks
from transfer.serializers.partner import PartnerSerializer


def transfer_partner():
    """
    Transfer data to Partner model only after transfer Establishment
    """
    establishments = Establishment.objects.filter(old_id__isnull=False).values_list('old_id', flat=True)
    queryset = EstablishmentBacklinks.objects.filter(
        establishment_id__in=list(establishments),
    ).values(
        'id',
        'establishment_id',
        'partnership_name',
        'partnership_icon',
        'backlink_url',
        'created_at',
        'type',
        'starting_date',
        'expiry_date',
        'price_per_month',
    )

    serialized_data = PartnerSerializer(data=list(queryset), many=True)
    if serialized_data.is_valid():
        # Partner.objects.all().delete()  # TODO: закоментить, если требуется сохранить старые записи
        serialized_data.save()
    else:
        pprint(f"Partner serializer errors: {serialized_data.errors}")


data_types = {
    "partner": [transfer_partner]
}
