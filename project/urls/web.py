"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include

app_name = 'web'

urlpatterns = [
    path('account/', include('account.urls.web')),
    path('booking/', include('booking.urls.web')),
    path('re_blocks/', include('advertisement.urls.web')),
    path('collections/', include('collection.urls.web')),
    path('establishments/', include('establishment.urls.web')),
    path('news/', include('news.urls.web')),
    path('notifications/', include(('notification.urls.web', "notification"),
                                   namespace='notification')),
    path('partner/', include('partner.urls.web')),
    path('location/', include('location.urls.web')),
    path('main/', include('main.urls.web')),
    path('recipes/', include('recipe.urls.web')),
    path('tags/', include('tag.urls.web')),
    path('translation/', include('translation.urls.web')),
    path('comments/', include('comment.urls.web')),
    path('favorites/', include('favorites.urls')),
    path('timetables/', include('timetable.urls.web')),
    path('products/', include('product.urls.web')),
    path('report/', include('report.urls.web')),
]
