"""
Структура fields:
key - поле в таблице postgres
value - поле или группа полей в таблице legacy

В случае передачи группы полей каждое поле представляет собой кортеж, где:
field[0] - название аргумента
field[1] - название поля в таблице legacy
Опционально: field[2] - тип данных для преобразования

"""

card = {
    "Image": {
        "data_type": "objects",
        "dependencies": None,
        "fields": {
            "MercuryImages": {
                "image": "attachment_file_name"
            }
        }
    },

}

used_apps = None
