from rest_framework import serializers

from partner import models


# Serializers
class PartnersForEstablishmentWebSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='partner.pk', read_only=True)
    name = serializers.CharField(source='partner.name', read_only=True)
    type = serializers.IntegerField(source='partner.type', read_only=True)
    type_display = serializers.CharField(source='partner.type_display', read_only=True)

    class Meta:
        model = models.PartnerToEstablishment
        fields = (
            'id',
            'name',
            'type',
            'type_display',
            'starting_date',  # own field
            'expiry_date',  # own field
            'price_per_month',  # own field
            'url',  # own field
            'image',  # own field
        )
