from django.core.validators import EMPTY_VALUES
from django_filters import rest_framework as filters

from main import models


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class AwardFilter(filters.FilterSet):
    """Award filter set."""

    establishment_id = filters.NumberFilter(field_name='object_id', )
    product_id = filters.NumberFilter(field_name='object_id', )
    employee_id = filters.NumberFilter(field_name='object_id', )
    award_entity_type_id__in = NumberInFilter(
        field_name='award_type__entity_type',
        lookup_expr='in',
        help_text='Filter by identifiers of an award entity type'
    )

    class Meta:
        """Meta class."""

        model = models.Award
        fields = (
            'establishment_id',
            'product_id',
            'employee_id',
            'state',
            'award_type',
            'vintage_year',
            'award_type',
            'award_entity_type_id__in',
        )


    def by_establishment_id(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_establishment_id(value, content_type='establishment')
        return queryset

    def by_product_id(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_product_id(value, content_type='product')
        return queryset

    def by_employee_id(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_employee_id(value, content_type='establishmentemployee')
        return queryset


class AwardTypeFilterSet(filters.FilterSet):
    """Award type FilterSet."""

    id = filters.NumberFilter(help_text='Filter by AwardType identifier.')
    name = filters.CharFilter(method='by_name', help_text='Filter by AwardType name.')
    award_entity_type_id__in = NumberInFilter(
        field_name='entity_type',
        lookup_expr='in',
        help_text='Filter by identifiers of an award entity type'
    )
    entity_type__in = filters.CharFilter(method='by_entity_id')

    class Meta:
        """Meta class."""
        model = models.AwardType
        fields = [
            'id',
            'name',
            'entity_type__in',
        ]

    def by_entity_id(self, queryset, name, value):
        """Search text."""
        if value not in EMPTY_VALUES:
            return queryset.filter(entity_type__pk__in=value.split('__'))
        return queryset

    def by_name(self, queryset, name, value):
        if value not in EMPTY_VALUES:
            return queryset.by_name(value)
        return queryset


class AwardEntityTypeFilterSet(filters.FilterSet):
    """Award entity type FilterSet."""

    class Meta:
        """Meta class."""

        model = models.AwardEntityType
        fields = ()


class PageTypeFilterSet(filters.FilterSet):
    class Meta:
        model = models.PageType
        fields = (
            'is_back_office',
        )
