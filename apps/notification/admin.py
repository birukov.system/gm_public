from django.contrib import admin
from notification import models


@admin.register(models.SubscriptionType)
class SubscriptionTypeAdmin(admin.ModelAdmin):
    """SubscriptionType admin."""
    list_display = ['index_name', 'country']


@admin.register(models.Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    """Subscriber admin."""
    raw_id_fields = ('user',)


@admin.register(models.Subscribe)
class SubscribeAdmin(admin.ModelAdmin):
    """Subscribe admin."""
    raw_id_fields = ('subscriber',)
