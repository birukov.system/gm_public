from .common import common_urlpatterns

app_name = 'translation'

urlpatterns = []
urlpatterns.extend(common_urlpatterns)
