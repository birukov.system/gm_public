from django.apps import AppConfig
from django.utils.text import gettext_lazy as _


class ReportConfig(AppConfig):
    name = 'report'
    verbose_name = _('Report')

