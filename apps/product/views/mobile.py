from rest_framework import generics

from product.models import Product
from product.views import common as views_common
from product.serializers import mobile as serializers


class ProductMobileDetailView(views_common.ProductDetailView, generics.RetrieveAPIView):
    serializer_class = serializers.ProductMobileDetailSerializer

    def get_queryset(self):
        return Product.objects.annotate_precaution(self.request.locale).prefetch_comments()
