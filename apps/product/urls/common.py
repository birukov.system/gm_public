"""Product url patterns."""
from django.urls import path

from product import views

app_name = 'product'

urlpatterns = [
    path('', views.ProductListView.as_view(), name='list'),
    path('slug/<slug:slug>/', views.ProductDetailView.as_view(), name='detail'),
    path('slug/<slug:slug>/favorites/', views.CreateFavoriteProductView.as_view(),
         name='create-destroy-favorites'),
    path('slug/<slug:slug>/comments/', views.ProductCommentListView.as_view(),
         name='list-comments'),
    path('slug/<slug:slug>/comments/create/', views.ProductCommentCreateView.as_view(),
         name='create-comment'),
    path('slug/<slug:slug>/comments/<int:comment_id>/', views.ProductCommentRUDView.as_view(),
         name='rud-comment'),

    # similar products by type/subtype
    # temporary uses single mechanism, bec. description in process
    # path('slug/<slug:slug>/similar/wines/', views.SimilarListView.as_view(),
    #      name='similar-wine'),
    # path('slug/<slug:slug>/similar/liquors/', views.SimilarListView.as_view(),
    #      name='similar-liquor'),
    # path('slug/<slug:slug>/similar/food/', views.SimilarListView.as_view(),
    #      name='similar-food'),

    path('slug/<slug:slug>/similar/<str:type>/', views.SimilarListView.as_view(),
         name='similar-products')
]
