# Generated by Django 2.2.7 on 2020-03-05 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tag', '0020_auto_20200226_1428'),
    ]

    operations = [
        migrations.AddField(
            model_name='tagcategory',
            name='priority',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
    ]
