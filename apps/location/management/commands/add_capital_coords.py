import json

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.gis.geos import Point
from tqdm import tqdm

from location.models import Country


class Command(BaseCommand):
    help = """Add capital coordinates to existing countries."""

    def handle(self, *args, **kwarg):
        data = {}

        with open(f"{settings.PROJECT_ROOT}/apps/location/management/commands/country-capitals.json",
                  "r") as capitals_source:
            capitals = json.load(capitals_source)
            data = {c.get('CountryCode'): [c.get('CapitalLatitude'), c.get('CapitalLongitude')] for c in capitals}

        country_qs = Country.objects.all()

        to_update = []
        for country in tqdm(country_qs, desc='Iterate over countries'):
            country_code = country.code.upper()

            # Workaround for England
            if country_code == 'EN': country_code = 'GB'

            coords = data.get(country_code, None)
            if not coords:
                self.stdout.write(self.style.ERROR(f"No coords for country_code: {country_code}"))
                continue

            lat, long = coords

            self.stdout.write(self.style.SUCCESS(f'Got coords for country_code {country_code}: {lat},{long}'))
            country.capital_coords = Point(float(long), float(lat))
            to_update.append(country)

        Country.objects.bulk_update(to_update, ['capital_coords', ])
        self.stdout.write(self.style.WARNING(f'Updated {len(to_update)} countries'))
