from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    FacetedSearchFilterBackend,
    OrderingFilterBackend,
)
from django_elasticsearch_dsl_drf import constants
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet
from news.models import News

from search_indexes import filters, serializers, utils
from search_indexes.documents import (UserDocument, TranslationDocument, EmployeeDocument, EstablishmentDocument,
                                      NewsDocument)
from utils.methods import get_permission_classes
from utils.pagination import ESDocumentPagination
from utils.permissions import (IsReviewManager, IsTranslator, IsEstablishmentManager, IsEstablishmentAdministrator,
                               IsContentPageManager)


class UsersBackDocumentViewSet(BaseDocumentViewSet):
    """
    Users document ViewSet.

    **GET**
    ```
    Implement getting list of Users document for bo section.
    ```
    """

    document = UserDocument
    lookup_field = 'id'
    pagination_class = ESDocumentPagination
    permission_classes = get_permission_classes(IsReviewManager)
    serializer_class = serializers.UserDocumentBackSerializer

    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.CustomSearchFilterBackend,
        FacetedSearchFilterBackend,
        filters.PostFilterBackend,
        OrderingFilterBackend,
        filters.CustomDefaultOrderingFilterBackend
    ]

    post_filter_fields = {
        'role': {
            'field': 'roles_indexing.role',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
            ]
        }
    }

    search_fields = {
        'username': {'boost': 300},
        'phone': {'boost': 100},
        'last_name': {'boost': 100},
        'first_name': {'boost': 50},
        'email': {'boost': 10},
    }

    faceted_search_fields = {
        'role': {
            'field': 'roles_indexing.role',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
    }

    ordering_fields = {
        'last_login': {
            'field': 'last_login',
        },
        'date_joined': {
            'field': 'date_joined',
        },
    }

    ordering = ('-last_login', )


class TranslationsDocumentViewSet(BaseDocumentViewSet):
    """
    Translations document ViewSet.

    **GET**
    ```
    Implement getting list of Translations document for bo section.
    ```
    """
    document = TranslationDocument
    lookup_field = 'id'
    pagination_class = ESDocumentPagination
    permission_classes = get_permission_classes(IsTranslator)
    serializer_class = serializers.TranslationESSerializer

    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.CustomSearchFilterBackend,
        FilteringFilterBackend,
        OrderingFilterBackend,
        filters.CustomDefaultOrderingFilterBackend
    ]

    filter_fields = {
        'pending': {
            'field': 'keys_pending_translated',
        },
        'done': {
            'field': 'keys_translated',
        }
    }

    search_fields = {
        'page': {'boost': 9000},
        'text_en_searchable': {'boost': 1000},
        'text_not_en_searchable': {'boost': 100},
        'keywords': {'boost': 10},
    }

    ordering_fields = {
        'modified': {
            'field': 'modified',
        },
    }

    ordering = ('-modified', )


class EmployeesBackDocumentViewSet(BaseDocumentViewSet):
    """
    Employees document ViewSet.

    **GET**
    ```
    Implement getting list of Employees document for bo section.
    ```
    """

    document = EmployeeDocument
    lookup_field = 'id'
    pagination_class = ESDocumentPagination
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    serializer_class = serializers.EmployeeEsSerializer

    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.CustomSearchFilterBackend,
        filters.PostFilterBackend,
        filters.EmployeesFacetedSearchFilterBackend,
        OrderingFilterBackend,
        filters.CustomDefaultOrderingFilterBackend
    ]

    post_filter_fields = {
        'toque_number': {
            'field': 'toque_number_indexing',
        },
        'public_mark': {
            'field': 'public_mark_indexing',
        },
        'positions': {
            'field': 'positions_indexing.id',
        }
    }

    search_fields = {
        'last_name': {'boost': 1000},
        'name': {'boost': 1000},
        'awards.title_searchable': {'boost': 700},
        'awards.vintage_year': {'boost': 600},
        'awards.award_type.name': {'boost': 500},
    }

    faceted_search_fields = {
        'toque_number': {
            'field': 'toque_number_indexing',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'public_mark': {
            'field': 'public_mark',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'positions': {
            'field': 'positions_indexing.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
    }

    ordering_fields = {
    }

    ordering = ('-modified', )


class EstablishmentBackOfficeDocumentViewSet(BaseDocumentViewSet):
    """
    Employees document ViewSet.

    **GET**
    ```
    Implement getting list of Establishment documents for bo section.
    ```
    """

    def dispatch(self, request, *args, **kwargs):

        review_filter_aplly_on = int(self.request.GET.get('review_apply_on', '0'))
        dependent_fields = ('review_status', 'toque_number', 'public_mark')
        param_to_native_field = (
            'last_published_review',
            'most_recent_review',
            'last_published_and_most_recent_reviews',
            'reviews'
        )
        native_field = param_to_native_field[review_filter_aplly_on]
        """
        0 - published review only
        1 - most recent review only
        2 - published and most recent
        3 - all reviews
        """
        self.post_filter_fields['review_status']['field'] = f'{native_field}.status'
        self.faceted_search_fields['review_status']['field'] = f'{native_field}.status'

        self.post_filter_fields['toque_number']['field'] = f'{native_field}.toque_number'
        self.faceted_search_fields['toque_number']['field'] = f'{native_field}.toque_number'

        self.post_filter_fields['mark']['field'] = f'{native_field}.mark'
        self.faceted_search_fields['mark']['field'] = f'{native_field}.mark'

        return super().dispatch(request, *args, **kwargs)

    document = EstablishmentDocument
    lookup_field = 'id'
    pagination_class = ESDocumentPagination
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    serializer_class = serializers.EstablishmentBackOfficeDocumentSerializer

    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.RbacObjectsBackend,
        filters.CustomSearchFilterBackend,
        filters.PostFilterBackend,
        filters.EmployeesFacetedSearchFilterBackend,
        OrderingFilterBackend,
        filters.CustomDefaultOrderingFilterBackend
    ]

    country_field = 'address.city.country.code'

    post_filter_fields = {
        'bo_type': {
            'field': 'bo_type',
        },
        'is_pop': {
            'field': 'is_pop',
        },
        'city_id': {
            'field': 'address.city.id',
        },
        'toque_number': {
            'field': 'toque_number',
        },
        'status': {
            'field': 'status',
        },
        'mark': {
            'field': 'public_mark',
        },
        'review_status': {
            'field': 'reviews.status',
        },
        'vintage_year': {
            'field': 'vintage_year',
        },
        'last_inquiry_by': {
            'field': 'last_inquiry_author.id',
        },
        'region': {
            'field': 'address.city.region.parent_region.id'
        },
        'subregion': {
            'field': 'address.city.region.id'
        },
        'wine_region': {
            'field': 'wine_origins.wine_region.id',
            'lookups': [
                constants.LOOKUP_QUERY_IN,
                constants.LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'employee_id': {
            'field': 'actual_employees_indexing.id',
        },
    }

    search_fields = {
        'name': {'boost': 100},
        'address.street_name_1': {'boost': 99},
        'address.street_name_2': {'boost': 98},
        'address.district_name': {'boost': 97},
        'transportation': {'boost': 96},
        'address.postal_code': {'boost': 95},
        'address.city.name_searchable': {'boost': 94},
        'address.city.region.parent_region.name': {'boost': 93},
        'wine_origins.wine_region.name': {'boost': 92},
        'contact_emails': {'boost': 91},
        'contact_phones': {'boost': 90},
        'team_members.username': {'boost': 89},
        'tags.label_searchable': {'boost': 88},
        'reviews.text_searchable': {'boost': 87},
    }

    faceted_search_fields = {
        'bo_type': {
            'field': 'bo_type',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'toque_number': {
            'field': 'toque_number',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'status': {
            'field': 'status',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'is_pop': {
            'field': 'is_pop',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'mark': {
            'field': 'public_mark',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'review_status': {
            'field': 'reviews.status',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'vintage_year': {
            'field': 'vintage_year',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'last_inquiry_by': {
            'field': 'last_inquiry_author.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'region': {
            'field': 'address.city.region.parent_region.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'subregion': {
            'field': 'address.city.region.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'wine_region': {
            'field': 'wine_origins.wine_region.id',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        }
    }

    ordering_fields = {
    }

    ordering = ('-last_updated', )


class BackOfficeNewsDocumentViewSet(BaseDocumentViewSet):

    document = NewsDocument
    country_field = 'country.code'
    search_fields = {
        'title_searchable': {'boost': 4},
        'subtitle_searchable': {'boost': 3},
        'description_searchable': {'boost': 2},
        'tags.label_searchable': {'boost': 1},
    }
    lookup_field = 'id'
    serializer_class = serializers.NewsDocumentBackSerializer
    pagination_class = ESDocumentPagination
    permission_classes = get_permission_classes(IsContentPageManager)    
    filter_backends = [
        filters.AccurateEsCountBackend,
        filters.RbacObjectsBackend,
        filters.CustomSearchFilterBackend,
        filters.PostFilterBackend,
        filters.EmployeesFacetedSearchFilterBackend,
        OrderingFilterBackend,
        filters.CustomDefaultOrderingFilterBackend
    ]

    def rbac_filter(self, request):
        return News.objects.available_news(request.user, request.country_code)

    ordering_fields = {
        'publication_datetime': 'start'
    }
    post_filter_fields = {
        'type': {
            'field': 'news_type.name',
        },
        'status': {
            'field': 'state',
        }
    }
    faceted_search_fields = {
        'type': {
            'field': 'news_type.name',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
        'status': {
            'field': 'state',
            'enabled': True,
            'facet': TermsFacet,
            'options': {
                'size': utils.FACET_MAX_RESPONSE,
            },
        },
    }
    ordering = ('-created', )