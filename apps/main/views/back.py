from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from django.db import IntegrityError
from django.db.models.deletion import ProtectedError
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from establishment.models import Employee
from establishment.serializers.back import EmployeeBackSerializers
from main import serializers
from main import tasks
from main.filters import AwardFilter, AwardTypeFilterSet, AwardEntityTypeFilterSet, PageTypeFilterSet
from main.models import (
    Award, Footer, PageType, Panel, SiteFeature,
    Feature, AwardType, Carousel, AwardEntityType
)
from main.serializers.back import PanelSerializer, BackCarouselListSerializer, BackSiteSerializer
from main.views import SiteSettingsView, SiteListView
from utils.methods import get_permission_classes
from utils.permissions import IsEstablishmentManager, IsReadOnly


class AwardBaseMixinView:
    """Base view for Award type."""
    queryset = Award.objects.all().with_base_related()
    permission_classes = get_permission_classes(
        IsEstablishmentManager
    )


class AwardListCreateView(AwardBaseMixinView, generics.ListCreateAPIView):
    """
    ## List of awards
    ### *GET*
    #### Description
    Return paginated list of awards.
    Available filters:
    * establishment_id (`int`) - Filter by establishment identifier
    * product_id (`int`) - Filter by product identifier
    * employee_id (`int`) - Filter by employee identifier
    * state (`enum`) - `0 (Waiting)`, `1 (Published)`
    * award_type (`str`) - Filter by award type identifier
    * vintage_year (`str`) - Filter by a vintage year
    * award_entity_type_id__in - Filter by identifiers of an object related entity
    (e.g. - ?award_entity_type_id__in=1,2)
    ##### Response
    E.g.:
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```

    ### *POST*
    #### Description
    Create a record in Award table.
    ##### Request
    ###### Required
    * content_type (`int`) - identifier of content type entity
    * object_id (`int`) - identifier of content object
    * award_type (`int`) - identifier of award type
    * title (`str`) - title of an award
    ###### Non required
    * vintage_year (str) - vintage year in a format - `yyyy`
    ##### Response
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.BackAwardSerializer
    filterset_class = AwardFilter


class AwardCreateAndBind(AwardBaseMixinView, generics.CreateAPIView):
    """
    ## Creating an Award for an Employee.
    ### *POST*
    #### Description
    Creating an Award for an Employee and return in response
    serialized Employee object.
    ##### Response
    E.g.
    ```
        {
            "id": 1,
            ...
        }
    ```
    ##### Request
    ###### Required
    * award_type (`int`) - identifier of award type
    * title (`str`) - title of an award
    ###### Non required
    * vintage_year (str) - vintage year in a format - `yyyy`
    ##### Response
    E.g.
    ```
        {
            "id": 1,
            ...
        }
    ```
    """
    serializer_class = serializers.BackAwardEmployeeCreateSerializer

    def create(self, request, *args, **kwargs):
        """Overridden create method."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        response_serializer = EmployeeBackSerializers(Employee.objects.get(pk=kwargs['employee_id']))
        headers = self.get_success_headers(response_serializer.data)
        return Response(response_serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class AwardRUDView(AwardBaseMixinView, generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve/Update/Destroy Award view
    ### *GET*
    #### Description
    Retrieving serialized object of an Award by an identifier
    #### Response
    E.g.
    ```
        {
            "id": 1,
            ...
        }
    ```

    ### *PATCH*
    #### Description
    Partially update Award object by identifier
    ##### Request
    Available:
    * content_type (`int`) - identifier of content type entity
    * object_id (`int`) - identifier of content object
    * award_type (`int`) - identifier of award type
    * title (`str`) - title of an award
    * vintage_year (str) - vintage year in a format - `yyyy`
    ##### Response
    E.g.
    ```
        {
            "id": 1,
            ...
        }
    ```

    ### *DELETE*
    #### Description
    Delete an Award instance by award identifier
    ##### Request
    ```
    No request data
    ```
    ##### Response
    E.g.
    ```
    No content
    ```
    """
    serializer_class = serializers.BackAwardSerializer
    lookup_field = 'id'


class AwardTypeMixinView:
    """Base view for Award type."""
    pagination_class = None
    lookup_field = 'id'
    permission_classes = get_permission_classes(
        IsEstablishmentManager
    )

    def get_queryset(self):
        """An overridden get_queryset method."""
        if hasattr(self, 'request') and hasattr(self.request, 'country_code'):
            return AwardType.objects.by_country_code(self.request.country_code).order_by('modified')
        return AwardType.objects.none()


class AwardTypesListCreateView(AwardTypeMixinView, generics.ListCreateAPIView):
    """
    ## List/Create view
    ### *GET*
    #### Description
    Return non paginated list of Award types filtered by request country code.
    Available filters:
    * id (`int`) - award type identifier
    * name (`str`) - award type name
    * award_entity_type_id__in - Filter by identifiers of an object related entity
    (e.g. - ?award_entity_type_id__in=1,2)
    ##### Response
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```

    ### *POST*
    #### Description
    Create new award type
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    ##### Request
    ###### Required
    county_id (`int`) - identifier of country
    name (`str`) - name of award type
    entity_type (`int`) - identifier of entity type
    ###### Available
    years (`list`) - list of years, e.g. - [2020, 2019, 2018]
    """
    serializer_class = serializers.AwardTypeBaseSerializer
    filter_class = AwardTypeFilterSet

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except ValidationError as e:
            if 'non_field_errors' in e.detail:
                from utils.exceptions import UniqueViolationError
                raise UniqueViolationError(model_name=self.serializer_class.Meta.model.__name__)
            else:
                raise e


class AwardEntityTypeListCreateView(generics.ListCreateAPIView):
    """
    ## Award entity type list view
    ### *GET*
    #### Description
    Returned non-paginated list of award entity types.
    ##### Response
    E.g.:
    ```
    {
      [
        {
          "id": 11,
          ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Create new entity of an award entity type
    ##### Request
    Required
    * content_type (`int`) - identifier of a related model
    * object_id (`int`) - identifier of an object
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    queryset = AwardEntityType.objects.all()
    pagination_class = None
    serializer_class = serializers.AwardEntityTypeBaseSerializer
    filter_class = AwardEntityTypeFilterSet
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsReadOnly,
    )


class AwardTypeDestroyView(AwardTypeMixinView, generics.DestroyAPIView):
    """
    ## Destroy view
    ### *DELETE*
    #### Description
    Delete an award type by an identifier
    ##### Response
    E.g.:
    ```
    No content
    ```
    """

    def delete(self, request, *args, **kwargs):
        try:
            return super().delete(request, *args, **kwargs)
        except ProtectedError:
            from utils.exceptions import ForeignKeyProtectError
            raise ForeignKeyProtectError(detail='Award couldn\'t be deleted.')


class ContentTypeView(generics.ListAPIView):
    """
    ## ContentType list view
    ### **GET**
    #### Description
    Return paginated list of a content types.
    #### Available filters
    * id (`int`) - by an identifier
    * model (`str`) - by model name
    * app_label (`str`) - by application label
    ##### Response
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    """
    queryset = ContentType.objects.all()
    serializer_class = serializers.ContentTypeBackSerializer
    permission_classes = get_permission_classes()
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = (
        'id',
        'model',
        'app_label',
    )


class FeatureBackView(generics.ListCreateAPIView):
    """
    ## Feature list or create View.
    ### **GET**
    #### Description
    Return non-paginated list of features.
    ##### Response
    E.g.
    ```
    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST**
    #### Description
    Create a new feature
    ##### Request
    Required
    * slug (`str`) - unique slug
    Non-required
    * priority (`int`) - priority of feature
    * route (`int`) - identifier of page type
    * site_settings (`int`) - identifier of site settings
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.FeatureSerializer
    queryset = Feature.objects.all()
    permission_classes = get_permission_classes()


class SiteFeatureBackView(generics.ListCreateAPIView):
    """
    ## Site feature list or create View.
    ### **GET**
    #### Description
    Return non-paginated list of site features.
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```

    ### **POST**
    #### Description
    Create a new site feature.
    ##### Request
    Required
    * feature_id (`int`) - identifier of feature
    * site_settings_id (`int`) - identifier of site_settings
    Non-required
    * published (`bool`) - default - `False`
    * main (`bool`) - default - `False`
    * backoffice (`bool`) - default - `False`,
    * nested (`int`) - identifier of recursive relationship
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.SiteFeatureSerializer
    queryset = SiteFeature.objects.all()
    pagination_class = None
    permission_classes = get_permission_classes()


class FeatureRUDBackView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Feature RUD View.
    ### **GET**
    #### Description
    Return serialized object of a feature by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a feature object by an identifier.
    ##### Request
    * slug (`str`) - unique slug
    * priority (`int`) - priority of feature
    * route (`int`) - identifier of page type
    * site_settings (`int`) - identifier of site settings
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete a feature instance by an identifier.
    ##### Request
    ```
    No content
    ```
    ##### Response
    ```
    No body
    ```
    """
    serializer_class = serializers.FeatureSerializer
    queryset = Feature.objects.all()
    permission_classes = get_permission_classes()


class SiteFeatureRUDBackView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Site feature RUD View.
    ### **GET**
    #### Description
    Return serialized object of a site feature by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a site feature object by an identifier.
    ##### Request
    * feature_id (`int`) - identifier of feature
    * site_settings_id (`int`) - identifier of site_settings
    * published (`bool`) - default - `False`
    * main (`bool`) - default - `False`
    * backoffice (`bool`) - default - `False`,
    * nested (`int`) - identifier of recursive relationship
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete a site feature instance by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.SiteFeatureSerializer
    queryset = SiteFeature.objects.all()
    permission_classes = get_permission_classes()


class SiteSettingsBackOfficeView(SiteSettingsView):
    """
    ## Site settings RUD view.
    ### **GET**
    #### Description
    Retrieve a serialized object of site settings by a `subdomain`.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a site settings object by a `subdomain`.
    ##### Request
    * subdomain (`str`)
    * pinterest_page_url (`str`)
    * twitter_page_url (`str`)
    * facebook_page_url (`str`)
    * instagram_page_url (`str`)
    * contact_email (`str`)
    * config (`JSON`) - JSON configuration
    * ad_config (`str`)
    ##### Response
    E.g.
    ```
    {
        "country_code": "fr",
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete a site settings by a `subdomain`.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = BackSiteSerializer
    permission_classes = get_permission_classes()


class SiteListBackOfficeView(SiteListView):
    """
    ## Site settings List/Create view.
    ### **GET**
    #### Description
    Return non-paginated list of site settings.
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```

    ### **POST**
    #### Description
    Create a new site settings instance.
    ##### Request
    Required
    * subdomain (`str`) - e.g. *fr*
    Non-required
    * pinterest_page_url (`str`)
    * twitter_page_url (`str`)
    * facebook_page_url (`str`)
    * instagram_page_url (`str`)
    * contact_email (`str`)
    * config (`JSON`) - JSON configuration
    * ad_config (`str`)
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.SiteSerializer
    permission_classes = get_permission_classes()


class FooterBackView(generics.ListCreateAPIView):
    """
    ## Footer back list/create view.
    ### **GET**
    #### Description
    Return paginated list of footers.
    ##### Response
    E.g.
    ```
        {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST**
    #### Description
    Create a new footer
    ##### Request
    * site_id (`int`) - identifier of site
    * about_us (`str`) - text section "about_us"
    * copyright (`str`) - text section "copyright"
    * links (`list`) - list of identifiers of footer links
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.FooterBackSerializer
    queryset = Footer.objects.all()
    permission_classes = get_permission_classes()


class FooterRUDBackView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Footer back RUD view.
    ### **GET**
    #### Description
    Return serialized object of a footer by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a footer object by an identifier.
    ##### Request
    * site_id (`int`) - identifier of site
    * about_us (`str`) - text section "about_us"
    * copyright (`str`) - text section "copyright"
    * links (`list`) - list of identifiers of footer links
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete a footer instance by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.FooterBackSerializer
    queryset = Footer.objects.all()
    permission_classes = get_permission_classes()


class PageTypeListCreateView(generics.ListCreateAPIView):
    """
    ## PageType back office view.
    ### **GET**
    #### Description
    Return non-paginated list of a page types.
    #### Available filters
    * is_back_office (`bool`) - by an existence in back office
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **POST**
    #### Description
    Create a new instance of page type.
    ##### Request
    * name (`str`) - name of page type
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    pagination_class = None
    serializer_class = serializers.PageTypeBaseSerializer
    queryset = PageType.objects.all()
    permission_classes = get_permission_classes()
    filter_class = PageTypeFilterSet


class PanelsListCreateView(generics.ListCreateAPIView):
    """
    ## Panels List/Create view.
    ### **GET**
    #### Description
    Return paginated list of panels.
    ##### Response
    E.g.
    ```
    {
        "count": 0,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```

    ### **POST**
    #### Description
    Create a new panel.
    ##### Request
    Required
    * name (`str`) - name of a panel
    * user_id (`int`) - identifier of a user
    Non-required
    * description (`str`) - description
    * query (`str`) - query
    * display (`str`) - enum, default - None
    ```
    TABLE = 'table'
    MAILING = 'mailing',
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = PanelSerializer
    queryset = Panel.objects.all()
    permission_classes = get_permission_classes()


class PanelsRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Panels RUD view.
    ### **GET**
    #### Description
    Return a serialized object of a panel by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Create a new panel.
    ##### Request
    Completely/Partially update a panel object by an identifier.
    * name (`str`) - name of a panel
    * user_id (`int`) - identifier of a user
    * description (`str`) - description
    * query (`str`) - query
    * display (`str`) - enum, default - None
    ```
    TABLE = 'table'
    MAILING = 'mailing',
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete a panel instance by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = PanelSerializer
    queryset = Panel.objects.all()
    permission_classes = get_permission_classes()


class PanelsExecuteView(generics.ListAPIView):
    """
    ## List panel view.
    ### **GET**
    #### Description
    Execute a panel **query** and showing its serialized data.
    Showing results would be paginated.
    ##### Response
    E.g.
    ```
    {
        "count": 0,
        "next": 0,
        "previous": None,
        "columns": None,
        "results": []
    }
    ```
    """
    queryset = Panel.objects.all()
    permission_classes = get_permission_classes()

    def list(self, request, *args, **kwargs):
        """
        ## List panel view.
        ### **GET**
        #### Description
        Execute a panel **query** and showing its serialized data.
        Showing results would be paginated.
        ##### Response
        E.g.
        ```
        {
            "count": 0,
            "next": 0,
            "previous": None,
            "columns": None,
            "results": []
        }
        ```
        """
        panel = get_object_or_404(Panel, id=self.kwargs['pk'])
        return Response(panel.execute_query(request))


class PanelsExportCSVView(PanelsExecuteView):
    """
    ## Export panels via csv view.
    ### **GET**
    #### Description
    Method allows exporting panel information to `csv` file that
    would be sent to **requested user** via an email.
    ##### Response
    E.g.
    ```
    {
        "success": 'The file will be sent to your email.'
    }
    ```
    """
    queryset = Panel.objects.all()
    permission_classes = get_permission_classes()

    def list(self, request, *args, **kwargs):
        """
        ## Export panels via csv view.
        ### **GET**
        #### Description
        Method allows exporting panel information to `csv` file that
        would be sent to **requested user** via an email.
        ##### Response
        E.g.
        ```
        {
            "success": 'The file will be sent to your email.'
        }
        ```
        """
        panel = get_object_or_404(Panel, id=self.kwargs['pk'])
        # make task for celery
        tasks.send_export_to_email.delay(
            panel_id=panel.id, user_id=request.user.id)
        return Response(
            {"success": _('The file will be sent to your email.')},
            status=status.HTTP_200_OK
        )


class PanelsExecuteXLSView(PanelsExecuteView):
    """
    ## Export panels via xlsx view.
    ### **GET**
    #### Description
    Method allows exporting panel information to `xlsx` file that
    would be sent to **requested user** via an email.
    ##### Response
    E.g.
    ```
    {
        "success": 'The file will be sent to your email.'
    }
    ```
    """
    queryset = Panel.objects.all()
    permission_classes = get_permission_classes()

    def list(self, request, *args, **kwargs):
        """
        ## Export panels via xlsx view.
        ### **GET**
        #### Description
        Method allows exporting panel information to `xlsx` file that
        would be sent to **requested user** via an email.
        ##### Response
        E.g.
        ```
        {
            "success": 'The file will be sent to your email.'
        }
        ```
        """
        panel = get_object_or_404(Panel, id=self.kwargs['pk'])
        # make task for celery
        tasks.send_export_to_email.delay(
            panel_id=panel.id, user_id=request.user.id, file_type='xls')
        return Response(
            {"success": _('The file will be sent to your email.')},
            status=status.HTTP_200_OK
        )


class BackCarouselListView(generics.ListAPIView):
    """
    ## List of carousel.
    ### *GET*
    #### Description
    Return non-paginated list of carousel items.
    ##### Response
    E.g.:
    ```
    {
      "id": 1,
      "model_name": "model_name",
      "name": "name",
      ...
      "awards": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    """

    serializer_class = BackCarouselListSerializer
    permission_classes = get_permission_classes()
    pagination_class = None

    def get_queryset(self):
        country_code = self.request.country_code
        qs = Carousel.objects.is_parsed().active()
        if country_code:
            qs = qs.by_country_code(country_code)
        return qs.order_by('-modified')


class BackCarouselRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve/Update/Destroy Carousel view
    ### *GET*
    #### Description
    Retrieving serialized object of a carousel by an identifier.
    #### Response
    E.g.
    ```
        {
            "id": 1,
            ...
        }
    ```

    ### *PATCH*
    #### Description
    Partially update a carousel object by an identifier.
    ##### Request
    Available:
    * active (`bool`)
    ##### Response
    E.g.
    ```
        {
            "id": 1,
            ...
        }
    ```

    ### *DELETE*
    #### Description
    Delete a carousel instance by an identifier.
    ##### Request
    ```
    No request data
    ```
    ##### Response
    E.g.
    ```
    No content
    ```
    """
    serializer_class = BackCarouselListSerializer
    queryset = Carousel.objects.all()
    permission_classes = get_permission_classes()
