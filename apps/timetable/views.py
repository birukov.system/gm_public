from rest_framework import generics, permissions
from timetable import serialziers, models


class TimetableListView(generics.ListAPIView):
    """Method to get timetables"""
    serializer_class = serialziers.TimetableSerializer
    queryset = models.Timetable.objects.all()
    pagination_class = None
    permission_classes = (permissions.AllowAny, )
