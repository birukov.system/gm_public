from django.core.management.base import BaseCommand
from tqdm import tqdm

from establishment.models import Establishment
from transfer.models import Descriptions


class Command(BaseCommand):
    help = """Add description to establishment from old db."""

    def handle(self, *args, **kwarg):
        establishments = Establishment.objects.exclude(old_id__isnull=True)

        self.stdout.write(self.style.WARNING(f'Clear old descriptions'))
        for item in tqdm(establishments):
            item.description = None
            item.save()

        queryset = Descriptions.objects.filter(
            establishment_id__in=list(establishments.values_list('old_id', flat=True)),
        ).values_list('establishment_id', 'locale', 'text')

        self.stdout.write(self.style.WARNING(f'Update new description'))
        for establishment_id, locale, text in tqdm(queryset):
            establishment = Establishment.objects.filter(old_id=establishment_id).first()
            if establishment:
                if establishment.description:
                    establishment.description.update({
                        locale: text
                    })
                else:
                    establishment.description = {locale: text}
                establishment.save()

        self.stdout.write(self.style.WARNING(f'Update en-GB description'))
        for establishment in tqdm(establishments.filter(description__isnull=False)):
            description = establishment.description
            if len(description) and 'en-GB' not in description:
                description.update({
                    'en-GB': next(iter(description.values()))
                })
                establishment.description = description
                establishment.save()

        self.stdout.write(self.style.WARNING(f'Done'))
