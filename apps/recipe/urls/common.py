"""Recipe app common urlpatterns."""
from django.urls import path
from recipe import views


app_name = 'recipe'
urlpatterns = [
    path('', views.RecipeListView.as_view(), name='list'),
    path('<str:slug>/', views.RecipeDetailView.as_view(), name='detail'),
]
