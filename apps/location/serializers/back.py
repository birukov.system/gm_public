from location import models
from location.serializers import common
from rest_framework import serializers
from utils.serializers import TranslatedField


class AddressCreateSerializer(common.AddressDetailSerializer):
    """Address create serializer."""


class CountryBackSerializer(common.CountrySerializer):
    """Country back-office serializer."""

    name_translated = TranslatedField()
    has_sub_region = serializers.BooleanField(read_only=True)

    class Meta:
        model = models.Country
        fields = [
            'id',
            'code',
            'flag_image',
            'name',
            'name_translated',
            'has_sub_region',
            'display_calling_code',
            'default_calling_code',
            'articles',
        ]
        extra_kwargs = {
            'calling_code': {'write_only': True}
        }


class PopularCitySerializer(common.CityShortSerializer):
    """Serializer for list of popular cities."""

    class Meta(common.CityShortSerializer.Meta):
        """Meta class."""
        fields = [
            'id',
            'name_translated',
            'is_popular',
        ]
        extra_kwargs = {
            'is_popular': {'write_only': True, }
        }
