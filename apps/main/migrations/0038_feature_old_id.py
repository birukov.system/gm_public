# Generated by Django 2.2.7 on 2019-11-26 11:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0037_sitesettings_old_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='feature',
            name='old_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
