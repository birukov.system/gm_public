"""Serializer for app timetable"""

from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from establishment.models import Establishment
from timetable.models import Timetable, Holiday


class GetEstablishmentMixin:
    def get_establishment_instance(self):
        establishment_pk = self.context.get('request') \
            .parser_context.get('view') \
            .kwargs.get('pk')

        establishment_slug = self.context.get('request') \
            .parser_context.get('view') \
            .kwargs.get('slug')

        search_kwargs = {'pk': establishment_pk} if establishment_pk else {'slug': establishment_slug}

        # Check if establishment exists.
        establishment_qs = Establishment.objects.filter(**search_kwargs)
        if not establishment_qs.exists():
            return None

        return establishment_qs.first()


class ScheduleRUDSerializer(GetEstablishmentMixin, serializers.ModelSerializer):
    """Serializer for Establishment model."""
    NULLABLE_FIELDS = ['lunch_start', 'lunch_end', 'dinner_start',
                       'dinner_end', 'opening_at', 'closed_at']

    weekday_display = serializers.CharField(source='get_weekday_display', read_only=True)
    weekday_display_short = serializers.CharField(read_only=True)

    lunch_start = serializers.TimeField(required=False, allow_null=True)
    lunch_end = serializers.TimeField(required=False, allow_null=True)
    dinner_start = serializers.TimeField(required=False, allow_null=True)
    dinner_end = serializers.TimeField(required=False, allow_null=True)
    opening_at = serializers.TimeField(required=False, allow_null=True)
    closed_at = serializers.TimeField(required=False, allow_null=True)

    class Meta:
        """Meta class."""
        model = Timetable
        fields = [
            'id',
            'weekday_display',
            'weekday_display_short',
            'weekday',
            'lunch_start',
            'lunch_end',
            'dinner_start',
            'dinner_end',
            'opening_at',
            'closed_at',
        ]

    def validate(self, attrs):
        """Override validate method"""
        establishment = self.get_establishment_instance()
        if establishment is None:
            raise serializers.ValidationError({'detail': _('Establishment not found.')})

        attrs['establishment'] = establishment

        # If fields not in request data then put it in attrs with None value.
        if not self.partial:
            for field in self.NULLABLE_FIELDS:
                if field not in attrs:
                    attrs.setdefault(field, None)
        return attrs


class ScheduleOneDayCreateSerializer(ScheduleRUDSerializer):
    SUITABLE_VARIANTS = (
        {'opening_at', 'closed_at'},
        {'lunch_start', 'lunch_end'},
        {'dinner_start', 'dinner_end'},
    )
    weekday = serializers.IntegerField(write_only=True)

    class Meta:
        """Meta class."""
        model = Timetable
        fields = ScheduleRUDSerializer.Meta.fields + [
            'weekday',
        ]
        extra_kwargs = {
            'id': {'read_only': False, 'required': False, 'allow_null': True}
        }

    def validate(self, attrs):
        not_null_attrs = {k: v for k, v in attrs.items() if v is not None}
        for variant in self.SUITABLE_VARIANTS:
            if len(variant - not_null_attrs.keys()) == 1:
                raise serializers.ValidationError(f'Broken pair: {variant}')

        if not self.partial:
            for field in self.NULLABLE_FIELDS:
                if field not in attrs:
                    attrs.setdefault(field, None)
        return attrs


class ScheduleCreateSerializer(GetEstablishmentMixin, serializers.Serializer):
    schedule = serializers.ListField(child=ScheduleOneDayCreateSerializer())

    def to_internal_value(self, data):
        if not isinstance(data, list):
            raise serializers.ValidationError({'non_field_errors': ['List expected']})

        # set weekday choice for any weekday
        for i, weekday in enumerate(data):
            weekday['weekday'] = i

        return super().to_internal_value({'schedule': data})

    def validate(self, attrs):
        if len(attrs['schedule']) != len(Timetable.WEEKDAYS_CHOICES):
            raise serializers.ValidationError(_('Not all weekdays were provided'))

        establishment = self.get_establishment_instance()
        if establishment is None:
            raise serializers.ValidationError(_('Establishment not found.'))

        attrs['establishment'] = establishment
        return attrs

    def create(self, validated_data):
        establishment = validated_data.pop('establishment')
        establishment.schedule.all().delete()
        timetables = []
        for timetable_data in validated_data['schedule']:
            instance = Timetable.objects.create(**timetable_data)
            timetables.append(instance)
            establishment.schedule.add(instance)

        return {'schedule': timetables}


class TimetableSerializer(serializers.ModelSerializer):
    """Serailzier for Timetable model."""
    weekday_display = serializers.CharField(source='get_weekday_display', read_only=True)
    weekday_display_short = serializers.CharField(read_only=True)

    class Meta:
        model = Timetable
        fields = (
            'id',
            'weekday_display',
            'weekday_display_short',
            'works_at_noon',
        )


class HolidaySerializer(serializers.ModelSerializer):
    """Serializer for Holiday model."""

    class Meta:
        model = Holiday
        fields = ('id', 'start_date', 'end_date')
        read_only_fields = ('id',)

    def validate(self, attrs):
        if attrs['start_date'] > attrs['end_date']:
            raise serializers.ValidationError('start_date older than end_date')

        return attrs

    def save(self, establishment, **kwargs):
        self.validated_data['establishment'] = establishment
        super().save(**kwargs)


class ScheduleForCurrentWeekSerializer(ScheduleRUDSerializer):
    is_holiday = serializers.BooleanField(read_only=True, source='is_holiday_for_current_week')

    class Meta(ScheduleRUDSerializer.Meta):
        fields = ScheduleRUDSerializer.Meta.fields + [
            'is_holiday'
        ]

    def to_representation(self, instance):
        if instance.is_holiday_for_current_week:
            instance.opening_at = None
            instance.closed_at = None
            instance.lunch_start = None
            instance.lunch_end = None
            instance.dinner_start = None
            instance.dinner_end = None
        return super().to_representation(instance)
