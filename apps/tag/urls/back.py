"""Urlconf for app tag."""
from rest_framework.routers import SimpleRouter
from tag import views

app_name = 'tag'

router = SimpleRouter()
router.register(r'categories', views.TagCategoryBackOfficeViewSet)
router.register(r'', views.TagBackOfficeViewSet)

urlpatterns = router.urls
