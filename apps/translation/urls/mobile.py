from django.urls import path

from translation.views import mobile as views

app_name = 'translation'

urlpatterns = [
    path('site/', views.MobileSiteInterfaceDictionaryListCreateView.as_view(), name='site-interface-list-create'),
]
