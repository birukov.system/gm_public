"""Serializers for back office app advertisements"""
from django.db import transaction
from rest_framework import serializers

from advertisement.models import Advertisement
from advertisement.serializers import AdvertisementBaseSerializer, LanguageSerializer
from main.models import Page
from translation.models import Language


class AdvertisementBackOfficeSerializer(AdvertisementBaseSerializer):
    image_url = serializers.URLField(required=True)
    page_type = serializers.CharField(required=True)
    device = serializers.IntegerField(required=True)
    target_language = serializers.PrimaryKeyRelatedField(required=True,
                                                         allow_null=True,
                                                         queryset=Language.objects.all())

    class Meta(AdvertisementBaseSerializer.Meta):
        extra_kwargs = {
            'block_level': {'required': True},
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['page_type'] = instance.page_type_detail

        if instance.target_language is not None:
            data['target_language'] = LanguageSerializer(instance.target_language).data

        return data

    def to_internal_value(self, data):
        target_language = data.get('target_language')
        if target_language is not None:
            language_instance = Language.objects.filter(title=target_language.get('title'),
                                                        locale=target_language.get('locale')).first()
            if language_instance is None:
                raise serializers.ValidationError('Language is wrong')

            data['target_language'] = language_instance.id

        return super().to_internal_value(data)

    def validate_page_type(self, value):
        if value not in map(lambda x: x[0], Page.PAGE_TYPES):
            raise serializers.ValidationError('Invalid value')

        return value

    @transaction.atomic
    def create(self, validated_data):
        advertisement_data = validated_data.copy()
        page_type = advertisement_data.pop('page_type')
        device = advertisement_data.pop('device')
        image_url = advertisement_data.pop('image_url')

        advertisement_instance = Advertisement.objects.create(**advertisement_data)
        Page.objects.create(advertisement=advertisement_instance,
                            page_type=page_type,
                            source=device,
                            image_url=image_url)

        return advertisement_instance

    @transaction.atomic
    def update(self, instance, validated_data):
        advertisement_data = validated_data.copy()
        page_type = advertisement_data.pop('page_type', None)
        device = advertisement_data.pop('device', None)
        image_url = advertisement_data.pop('image_url', None)

        for attr, value in advertisement_data.items():
            setattr(instance, attr, value)

        if page_type is not None:
            instance.page.page_type = page_type
        if device is not None:
            instance.page.source = device
        if image_url is not None:
            instance.page.image_url = image_url

        instance.page.save()
        instance.save()

        return instance
