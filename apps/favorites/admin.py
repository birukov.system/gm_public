from django.contrib import admin

from favorites import models


@admin.register(models.Favorites)
class FavoritesModelAdmin(admin.ModelAdmin):
    """Admin model for model Favorites"""
    list_display = ('id', 'user', )
    list_filter = ('user', )

