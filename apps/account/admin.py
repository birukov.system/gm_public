"""Account app admin settings."""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _
from account import models


@admin.register(models.Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ['id', 'role', 'country', 'establishment_subtype', ]
    raw_id_fields = ['country', 'establishment_subtype', ]


@admin.register(models.UserRole)
class UserRoleAdmin(admin.ModelAdmin):
    list_display = ['user', 'role', ]
    raw_id_fields = ['user', 'role', 'requester', 'establishment', ]


@admin.register(models.User)
class UserAdmin(BaseUserAdmin):
    """User model admin settings."""

    list_display = ('id', 'username', 'short_name', 'date_joined', 'is_active',
                    'is_staff', 'is_superuser', 'email_confirmed')
    list_filter = ('is_active', 'is_staff', 'is_superuser', 'email_confirmed',
                   'groups',)
    search_fields = ('email', 'first_name', 'last_name', 'phone',)
    readonly_fields = ('last_login', 'date_joined', 'image_preview', 'cropped_image_preview', 'last_ip', 'last_country')
    fieldsets = (
        (None, {'fields': ('email', 'password',)}),
        (_('Personal info'), {
            'fields': ('username', 'first_name', 'last_name',
                       'image_url', 'image_preview', 'phone',
                       'cropped_image_url', 'cropped_image_preview',)}),
        (_('Subscription'), {
            'fields': (
                'newsletter',
            )
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined', 'last_ip', 'last_country')}),
        (_('Permissions'), {
            'fields': (
                'is_active', 'is_staff', 'is_superuser', 'email_confirmed',
                'groups', 'user_permissions'),
            'classes': ('collapse',)
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('password1', 'password2'),
        }),
    )
    ordering = ('-date_joined', 'id',)

    def short_name(self, obj):
        """Get user's short name."""
        return obj.get_short_name()

    short_name.short_description = _('Name')

    def image_preview(self, obj):
        """Get user image preview"""
        return obj.image_tag

    image_preview.short_description = 'Image preview'

    def cropped_image_preview(self, obj):
        """Get user cropped image preview"""
        return obj.cropped_image_tag

    cropped_image_preview.short_description = 'Cropped image preview'
