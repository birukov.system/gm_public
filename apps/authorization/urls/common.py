"""Common url routing for application authorization"""
from django.conf import settings
from django.conf.urls import url
from django.urls import path
from oauth2_provider.views import AuthorizationView
from rest_framework_social_oauth2 import views as drf_social_oauth2_views
from social_core.utils import setting_name
from social_django import views as social_django_views

from authorization.views import common as views

extra = getattr(settings, setting_name('TRAILING_SLASH'), True) and '/' or ''


app_name = 'auth'

urlpatterns_social_django = [
    url(r'^authorize/?$', AuthorizationView.as_view(), name="authorize"),
    url(r'^complete/(?P<backend>[^/]+){0}$'.format(extra), social_django_views.complete,
        name='complete'),
]

urlpatterns_oauth2 = [
    path('oauth2/signup/facebook/', views.OAuth2SignUpView.as_view(),
         name='oauth2-signup-facebook'),
    # for sign up via facebook
    path('oauth2/token/', drf_social_oauth2_views .TokenView.as_view(), name="token"),
]

urlpatterns_jwt = [
    path('signup/', views.SignUpView.as_view(), name='signup'),
    path('signup/confirm/<uidb64>/<token>/', views.ConfirmationEmailView.as_view(), name='signup-confirm'),
    path('login/', views.LoginByUsernameOrEmailView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name="logout"),
    path('refresh-token/', views.RefreshTokenView.as_view(), name='refresh-token'),
]


urlpatterns = urlpatterns_jwt + \
              urlpatterns_oauth2 + \
              urlpatterns_social_django  # for social oauth2
