# Generated by Django 2.2.7 on 2020-03-05 15:52

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0045_merge_20200304_0824'),
    ]

    operations = [
        migrations.AddField(
            model_name='country',
            name='articles',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=15), blank=True, default=None, null=True, size=None),
        ),
    ]
