from django.core.validators import EMPTY_VALUES
from tqdm import tqdm

from account.models import User
from establishment.models import Employee, EstablishmentEmployee, Position, Establishment, EstablishmentType, \
    EstablishmentSubType
from gallery.models import Image
from transfer.models import Profiles, Affiliations, ProfilePictures


def get_name(name, last_name, email):
    if name not in EMPTY_VALUES:
        return name
    elif last_name not in EMPTY_VALUES:
        return last_name
    elif email not in EMPTY_VALUES:
        return email
    return None


def get_gender(gender):
    result = None
    if gender == 'man':
        result = Employee.MALE
    elif gender == 'women':
        result = Employee.FEMALE
    return result


def get_phone(code, number):
    result = ''
    if code and number:
        result = f'{code}{number}'
    if len(result) < 128:
        return result


def get_user_id(user_id):
    user = User.objects.exclude(old_id__isnull=True).filter(old_id=user_id).first()
    if user:
        return user.id
    return None


def delete_all_old_employees():
    est_employees = EstablishmentEmployee.objects.filter(old_id__isnull=False)
    print(f'delete {est_employees.count()} old EstablishmentEmployee')
    est_employees.delete()

    employees = Employee.objects.filter(old_id__isnull=False)
    print(f'delete {employees.count()} old Employee')
    employees.delete()


def transfer_employee():
    """
    Profiles -> Employee
    ---
    Requirements for Employee:
    - User
    """
    profiles = Profiles.objects.all()
    print(f'find {profiles.count()} profiles in old database')

    objects = []
    for profile in tqdm(profiles, desc='create objects list of employees for bulk_create:'):
        if get_name(profile.firstname, profile.lastname, profile.email):
            objects.append(
                Employee(
                    old_id=profile.id,
                    user_id=get_user_id(profile.account_id),
                    name=get_name(profile.firstname, profile.lastname, profile.email),
                    last_name=profile.lastname,
                    sex=get_gender(profile.gender),
                    birth_date=profile.dob,
                    email=profile.email,
                    phone=get_phone(profile.phone_country_code, profile.phone_local_number),
                    created=profile.created_at,
                    created_by_id=get_user_id(profile.requester_id),
                    available_for_events=True if profile.available_for_events == 1 else False
                )
            )

    print(f'create Employees...')
    Employee.objects.bulk_create(objects)


def transfer_position():
    """
    Affiliations -> Position
    ---
    Requirements for Position:
    ---
    Positions are now binded to establishment type/subtype (foreign key on postion instance)
    null on both foreign keys means that position is common. otherwise value is required
    don't forget to fix transfer script according to following changes:

    common to all establishment types:
    owner, manager, marketing, contact, journalist, employee
    restaurant: chef, sous_chef, pastry_chef, header_waiter, sommelier
    artisan: production_director
    winery: cellar_master, oenologist, production_director
    distillery: production_director
    """
    affiliations = Affiliations.objects.all().values_list('role', flat=True)
    role_set = set(affiliations)
    print(f'find {len(role_set)} roles in old database')

    try:
        restaurant = EstablishmentType.objects.get(index_name=EstablishmentType.RESTAURANT)
    except EstablishmentType.DoesNotExist as ex:
        restaurant = None

    # try:
    #     artisan = EstablishmentType.objects.get(index_name=EstablishmentType.ARTISAN)
    # except EstablishmentType.DoesNotExist as ex:
    #     artisan = None

    try:
        winery = EstablishmentSubType.objects.get(index_name=EstablishmentSubType.WINERY)
    except EstablishmentSubType.DoesNotExist as ex:
        winery = None

    # try:
    #     distillery = EstablishmentSubType.objects.get(index_name=EstablishmentSubType.DISTILLERIES)
    # except EstablishmentSubType.DoesNotExist as ex:
    #     distillery = None

    keys = {
        'chef': (restaurant, 'establishment_type'),
        'sous_chef': (restaurant, 'establishment_type'),
        'pastry_chef': (restaurant, 'establishment_type'),
        'header_waiter': (restaurant, 'establishment_type'),
        'sommelier': (restaurant, 'establishment_type'),
        # 'production_director': artisan,
        'cellar_master': (winery, 'establishment_subtype'),
        'oenologist': (winery, 'establishment_subtype'),
        # 'production_director': winery,
        # 'production_director': distillery,
    }

    for role in tqdm(role_set, desc='create Positions:'):
        index_name = role.lower().replace(' ', '_')
        data = {
            'index_name': index_name,
            'name': {"en-GB": role.capitalize()},
        }
        obj, _ = Position.objects.update_or_create(
            index_name=index_name,
            defaults=data,
        )


def transfer_establishment_employee():
    """
    Affiliations -> EstablishmentEmployee
    ---
    Requirements for EstablishmentEmployee:
    - Establishment
    - Employee
    - Position
    """
    affiliations = Affiliations.objects.exclude(state__isnull=True)

    objects = []
    for item in tqdm(affiliations, desc='create objects list of establishment employee for bulk_create:'):
        position = Position.objects.filter(name={"en-GB": item.role.capitalize()}).first()
        employee = Employee.objects.filter(old_id=item.profile_id).first()
        establishment = Establishment.objects.filter(old_id=item.establishment_id).first()

        if position and employee and establishment:
            establishment_employee = EstablishmentEmployee(
                from_date=item.start_date,
                to_date=item.end_date,
                old_id=item.id,
                establishment=establishment,
                position=position,
                employee=employee,
                created_by_id=get_user_id(item.requester_id),
                created=item.created_at,
                status=EstablishmentEmployee.ACCEPTED if item.state == 'validated' else EstablishmentEmployee.IDLE
            )
            objects.append(establishment_employee)

    print(f'create EstablishmentEmployees...')
    EstablishmentEmployee.objects.bulk_create(objects)


def transfer_employee_photo():
    """
    ProfilePictures -> Image
    ---
    Requirements:
    - Employee
    """
    pictures = ProfilePictures.objects.filter(attachment_suffix_url__isnull=False)

    objects = []
    for item in tqdm(pictures, desc='create objects list of employee for bulk_update:'):
        employee = Employee.objects.filter(old_id=item.profile_id).first()

        if employee:
            image, image_created = Image.objects.get_or_create(
                orientation=Image.HORIZONTAL,
                title=item.attachment_file_name,
                image=item.attachment_suffix_url,
                created=item.created_at,
            )

            employee.photo = image
            objects.append(employee)

    print(f'update Employee...')
    Employee.objects.bulk_update(objects, ['photo', ])
