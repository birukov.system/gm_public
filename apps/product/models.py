"""Product app models."""
from django.conf import settings
from django.contrib.contenttypes import fields as generic
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields.jsonb import KeyTextTransform
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Case, When, F
from django.utils.translation import gettext_lazy as _

from location.models import WineOriginAddressMixin
from review.models import Review
from translation.models import SiteInterfaceDictionary
from utils.methods import transform_into_readable_str
from utils.models import (BaseAttributes, ProjectBaseMixin, HasTagsMixin,
                          TranslatedFieldsMixin, TJSONField, FavoritesMixin,
                          GalleryMixin, IntermediateGalleryModelMixin,
                          TypeDefaultImageMixin, CarouselMixin, AwardsModelMixin)


class ProductType(TypeDefaultImageMixin, TranslatedFieldsMixin, ProjectBaseMixin):
    """ProductType model."""

    STR_FIELD_NAME = 'name'

    # EXAMPLE OF INDEX NAME CHOICES
    FOOD = 'food'
    WINE = 'wine'
    LIQUOR = 'liquor'
    SOUVENIR = 'souvenir'
    BOOK = 'book'

    INDEX_CHOICES = (
        (FOOD, 'food'),
        (WINE, 'wine'),
        (LIQUOR, 'liquor'),
        (SOUVENIR, 'souvenir'),
        (BOOK, 'book'),
    )

    INDEX_PLURAL_ONE = {
        'food': 'food',
        'wines': 'wine',
        'liquors': 'liquor',
    }

    name = TJSONField(blank=True, null=True, default=None,
                      verbose_name=_('Name'), help_text='{"en-GB":"some text"}')
    index_name = models.CharField(max_length=50, unique=True, db_index=True,
                                  verbose_name=_('Index name'), choices=INDEX_CHOICES)
    use_subtypes = models.BooleanField(_('Use subtypes'), default=True)
    tag_categories = models.ManyToManyField('tag.TagCategory',
                                            blank=True,
                                            related_name='product_types',
                                            verbose_name=_('Tag categories'))
    default_image = models.ForeignKey('gallery.Image', on_delete=models.SET_NULL,
                                      related_name='product_types',
                                      blank=True, null=True, default=None,
                                      verbose_name='default image')

    class Meta:
        """Meta class."""

        verbose_name = _('Product type')
        verbose_name_plural = _('Product types')

    @property
    def label(self):
        return transform_into_readable_str(self.index_name)


class ProductSubTypeQuerySet(models.QuerySet):
    """ProductSubType QuerySet."""

    def annotate_name_translated(self, locale: str):
        """Annotate a translated name."""
        return self.annotate(
            name_translated=models.Case(
                models.When(
                    name__isnull=False,
                    then=KeyTextTransform(locale, 'name')
                ),
                output_field=models.CharField()
            )
        )


class ProductSubType(TypeDefaultImageMixin, TranslatedFieldsMixin, ProjectBaseMixin):
    """ProductSubtype model."""

    STR_FIELD_NAME = 'name'

    # EXAMPLE OF INDEX NAME CHOICES
    RUM = 'rum'
    PLATE = 'plate'
    OTHER = 'other'
    RED = 'red'
    SEAFOOD = 'seafood'
    VODKA = 'vodka'
    WHISKEY = 'whiskey'
    CHEESE = 'cheese'
    MILK = 'milk'
    MEAT = 'meat'
    MUSHROOMS = 'mushrooms'

    INDEX_NAME_CHOICES = [
        (RUM, _('Rum')),
        (PLATE, _('Plate')),
        (OTHER, _('Other')),
        (RED, _('Red')),
        (SEAFOOD, _('Seafood')),
        (VODKA, _('Vodka')),
        (WHISKEY, _('Whiskey')),
        (CHEESE, _('Cheese')),
        (MILK, _('Milk')),
        (MEAT, _('Meat')),
        (MUSHROOMS, _('Mushrooms')),
    ]

    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE,
                                     related_name='subtypes',
                                     verbose_name=_('Product type'))
    name = TJSONField(blank=True, null=True, default=None,
                      verbose_name=_('Name'), help_text='{"en-GB":"some text"}')
    index_name = models.CharField(max_length=50, unique=True,
                                  db_index=True, choices=INDEX_NAME_CHOICES,
                                  verbose_name=_('Index name'))
    default_image = models.ForeignKey('gallery.Image', on_delete=models.SET_NULL,
                                      related_name='product_sub_types',
                                      blank=True, null=True, default=None,
                                      verbose_name='default image')

    objects = ProductSubTypeQuerySet.as_manager()

    class Meta:
        """Meta class."""

        verbose_name = _('Product subtype')
        verbose_name_plural = _('Product subtypes')

    def clean_fields(self, exclude=None):
        if not self.product_type.use_subtypes:
            raise ValidationError(_('Product type is not use subtypes.'))


class ProductManager(models.Manager):
    """Extended manager for Product model."""


class ProductQuerySet(models.QuerySet):
    """Product queryset."""

    def with_base_related(self):
        return self.select_related('product_type', 'establishment') \
            .prefetch_related('product_type__subtypes', 'tags', 'tags__translation')

    def with_extended_related(self):
        """Returns qs with almost all related objects."""
        return self.with_base_related() \
            .prefetch_related('tags', 'tags__category', 'tags__category__country',
                              'standards', 'classifications', 'classifications__standard',
                              'establishment__address', 'establishment__establishment_type',
                              'establishment__address__city', 'establishment__address__city__country',
                              'establishment__establishment_subtypes', 'product_gallery',
                              'gallery', 'product_type', 'subtypes',
                              'classifications__classification_type', 'classifications__tags',
                              'wine_origins__wine_region', 'wine_origins__wine_sub_region', )

    def common(self):
        return self.filter(category=self.model.COMMON)

    def online(self):
        return self.filter(category=self.model.ONLINE)

    def wines(self):
        return self.filter(product_type__index_name__icontains=ProductType.WINE)

    def without_current_product(self, current_product: str):
        """Exclude by current product."""
        kwargs = {'pk': int(current_product)} if current_product.isdigit() else {'slug': current_product}
        return self.exclude(**kwargs)

    def by_product_type(self, product_type: str):
        """Filter by type."""
        return self.filter(product_type__index_name__icontains=product_type)

    def by_product_subtype(self, product_subtype: str):
        """Filter by subtype."""
        return self.filter(subtypes__index_name__icontains=product_subtype)

    def by_country_code(self, country_code):
        """Filter by country of produce."""
        return self.filter(establishment__address__city__country__code=country_code)

    def published(self):
        """Filter products by published state."""
        return self.filter(state=self.model.PUBLISHED)

    def annotate_in_favorites(self, user):
        """Annotate flag in_favorites"""
        favorite_product_ids = []
        if user.is_authenticated:
            favorite_product_ids = user.favorite_product_ids
        return self.annotate(
            in_favorites=Case(
                When(id__in=favorite_product_ids, then=True),
                default=False,
                output_field=models.BooleanField(default=False)
            )
        )

    def annotate_distance(self, point: Point = None):
        """
        Return QuerySet with annotated field - distance
        Description:

        """
        return self.annotate(distance=Distance('establishment__address__coordinates',
                                               point,
                                               srid=settings.GEO_DEFAULT_SRID))

    def has_location(self):
        """Return objects with geo location."""
        return self.filter(establishment__address__coordinates__isnull=False)

    def annotate_same_subtype(self, product):
        """Annotate flag same subtype."""
        return self.annotate(same_subtype=Case(
            models.When(
                subtypes__in=product.subtypes.all(),
                then=True
            ),
            default=False,
            output_field=models.BooleanField(default=False)
        ))

    def similar_base(self, product):
        """Return QuerySet filtered by base filters for Product model."""
        filters = {
            'reviews__status': Review.READY,
            'product_type': product.product_type,
        }
        qs = self.exclude(id=product.id)\
            .exclude(establishment_id=product.establishment_id) \
            .filter(**filters)
        if product.subtypes.exists():
            filters.update(
                {'subtypes__in': product.subtypes.all()})
        if product.establishment.address and product.establishment.location:
            qs = qs.annotate_distance(point=product.establishment.location)
        return qs

    def similar(self, product):
        """
        Return QuerySet with objects that similar to Product.
        :param product: instance of Product model.
        """
        similarity_rules = {
            'ordering': [F('same_subtype').desc(), ],
            'distinction': ['same_subtype', ]
        }
        if product.establishment.address and product.establishment.location:
            similarity_rules['ordering'].append(F('distance').asc())
            similarity_rules['distinction'].append('distance')
        return self.similar_base(product) \
            .annotate_same_subtype(product) \
            .order_by(*similarity_rules['ordering']) \
            .distinct(*similarity_rules['distinction'],
                      'id')

    def available_products(self, user, country_code: str):
        """Return QuerySet with products that user has an access."""
        from account.models import UserRole

        filters = {'establishment__address__city__country__code': country_code}
        if user.is_establishment_administrator and not user.has_admin_roles:
            filters.update({
                'establishment__id__in': models.Subquery(
                    UserRole.objects.filter(user=user, role__site__country__code=country_code)
                        .validated()
                        .distinct('user', 'establishment')
                        .values_list('establishment', flat=True)
                )
            })
        return self.filter(**filters)

    def by_product_states(self, states):
        return self.filter(state__in=states)

    def by_all_states(self, state):
        return self.filter(state__in=(Product.PUBLISHED, Product.REMOVED_FROM_SELECTION, Product.UNPUBLISHED))

    def by_deleted(self, state):
        return self.filter(state=Product.ABANDONED)

    def annotate_precaution(self, locale):
        text = models.Subquery(
            SiteInterfaceDictionary.objects.filter(
                keywords='precaution.text'
            ).values_list('text', flat=True)[:1]
        )

        return self.annotate(precaution=models.Case(
            models.When(
                models.Q(product_type_id__in=models.Subquery(
                    ProductType.objects.filter(index_name__in=[
                        ProductType.WINE,
                        ProductType.LIQUOR
                    ]).values_list('id', flat=True)
                )),
                then=KeyTextTransform(settings.FALLBACK_LOCALE, text)
            ),
            default=None,
            output_field=models.CharField()
        ))

    def prefetch_comments(self):
        """Prefetch last comment."""
        from comment.models import Comment
        return self.prefetch_related(
            models.Prefetch('comments',
                            queryset=Comment.objects.published().order_by('-created'),
                            to_attr='comments_prefetched')
        )


class Product(GalleryMixin,
              TranslatedFieldsMixin,
              BaseAttributes,
              HasTagsMixin,
              FavoritesMixin,
              CarouselMixin,
              AwardsModelMixin):
    """Product models."""

    EARLIEST_VINTAGE_YEAR = 1700
    LATEST_VINTAGE_YEAR = 2100

    COMMON = 0
    ONLINE = 1

    CATEGORY_CHOICES = (
        (COMMON, _('Common')),
        (ONLINE, _('Online')),
    )

    PUBLISHED = 0
    REMOVED_FROM_SELECTION = 1
    UNPUBLISHED = 2
    ABANDONED = 3

    STATE_CHOICES = (
        (PUBLISHED, _('Published')),
        (REMOVED_FROM_SELECTION, _('RemovedFromSelection')),
        (UNPUBLISHED, _('Unpublished')),
        (ABANDONED, _('Abandoned')),
    )

    category = models.PositiveIntegerField(choices=CATEGORY_CHOICES,
                                           default=COMMON)
    name = models.CharField(max_length=255,
                            default=None, null=True,
                            verbose_name=_('name'))
    description = TJSONField(_('description'), null=True, blank=True,
                             default=None, help_text='{"en-GB":"some text"}')
    available = models.BooleanField(_('available'), default=True)
    product_type = models.ForeignKey(ProductType, on_delete=models.PROTECT,
                                     null=True, blank=True, default=None,
                                     related_name='products', verbose_name=_('Type'))
    subtypes = models.ManyToManyField(ProductSubType, blank=True,
                                      related_name='products',
                                      verbose_name=_('Subtypes'))
    establishment = models.ForeignKey('establishment.Establishment', on_delete=models.PROTECT,
                                      blank=True, null=True,
                                      related_name='products',
                                      verbose_name=_('establishment'))
    public_mark = models.PositiveIntegerField(blank=True, null=True, default=None,
                                              verbose_name=_('public mark'), )
    classifications = models.ManyToManyField('ProductClassification',
                                             blank=True,
                                             verbose_name=_('classifications'))
    standards = models.ManyToManyField('ProductStandard',
                                       blank=True,
                                       verbose_name=_('standards'),
                                       help_text=_('attribute from legacy db'))
    wine_village = models.ForeignKey('location.WineVillage', on_delete=models.PROTECT,
                                     blank=True, null=True,
                                     verbose_name=_('wine village'))
    slug = models.SlugField(unique=True, max_length=255, null=True,
                            verbose_name=_('Slug'))
    favorites = generic.GenericRelation(to='favorites.Favorites')
    old_id = models.PositiveIntegerField(_('old id'), default=None,
                                         blank=True, null=True)
    state = models.PositiveIntegerField(choices=STATE_CHOICES,
                                        default=UNPUBLISHED,
                                        verbose_name=_('state'),
                                        help_text=_('attribute from legacy db'))
    tags = models.ManyToManyField('tag.Tag', related_name='products',
                                  verbose_name=_('Tag'))
    old_unique_key = models.CharField(max_length=255, unique=True,
                                      blank=True, null=True, default=None,
                                      help_text=_('attribute from legacy db'))
    vintage = models.IntegerField(verbose_name=_('vintage year'),
                                  null=True, blank=True, default=None,
                                  validators=[MinValueValidator(EARLIEST_VINTAGE_YEAR),
                                              MaxValueValidator(LATEST_VINTAGE_YEAR)])
    average_price = models.DecimalField(max_digits=14, decimal_places=2,
                                        blank=True, null=True, default=None,
                                        verbose_name=_('average price'))
    gallery = models.ManyToManyField('gallery.Image', through='ProductGallery')
    reviews = generic.GenericRelation(to='review.Review')
    comments = generic.GenericRelation(to='comment.Comment')
    awards = generic.GenericRelation(to='main.Award', related_query_name='product')

    serial_number = models.CharField(max_length=255,
                                     default=None, null=True,
                                     verbose_name=_('Serial number'))

    site = models.ForeignKey(to='main.SiteSettings', null=True, blank=True, on_delete=models.CASCADE)

    objects = ProductManager.from_queryset(ProductQuerySet)()

    class Meta:
        """Meta class."""

        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        """Override str dunder method."""
        return f'{self.name}'

    @property
    def country_id(self):
        """
        Return Country id of establishment location
        """
        return self.establishment.address.country_id if hasattr(self.establishment, 'address') and hasattr(
            self.establishment.address, 'country_id') else None

    @property
    def product_type_translated_name(self):
        """Get translated name of product type."""
        return self.product_type.name_translated if self.product_type else None

    @property
    def last_published_review(self):
        """Return last published review"""
        return self.reviews.published().order_by('-published_at').first()

    @property
    def recent_review(self):
        """Return last review"""
        return self.reviews.recent().first()

    @property
    def sugar_contents(self):
        return self.tags.filter(category__index_name='sugar-content')

    @property
    def wine_colors(self):
        return self.tags.filter(category__index_name='wine-color')

    @property
    def bottles_produced(self):
        return self.tags.filter(category__index_name='bottles-produced')

    @property
    def grape_variety(self):
        return self.tags.filter(category__index_name='grape-variety')

    @property
    def bottle_sizes(self):
        return self.tags.filter(category__index_name='bottle_size')

    @property
    def alcohol_percentage(self):
        qs = self.tags.filter(category__index_name='alcohol_percentage')
        if qs.exists():
            return qs.first()

    @property
    def related_tags(self):
        return super().visible_tags.exclude(category__index_name__in=[
            'sugar-content', 'wine-color', 'bottles-produced',
            'serial-number', 'grape-variety', 'serial_number',
            'alcohol_percentage', 'bottle_size',
        ])

    @property
    def display_name(self):
        name = f'{self.name} ' \
               f'({self.vintage if self.vintage else "BSA"})'
        if self.establishment and self.establishment.name and self.product_type.index_name == ProductType.WINE:
            name = f'{self.establishment.name} - ' + name
        return name

    @property
    def main_image(self):
        qs = self.product_gallery.main_image()
        if qs.exists():
            return qs.first().image

    @property
    def default_image(self):
        return self.product_type.default_image_url

    @property
    def image_url(self):
        return self.main_image.image.url if self.main_image else self.default_image

    @property
    def preview_image_url(self):
        if self.main_image:
            return self.main_image.get_image_url(thumbnail_key='product_preview')

    @property
    def wine_region(self):
        if self.wine_origins.exists():
            return self.wine_origins.first().wine_region

    @property
    def last_published_review_data(self):
        if self.last_published_review:
            return self.last_published_review.text

    @property
    def establishment_subtype_labels(self):
        if self.subtypes:
            return [transform_into_readable_str(label)
                    for label in self.subtypes.all().values_list('index_name', flat=True)]

    @property
    def wine_region_label(self):
        if self.wine_region:
            return self.wine_region.name

    @property
    def metadata(self):
        if self.product_type:
            metadata = []
            tag_categories = (
                self.product_type.tag_categories.exclude(index_name__in=[
                    'business_tag', 'purchased_item', 'accepted_payments_de',
                    'accepted_payments_hr', 'drinks', 'bottles_per_year',
                    'serial_number', 'surface', 'cooperative', 'tag',
                    'outside_sits', 'private_room']).values_list('index_name', flat=True)
            )
            for category in tag_categories:
                tags = self.tags.filter(category__index_name=category).values_list('value', flat=True)

                if tags.exists():
                    category_tags = {category: []}
                    for tag in tags:
                        category_tags[category].append(tag)
                    metadata.append(category_tags)
            return metadata

    @property
    def state_display(self):
        return self.STATE_CHOICES[self.state][1]

    @property
    def last_comment(self):
        if hasattr(self, 'comments_prefetched') and len(self.comments_prefetched):
            return self.comments_prefetched[0]


class OnlineProductManager(ProductManager):
    """Extended manger for OnlineProduct model."""

    def get_queryset(self):
        """Overridden get_queryset method."""
        return super().get_queryset().online()


class OnlineProduct(Product):
    """Online product."""

    objects = OnlineProductManager.from_queryset(ProductQuerySet)()

    class Meta:
        """Meta class."""

        proxy = True
        verbose_name = _('Online product')
        verbose_name_plural = _('Online products')


class PurchasedProduct(models.Model):
    """Model for storing establishment purchased plaques."""

    establishment = models.ForeignKey('establishment.Establishment', on_delete=models.CASCADE,
                                      related_name='purchased_plaques',
                                      verbose_name=_('establishment'))
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE,
                                related_name='purchased_by_establishments',
                                verbose_name=_('plaque'))
    is_gifted = models.NullBooleanField(default=None,
                                        verbose_name=_('is gifted'))
    quantity = models.PositiveSmallIntegerField(verbose_name=_('quantity'))

    class Meta:
        """Meta class."""
        verbose_name = _('purchased plaque')
        verbose_name_plural = _('purchased plaques')
        unique_together = ('establishment', 'product')


class Unit(models.Model):
    """Product unit model."""
    name = models.CharField(max_length=255,
                            verbose_name=_('name'))
    value = models.CharField(max_length=255,
                             verbose_name=_('value'))

    class Meta:
        """Meta class."""
        verbose_name = _('unit')
        verbose_name_plural = _('units')

    def __str__(self):
        """Overridden dunder method."""
        return self.name


class ProductStandardQuerySet(models.QuerySet):
    """Product standard queryset."""


class ProductStandard(models.Model):
    """Product standard model."""

    APPELLATION = 0
    WINEQUALITY = 1
    YARDCLASSIFICATION = 2

    STANDARDS = (
        (APPELLATION, _('Appellation')),
        (WINEQUALITY, _('Wine quality')),
        (YARDCLASSIFICATION, _('Yard classification')),
    )

    name = models.CharField(_('name'), max_length=255)
    standard_type = models.PositiveSmallIntegerField(choices=STANDARDS,
                                                     verbose_name=_('standard type'))
    coordinates = gis_models.PointField(
        _('Coordinates'), blank=True, null=True, default=None)
    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)

    objects = ProductStandardQuerySet.as_manager()

    class Meta:
        """Meta class."""
        verbose_name_plural = _('wine standards')
        verbose_name = _('wine standard')


class ProductGallery(IntermediateGalleryModelMixin):
    """Gallery for model Product."""
    product = models.ForeignKey(Product, null=True,
                                related_name='product_gallery',
                                on_delete=models.CASCADE,
                                verbose_name=_('product'))
    image = models.ForeignKey('gallery.Image', null=True,
                              related_name='product_gallery',
                              on_delete=models.CASCADE,
                              verbose_name=_('image'))

    class Meta:
        """ProductGallery meta class."""
        verbose_name = _('product gallery')
        verbose_name_plural = _('product galleries')
        unique_together = (('product', 'is_main'), ('product', 'image'))


class ProductClassificationType(models.Model):
    """Product classification type."""

    name = models.CharField(max_length=255, unique=True,
                            verbose_name=_('classification type'))
    product_type = models.ForeignKey(ProductType, on_delete=models.PROTECT,
                                     null=True, default=None,
                                     verbose_name=_('product type'))
    product_sub_type = models.ForeignKey(ProductSubType, on_delete=models.PROTECT,
                                         blank=True, null=True, default=None,
                                         verbose_name=_('product subtype'),
                                         help_text=_('Legacy attribute - possible_type (product type).'
                                                     'Product type in our case is product subtype.'))

    class Meta:
        """Meta class."""
        verbose_name = _('wine classification type')
        verbose_name_plural = _('wine classification types')

    def __str__(self):
        """Override str dunder."""
        return self.name


class ProductClassificationQuerySet(models.QuerySet):
    """Product classification QuerySet."""


class ProductClassification(models.Model):
    """Product classification model."""

    classification_type = models.ForeignKey(ProductClassificationType, on_delete=models.PROTECT,
                                            verbose_name=_('classification type'))
    standard = models.ForeignKey(ProductStandard, on_delete=models.PROTECT,
                                 null=True, blank=True, default=None,
                                 verbose_name=_('standard'))
    tags = models.ManyToManyField('tag.Tag', related_name='product_classifications',
                                  verbose_name=_('Tag'))
    old_id = models.PositiveIntegerField(_('old id'), blank=True, null=True, default=None)

    objects = ProductClassificationQuerySet.as_manager()

    class Meta:
        """Meta class."""
        verbose_name = _('product classification')
        verbose_name_plural = _('product classifications')


class ProductNoteQuerySet(models.QuerySet):
    """QuerySet for model ProductNote."""


class ProductNote(ProjectBaseMixin):
    """Note model for Product entity."""
    old_id = models.PositiveIntegerField(null=True, blank=True)
    text = models.TextField(verbose_name=_('text'))
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name='notes',
                                verbose_name=_('product'))
    user = models.ForeignKey('account.User', on_delete=models.PROTECT,
                             null=True,
                             related_name='product_notes',
                             verbose_name=_('author'))

    objects = ProductNoteQuerySet.as_manager()

    class Meta:
        """Meta class."""
        verbose_name_plural = _('product note')
        verbose_name = _('product notes')
