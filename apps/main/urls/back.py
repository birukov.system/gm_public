"""Back main URLs"""
from django.urls import path

from main import views

app_name = 'main'

urlpatterns = [
    path('awards/', views.AwardListCreateView.as_view(), name='awards-list-create'),
    path('awards/<int:id>/', views.AwardRUDView.as_view(), name='awards-rud'),
    path('awards/create-and-bind/<int:employee_id>/', views.AwardCreateAndBind.as_view(),
         name='award-employee-create'),
    path('award-types/', views.AwardTypesListCreateView.as_view(), name='awards-types-list-create'),
    path('award-types/entity-types/', views.AwardEntityTypeListCreateView.as_view(),
         name='awards-entity-type-list-create'),
    path('award-types/<int:id>/', views.AwardTypeDestroyView.as_view(), name='awards-types-destroy'),
    path('content_type/', views.ContentTypeView.as_view(), name='content_type-list'),
    path('sites/', views.SiteListBackOfficeView.as_view(), name='site-list-create'),
    path('site-settings/<subdomain>/', views.SiteSettingsBackOfficeView.as_view(),
         name='site-settings'),
    path('feature/', views.FeatureBackView.as_view(), name='feature-list-create'),
    path('feature/<int:pk>/', views.FeatureRUDBackView.as_view(), name='feature-rud'),
    path('site-feature/', views.SiteFeatureBackView.as_view(),
         name='site-feature-list-create'),
    path('site-feature/<int:pk>/', views.SiteFeatureRUDBackView.as_view(),
         name='site-feature-rud'),
    path('footer/', views.FooterBackView.as_view(), name='footer-list-create'),
    path('footer/<int:pk>/', views.FooterRUDBackView.as_view(), name='footer-rud'),
    path('page-types/', views.PageTypeListCreateView.as_view(),
         name='page-types-list-create'),
    path('panels/', views.PanelsListCreateView.as_view(), name='panels'),
    path('panels/<int:pk>/', views.PanelsRUDView.as_view(), name='panels-rud'),
    path('panels/<int:pk>/execute/', views.PanelsExecuteView.as_view(), name='panels-execute'),
    path('panels/<int:pk>/csv/', views.PanelsExportCSVView.as_view(), name='panels-csv'),
    path('panels/<int:pk>/xls/', views.PanelsExecuteXLSView.as_view(), name='panels-xls'),
    path('carousel/', views.BackCarouselListView.as_view(), name='carousel-list'),
    path('carousel/<int:pk>/', views.BackCarouselRUDView.as_view(), name='carousel-rud'),
]
