from django.core.management.base import BaseCommand
from comment.models import Comment
from tqdm import tqdm


class Command(BaseCommand):
    help = 'Add status to comments from is_publish_ flag.'

    def handle(self, *args, **kwargs):
        to_update = []

        for comment in tqdm(Comment.objects.all()):
            if hasattr(comment, 'is_publish') and hasattr(comment, 'status'):
                comment.status = Comment.PUBLISHED if comment.is_publish else Comment.WAITING
                to_update.append(comment)

        Comment.objects.bulk_update(to_update, ('status', ))
        self.stdout.write(self.style.WARNING(f'Updated {len(to_update)} objects.'))
