"""Advertisement common urlpaths."""
from django.urls import path

from advertisement.views import web as views
from .common import common_urlpatterns

app_name = 'advertisements'

urlpatterns = [
    path('<page_type>/', views.AdvertisementPageTypeWebListView.as_view(), name='list'),
]

urlpatterns += common_urlpatterns
