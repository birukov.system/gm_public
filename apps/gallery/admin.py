from django.contrib import admin

from gallery.models import Image


@admin.register(Image)
class ImageModelAdmin(admin.ModelAdmin):
    """Image model admin."""
    list_display = ['id', 'title', 'orientation_display', 'image_tag', ]

    def orientation_display(self, obj):
        """Get image orientation name."""
        return obj.get_orientation_display() if obj.orientation else None
