from rest_framework import serializers
from gallery.models import Image


class ImageSerializer(serializers.ModelSerializer):
    attachment_file_name = serializers.CharField(source="image")

    class Meta:
        model = Image
        fields = (
            "attachment_file_name",
        )

    def create(self, validated_data):
        return Image.objects.create(**validated_data)
