"""Establishment admin conf."""
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from django.utils.translation import gettext_lazy as _

from comment.models import Comment
from establishment import models
from main.models import Award
from product.models import Product, PurchasedProduct
from review import models as review_models
from utils.admin import BaseModelAdminMixin


@admin.register(models.EstablishmentType)
class EstablishmentTypeAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """EstablishmentType admin."""
    raw_id_fields = ('tag_categories', 'default_image', )


@admin.register(models.EstablishmentSubType)
class EstablishmentSubTypeAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """EstablishmentSubType admin."""
    raw_id_fields = ('tag_categories', 'default_image', )


class AwardInline(GenericTabularInline):
    model = Award
    extra = 0


class ContactPhoneInline(admin.TabularInline):
    """Contact phone inline admin."""
    model = models.ContactPhone
    extra = 0


class GalleryImageInline(admin.TabularInline):
    """Gallery image inline admin."""
    model = models.EstablishmentGallery
    raw_id_fields = ['image', ]
    extra = 0


class ContactEmailInline(admin.TabularInline):
    """Contact email inline admin."""
    model = models.ContactEmail
    extra = 0


class ReviewInline(BaseModelAdminMixin, GenericTabularInline):
    model = review_models.Review
    extra = 0


class CommentInline(GenericTabularInline):
    model = Comment
    extra = 0


class ProductInline(admin.TabularInline):
    model = Product
    extra = 0


class CompanyInline(admin.TabularInline):
    model = models.Company
    raw_id_fields = ['establishment', 'address']
    extra = 0


class EstablishmentNote(admin.TabularInline):
    model = models.EstablishmentNote
    extra = 0
    raw_id_fields = ['user', ]


class PurchasedProductInline(admin.TabularInline):
    model = PurchasedProduct
    extra = 0
    raw_id_fields = ['product', ]


@admin.register(models.Establishment)
class EstablishmentAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Establishment admin."""
    list_display = ['id', '__str__',]
    search_fields = ['id', 'name', 'index_name', 'slug']
    list_filter = ['public_mark', 'toque_number']
    inlines = [CompanyInline, EstablishmentNote, GalleryImageInline,
               PurchasedProductInline, ContactPhoneInline, ]
    # inlines = [
    #     AwardInline, ContactPhoneInline, ContactEmailInline,
    #     ReviewInline, CommentInline, ProductInline]
    raw_id_fields = ('address', 'collections', 'tags', )


@admin.register(models.Position)
class PositionAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Position admin."""


class PlateInline(admin.TabularInline):
    """Plate inline admin"""
    model = models.Plate
    extra = 0


@admin.register(models.Menu)
class MenuAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Menu admin."""
    list_display = ['id', 'category_translated']
    raw_id_fields = ['establishment']
    inlines = [
        PlateInline,
    ]

    def category_translated(self, obj):
        """Get user's short name."""
        return obj.category_translated

    category_translated.short_description = _('category')


@admin.register(models.RatingStrategy)
class RatingStrategyAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin conf for Rating Strategy model."""


@admin.register(models.SocialChoice)
class SocialChoiceAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin conf for SocialChoice model."""


@admin.register(models.SocialNetwork)
class SocialNetworkAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin conf for SocialNetwork model."""
    raw_id_fields = ('establishment',)


@admin.register(models.Company)
class CompanyAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """Admin conf for Company model."""
    raw_id_fields = ['establishment', 'address', ]


@admin.register(models.Employee)
class EmployeeAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    raw_id_fields = ['establishments', 'tags', 'user', 'photo', ]
