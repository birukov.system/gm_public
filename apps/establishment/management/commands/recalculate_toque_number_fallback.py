"""Run recalculating toque number for establishments without indexing."""
from django.core.management.base import BaseCommand
from establishment.models import Establishment, RatingStrategy
from location.models import Country


class Command(BaseCommand):

    help = 'Recalculation toque number for all establishments without indexing.'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def handle(self, *args, **options):
        restaurants = Establishment.objects.restaurants()

        # update establishments with a specific rating strategy
        strategies = RatingStrategy.objects.with_country()
        for strategy in strategies:
            qs = restaurants.by_country(strategy.country). \
                by_public_mark_range(strategy.public_mark_min_value,
                                     strategy.public_mark_max_value)
            qs.update(toque_number=strategy.toque_number)

        countries = Country.objects.filter(pk__in=strategies.values('country'))

        for country in countries:
            rating_strategies = RatingStrategy.objects.by_country(country)
            qs = Establishment.objects.restaurants(). \
                by_country(country). \
                exclude_public_mark_ranges(
                ranges=[(strategy.public_mark_min_value,
                         strategy.public_mark_max_value) for
                        strategy in rating_strategies])
            qs.update(toque_number=0)

        # update establishments for other countries
        strategy_for_other_countries = RatingStrategy.objects.with_country(False)
        for strategy in strategy_for_other_countries:
            qs = Establishment.objects.restaurants(). \
                exclude_countries(countries). \
                by_public_mark_range(strategy.public_mark_min_value,
                                     strategy.public_mark_max_value)
            qs.update(toque_number=strategy.toque_number)

        # set null for others
        qs = Establishment.objects.restaurants(). \
            exclude_countries(countries). \
            exclude_public_mark_ranges(
            ranges=[(strategy.public_mark_min_value,
                     strategy.public_mark_max_value)
                    for strategy in
                    strategy_for_other_countries])
        qs.update(toque_number=0)
