"""Main app admin conf."""
from django.contrib import admin
from main import models


class SiteSettingsInline(admin.TabularInline):
    model = models.SiteFeature
    extra = 1


@admin.register(models.SiteSettings)
class SiteSettingsAdmin(admin.ModelAdmin):
    """Site settings admin conf."""
    inlines = [SiteSettingsInline, ]


@admin.register(models.SiteFeature)
class SiteFeatureAdmin(admin.ModelAdmin):
    """Site feature admin conf."""
    list_display = ['id', 'site_settings', 'feature',
                    'published', 'main', 'backoffice', ]
    raw_id_fields = ['site_settings', 'feature', ]


@admin.register(models.Feature)
class FeatureAdmin(admin.ModelAdmin):
    """Feature admin conf."""
    list_display = ['id', '__str__', 'priority', 'route', ]


@admin.register(models.AwardType)
class AwardTypeAdmin(admin.ModelAdmin):
    """Award type admin conf."""


@admin.register(models.AwardEntityType)
class AwardEntityTypeAdmin(admin.ModelAdmin):
    """Award entity type admin conf."""


@admin.register(models.Award)
class AwardAdmin(admin.ModelAdmin):
    """Award admin conf."""
    # list_display = ['id', '__str__']
    # list_display_links = ['id', '__str__']


@admin.register(models.Currency)
class CurrencyContentAdmin(admin.ModelAdmin):
    """Currency Content admin"""
    list_display = ['id', 'slug', 'code']
    list_display_links = ['slug']


@admin.register(models.Carousel)
class CarouselAdmin(admin.ModelAdmin):
    """Carousel admin."""


@admin.register(models.PageType)
class PageTypeAdmin(admin.ModelAdmin):
    """PageType admin."""
    list_display = ['id', '__str__', ]


@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    """Page admin."""
    list_display = ('id', '__str__', 'advertisement')
    list_filter = ('advertisement__url', 'source')
    date_hierarchy = 'created'


class FooterLinkInline(admin.TabularInline):
    model = models.Footer.links.through
    extra = 1


@admin.register(models.Footer)
class FooterAdmin(admin.ModelAdmin):
    """Footer admin."""
    list_display = ('id', 'site',)
    exclude = ('links',)
    inlines = [FooterLinkInline, ]


@admin.register(models.FooterLink)
class FooterLinkAdmin(admin.ModelAdmin):
    """FooterLink admin."""


@admin.register(models.Panel)
class PanelAdmin(admin.ModelAdmin):
    """Panel admin."""
    list_display = ('id', 'name', 'user', 'created',)
    raw_id_fields = ('user',)
    list_display_links = ('id', 'name',)


@admin.register(models.NavigationBarPermission)
class NavigationBarPermissionAdmin(admin.ModelAdmin):
    """NavigationBarPermission admin."""
    list_display = ('id', 'permission_mode_display', )

    def permission_mode_display(self, obj):
        """Get permission mode display."""
        return obj.get_permission_mode_display()
