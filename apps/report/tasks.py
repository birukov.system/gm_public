"""Report app tasks."""
import logging

from celery import shared_task

logger = logging.getLogger(__name__)


@shared_task
def send_report_task(report_id: int):
    from report.models import Report

    report_qs = Report.objects.filter(id=report_id)
    if report_qs.exists():
        report = report_qs.first()
        report.send_email()
    else:
        logger.error(f'Error sending report {report_id}')
