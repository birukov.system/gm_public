from pprint import pprint

from transfer import models as transfer_models
from transfer.serializers import product as product_serializers


def transfer_wine_color():
    queryset = transfer_models.WineColor.objects.all()
    serialized_data = product_serializers.WineColorSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_wine_color errors: {serialized_data.errors}")


def transfer_wine_sugar_content():
    queryset = transfer_models.WineType.objects.all()
    serialized_data = product_serializers.WineTypeSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_wine_sugar_content errors: {serialized_data.errors}")


def transfer_wine_bottles_produced():
    raw_queryset = transfer_models.Products.objects.raw(
        """
        SELECT
           DISTINCT bottles_produced,
           1 as id
        FROM products
        WHERE bottles_produced IS NOT NULL 
        AND bottles_produced !='';
        """
    )
    queryset = [vars(query) for query in raw_queryset]
    serialized_data = product_serializers.WineBottlesProducedSerializer(
        data=queryset,
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_wine_bottles_produced errors: {serialized_data.errors}")


def transfer_wine_classification_type():
    raw_queryset = transfer_models.ProductClassification.objects.raw(
        """
        SELECT
           DISTINCT name,
           1 as id
        FROM wine_classifications;
        """
    )
    queryset = [vars(query) for query in raw_queryset]
    serialized_data = product_serializers.WineClassificationTypeSerializer(
        data=queryset,
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_classification errors: {serialized_data.errors}")


def transfer_wine_standard():
    queryset = transfer_models.ProductClassification.objects.filter(parent_id__isnull=True) \
        .exclude(type='Classification')
    serialized_data = product_serializers.ProductStandardSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_wine_standard errors: {serialized_data.errors}")


def transfer_wine_classifications():
    queryset = transfer_models.ProductClassification.objects.filter(type='Classification')
    serialized_data = product_serializers.ProductClassificationSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_wine_classifications errors: {serialized_data.errors}")


def transfer_product():
    errors = []
    queryset = transfer_models.Products.objects.all()
    serialized_data = product_serializers.ProductSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        for d in serialized_data.errors: errors.append(d) if d else None
        pprint(f"transfer_product errors: {errors}")


def transfer_product_note():
    errors = []
    queryset = transfer_models.ProductNotes.objects.exclude(text='')
    serialized_data = product_serializers.ProductNoteSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        for d in serialized_data.errors: errors.append(d) if d else None
        pprint(f"transfer_product_note errors: {errors}")


def transfer_plate():
    errors = []
    queryset = transfer_models.Merchandise.objects.all()
    serialized_data = product_serializers.PlateSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        for d in serialized_data.errors: errors.append(d) if d else None
        pprint(f"transfer_plates errors: {errors}")


def transfer_plate_image():
    errors = []
    queryset = transfer_models.Merchandise.objects.filter(attachment_suffix_url__isnull=False)
    serialized_data = product_serializers.PlateImageSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        for d in serialized_data.errors:
            errors.append(d) if d else None
        pprint(f"transfer_plates_images errors: {errors}")


data_types = {
    "wine_characteristics": [
        transfer_wine_sugar_content,
        transfer_wine_color,
        transfer_wine_bottles_produced,
        transfer_wine_classification_type,
        transfer_wine_standard,
        transfer_wine_classifications,
    ],
    "product": [
        transfer_product,
    ],
    "product_note": [
        transfer_product_note,
    ],
    "souvenir": [
        transfer_plate,
        transfer_plate_image,
    ]
}
