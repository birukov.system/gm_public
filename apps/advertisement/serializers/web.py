"""Serializers for web app advertisements"""
from advertisement.serializers import AdvertisementSerializer
from main.serializers import PageBaseSerializer


class AdvertisementPageTypeWebListSerializer(AdvertisementSerializer):
    """Serializer for AdvertisementPageTypeWebView."""

    pages = PageBaseSerializer(many=True, source='web_pages', read_only=True)

    class Meta(AdvertisementSerializer.Meta):
        """Meta class."""
        fields = AdvertisementSerializer.Meta.fields + [
            'pages',
        ]
