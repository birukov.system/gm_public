"""Product app back-office views."""
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from product import filters
from product import serializers, models
from product.views import ProductBaseView
from utils.methods import get_permission_classes
from utils.permissions import (
    IsEstablishmentManager, IsEstablishmentAdministrator)
from utils.serializers import ImageBaseSerializer
from utils.views import BindObjectMixin
from utils.views import CreateDestroyGalleryViewMixin, ChoseToListMixin


class ProductBackOfficeMixinView(ProductBaseView):
    """Product back-office mixin view."""
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager
    )

    def get_queryset(self):
        """Override get_queryset method."""
        queryset = (
            models.Product.objects.with_base_related()
                                  .with_extended_related()
                                  .annotate_in_favorites(self.request.user)
        )
        if (hasattr(self, 'request') and
                (hasattr(self.request, 'user') and hasattr(self.request, 'country_code')) and
                self.request.user.is_authenticated):
            return queryset.available_products(self.request.user, self.request.country_code)
        return queryset.none()


class ProductTypeBackOfficeMixinView:
    """Product type back-office mixin view."""
    permission_classes = get_permission_classes()
    queryset = models.ProductType.objects.all()


class ProductSubTypeBackOfficeMixinView:
    """Product sub type back-office mixin view."""
    permission_classes = get_permission_classes()
    queryset = models.ProductSubType.objects.all()


class ProductBackOfficeGalleryCreateDestroyView(ProductBackOfficeMixinView,
                                                CreateDestroyGalleryViewMixin):
    """
    ## Product gallery image Create/Destroy view
    ### *POST*
    #### Description
    Attaching existing **image** by `image identifier` to **product** by `product identifier`
    in request kwargs.
    ##### Request
    ```
    No body
    ```
    ##### Response
    E.g.:
    ```
    No content
    ```

    ### *DELETE*
    #### Description
    Delete existing **gallery image** from **product** gallery, by `image identifier`
    and `product identifier` in request kwargs.

    **Note**:
    > Image wouldn't be deleted after all.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.ProductBackOfficeGallerySerializer

    def get_object(self):
        """
        Returns the object the view is displaying.
        """
        product_qs = self.get_queryset()

        product = get_object_or_404(product_qs, pk=self.kwargs.get('pk'))
        gallery = get_object_or_404(product.product_gallery, image_id=self.kwargs.get('image_id'))

        # May raise a permission denied
        self.check_object_permissions(self.request, gallery)

        return gallery


class ProductBackOfficeGalleryListView(ProductBackOfficeMixinView,
                                       generics.ListAPIView):
    """
    ## Product gallery image list view
    ### *GET*
    #### Description
    Returning paginated list of product images by `product identifier`,
    with cropped images.
    ##### Response
    E.g.:
    ```
    {
      "count": 1,
      "next": null,
      "previous": null,
      "results": [
        {
          "id": 11,
          ...
        }
      ]
    }
    ```
    """
    serializer_class = ImageBaseSerializer

    def get_object(self):
        """Override get_object method."""
        qs = super(ProductBackOfficeGalleryListView, self).get_queryset()
        product = get_object_or_404(qs, pk=self.kwargs.get('pk'))

        # May raise a permission denied
        self.check_object_permissions(self.request, product)

        return product

    def get_queryset(self):
        """Override get_queryset method."""
        return self.get_object().crop_gallery


class ProductDetailBackOfficeView(ProductBackOfficeMixinView,
                                  generics.RetrieveUpdateDestroyAPIView):
    """
    Product back-office R/U/D view.
    Available for roles Establishment-Administrator, Establishment-Manager

    ### *GET*
    Implement getting ProductBackOffice object.

    ### *PUT*
    Implement updating ProductBackOffice.

    ### *PATCH*
    Implement partial updating ProductBackOffice.

    ### *DELETE*
    Implement deleting ProductBackOffice.
    """
    serializer_class = serializers.ProductBackOfficeDetailSerializer

    def perform_destroy(self, instance):
        if isinstance(instance, models.Product) and instance.state != models.Product.ABANDONED:
            instance.state = models.Product.ABANDONED
            instance.save()
        else:
            raise Http404('Product not found.')


class ProductListCreateBackOfficeView(ProductBackOfficeMixinView,
                                      generics.ListCreateAPIView):
    """
    Product back-office list or create view.
    Available for roles Establishment-Administrator, Establishment -Manager

    ### *GET*

    Implement getting product back-office list.

    #### Description
    Available filters:
    * product_type (`str`) - filter by type
    * product_subtype (`str`) - filter by subtype
    * product_state (`str`) - filter by state or list of states
    * all_states (`bool`) - filter by state__in=(Product.PUBLISHED, Product.OUT_OF_PRODUCTION, Product.WAITING) (0,1,2)
    * deleted (`bool`) - filter by state=Product.ABANDONNED (3)

    ### *POST*
    Creating a back-office product for the institution by Establishment.slug

    #### Description
    Request data
    * slug (`str`) - product slug
    * public_mark (`int`) - public mark
    * vintage (`int`) - vintage year


    """
    serializer_class = serializers.ProductBackOfficeDetailSerializer
    filter_class = filters.ProductBackFilterSet

    def get_queryset(self):
        """Override method."""
        qs = super().get_queryset()
        establishment_slug = self.kwargs.get('establishment_slug')

        if establishment_slug:
            qs = qs.filter(establishment__slug=establishment_slug)

        return qs


class ProductTypeListCreateBackOfficeView(ProductTypeBackOfficeMixinView,
                                          generics.ListCreateAPIView):
    """
    ## Product type back-office list-create view.
    ### **GET**
    #### Description
    Return paginated list of a product types.
    ##### Response
    E.g.
    ```
    {
      "count": 40595,
      "next": 2,
      "previous": null,
      "results": [
        {
          "id": 47336,
          ...
        }
      ]
    }

    ### **POST**
    #### Description
    Create a new product type.
    ##### Request
    Required
    * index_name (`str`) - type index name
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    Non-required
    * default_image_id (`int`) - identifier of a default image
    * use_subtypes (`bool`) - flag that responds for availability of using subtypes with this type
    * tag_category_ids (`list`) - list of a tag category identifiers
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.ProductTypeBackOfficeDetailSerializer


class ProductTypeRUDBackOfficeView(ProductTypeBackOfficeMixinView,
                                   generics.RetrieveUpdateDestroyAPIView):
    """
    ## Product type back-office retrieve-update-destroy view.
    ### **GET**
    #### Description
    Return serialized object of a product type by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a product type object by an identifier.
    ##### Request
    * index_name (`str`) - type index name
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * default_image_id (`int`) - identifier of a default image
    * use_subtypes (`bool`) - flag that responds for availability of using subtypes with this type
    * tag_category_ids (`list`) - list of a tag category identifiers
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete an instance of product type by an identifier
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.ProductTypeBackOfficeDetailSerializer


class ProductTypeTagCategoryCreateBackOfficeView(ProductTypeBackOfficeMixinView,
                                                 generics.CreateAPIView):
    """
    ## Binding a tag category to a product type view.
    ### **POST**
    #### Description
    Method for attaching tag category to product type.
    * product_type_id (`int`) - identifier of a product type
    * tag_category_id (`int`) - identifier of a tag category
    ##### Response
    E.g.
    ```
    No content
    ```
    """
    serializer_class = serializers.ProductTypeTagCategorySerializer

    def create(self, request, *args, **kwargs):
        """
        Implement creating ProductTypeTagCategory.
        """
        super().create(request, *args, **kwargs)
        return Response(status=status.HTTP_201_CREATED)


class ProductSubTypeListCreateBackOfficeView(ProductSubTypeBackOfficeMixinView,
                                             generics.ListCreateAPIView):
    """
    ## Product sub type back-office list-create view.
    ### **GET**
    #### Description
    Return paginated list of a product sub types.
    ##### Response
    E.g.
    ```
    {
      "count": 40595,
      "next": 2,
      "previous": null,
      "results": [
        {
          "id": 47336,
          ...
        }
      ]
    }

    ### **POST**
    #### Description
    Create a new product sub type.
    ##### Request
    Required
    * index_name (`str`) - type index name
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * product_type_id (`int`) - identifier of a product type
    Non-required
    * default_image_id (`int`) - identifier of a default image
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.ProductSubTypeBackOfficeDetailSerializer


class ProductSubTypeRUDBackOfficeView(ProductSubTypeBackOfficeMixinView,
                                      generics.RetrieveUpdateDestroyAPIView):
    """
    ## Product sub type back-office retrieve-update-destroy view.
    ### **GET**
    #### Description
    Return serialized object of a product sub type by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a product sub type object by an identifier.
    ##### Request
    * index_name (`str`) - type index name
    * name (`JSON`) - JSON that contain key as language of review and value as its text
    * product_type_id (`int`) - identifier of a product type
    * default_image_id (`int`) - identifier of a default image
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete an instance of product sub type by an identifier
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.ProductSubTypeBackOfficeDetailSerializer


class ProductNoteListCreateView(ProductBackOfficeMixinView,
                                generics.ListCreateAPIView):
    """
    ## Retrieve|Update|Destroy product note view.
        ### *GET*
    #### Description
    Return paginated list of product notes by `product identifier`.
    Product would filter by country code.
    ##### Response
    E.g.:
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Create new note for product by `product identifier`
    ##### Request
    Required
    * text (`str`) - text of note
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```

    """

    serializer_class = serializers.ProductNoteListCreateSerializer

    def get_object(self):
        """Returns the object the view is displaying."""
        product_qs = super(ProductNoteListCreateView, self).get_queryset()
        filtered_product_qs = self.filter_queryset(product_qs)

        product = get_object_or_404(filtered_product_qs, pk=self.kwargs.get('pk'))

        # May raise a permission denied
        self.check_object_permissions(self.request, product)

        return product

    def get_queryset(self):
        """Overridden get_queryset method."""
        return self.get_object().notes.all()


class ProductNoteRUDView(ProductBackOfficeMixinView,
                         generics.RetrieveUpdateDestroyAPIView):
    """
    ## Create|Retrieve|Update|Destroy product note view.
    ### *GET*
    #### Description
    Return paginated list of product notes by `product identifier`.
    Products would filter by country code.
    ##### Response
    E.g.:
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a note object by `product identifier`.
    ##### Request
    Available
    * text (`str`) - text of note
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an object note by `product identifier`.
    ##### Response
    E.g.:
    ```
    No content
    ```
    """

    serializer_class = serializers.ProductNoteBaseSerializer

    def get_object(self):
        """Returns the object the view is displaying."""
        product_qs = super(ProductNoteRUDView, self).get_queryset()
        filtered_product_qs = self.filter_queryset(product_qs)

        product = get_object_or_404(filtered_product_qs, pk=self.kwargs.get('pk'))
        note = get_object_or_404(product.notes.all(), pk=self.kwargs.get('note_pk'))

        # May raise a permission denied
        self.check_object_permissions(self.request, note)

        return note


class ProductAvailableStatesSetView(ChoseToListMixin, generics.ListAPIView):
    """Gets a list of available state values ​​for the product."""
    chose_field = models.Product.STATE_CHOICES


class ProductAwardBindViewSet(BindObjectMixin, GenericViewSet):
    """
    ## Product award bind view.
    ### **POST**
    #### Description
    Bind an award to a product instance by a product identifier.
    ##### Request
    * object_id (`int`) - identifier of an award object
    * title (`str`) - award title
    * award_type (`int`) - identifier of an award type
    ###### Response
    ```
    {}
    ```

    ### **DELETE**
    #### Description
    Unbinding an award from product by a product identifier.
    ##### Request
    * object_id (`int`) - identifier of an award object
    ###### Response
    ```
    No content
    ```
    """

    bind_object_serializer_class = serializers.ProductAwardBindObjectSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager
    )

    def perform_binding(self, serializer):
        serializer.validated_data

    def perform_unbinding(self, serializer):
        data = serializer.validated_data
        product = data.get('product')
        award = data.get('award')
        product.awards.remove(award)
