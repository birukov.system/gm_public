from django_elasticsearch_dsl_drf.filter_backends import FacetedSearchFilterBackend
from search_indexes.filters.common import FilteringFilterHelper
from elasticsearch_dsl.query import Q
from six import iteritems


class EmployeesFacetedSearchFilterBackend(FacetedSearchFilterBackend):

    def aggregate(self, request, queryset, view):
        __facets = self.construct_facets(request, view)
        for __field, __facet in iteritems(__facets):
            agg = __facet['facet'].get_aggregation()
            # apply all from query except filters for same field (OR between different types)
            agg_filter = Q('match_all')

            if __facet['global']:
                queryset.aggs.bucket(
                    '_filter_' + __field,
                    'global'
                ).bucket(__field, agg)
            else:
                setattr(view, 'except_field', __field)
                temp_qs = FilteringFilterHelper().filter_queryset(request, queryset, view)
                queryset.aggs.bucket(
                    '_filter_' + __field,
                    'filter',
                    filter=temp_qs.query
                ).bucket(__field, agg)

        return queryset