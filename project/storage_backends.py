"""Extend storage backend for adding custom parameters"""
from storages.backends.s3boto3 import S3Boto3Storage


class PublicMediaStorage(S3Boto3Storage):
    location = 'media'
    file_overwrite = False


class PublicStaticStorage(S3Boto3Storage):
    location = 'static'
    file_overwrite = False
