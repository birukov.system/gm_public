"""News app urlconf."""
from django.urls import path

from news import views
from news.urls.common import common_urlpatterns

app_name = 'news'

urlpatterns = [
    path('', views.NewsMobileListView.as_view(), name='list'),
]

urlpatterns.extend(common_urlpatterns)
