from django.core.management.base import BaseCommand

from location.models import WineRegion
from transfer.models import CepageRegions


class Command(BaseCommand):
    help = 'Cleared wine region tag categories (cleared M2M relation with tag categories)'

    def handle(self, *args, **kwarg):
        cepage_wine_regions = CepageRegions.objects.exclude(cepage_id__isnull=True) \
                                                   .exclude(wine_region_id__isnull=True)\
                                                   .distinct() \
                                                   .values_list('wine_region_id', flat=True)
        wine_regions = WineRegion.objects.filter(id__in=tuple(cepage_wine_regions))
        for region in wine_regions:
            region.tags.clear()
        self.stdout.write(self.style.WARNING(f'Cleared wine region tag categories'))
