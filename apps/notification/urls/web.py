"""Establishment app web urlconf."""
from notification.urls.common import urlpatterns as common_urlpatterns


urlpatterns = []

urlpatterns.extend(common_urlpatterns)
