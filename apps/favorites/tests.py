# Create your tests here.
from http.cookies import SimpleCookie

from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from account.models import User
from establishment.models import Establishment, EstablishmentType
from favorites.models import Favorites
from news.models import NewsType, News
from datetime import datetime


class BaseTestCase(APITestCase):

    def setUp(self):
        self.username = 'sedragurda'
        self.password = 'sedragurdaredips19'
        self.email = 'sedragurda@desoz.com'
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password
        )

        tokens = User.create_jwt_tokens(self.user)
        self.client.cookies = SimpleCookie(
            {'access_token': tokens.get('access_token'),
             'refresh_token': tokens.get('refresh_token')})

        self.test_news_type = NewsType.objects.create(
            name="Test news type",
        )
        self.test_news = News.objects.create(
            created_by=self.user,
            modified_by=self.user,
            title={"en-GB": "Test news"},
            news_type=self.test_news_type,
            description={"en-GB": "Description test news"},
            end=datetime.fromisoformat("2020-12-03 12:00:00"),
            state=News.PUBLISHED,
            slugs={'en-GB': 'test-news'}
        )
        self.slug = next(iter(self.test_news.slugs.values()))

        self.test_content_type = ContentType.objects.get(
            app_label="news", model="news")

        self.test_favorites = Favorites.objects.create(
            user=self.user, content_type=self.test_content_type,
            object_id=self.test_news.id)

        self.test_establishment_type = EstablishmentType.objects.create(
            name={"en-GB": "test establishment type"},
            use_subtypes=False)

        self.test_establishment = Establishment.objects.create(
            name="test establishment",
            description={"en-GB": "description of test establishment"},
            establishment_type=self.test_establishment_type,
            is_publish=True)
        self.test_establishment.favorites.set([self.test_favorites])


class FavoritesTestCase(BaseTestCase):

    def test_func(self):
        url = reverse('web:favorites:establishment-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)