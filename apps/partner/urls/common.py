"""Partner app common urlconf."""
from django.urls import path

from partner.views import common as views

app_name = 'partner'

urlpatterns = [
    path('', views.PartnerListView.as_view(), name='list')
]
