from django.core.management.base import BaseCommand
from tqdm import tqdm
import re

from establishment.models import Establishment
from transfer.models import Schedules as OldSchedule
from transfer.serializers.establishment import EstablishmentSerializer
from timetable.models import Timetable
from django.db import transaction


class Command(BaseCommand):
    help = 'Fix scheduler'

    @transaction.atomic
    def handle(self, *args, **kwargs):
        count = 0
        old_schedules = OldSchedule.objects.all().values('establishment', 'timetable')

        # remove old records of Timetable
        Timetable.objects.all().delete()
        time_fields = ('lunch_start', 'lunch_end', 'dinner_start', 'dinner_end', 'opening_at', 'closed_at')

        for old_schedule in tqdm(old_schedules, desc=self.help):
            old_timetable = old_schedule['timetable']
            old_est_id = old_schedule['establishment']

            if old_timetable is None:
                continue

            est = Establishment.objects.filter(old_id=old_est_id).values('id').first()

            if est is None:
                continue

            schedule_data = EstablishmentSerializer.get_schedules_payload(old_timetable)
            for timetable_data in schedule_data:
                prepared_timetable_data = {}
                for k, v in timetable_data.items():
                    if k not in time_fields:
                        prepared_timetable_data[k] = v
                        continue

                    if v == '' or v is None:
                        prepared_timetable_data[k] = None
                        continue

                    if not isinstance(v, str) or not re.findall(r'^(\d{1,2}:\d{1,2})$|^(\d{1,2}:\d{1,2}:\d{1,2})$', v):
                        print('UNEXPECTED:', k, v)
                        continue

                    prepared_timetable_data[k] = v

                prepared_timetable_data['schedule_id'] = est['id']
                Timetable.objects.create(**prepared_timetable_data)

            count += 1

        self.stdout.write(self.style.WARNING(f'Update {count} objects.'))
