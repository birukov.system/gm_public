from django.core.management.base import BaseCommand

from transfer.models import CepageRegions
from transfer.serializers.location import CepageWineRegionSerializer


class Command(BaseCommand):
    help = 'Add cepage tag to wine region'

    def handle(self, *args, **kwarg):
        errors = []
        legacy_products = CepageRegions.objects.exclude(cepage_id__isnull=True) \
                                               .exclude(wine_region_id__isnull=True)
        serialized_data = CepageWineRegionSerializer(
            data=list(legacy_products.values()),
            many=True)
        if serialized_data.is_valid():
            serialized_data.save()
        else:
            for d in serialized_data.errors: errors.append(d) if d else None

        self.stdout.write(self.style.WARNING(f'Error count: {len(errors)}\nErrors: {errors}'))
