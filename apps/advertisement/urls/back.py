"""Advertisement back office urlpaths."""
from django.urls import path
from advertisement import views
from advertisement.urls.common import common_urlpatterns


app_name = 'advertisements'

urlpatterns = [
    path('', views.AdvertisementListCreateView.as_view(), name='list-create'),
    path('page-types/', views.AdvertisementPageTypeListView.as_view(),
         name='page-type-list'),
    path('devices/', views.AdvertisementDeviceListView.as_view(),
         name='device-list'),
    path('block-levels/', views.AdvertisementBlockLevelListView.as_view(),
         name='block-level-list'),
    path('<int:pk>/', views.AdvertisementRUDView.as_view(), name='rud'),
    path('<int:pk>/pages/', views.AdvertisementPageCreateView.as_view(),
         name='ad-page-create'),
    path('<int:ad_pk>/pages/<int:page_pk>/', views.AdvertisementPageUDView.as_view(),
         name='ad-page-update-destroy')
]

urlpatterns += common_urlpatterns
