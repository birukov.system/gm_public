from pprint import pprint

from transfer import models as transfer_models
from transfer.serializers.tag import AssemblageTagSerializer


def transfer_assemblage():
    queryset = transfer_models.Assemblages.objects.all()
    serialized_data = AssemblageTagSerializer(
        data=list(queryset.values()),
        many=True)
    if serialized_data.is_valid():
        serialized_data.save()
    else:
        pprint(f"transfer_assemblage errors: {serialized_data.errors}")


data_types = {
    "assemblage": [
        transfer_assemblage,
    ]
}
