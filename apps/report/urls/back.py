"""Back office URL patterns for application report."""
from django.urls import path

from report.views import back as views

app_name = 'report'

urlpatterns = [
    path('', views.ReportListCreateView.as_view(), name='report-list-create'),
]
