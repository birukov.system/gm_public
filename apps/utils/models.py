"""Utils app models."""
import logging
import re
from json import dumps
from os.path import exists
from typing import Union
from urllib.parse import urlsplit, urlunsplit

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.db import models
from django.contrib.postgres.aggregates import ArrayAgg
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db.models import URLField
from django.forms.fields import URLField as FormURLField
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _, get_language
from easy_thumbnails.fields import ThumbnailerImageField
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.fields import ImageField as SORLImageField

from configuration.models import TranslationSettings
from utils.methods import image_path, svg_image_path, file_path
from utils.validators import svg_image_validator, ExtendedURLValidator

logger = logging.getLogger(__name__)


class ProjectBaseMixin(models.Model):
    """Base mixin model."""

    created = models.DateTimeField(default=timezone.now, editable=False,
                                   verbose_name=_('Date created'))
    modified = models.DateTimeField(auto_now=True,
                                    verbose_name=_('Date updated'))

    class Meta:
        """Meta class."""

        abstract = True


class TJSONField(JSONField):
    """Overrided JsonField."""


def to_locale(language):
    """Turn a language name (en-us) into a locale name (en_US)."""
    if language:
        language, _, country = language.lower().partition('-')
        if not country:
            return language
        # A language with > 2 characters after the dash only has its first
        # character after the dash capitalized; e.g. sr-latn becomes sr-Latn.
        # A language with 2 characters after the dash has both characters
        # capitalized; e.g. en-us becomes en-US.
        country, _, tail = country.partition('-')
        country = country.title() if len(country) > 2 else country.upper()
        if tail:
            country += '-' + tail
        return language + '-' + country


def get_current_locale():
    """Get current language."""
    return to_locale(get_language())


def get_default_locale():
    return TranslationSettings.get_solo().default_language or \
           settings.FALLBACK_LOCALE


def translate_field(self, field_name, toggle_field_name=None):
    def translate(self):
        field = getattr(self, field_name)
        toggler = getattr(self, toggle_field_name, None)
        if isinstance(field, dict):
            if toggler:
                field = {locale: v for locale, v in field.items() if toggler.get(locale) in [True, 'True', 'true']}
            value = field.get(to_locale(get_language()))
            # fallback
            if value is None:
                value = field.get(get_default_locale())
                if value is None:
                    try:
                        value = next(iter(field.values()))
                    except StopIteration:
                        # field values are absent
                        return None
            return value
        return None

    return translate


# todo: refactor this
class IndexJSON:

    def __getattr__(self, item):
        return None


def index_field(self, field_name):
    def index(self):
        field = getattr(self, field_name)
        obj = IndexJSON()
        if isinstance(field, dict):
            for key, value in field.items():
                setattr(obj, key, value)
        return obj

    return index


def values_field(self, field_name):
    def index(self):
        field = getattr(self, field_name)
        if isinstance(field, dict):
            return list(field.values())
        return []

    return index


def serialized_field(self, field_name):
    def index(self):
        field = getattr(self, field_name)
        if isinstance(field, dict):
            return dumps(field)
        return dumps({})

    return index


class TranslatedFieldsMixin:
    """Translated Fields mixin"""

    STR_FIELD_NAME = ''

    def __init__(self, *args, **kwargs):
        """Overrided __init__ method."""
        super(TranslatedFieldsMixin, self).__init__(*args, **kwargs)
        cls = self.__class__
        for field in cls._meta.fields:
            field_name = field.name
            if isinstance(field, TJSONField):
                setattr(cls, f'{field.name}_translated',
                        property(translate_field(self, field_name, f'locale_to_{field_name}_is_active')))
                setattr(cls, f'{field_name}_indexing',
                        property(index_field(self, field_name)))
                setattr(cls, f'{field.name}_searchable',
                        property(values_field(self, field_name)))
                setattr(cls, f'{field.name}_serialized',
                        property(serialized_field(self, field_name)))

    def __str__(self):
        """Overrided __str__ method."""
        value = None
        if self.STR_FIELD_NAME:
            field = getattr(self, getattr(self, 'STR_FIELD_NAME'))
            if isinstance(field, dict):
                value = field.get(get_current_locale())
        return value if value else super(TranslatedFieldsMixin, self).__str__()


class OAuthProjectMixin:
    """OAuth2 mixin for project GM"""

    def get_source(self):
        """Method to get of platform"""
        return NotImplementedError()


class BaseAttributes(ProjectBaseMixin):
    created_by = models.ForeignKey(
        'account.User', on_delete=models.SET_NULL, verbose_name=_('created by'),
        null=True, related_name='%(class)s_records_created'
    )
    modified_by = models.ForeignKey(
        'account.User', on_delete=models.SET_NULL, verbose_name=_('modified by'),
        null=True, related_name='%(class)s_records_modified'
    )

    class Meta:
        """Meta class."""

        abstract = True


class FileMixin(models.Model):
    """File model."""

    file = models.FileField(upload_to=file_path,
                            blank=True, null=True, default=None,
                            verbose_name=_('File'),
                            validators=[FileExtensionValidator(
                                allowed_extensions=('jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf')
                            )])

    class Meta:
        """Meta class."""

        abstract = True

    def get_file_url(self):
        """Get file url."""
        return self.file.url if self.file else None

    def get_full_file_url(self, request):
        """Get full file url"""
        if self.file and exists(self.file.path):
            return request.build_absolute_uri(self.file.url)
        else:
            return None


class ImageMixin(models.Model):
    """Avatar model."""

    THUMBNAIL_KEY = 'default'

    image = ThumbnailerImageField(upload_to=image_path,
                                  blank=True, null=True, default=None,
                                  verbose_name=_('Image'))

    class Meta:
        """Meta class."""

        abstract = True

    def get_image(self, key=None):
        """Get thumbnailed image file."""
        return self.image[key or self.THUMBNAIL_KEY] if self.image else None

    def get_image_url(self, key=None):
        """Get image thumbnail url."""
        return self.get_image(key).url if self.image else None

    def image_tag(self):
        """Admin preview tag."""
        if self.image:
            return mark_safe('<img src="%s" />' % self.get_image_url())
        else:
            return None

    def get_full_image_url(self, request, thumbnail_key=None):
        """Get full image url"""
        if self.image and exists(self.image.path):
            return request.build_absolute_uri(self.get_image(thumbnail_key).url)
        else:
            return None

    image_tag.short_description = _('Image')
    image_tag.allow_tags = True


class SORLImageMixin(models.Model):
    """Abstract model for SORL ImageField"""

    image = SORLImageField(upload_to=image_path,
                           blank=True, null=True, default=None,
                           verbose_name=_('Image'))

    class Meta:
        """Meta class."""
        abstract = True

    def get_image(self, thumbnail_key: str):
        """Get thumbnail image file."""
        if thumbnail_key in settings.SORL_THUMBNAIL_ALIASES:
            return get_thumbnail(
                file_=self.image,
                **settings.SORL_THUMBNAIL_ALIASES[thumbnail_key])

    def get_image_url(self, thumbnail_key: str):
        """Get image thumbnail url."""
        crop_image = self.get_image(thumbnail_key)
        if hasattr(crop_image, 'url'):
            return crop_image.url

    def image_tag(self):
        """Admin preview tag."""
        if self.image:
            return mark_safe(f'<img src="{self.image.url}" style="max-height: 25%; max-width: 25%" />')
        else:
            return None

    def get_cropped_image(self, geometry: str, quality: int, cropbox: str) -> dict:
        cropped_image = get_thumbnail(self.image,
                                      geometry_string=geometry,
                                      # crop=crop,
                                      # upscale=False,
                                      cropbox=cropbox,
                                      quality=quality)
        return {
            'geometry_string': geometry,
            'crop_url': cropped_image.url,
            'quality': quality,
            # 'crop': crop,
            'cropbox': cropbox,
        }

    image_tag.short_description = _('Image')
    image_tag.allow_tags = True


class SVGImageMixin(models.Model):
    """SVG image model."""

    svg_image = models.FileField(upload_to=svg_image_path,
                                 blank=True, null=True, default=None,
                                 validators=[svg_image_validator, ],
                                 verbose_name=_('SVG image'))

    @property
    def svg_image_indexing(self):
        return self.svg_image.url if self.svg_image else None

    class Meta:
        abstract = True


class URLImageMixin(models.Model):
    """URl for image and special methods."""
    image_url = models.URLField(verbose_name=_('Image URL path'),
                                blank=True, null=True, default=None)

    class Meta:
        abstract = True

    def image_tag(self):
        if self.image_url:
            return mark_safe(
                f'<a href="{self.image_url}"><img src="{self.image_url}" height="50"/>')
        else:
            return None

    image_tag.short_description = _('Image')
    image_tag.allow_tags = True


class PlatformMixin(models.Model):
    """Platforms"""

    MOBILE = 0
    WEB = 1
    ALL = 2

    SOURCES = (
        (MOBILE, _('Mobile')),
        (WEB, _('Web')),
        (ALL, _('All'))
    )
    source = models.PositiveSmallIntegerField(choices=SOURCES, default=MOBILE,
                                              verbose_name=_('Source'))

    class Meta:
        """Meta class"""
        abstract = True


class LocaleManagerMixin(models.Manager):
    """Manager for locale"""


class GMTokenGenerator(PasswordResetTokenGenerator):
    CHANGE_EMAIL = 0
    RESET_PASSWORD = 1
    CHANGE_PASSWORD = 2
    CONFIRM_EMAIL = 3

    TOKEN_CHOICES = (
        CHANGE_EMAIL,
        RESET_PASSWORD,
        CHANGE_PASSWORD,
        CONFIRM_EMAIL
    )

    def __init__(self, purpose: int):
        if purpose in self.TOKEN_CHOICES:
            self.purpose = purpose

    def get_fields(self, user, timestamp):
        """
        Get user fields for hash value.
        """
        fields = [str(timestamp), str(user.is_active), str(user.pk)]
        if self.purpose == self.CHANGE_EMAIL or \
                self.purpose == self.CONFIRM_EMAIL:
            fields.extend([str(user.email_confirmed), str(user.email)])
        elif self.purpose == self.RESET_PASSWORD or \
                self.purpose == self.CHANGE_PASSWORD:
            fields.append(str(user.password))
        return fields

    def _make_hash_value(self, user, timestamp):
        """
        Hash the user's primary key and some user state that's sure to change
        after a password reset to produce a token that invalidated when it's
        used.
        """
        return self.get_fields(user, timestamp)


class GalleryMixin:
    """Mixin for models that has gallery."""

    @cached_property
    def _crop_parameters(self):
        if not hasattr(self, '_meta'):
            return None

        return [p for p in settings.SORL_THUMBNAIL_ALIASES
                if p.startswith(self._meta.model_name.lower())]

    def _get_cropped_image(self, image_instance):
        data = {
            'id': image_instance.id,
            'title': image_instance.title,
            'original_url': image_instance.image.url,
            'orientation_display': image_instance.get_orientation_display(),
            'auto_crop_images': {},
        }

        crop_parameters = self._crop_parameters or []
        for crop in crop_parameters:
            data['auto_crop_images'].update({crop: image_instance.get_image_url(crop)})

        return data

    @property
    def gallery_include_videos_with_cropped_images(self):
        if not hasattr(self, 'gallery'):
            return None

        gallery = []
        media = self.gallery.all()
        if hasattr(self, '_meta') and self._meta.model_name.lower() == 'establishment':
            media = media.order_by('-establishment_gallery__is_main')

        for media_entity in media:
            # image
            if media_entity.type == media_entity.MEDIA_TYPES[0]:
                data = self._get_cropped_image(media_entity)
                data['preview'] = media_entity.preview_indexing
                data['type'] = media_entity.type
                gallery.append(data)
                continue

            # video
            data = {
                'id': media_entity.id,
                'title': media_entity.title,
                'original_url': media_entity.link,
                'orientation_display': media_entity.get_orientation_display(),
                'preview': media_entity.preview_indexing,
                'type': media_entity.type,
            }
            gallery.append(data)

        return gallery

    @property
    def crop_gallery(self):
        if hasattr(self, 'gallery') and hasattr(self, '_meta'):
            gallery = []
            images = self.gallery.all()
            for image in list(filter(lambda image: image.is_image, images)):
                gallery.append(self._get_cropped_image(image))
            return gallery

    @property
    def crop_main_image(self):
        if hasattr(self, 'main_image') and hasattr(self, '_meta'):
            if self.main_image:
                image = self.main_image
                image_property = {
                    'id': image.id,
                    'title': image.title,
                    'original_url': image.image.url,
                    'orientation_display': image.get_orientation_display(),
                    'auto_crop_images': {},
                }
                crop_parameters = [p for p in settings.SORL_THUMBNAIL_ALIASES
                                   if p.startswith(self._meta.model_name.lower())]
                for crop in crop_parameters:
                    image_property['auto_crop_images'].update(
                        {crop: image.get_image_url(crop)}
                    )
                return image_property


class IntermediateGalleryModelQuerySet(models.QuerySet):
    """Extended QuerySet."""

    def main_image(self):
        """Return objects with flag is_main is True"""
        return self.filter(is_main=True)


class IntermediateGalleryModelMixin(models.Model):
    """Mixin for intermediate gallery model."""

    is_main = models.BooleanField(default=False,
                                  verbose_name=_('Is the main image'))

    objects = IntermediateGalleryModelQuerySet.as_manager()

    class Meta:
        """Meta class."""
        abstract = True

    def __str__(self):
        """Overridden str method."""
        if hasattr(self, 'image'):
            return self.image.title if self.image.title else self.id


class HasTagsMixin(models.Model):
    """Mixin for filtering tags"""

    @property
    def visible_tags(self):
        return self.tags.filter(category__public=True).prefetch_related(
            'category',
            'translation',
            'category__translation',
        ).exclude(category__value_type='bool')

    class Meta:
        """Meta class."""
        abstract = True


class FavoritesMixin:
    """Append favorites_for_user property."""

    @property
    def favorites_for_users(self):
        return self.favorites.aggregate(arr=ArrayAgg('user_id')).get('arr')


timezone.datetime.now().date().isoformat()


class TypeDefaultImageMixin:
    """Model mixin for default image."""

    @property
    def default_image_url(self):
        """Return image url."""
        if hasattr(self, 'default_image') and self.default_image:
            return self.default_image.image.url

    @property
    def preview_image_url(self):
        if hasattr(self, 'default_image') and self.default_image:
            return self.default_image.get_image_url(thumbnail_key='type_preview')


def default_menu_bool_array():
    return [False] * 7


class PhoneModelMixin:
    """Mixin for PhoneNumberField."""

    @cached_property
    def country_calling_code(self):
        """Return phone code from PhonеNumberField."""
        if hasattr(self, 'phone') and self.phone:
            return f'+{self.phone.country_code}'

    @cached_property
    def national_calling_number(self):
        """Return phone national number from PhonеNumberField."""
        if hasattr(self, 'phone') and (self.phone and hasattr(self.phone, 'national_number')):
            return self.phone.national_number


class AwardsModelMixin:
    def remove_award(self, award_id: int):
        from main.models import Award
        award = get_object_or_404(Award, pk=award_id)

        if hasattr(self, 'awards'):
            self.awards.remove(award)


class CarouselMixin:
    def __must_of_the_week_info_from_self(self) -> dict:
        from location.models import Country

        country = getattr(self, 'country', None)
        country_id = getattr(self, 'country_id', None)

        if not country and country_id:
            country_qs = Country.objects.filter(id=country_id)
            country = country_qs.first() if country_qs.exists() else None

        return {
            'content_type': ContentType.objects.get_for_model(self),
            'object_id': getattr(self, 'pk'),
            'country': country,
            'active': True,
            'is_parse': True,
        }

    def __must_of_the_week_has_fields(self) -> bool:
        return hasattr(self, 'pk') and (hasattr(self, 'country') or hasattr(self, 'country_id'))

    @property
    def must_of_the_week(self) -> bool:
        from main import models as main_models

        """Detects whether current item in carousel"""
        if self.__must_of_the_week_has_fields():
            kwargs = self.__must_of_the_week_info_from_self()
            return main_models.Carousel.objects.filter(**kwargs).exists()

        return False

    @must_of_the_week.setter
    def must_of_the_week(self, status: bool):
        from main import models as main_models

        """Update field by status"""
        if status != self.must_of_the_week and self.__must_of_the_week_has_fields():
            carousel, _ = main_models.Carousel.objects.get_or_create(
                **self.__must_of_the_week_info_from_self()
            )

            if not status and isinstance(carousel, main_models.Carousel):
                carousel.delete()


class UpdateByMixin(models.Model):
    """Modify by mixin"""
    last_update_by_manager = models.DateTimeField(null=True)
    last_update_by_gm = models.DateTimeField(null=True)
    update_by = models.ForeignKey('account.User', null=True, verbose_name=_('User'), on_delete=models.SET_NULL)

    class Meta:
        """Meta class."""
        abstract = True


class ExtendedURLFormField(FormURLField):
    default_validators = [ExtendedURLValidator(), ]

    def to_python(self, value):

        def split_url(url):
            """
            Return a list of url parts via urlparse.urlsplit(), or raise
            ValidationError for some malformed URLs.
            """
            try:
                return list(urlsplit(url))
            except ValueError:
                # urlparse.urlsplit can raise a ValueError with some
                # misformatted URLs.
                raise ValidationError(self.error_messages['invalid'], code='invalid')

        value = super().to_python(value)
        if value:
            url_fields = split_url(value)
            if not url_fields[0]:
                # If no URL scheme given, assume http://
                url_fields[0] = 'http'
            if not url_fields[1]:
                # Assume that if no domain is provided, that the path segment
                # contains the domain.
                url_fields[1] = url_fields[2]
                url_fields[2] = ''
                # Rebuild the url_fields list, since the domain segment may now
                # contain the path too.
                url_fields = split_url(urlunsplit(url_fields))
            # Cut double slashes after handling
            if url_fields[0] == 'mailto':
                value = ''.join(urlunsplit(url_fields).split('//'))
            else:
                value = urlunsplit(url_fields)
        return value


class ExtendedURLField(URLField):
    """URL-field that accepts URLs that start with mailto: only."""
    default_validators = [ExtendedURLValidator(), ]

    def formfield(self, **kwargs):
        return super(ExtendedURLField, self).formfield(**{
            'form_class': ExtendedURLFormField,
        })


class IndexNameGeneratorMixin:

    @property
    def _country(self):
        """Return instance of Country model from a related object."""
        address = getattr(self, 'address', None)
        city = getattr(address, 'city', None)
        country = getattr(city, 'country', None)
        return country

    @property
    def _uppercase_country_articles(self) -> Union[list]:
        """Return articles in uppercase by country code."""
        if self._country and hasattr(self._country, 'articles') and self._country.articles:
            return [article.upper() for article in self._country.articles]
        return []

    @property
    def _uppercase_name(self):
        """Return name in uppercase."""
        if hasattr(self, 'name') and self.name:
            return self.name.upper()

    def convert_name_to_index_name(self) -> str:
        """
        Method to convert name to index name.
        :return str
        """
        name = self._uppercase_name
        if name:
            if "'" in name:
                name = self._uppercase_name.replace("'", " ", 1)

            if self._uppercase_country_articles:
                for article in self._uppercase_country_articles:
                    pattern = rf"^{article}\b"
                    new_name = re.sub(pattern, '', name).strip()
                    if new_name != name:
                        return f"{new_name} ({article})"
            return name
