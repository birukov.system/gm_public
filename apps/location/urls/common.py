"""Location app common urlconf."""
from django.urls import path

from location import views

app_name = 'location'

urlpatterns = [
    path('addresses/', views.AddressListView.as_view(), name='address-list'),
    path('addresses/<int:pk>/', views.AddressRetrieveView.as_view(), name='address-retrieve'),

    path('cities/', views.CityListView.as_view(), name='city-list'),
    path('cities/<int:pk>/', views.CityRetrieveView.as_view(), name='city-retrieve'),

    path('countries/', views.CountryListView.as_view(), name='country-list'),
    path('countries/<int:pk>/', views.CountryRetrieveView.as_view(), name='country-retrieve'),

    path('regions/', views.RegionListView.as_view(), name='region-list'),
    path('regions/<int:pk>/', views.RegionRetrieveView.as_view(), name='region-retrieve'),

    path('wine-regions/', views.WineRegionListView.as_view(), name='wine-region-list'),
]
