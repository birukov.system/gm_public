# Generated by Django 2.2.7 on 2019-11-13 12:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tag', '0014_tag_old_id_meta_product'),
        ('location', '0023_auto_20191112_0104'),
    ]

    operations = [
        migrations.AddField(
            model_name='wineregion',
            name='tag_categories',
            field=models.ManyToManyField(blank=True, help_text='attribute from legacy db', related_name='wine_regions', to='tag.TagCategory'),
        ),
    ]
