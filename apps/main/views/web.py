from typing import Iterable

from rest_framework import generics, permissions

from utils.serializers import EmptySerializer
from rest_framework.response import Response
from main import methods, models, serializers


class DetermineSiteView(generics.GenericAPIView):
    """Determine user's site."""

    permission_classes = (permissions.AllowAny,)
    serializer_class = EmptySerializer

    def get(self, request, *args, **kwargs):
        country_code = methods.determine_country_code(request)
        url = methods.determine_user_site_url(country_code)
        return Response(data={'url': url})


class SiteSettingsView(generics.RetrieveUpdateDestroyAPIView):
    """Site settings View."""

    lookup_field = 'subdomain'
    permission_classes = (permissions.AllowAny,)
    queryset = models.SiteSettings.objects.all()
    serializer_class = serializers.SiteSettingsBackOfficeSerializer


class SiteListView(generics.ListCreateAPIView):
    """Site settings View."""

    pagination_class = None
    permission_classes = (permissions.AllowAny,)
    queryset = models.SiteSettings.objects.with_country()
    serializer_class = serializers.SiteSerializer
