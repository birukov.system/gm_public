import csv
from datetime import datetime

from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from rest_framework import generics
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from account.models import User, Role
from account.serializers import UserBaseSerializer
from establishment.models import EstablishmentType, EstablishmentSubType
from review import filters
from review import models
from review import serializers
from review.serializers.back import ReviewBackCreateSerializer, ReviewBackListSerializer, ReviewBackDetailSerializer
from utils.methods import get_permission_classes
from utils.permissions import (
    IsReviewManager, IsRestaurantInspector, IsWineryWineInspector,
    IsArtisanInspector, IsProducerFoodInspector, IsDistilleryLiquorInspector,
)
from utils.views import ChoseToListMixin
from establishment.views.back import EstablishmentLastUpdatedMixin


class PermissionMixinView:
    """Permission mixin view."""
    permission_classes = get_permission_classes(
        IsReviewManager, IsRestaurantInspector, IsWineryWineInspector,
        IsArtisanInspector, IsProducerFoodInspector, IsDistilleryLiquorInspector,
    )


class ReviewMixinView:
    """Review mixin."""
    queryset = models.Review.objects.with_base_related()


class ReviewListView(EstablishmentLastUpdatedMixin,
                     PermissionMixinView,
                     ReviewMixinView,
                     generics.ListCreateAPIView):
    """
    ## Review list/create view.
    ### **GET**
    #### Description
    Return paginated QuerySet with available filters
    * establishment_id (`int`) - by an identifier of establishment
    * product_id (`int`) - by an identifier of product
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### **POST**
    #### Description
    Create new review.
    ##### Request
    Required
    * vintage (`int`) - vintage year (from 1900 to 2100)
    * content_type (`int`) - identifier of reviewed entity
    * object_id (`int`) - identifier of object of reviewed entity
    Non-required
    * reviewer (`int`) - identifier of User
    * text (`JSON`) - JSON that contain key as language of review and value as its text
    * status (`int`) - enum
    ```
    TO_INVESTIGATE = 0  * default
    PENDING_RECEPTION = 1
    TO_VALIDATE = 2
    TO_TRANSLATE = 3
    READY = 4
    PUBLISHED = 5
    ABANDONED = 6
    HIDDEN = 7
    ```
    * mark (`float`) - review mark, default None
    * priority (`int`) - priority of review, default None
    * published_at (`str`) - datetime in a format ISO-8601
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    filterset_class = filters.ReviewFilter

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return ReviewBackCreateSerializer

        return ReviewBackListSerializer

    def should_control_last_updating(self):
        establishment_content_type = ContentType.objects.get(app_label="establishment", model="establishment")

        # if its Establishment review then we need to update establishment last update fields
        if self.request.data.get('content_type') == establishment_content_type.id:
            return True

        return False

    def get_establishment_search_kwargs(self):
        return {'pk': self.request.data.get('object_id')}


class ReviewRUDView(EstablishmentLastUpdatedMixin,
                    PermissionMixinView,
                    ReviewMixinView,
                    generics.RetrieveUpdateDestroyAPIView):
    """Review RUD view.

    ## Review RUD view.
    ### **GET**
    #### Description
    Return serialized instance of a review by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### **PUT**/**PATCH**
    #### Description
    Completely/Partially update a news object by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ##### Request
    * vintage (`int`) - vintage year (from 1900 to 2100)
    * content_type (`int`) - identifier of reviewed entity
    * object_id (`int`) - identifier of object of reviewed entity
    * reviewer (`int`) - identifier of User
    * text (`JSON`) - JSON that contain key as language of review and value as its text
    * status (`int`) - enum
    ```
    TO_INVESTIGATE = 0  * default
    PENDING_RECEPTION = 1
    TO_VALIDATE = 2
    TO_TRANSLATE = 3
    READY = 4
    PUBLISHED = 5
    ABANDONED = 6
    HIDDEN = 7
    ```
    * mark (`float`) - review mark, default None
    * priority (`int`) - priority of review, default None
    * published_at (`str`) - datetime in a format ISO-8601

    ### **DELETE**
    #### Description
    Delete an instance of review by an identifier.
    ##### Response
    ```
    No content
    ```
    ##### Request
    ```
    No body
    ```
    """
    serializer_class = ReviewBackDetailSerializer
    lookup_field = 'id'

    def get_establishment_search_kwargs(self):
        return self._establishment_search_kwargs

    def should_control_last_updating(self):
        review_instance = self.get_object()

        establishment_content_type = review_instance.content_type

        # if its Establishment review then we need to update establishment last update fields
        if self.request.data.get('content_type') == establishment_content_type.id:
            self._establishment_search_kwargs = {'pk': review_instance.object_id}
            return True

        return False


class InquiriesListView(EstablishmentLastUpdatedMixin, PermissionMixinView, generics.ListCreateAPIView):
    """
    ## Inquiries list create view.
    ### **GET**
    #### Description
    Return inquiries (optional: by a review identifier).
    ##### Response
    E.g.
    ```
    {
        "count": 0,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```
    ### **POST**
    #### Description
    Create a new inquiry for a review by its identifier.
    ##### Request
    Required
    * author_id (`int`) - identifier of user
    Non-required
    * review (`int`) - identifier of a review (if not in request data,
    then would be searched in URL kwargs)
    * comment (`str`) - comment
    * final_comment (`str`) - final comment
    * mark (`float`) - mark
    * attachment_file (`str`) - URL of attachment file
    * bill_file (`str`) - URL of bill file
    * price (`decimal`) - price (max digits=7, decimal places=2)
    * moment (`int`) - enum
    ```
    NONE = 0  # default
    DINER = 1
    LUNCH = 2
    ```
    * decibels (`str`) - decibels
    * nomination (`str`) - nomination
    * nominee (`str`) - nominee
    * published (`bool`) - flag that responds on showing on page, default `False`
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """

    serializer_class = serializers.InquiriesBaseSerializer
    queryset = models.Inquiries.objects.all()

    def get_queryset(self):
        review_id = self.kwargs.get('review_id')
        if review_id:
            return super().get_queryset().filter(review_id=review_id)
        return super().get_queryset()

    def should_control_last_updating(self):
        establishment_content_type = ContentType.objects.get(app_label="establishment", model="establishment")

        review_instance = models.Review.objects.filter(pk=self.kwargs.get('review_id')).first()
        if review_instance is None:
            return False

        # if its Establishment review then we need to update establishment last update fields
        if review_instance.content_type.id == establishment_content_type.id:
            self._establishment_search_kwargs = {'pk': review_instance.object_id}
            return True

        return False

    def get_establishment_search_kwargs(self):
        return self._establishment_search_kwargs


class InquiriesRUDView(EstablishmentLastUpdatedMixin, PermissionMixinView, generics.RetrieveUpdateDestroyAPIView):
    """
    ## Inquiries RUD view.
    ### **GET**
    #### Description
    Return serialized object of an inquiry.
    ##### Request
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### **PUT/PATCH**
    #### Description
    Completely/Partially update an inquiry object by an identifier.
    ##### Request
    * author_id (`int`) - identifier of user
    * review (`int`) - identifier of a review
    * comment (`str`) - comment
    * final_comment (`str`) - final comment
    * mark (`float`) - mark
    * attachment_file (`str`) - URL of attachment file
    * bill_file (`str`) - URL of bill file
    * price (`decimal`) - price (max digits=7, decimal places=2)
    * moment (`int`) - enum
    ```
    NONE = 0  # default
    DINER = 1
    LUNCH = 2
    ```
    * decibels (`str`) - decibels
    * nomination (`str`) - nomination
    * nominee (`str`) - nominee
    * published (`bool`) - flag that responds on showing on page, default `False`

    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### **DELETE**
    #### Description
    Delete an instance of inquiry by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.InquiriesBaseSerializer
    queryset = models.Inquiries.objects.all()
    lookup_field = 'id'

    def should_control_last_updating(self):
        establishment_content_type = ContentType.objects.get(app_label="establishment", model="establishment")

        inquiry_instance = models.Inquiries.objects\
            .filter(pk=self.kwargs.get(self.lookup_field))\
            .select_related('review')\
            .first()

        review_instance = inquiry_instance.review
        if review_instance is None:
            return False

        # if its Establishment review then we need to update establishment last update fields
        if review_instance.content_type.id == establishment_content_type.id:
            self._establishment_search_kwargs = {'pk': review_instance.object_id}
            return True

        return False

    def get_establishment_search_kwargs(self):
        return self._establishment_search_kwargs


class GridItemsListView(EstablishmentLastUpdatedMixin, PermissionMixinView, generics.ListCreateAPIView):
    """
    ## GridItems list/create view.
    ### **GET**
    #### Description
    Return list of grid items (optional: if `inquiry_id` in URL-kwargs,
    then filtered by its identifier.)
    ##### Response
    E.g.
    ```
    {
      "count": 58,
      "next": null,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    ### **POST**
    #### Description
    Create a new grid item
    ##### Request
    Required
    * inquiry (`int`) - identifier of an inquiry (if not in request data,
    then would be searched in URL kwargs)
    Non-required
    * sub_name (`str`) - sub name
    * name (`str`) - name
    * value (`float`) - value of a grid inquiry
    * desc (`str`) - description
    * dish_title (`str`) - dish title
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.GridItemsBaseSerializer
    queryset = models.GridItems.objects.all()

    def get_queryset(self):
        inquiry_id = self.kwargs.get('inquiry_id')
        if inquiry_id:
            return super().get_queryset().filter(inquiry_id=inquiry_id)
        return super().get_queryset()

    def should_control_last_updating(self):
        establishment_content_type = ContentType.objects.get(app_label="establishment", model="establishment")

        inquiry_instance = models.Inquiries.objects\
            .filter(pk=self.kwargs.get('inquiry_id'))\
            .select_related('review')\
            .first()

        review_instance = inquiry_instance.review
        if review_instance is None:
            return False

        # if its Establishment review then we need to update establishment last update fields
        if review_instance.content_type.id == establishment_content_type.id:
            self._establishment_search_kwargs = {'pk': review_instance.object_id}
            return True

        return False

    def get_establishment_search_kwargs(self):
        return self._establishment_search_kwargs


class GridItemsRUDView(EstablishmentLastUpdatedMixin, PermissionMixinView, generics.RetrieveUpdateDestroyAPIView):
    """
    ## GridItems RUD view.
    ### **GET**
    #### Description
    Return serialized object of a grid item by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **PUT/PATCH**
    #### Description
    Completely/Partially update a grid item object by an identifier.
    ##### Request
    * inquiry (`int`) - identifier of an inquiry
    * sub_name (`str`) - sub name
    * name (`str`) - name
    * value (`float`) - value of a grid inquiry
    * desc (`str`) - description
    * dish_title (`str`) - dish title
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### **DELETE**
    #### Description
    Delete an instance of grid item by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.GridItemsBaseSerializer
    queryset = models.GridItems.objects.all()
    lookup_field = 'id'

    def should_control_last_updating(self):
        establishment_content_type = ContentType.objects.get(app_label="establishment", model="establishment")

        grid_instance = models.GridItems.objects\
            .filter(pk=self.kwargs.get(self.lookup_field))\
            .select_related('inquiry__review')\
            .first()

        review_instance = grid_instance.inquiry.review
        if review_instance is None:
            return False

        # if its Establishment review then we need to update establishment last update fields
        if review_instance.content_type.id == establishment_content_type.id:
            self._establishment_search_kwargs = {'pk': review_instance.object_id}
            return True

        return False

    def get_establishment_search_kwargs(self):
        return self._establishment_search_kwargs


class ReviewAvailableStatusesSetView(ChoseToListMixin, generics.ListAPIView):
    """

    ## Return list of available review statuses formatted in JSON.
    ### **GET**
    #### Description
    Raw statuses
    ```
    TO_INVESTIGATE = 0
    PENDING_RECEPTION = 1
    TO_VALIDATE = 2
    TO_TRANSLATE = 3
    READY = 4
    PUBLISHED = 5
    ABANDONED = 6
    HIDDEN = 7
    ```
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    """
    chose_field = models.Review.REVIEW_STATUSES


class InquiriesInspectorsListView(generics.ListAPIView):
    serializer_class = UserBaseSerializer
    pagination_class = None

    def get_queryset(self):
        inspector_for_type = {
            EstablishmentType.RESTAURANT: Role.RESTAURANT_INSPECTOR,
            EstablishmentSubType.WINERY: Role.WINERY_WINE_INSPECTOR,
            EstablishmentSubType.DISTILLERIES: Role.DISTILLERY_LIQUOR_INSPECTOR,
            EstablishmentSubType.FOOD_PRODUCER: Role.PRODUCER_FOOD_INSPECTOR,
            EstablishmentType.ARTISAN: Role.ARTISAN_INSPECTOR,
        }
        type = self.request.query_params.get('type')
        if type is None:
            raise ValidationError('`type` query param expected')

        if type not in inspector_for_type:
            raise ValidationError('Invalid `type`')

        return User.objects.by_role(inspector_for_type[type])


class ReviewVintageListView(generics.GenericAPIView):

    def get(self, request):
        vintages = models.Review.objects.values_list('vintage', flat=True).order_by('-vintage').distinct()
        return Response(vintages)


class ReviewAuthorListView(generics.ListAPIView):
    serializer_class = UserBaseSerializer
    pagination_class = None

    def get_queryset(self):
        return User.objects.exclude(reviews__isnull=True)


class ReviewCSVListView(PermissionMixinView,
                        ReviewMixinView,
                        generics.ListAPIView):
    serializer_class = serializers.ReviewBackCSVSerializer
    pagination_class = None

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="reviews_{datetime.now().date()}.csv"'

        writer = csv.writer(response)
        writer.writerow((
            'id',
            'vintage',
            'mark',
        ))

        for item in serializer.data:
            writer.writerow(item.values())

        return response
