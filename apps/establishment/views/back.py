"""Establishment app views."""
from datetime import datetime

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.db.models import Prefetch
from django.db.models.query_utils import Q
from django.db.models.query import prefetch_related_objects
from django.http import Http404
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from django.utils import timezone
from rest_framework import generics, response, status, mixins, viewsets
from rest_framework.response import Response
from rest_framework.serializers import DateTimeField

from account.models import User, Role, UserRole
from collection.models import Guide
from establishment import filters, models, serializers
from establishment.models import EstablishmentEmployee, Menu
from main import models as main_models
from main import serializers as main_serializers
from timetable.models import Timetable
from timetable.serialziers import HolidaySerializer
from timetable.serialziers import ScheduleCreateSerializer, ScheduleRUDSerializer
from utils.methods import get_permission_classes
from utils.permissions import (IsEstablishmentAdministrator, IsEstablishmentManager)
from utils.views import CreateDestroyGalleryViewMixin, ChoseToListMixin


class MenuRUDMixinViews:
    """Menu mixin"""

    def get_object(self):
        instance = self.get_queryset().filter(
            Q(establishment__slug=self.kwargs.get('slug')) | Q(establishment__id=self.kwargs.get('pk'))
        ).first()

        if instance is None:
            raise Http404

        return instance


class EstablishmentMixinViews:
    """Establishment mixin."""

    def get_queryset(self):
        """Overridden method 'get_queryset'."""
        queryset = models.Establishment.objects.with_base_related
        if (hasattr(self, 'request') and
                (hasattr(self.request, 'user') and hasattr(self.request, 'country_code')) and
                self.request.user.is_authenticated):
            return queryset().available_establishments(self.request.user, self.request.country_code)
        return queryset().none()


class EstablishmentLastUpdatedMixin:
    """ Mixin for updating establishment last change fields

        control_last_updating - on/off mixin
    """
    control_last_updating = True
    last_updated_mixin_methods = ('post', 'put', 'patch', 'delete')
    establishment_search_field_name = None
    establishment_search_kwarg_key = None

    def dispatch(self, request, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers  # deprecate?
        try:
            self.initial(request, *args, **kwargs)
            if request.method.lower() in self.http_method_names:
                handler = getattr(self, request.method.lower(),
                                  self.http_method_not_allowed)
            else:
                handler = self.http_method_not_allowed

            #  Custom block start

            if not self.should_control_last_updating() \
                    or self.request.method.lower() not in self.last_updated_mixin_methods \
                    or not self.request.user.is_authenticated:
                response = handler(request, *args, **kwargs)
            else:
                response = handler(request, *args, **kwargs)
                # getting establishment instance after dispatch because object can be changed after dispatch
                establishment_instance = self.get_establishment_for_last_updated_mixin()

                if status.is_success(response.status_code):
                    last_update_by_manager, last_update_by_gm = self._update_last_change_datetime_fields(
                        establishment_instance)
                    self.update_response_with_last_change_datetime(response,
                                                                   last_update_by_manager,
                                                                   last_update_by_gm)
                if hasattr(self, 'update_es_document') and self.update_es_document:
                    # huck to update es establishment manually
                    from django_elasticsearch_dsl.registries import registry
                    registry.update(establishment_instance)

            #  Custom block end

        except Exception as exc:
            response = self.handle_exception(exc)

        self.response = self.finalize_response(request, response, *args, **kwargs)
        return self.response

    def should_control_last_updating(self):
        return self.control_last_updating

    def update_response_with_last_change_datetime(self, response, last_update_by_manager, last_update_by_gm):
        """ You can update response with new last change fields

        :param last_update_by_manager: datetime
        :param last_update_by_gm: datetime
        """
        pass

    def get_establishment_search_kwargs(self):
        """ Kwargs for search establishment for updating
        """
        establishment_pk = self.kwargs.get(self.establishment_search_kwarg_key)
        if establishment_pk:
            return {self.establishment_search_field_name: establishment_pk}

        establishment_pk = self.kwargs.get('pk')
        if establishment_pk:
            return {'pk': establishment_pk}

        establishment_slug = self.kwargs.get('slug')
        if establishment_slug:
            return {'slug': establishment_slug}

        return None

    def get_establishment_for_last_updated_mixin(self):
        """ Getting Establishment instance for updating"""
        search_kwargs = self.get_establishment_search_kwargs()
        if search_kwargs is None:
            return None

        establishment_instance = models.Establishment.objects.filter(**search_kwargs).first()
        return establishment_instance

    def _update_last_change_datetime_fields(self, establishment_instance):
        """ Update last change fields if needed"""
        if establishment_instance is None:
            return  # possibly deleted
            # raise ValueError('No establishment found for updating')

        user = self.request.user
        last_update_by_manager = None
        last_update_by_gm = None

        if UserRole.objects \
                .filter(user=user, establishment=establishment_instance) \
                .has_role(Role.ESTABLISHMENT_ADMINISTRATOR).exists():
            last_update_by_manager = datetime.now(tz=establishment_instance.tz)
            establishment_instance.last_update_by_manager = last_update_by_manager

        if UserRole.objects \
                .filter(user=user, establishment=establishment_instance) \
                .gm_employee().exists():
            last_update_by_gm = datetime.now(tz=establishment_instance.tz)
            establishment_instance.last_update_by_gm = last_update_by_gm

        establishment_instance.update_by = user
        establishment_instance.save()
        return last_update_by_manager, last_update_by_gm


class EstablishmentListCreateView(EstablishmentMixinViews, generics.ListCreateAPIView):
    """
    ## Establishment list/create view.
    ### *GET*
    #### Description
    Return a paginated QuerySet of available establishment for a role and filtered by country.
    Available filters
    * tag_id (`int`) - filter by a tag identifier
    * award_id (`int`) - filter by an award identifier
    * search ('str') - fuzzy search by fields - (name, description)
    > in according to a locale
    * type (`str`) - filter by a type index name
    * subtype (`str`) - filter by a subtype index name
    * city_id (`int`) - filter by a city identifier
    * city_name (`str`) - filter by a city name
    * review_status (`int`) - enum
    ```
    TO_INVESTIGATE = 0
    TO_REVIEW = 1
    READY = 2
    ```
    > for filtering by several stautus, use syntax below
    ```?review_status=2&review_status=0```
    ##### Response
    E.g.
    ```
    {
      "count": 1,
      "next": null,
      "previous": null,
      "results": [
        {
          "id": 11,
          ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Create a new establishment.
    ##### Request
    Required
    * type_index_name (`str) - index_name of type
    * tz (`str`) - timezone
    * address ('object') - Address object

    Available
    * name (`str`) - name
    * transliterated_name (`str`) - transliterated name
    * index_name (`str`) - establishment index name
    * website (`str`) - establishment website URL, e.g. http://google.com
    * phones (`str`) - list of phone numbers, e.g. ["+79000000000"]
    * toque_number (`str`) - toque number
    * public_mark (`str`) - public mark
    * emails (`str`) - list of emails, e.g. ["billgates@msn.com"]
    * price_level (`str`) - price level
    * subtype_index_names (`array of str`) - array of subtypes' index_name
    * image_url (`str`) - establishment image URL, e.g. http://google.com/cars.jpg
    * slug (`str`) - establishment slug (if null, then would be generated from index_name)
    * facebook (`str`) - facebook page
    * twitter (`str`) - twitter page
    * instagram (`str`) - instagram page
    * is_publish (`boolean`) - flag that responds for publication on site
    * guestonline_id (`str`) - identifier of a guestonline
    * lastable_id (`str`) - identifier of a lastable
    * status (`int`) - available statuses
    ```
    ABANDONED = 0
    CLOSED = 1
    PUBLISHED = 2
    WAITING = 4
    OUT_OF_SELECTION = 7
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """

    filter_class = filters.EstablishmentFilter
    serializer_class = serializers.EstablishmentListCreateSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_queryset(self):
        return super().get_queryset() \
            .with_extended_address_related() \
            .with_certain_tag_category_related('category', 'restaurant_category') \
            .with_certain_tag_category_related('cuisine', 'restaurant_cuisine') \
            .with_certain_tag_category_related('shop_category', 'artisan_category') \
            .with_certain_tag_category_related('distillery_type', 'distillery_type') \
            .with_certain_tag_category_related('producer_type', 'food_producer') \
            .with_reviews_sorted()


class EmployeeEstablishmentPositionsView(generics.ListAPIView):
    """
    ## Establishment employee positions filtered by employee identifier.
    ### *GET*
    #### Description
    Return paginated list of results from an intermediate table filtered by employee
    identifier, that contains connection between employee establishment,
    employee hiring dates, position, status `'I' (Idle)`, `'A' (Accepted)`, `'D' (Declined)`.
    ##### Response
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    """
    queryset = models.EstablishmentEmployee.objects.all().order_by('created')
    pagination_class = None
    serializer_class = serializers.EstablishmentEmployeePositionsSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        employee_pk = self.kwargs.get('pk')
        return super().get_queryset().filter(employee__id=employee_pk).all().prefetch_related(
            'establishment').select_related('position')


class EmployeeEstablishmentsListView(generics.ListAPIView):
    """
    ## Employee establishments filtered by employee identifier.
    ### *GET*
    #### Description
    Return paginated list of establishments filtered by employee identifier.
    ##### Response
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    """
    serializer_class = serializers.EstablishmentListCreateSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        employee = get_object_or_404(models.Employee, pk=pk)
        return employee.establishments.with_extended_related() \
            .with_certain_tag_category_related('category', 'restaurant_category') \
            .with_certain_tag_category_related('cuisine', 'restaurant_cuisine') \
            .with_certain_tag_category_related('shop_category', 'artisan_category') \
            .with_certain_tag_category_related('distillery_type', 'distillery_type') \
            .with_certain_tag_category_related('producer_type', 'food_producer') \
            .distinct('id')


class EmployeePositionsListView(generics.ListAPIView):
    """
    ## Paginated list of establishments filtered by employee identifier
    ### *GET*
    #### Description
    Return a paginated list of establishments of an employee by employee identifier.
    ##### Response
    ```
    {
        "count": 2,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
    }
    ```
    """
    queryset = models.Establishment.objects.all()
    serializer_class = serializers.EstablishmentPositionListSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        employee = get_object_or_404(models.Employee, pk=pk)
        return employee.establishments.with_extended_related()


class EstablishmentRUDView(EstablishmentLastUpdatedMixin,
                           EstablishmentMixinViews,
                           generics.RetrieveUpdateDestroyAPIView):
    """
    ## Establishment by a slug
    ### *GET*
    #### Description
    Return serialized object of an Establishment model by a slug.
    *If* establishment type is restaurant, only chef-employees were shown,
    for other types, owners and managers are being show.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an object by a slug.
    ##### Request
    * type_id (`int`) - identifier of EstablishmentType model
    * emails (`list`) - list of email addresses
    * phones (`list`) - list of phone numbers
    * must_of_the_week (`bool`) - flag that responds for showing on *Must of the week*
    * slug (`str`) - slug of establishments
    * transliterated_name (`str`) - transliterated name of establishments
    * name (`str`) - establishment name
    * index_name (`str`) - establishment index name
    * website (`str`) - establishment website
    * price_level (`int`) - price level for establishment
    * toque_number (`int`) - toques number of establishment
    * facebook (`str`) - social network link (*legacy*)
    * twitter (`str`) - social network link (*legacy*)
    * instagram (`str`) - social network link (*legacy*)
    * is_publish (`bool`) - flag that responds for showing on public
    * transportation (`str`) - establishment transportation
    * status (`int`) - enum
    ```
    ABANDONED = 0
    CLOSED = 1
    PUBLISHED = 2
    WAITING = 4
    OUT_OF_SELECTION = 7
    ```
    * last_update_by_manager (`datetime`) - datetime in ISO-8601 format
    * last_update_by_gm (`datetime`) - datetime in ISO-8601 format
    * public_mark (`int`) - public mark
    * update_by (`datetime`) - datetime in ISO-8601 format
    ###### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of establishment object by slug.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    lookup_field = 'slug'
    last_updated_mixin_methods = ('put', 'patch')
    serializer_class = serializers.EstablishmentRUDSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    update_es_document = True

    def update_response_with_last_change_datetime(self, response, last_update_by_manager, last_update_by_gm):
        if last_update_by_manager is not None:
            response.data['last_update_by_manager'] = DateTimeField().to_representation(last_update_by_manager)

        if last_update_by_gm is not None:
            response.data['last_update_by_gm'] = DateTimeField().to_representation(last_update_by_gm)

    def get_queryset(self):
        """An overridden get_queryset method."""
        qs = super(EstablishmentRUDView, self).get_queryset()
        return qs.prefetch_related(
            'establishmentemployee_set',
            'establishmentemployee_set__establishment',
        ).prefetch_inquiries_sorted()

    def get_object(self):
        establishment = super().get_object()
        is_restaurant = establishment.establishment_type.index_name == models.EstablishmentType.RESTAURANT
        employees_sidebar_qs = models.Employee.objects.all()
        if is_restaurant:
            # for restaurant show only chefs
            employees_sidebar_qs = employees_sidebar_qs.filter(establishmentemployee__position__index_name='chef')
        else:
            # for other types owners and managers are shown
            employees_sidebar_qs = employees_sidebar_qs. \
                filter(establishmentemployee__position__index_name__in=('owner', 'manager'))
        employees_sidebar_qs = employees_sidebar_qs.distinct('id')
        prefetch_related_objects([establishment],
                                 Prefetch('employees',
                                          queryset=employees_sidebar_qs, to_attr='sidebar_employees'))
        return establishment


class EstablishmentScheduleRUDView(EstablishmentLastUpdatedMixin,
                                   EstablishmentMixinViews,
                                   generics.RetrieveUpdateDestroyAPIView):
    """
    Establishment schedule Retrieve Update Delete view
    Work with the specified schedule for the Esitablishment.slug
    Available for roles Establishment-Manager, Establishment-Administrator

    ### *GET*
    Implement getting object of Establishment schedule.

    ### *PUT*
    Implement update Establishment schedule.

    ### *PATCH*
    Implement partial update Establishment schedule.

    ### *DELETE*
    Implement delete Establement schedule.

    """
    lookup_field = 'slug'
    serializer_class = ScheduleRUDSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_object(self):
        """
        Returns the object the view is displaying.
        """
        establishment_slug = self.kwargs['slug']
        schedule_id = self.kwargs['schedule_id']

        establishment = get_object_or_404(klass=super(EstablishmentScheduleRUDView, self).get_queryset(),
                                          slug=establishment_slug)
        schedule = get_object_or_404(klass=establishment.schedule,
                                     id=schedule_id)

        # May raise a permission denied
        self.check_object_permissions(self.request, establishment)
        self.check_object_permissions(self.request, schedule)

        return schedule


class EstablishmentScheduleListCreateView(EstablishmentLastUpdatedMixin,
                                          EstablishmentMixinViews,
                                          generics.ListCreateAPIView):
    """
    ## Create establishment schedule
    ### *POST*
    #### Description
    Create schedule for establishment by establishment `slug`.
    ##### Request
    ###### Required
    * weekday (`enum`)
    ```
    0 (Monday),
    1 (Tuesday),
    2 (Wednesday),
    3 (Thursday),
    4 (Friday),
    5 (Saturday),
    6 (Sunday)
    ```
    ###### Non-required
    * lunch_start (str) - time in a format (`ISO-8601`, e.g. - `hh:mm:ss`)
    * lunch_end (str) - time in a format (`ISO-8601`, e.g. - `hh:mm:ss`)
    * dinner_start (str) - time in a format (`ISO-8601`, e.g. - `hh:mm:ss`)
    * dinner_end (str) - time in a format (`ISO-8601`, e.g. - `hh:mm:ss`)
    * opening_at (str) - time in a format (`ISO-8601`, e.g. - `hh:mm:ss`)
    * closed_at (str) - time in a format (`ISO-8601`, e.g. - `hh:mm:ss`)
    ##### Response
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    lookup_field = 'slug'
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data['schedule'], status=status.HTTP_201_CREATED, headers=headers)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return ScheduleCreateSerializer

        return ScheduleRUDSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        # if no schedule for establishment, then send empty schedule
        if not queryset:
            result = []
            for weekday in map(lambda x: x[0], Timetable.WEEKDAYS_CHOICES):
                instance = Timetable(weekday=weekday)
                result.append(ScheduleRUDSerializer(instance=instance).data)

            return Response(result)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        establishment_slug = self.kwargs['slug']
        establishment = get_object_or_404(klass=super().get_queryset(),
                                          slug=establishment_slug)
        self.check_object_permissions(self.request, establishment)
        return Timetable.objects.filter(schedule=establishment)


class EstablishmentHolidaysCDLViewSet(EstablishmentLastUpdatedMixin,
                                      EstablishmentMixinViews,
                                      mixins.CreateModelMixin,
                                      mixins.DestroyModelMixin,
                                      mixins.ListModelMixin,
                                      viewsets.GenericViewSet):
    """ Establishment Holidays Create Destroy List ViewSet

    **GET**
    Getting all holidays for establishment

    **POST**
    Create new holiday for establishment

    **DELETE**
    Delete holiday for establishment
    """
    lookup_field = 'slug'
    pagination_class = None
    serializer_class = HolidaySerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_establishment_instance(self):
        establishment_qs = super().get_queryset()
        establishment_slug = self.kwargs['slug']
        establishment = get_object_or_404(klass=establishment_qs,
                                          slug=establishment_slug)

        self.check_object_permissions(self.request, establishment)
        return establishment

    def get_queryset(self):
        establishment = self.get_establishment_instance()
        return establishment.holidays.all()

    def get_object(self):
        """
        Returns the object the view is displaying.
        """
        establishment = self.get_establishment_instance()
        holiday_id = self.kwargs['holiday_id']
        holiday = get_object_or_404(klass=establishment.holidays,
                                    id=holiday_id)

        self.check_object_permissions(self.request, holiday)
        return holiday

    def perform_create(self, serializer):
        establishment = self.get_establishment_instance()
        serializer.save(establishment)


class CreateMenuForCertainEstablishmentView(EstablishmentLastUpdatedMixin,
                                            generics.CreateAPIView):
    """Creates formulas menu for certain establishment"""
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    # from rest_framework.permissions import AllowAny
    # permission_classes = (AllowAny, )
    queryset = models.Menu.objects.with_base_related().by_type(menu_type=models.Menu.FORMULAS)
    serializer_class = serializers.MenuBackOfficeSerializer

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data['establishment_id'] = kwargs['establishment_id']
        data['type'] = models.Menu.FORMULAS
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        with transaction.atomic():
            self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class MenuListCreateView(EstablishmentLastUpdatedMixin,
                         generics.ListCreateAPIView):
    """
    ## Menu list/create view. This view works only with menu formulas type.
    Available for roles Establishment-manager, Establishment-Administrator
    ### *GET*
    #### Description
    Return a paginated QuerySet of available menu for role.
    Available filters
    * establishment (`int`) - filter by a establishment identifier
    * establishment__slug (`str`) - filter by an establishment slug
    ##### Response
    E.g.
    ```
    {
      "count": 1534,
      "next": 2,
      "previous": null,
      "results": [
        {
          "id": 11,
          ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Create a new Menu.
    ##### Request
    Required
    * name (`string`) - Name
    * establishment_id (`integer`) - Establishment id
    * drinks_included ('boolean') - Drinks included
    * uploads_ids ('integer list') - Menu files

    Available
    * type (`str`) - MENU_CHOICES
    * name (`str`) - name
    * establishment_id (`integer`) - Establishment id
    * price (`number`) - Price
    * drinks_included (`boolean`) - Drinks included
    * diner (`list of boolean`) - Diner
    * lunch (`list of boolean`) - Lunch
    * uploads_ids ('integer list') - Menu files
    ```
    FORMULAS = 'formulas'
    STARTER = 'starter'
    DESSERT = 'dessert'
    MAIN_COURSE = 'main_course'

    MENU_CHOICES = (
        (FORMULAS, _('formulas')),
        (STARTER, _('starter')),
        (DESSERT, _('dessert')),
        (MAIN_COURSE, _('main_course')),
    )
    ```
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.MenuBackOfficeSerializer
    queryset = models.Menu.objects.with_base_related().by_type(menu_type=models.Menu.FORMULAS)
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    filter_backends = (DjangoFilterBackend,)
    filterset_fields = (
        'establishment',
        'establishment__slug',
    )

    def get_establishment_search_kwargs(self):
        return {'pk': self.request.data.get('establishment_id')}

    def create(self, request, *args, **kwargs):
        response_data = []
        with transaction.atomic():
            establishment = get_object_or_404(models.Establishment, pk=next(iter(request.data))['establishment_id'])
            update_ids = [menu['id'] for menu in request.data if 'id' in menu]
            for menu_instance in establishment.menu_set.by_type(models.Menu.FORMULAS):
                if menu_instance.pk not in update_ids:
                    menu_instance.delete()
            for menu in request.data:
                menu.update({'type': models.Menu.FORMULAS})
                if 'id' in menu:

                    instance = get_object_or_404(Menu, pk=menu['id'])
                    serializer = self.get_serializer(instance, data=menu)
                else:

                    serializer = self.get_serializer(data=menu)

                serializer.is_valid(raise_exception=True)
                serializer.save()
                response_data.append(serializer.data)

        return Response(response_data, status=status.HTTP_201_CREATED)


class MenuRUDView(EstablishmentLastUpdatedMixin,
                  generics.UpdateAPIView,
                  generics.DestroyAPIView):
    """
    ## Menu RUD by id
    Implements update, delete methods
    Available for roles Establishment-manager, Establishment-Administrator
    ### *PUT*
    MenuRUDView PUT method
    Update of Menu by id
    ### *PATCH*
    MenuRUDView PATCH method
    Partial update of Menu by id
    ### *DELETE*
    MenuRUDView DELETE method
    Delete Menu by id
    """
    serializer_class = serializers.MenuBackOfficeSerializer
    queryset = models.Menu.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_establishment_for_last_updated_mixin(self):
        menu_instance = self.get_object()
        return menu_instance.establishment

    def get_object(self):
        return get_object_or_404(klass=models.Menu, pk=self.kwargs['menu_id'])

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = request.data.copy()
        data['establishment_id'] = instance.establishment.pk
        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        with transaction.atomic():
            self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class MenuUploadsRUDView(EstablishmentLastUpdatedMixin, generics.RetrieveDestroyAPIView):
    """
    ## MenuFiles RUD by id
    Implements update, delete methods
    Available for roles Establishment-manager, Establishment-Administrator
    ### *GET*
    #### Description
    Return MenuFiles object by id.
    ##### Response
    E.g
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    MenuUploadsRUDView DELETE method
    Delete MenuFiles by id
    """
    serializer_class = serializers.MenuFilesSerializers
    queryset = models.MenuFiles.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_establishment_for_last_updated_mixin(self):
        menu_file_instance = self.get_object()
        menu_instance = menu_file_instance.menu_set.first()
        return menu_instance.establishment


class MenuUploadsCreateView(generics.CreateAPIView):
    """
    ## MenuFiles create view.
    Available for roles Establishment-manager, Establishment-Administrator
    Available extension 'doc', 'docx', 'pdf'
    ### *POST*
    #### Description
    Create new plate
    ##### Request
    Available
    * name (`str`) - name
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.MenuFilesSerializers
    queryset = models.MenuFiles.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class CardAndWinesListView(generics.RetrieveAPIView):
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    serializer_class = serializers.CardAndWinesSerializer
    queryset = models.Establishment.objects.with_base_related()

    def get_object(self):
        establishment = models.Establishment.objects.prefetch_plates() \
            .filter(pk=self.kwargs['establishment_id']) \
            .first()
        if establishment is None:
            raise Http404
        plates = []
        for menu in establishment.menu_set.prefetch_related('plates').exclude(type=Menu.FORMULAS):
            plates += list(menu.plates.all().prefetch_related('menu'))
        setattr(establishment, 'card_and_wine_plates', plates)
        return establishment


class EstablishmentWineView(EstablishmentLastUpdatedMixin, generics.CreateAPIView):
    """Create and update establishment wine"""
    serializer_class = serializers.EstablishmentBackOfficeWineSerializer
    queryset = models.EstablishmentBackOfficeWine.objects.all()
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data.update({'establishment_id': self.kwargs['establishment_id']})
        if 'id' in data:
            """Update"""
            instance = get_object_or_404(models.EstablishmentBackOfficeWine, pk=data['id'])
            serializer = self.get_serializer(instance, data=data)
        else:
            """create"""
            serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class DishCreateView(EstablishmentLastUpdatedMixin, EstablishmentMixinViews, generics.CreateAPIView):
    """View for creating and binding dish to establishment"""
    serializer_class = serializers.CardAndWinesPlatesSerializer
    queryset = models.Plate.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'


class DishUpdateDeleteView(EstablishmentLastUpdatedMixin, generics.UpdateAPIView, generics.DestroyAPIView):
    serializer_class = serializers.CardAndWinesPlatesSerializer
    queryset = models.Plate.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_establishment_for_last_updated_mixin(self):
        return getattr(self, 'cached_establishment')

    def get_object(self):
        ret = get_object_or_404(models.Plate, pk=self.kwargs['dish_id'])
        establishment_instance = models.Establishment.objects.filter(pk=ret.establishment_id).first()
        setattr(self, 'cached_establishment', establishment_instance)
        return ret

    def update(self, request, *args, **kwargs):
        with transaction.atomic():
            return super().update(request, *args, **kwargs)


class SocialChoiceListCreateView(generics.ListCreateAPIView):
    """
    ## SocialChoice list create view.
    ### *GET*
    #### Description
    Return non-paginated list of available social networks that can be used.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *POST*
    #### Description
    Add new social network to available.
    ##### Request
    * title (`str`) - title of the social network
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.SocialChoiceSerializers
    queryset = models.SocialChoice.objects.all()
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class SocialChoiceRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## SocialChoice RUD view.
    ### *GET*
    #### Description
    Return serialized object of social network by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a social network object by an identifier.
    ##### Request
    * title (`str`) - title of social network
    ###### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of social network object by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.SocialChoiceSerializers
    queryset = models.SocialChoice.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class SocialListCreateView(generics.ListCreateAPIView):
    """
    ## Social list create view.
    ### *GET*
    #### Description
    Return non-paginated list of an establishment social networks.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *POST*
    #### Description
    Add new social network to establishment.
    ##### Request
    * establishment (`int`) - establishment identifier
    * network (`int`) - network identifier
    * url (`str`) - network URL address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.SocialNetworkSerializers
    queryset = models.SocialNetwork.objects.all()
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class SocialRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Social RUD view.
    ### *GET*
    #### Description
    Return serialized object of establishment social network by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an establishment social network object by an identifier.
    ##### Request
    * establishment (`int`) - establishment identifier
    * network (`int`) - network identifier
    * url (`str`) - network URL address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of establishment social network object by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.SocialNetworkSerializers
    queryset = models.SocialNetwork.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class PlateListCreateView(generics.ListCreateAPIView):
    """
    ## Plate list create view.
    ### *GET*
    #### Description
    Return non-paginated list of plates
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    ### *POST*
    #### Description
    Create new plate
    ##### Request
    Required
    * menu (`int`) - identifier of menu
    * currency_id (`int`) - identifier of currency
    Available
    * name (`str`) - plate name
    * price (`decimal`) - price of plate (max digits = 14, decimal places = 2)
    * is_signature_plate (`bool`) - is this plate is signature
    * currency_code (`str`) - currency code of plate
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.PlatesSerializers
    queryset = models.Plate.objects.all()
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )


class PlateRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Plate RUD view.
    ### *GET*
    #### Description
    Retrieve an object by an identifier of plate
    ##### Request
    Required
    * menu (`int`) - identifier of menu
    * currency_id (`int`) - identifier of currency
    Available
    * name (`str`) - plate name
    * price (`decimal`) - price of plate (max digits = 14, decimal places = 2)
    * is_signature_plate (`bool`) - is this plate is signature
    * currency_code (`str`) - currency code of plate
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an object by an identifier of plate
    ##### Request
    Available
    * menu (`int`) - identifier of menu
    * currency_id (`int`) - identifier of currency
    * name (`str`) - plate name
    * price (`decimal`) - price of plate (max digits = 14, decimal places = 2)
    * is_signature_plate (`bool`) - is this plate is signature
    * currency_code (`str`) - currency code of plate
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an object by an identifier of plate
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.PlatesSerializers
    queryset = models.Plate.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )


class PhonesListCreateView(generics.ListCreateAPIView):
    """
    ## Phone list create view.
    ### *GET*
    #### Description
    Return non-paginated list of contact phone numbers of establishments.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *POST*
    #### Description
    Create a new contact phone number for an establishment.
    ##### Request
    * establishment (`int`) - establishment identifier
    * phone (`str`) - phone number
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.ContactPhoneBackSerializers
    queryset = models.ContactPhone.objects.all()
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )


class PhonesRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Contact phone number RUD view.
    ### *GET*
    #### Description
    Retrieve serialized object of a contact phone numbers by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a contact phone number by an identifier.
    ##### Request
    * establishment (`int`) - establishment identifier
    * phone (`str`) - phone number
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of contact phone number by an identifier.
    ##### Request
    ```
    No body
    ```
    ###### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.ContactPhoneBackSerializers
    queryset = models.ContactPhone.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )


class EmailListCreateView(generics.ListCreateAPIView):
    """
    ## Email list create view.
    ### *GET*
    #### Description
    Return non-paginated list of contact email addresses of establishments.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *POST*
    #### Description
    Create a new contact email for an establishment.
    ##### Request
    * establishment (`int`) - establishment identifier
    * email (`str`) - email address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.ContactEmailBackSerializers
    queryset = models.ContactEmail.objects.all()
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )


class EmailRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Email RUD view.
    ### *GET*
    #### Description
    Retrieve serialized object of a contact email by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a contact email by an identifier.
    ##### Request
    * establishment (`int`) - establishment identifier
    * email (`str`) - email address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of contact email by an identifier.
    ##### Request
    ```
    No body
    ```
    ###### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.ContactEmailBackSerializers
    queryset = models.ContactEmail.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )


class EmployeeListCreateView(generics.ListCreateAPIView):
    """
    ## Employee list/create view.
    ### *GET*
    #### Description
    Return paginated list of employees with available filters:
    * search (`str`) - filter by name or last name with mistakes
    * position_id (`int`) - filter by employees position identifier
    * public_mark (`str`) - filter by establishment public mark
    * toque_number (`str`) - filter by establishment toque number
    * username (`str`) - filter by a username or name

    #### Response
    ```
    {
        "count": 12765,
        "next": 2,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
    {
    ```

    ### *POST*
    #### Description
    Create a new employee.

    #### Response
    ```
    {
        "id": 1,
        ...
    {
    ```

    #### Request
    Required fields:
    * name (`str`) - employee name

    Non-required fields:
    * name (`str`) - employee name
    * last_name (`str`) - employee last name
    * user (`int`) - user identifier
    * sex (`int`) - enum: `0 (Male), 1 (Female)`
    * birth_date (`str`) - birth datetime (datetime in a format `ISO-8601`)
    * email (`str`) - email address
    * phone (`str`) - phone number in a format `E164`
    * photo_id (`int`) - photo identifier
    * available_for_events (bool) - flag that responds for availability for events
    """
    filter_class = filters.EmployeeBackFilter
    serializer_class = serializers.EmployeeBackSerializers
    queryset = models.Employee.objects.all().distinct().with_back_office_related()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.country_code:
            qs = qs.filter(establishments__address__city__country__code=self.request.country_code)
        return qs


class EmployeesListSearchViews(generics.ListAPIView):
    """
    ## Employee search view.
    ### *GET*
    ##### Description
    Return a non-paginated list of employees.
    Available filters:
    * search (`str`) - filter by name or last name with mistakes
    * position_id (`int`) - filter by employees position identifier
    * public_mark (`str`) - filter by establishment public mark
    * toque_number (`str`) - filter by establishment toque number
    * username (`str`) - filter by a username or name
    (with limitations by the minimum number of characters)

    ###### Response
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    """
    pagination_class = None
    filter_class = filters.EmployeeBackSearchFilter
    serializer_class = serializers.EmployeeBackSerializers
    queryset = (
        models.Employee.objects.with_back_office_related()
            .select_related('photo')
    )
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class EstablishmentEmployeeListView(EstablishmentLastUpdatedMixin, generics.ListCreateAPIView):
    """
    ## Establishment employees List/Create view.
    ### *GET*
    #### Description
    Returning non-paginated list of employees by establishment identifier.
    ##### Response
    E.g.:
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```

    ### *POST*
    #### Description
    Create a new instance of employee for establishment by establishment identifier.
    ##### Request
    ###### Required
    * name (`str`) - employee name

    ###### Additional
    * last_name (`str`) - employee last name
    * user (`int`) - user identifier
    * sex (`int`) - enum: `0 (Male), 1 (Female)`
    * birth_date (`str`) - birth datetime (datetime in a format `ISO-8601`)
    * email (`str`) - email address
    * phone (`str`) - phone number in a format `E164`
    * available_for_events (bool) - flag that responds for availability for events
    * photo_id (`int`) - photo identifier

    #### Response
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.EstEmployeeBackSerializer
    pagination_class = None
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'

    def get_queryset(self):
        establishment_id = self.kwargs['establishment_id']
        establishment = get_object_or_404(models.Establishment, pk=establishment_id)
        self.kwargs['establishment_instance'] = establishment
        self.kwargs['establishment_instance_subtypes'] = list(establishment.establishment_subtypes.all())
        self.kwargs['establishment_instance_type'] = establishment.establishment_type
        return models.Employee.objects.filter(
            establishmentemployee__establishment_id=establishment_id,
        ).distinct()


class EmployeeRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Employee Retrieve/Update/Destroy view
    ### *GET*
    #### Description
    Retrieve a serialized object of employee.
    ##### Response
    ```
    {
      "id": 1,
      ...
    }
    ```

    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an employee object.
    ##### Request
    Available fields:
    * name (`str`) - employee name
    * last_name (`str`) - employee last name
    * sex (`enum`) - 0 (Male), 1 (Female)
    * birth_date (`str`) - datetime in a format `ISO-8601`
    * email (`str`) - employee email address
    * phone (`str`) - phone number in E164 format
    * toque_number (`int`) - employee toque number
    * available_for_events (`bool`) - flag that responds for availability for events
    * photo_id (`int`) - image identifier
    ##### Response
    Return an employee serialized object
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```

    ### *DELETE*
    #### Description
    Delete an instance of employee
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.EmployeeDetailBackSerializers
    queryset = models.Employee.objects.with_back_office_related()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator,
    )


class AdminEmployeeListView(generics.ListAPIView):
    """
    ## Employee list view, where request user is ESTABLISHMENT_ADMINISTRATOR.
    ### *GET*
    #### Description
    Return list of employees.

    #### Response
    ```
    {
        "id": 1324,
        "name": "Alex",
        "last_name": "Wolf",
    {
    ```

    """
    serializer_class = serializers.AdminEmployeeBackSerializers
    permission_classes = get_permission_classes(IsEstablishmentAdministrator, )
    pagination_class = None

    def get_queryset(self):
        user = self.request.user
        if user.is_anonymous:
            return None
        est_ids = models.Establishment.objects.filter(
            userrole__user=user,
            userrole__role__role=Role.ESTABLISHMENT_ADMINISTRATOR,
        ).values_list('id', flat=True)

        qs = models.Employee.objects.filter(
            establishments__in=est_ids).search_by_actual_employee().distinct().with_back_office_related()

        if self.request.country_code:
            qs = qs.filter(establishments__address__city__country__code=self.request.country_code)
        return qs


class AdminEstablishmentListView(generics.ListAPIView):
    """
    ## Establishment list view, where request user is ESTABLISHMENT_ADMINISTRATOR.
    ### *GET*
    #### Description
    Return list of establishments.

    #### Response
    ```
    {
        "id": 1324,
        "name": "Name",
        "slug": "slug",
    {
    ```

    """
    serializer_class = serializers.AdminEstablishmentBackSerializers
    permission_classes = get_permission_classes(IsEstablishmentAdministrator, )
    pagination_class = None

    def get_queryset(self):
        user = self.request.user
        if user.is_anonymous:
            return None
        qs = models.Establishment.objects.filter(
            userrole__user=user,
            userrole__role__role=Role.ESTABLISHMENT_ADMINISTRATOR,
        )

        if self.request.country_code:
            qs = qs.filter(address__city__country__code=self.request.country_code)
        return qs


class RemoveAwardView(generics.DestroyAPIView):
    """
    ## Remove award view.
    ### *DELETE*
    #### Description
    Remove an award from an employee by an employee identifier and an award identifier.
    ##### Response
    ```
    No content
    ```
    """
    lookup_field = 'pk'
    serializer_class = serializers.EmployeeBackSerializers
    queryset = models.Employee.objects.with_back_office_related()
    permission_classes = get_permission_classes()

    def get_object(self):
        employee = super().get_object()
        employee.remove_award(self.kwargs['award_id'])
        return employee

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        return Response(status=status.HTTP_204_NO_CONTENT)


class EstablishmentTypeListCreateView(generics.ListCreateAPIView):
    """
    ## Establishment type list/create view.
    ### *GET*
    #### Description
    Non-paginated list of establishment types.
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    ### *POST*
    #### Description
    Create new establishment type
    ##### Request
    ###### Required
    * index_name (`str`) - index name of establishment type

    ###### Available
    * name (`JSON`) - translations of establishment type
    * use_subtypes (`bool`) - flag that responds for an availability use establishment type
    with subtypes
    * default_image (`file`) - default image file
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.EstablishmentTypeBaseSerializer
    queryset = models.EstablishmentType.objects.select_related('default_image')
    pagination_class = None
    permission_classes = get_permission_classes()


class EstablishmentTypeRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Establishment type retrieve/update/destroy view.
    ### *GET*
    #### Description
    Return serialized object of an establishment type by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an establishment type object by an identifier.
    ##### Request
    * index_name (`str`) - index name of establishment type
    * name (`JSON`) - translations of establishment type
    * use_subtypes (`bool`) - flag that responds for an availability use establishment type
    with subtypes
    * default_image (`file`) - default image file
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of establishment type by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.EstablishmentTypeBaseSerializer
    queryset = models.EstablishmentType.objects.select_related('default_image')
    permission_classes = get_permission_classes()


class EstablishmentSubtypeListCreateView(generics.ListCreateAPIView):
    """
    ## Establishment subtype list/create view.
    ### *GET*
    #### Description
    Non-paginated list of establishment subtypes.
    ##### Response
    E.g.
    ```
    [
        {
            "id": 1,
            ...
        }
    ]
    ```
    ### *POST*
    #### Description
    Create new establishment subtype
    ##### Request
    ###### Required
    * index_name (`str`) - index name of establishment subtype
    * establishment_type (`int`) - identifier of establishment type

    ###### Available
    * name (`JSON`) - translations of establishment subtype
    * use_subtypes (`bool`) - flag that responds for an availability use establishment subtype
    with subtypes
    * default_image (`file`) - default image file
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.EstablishmentSubTypeBaseSerializer
    queryset = models.EstablishmentSubType.objects.select_related('default_image')
    pagination_class = None
    permission_classes = get_permission_classes()


class EstablishmentSubtypeRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    ## Establishment subtype retrieve/update/destroy view.
    ### *GET*
    #### Description
    Return serialized object of an establishment subtype by an identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update an establishment subtype object by an identifier.
    ##### Request
    * index_name (`str`) - index name of establishment subtype
    * name (`JSON`) - translations of establishment subtype
    * use_subtypes (`bool`) - flag that responds for an availability use establishment subtype
    with subtypes
    * default_image (`file`) - default image file
    * establishment_type (`int`) - identifier of establishment type
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of establishment subtype by an identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    serializer_class = serializers.EstablishmentSubTypeBaseSerializer
    queryset = models.EstablishmentSubType.objects.select_related('default_image')
    permission_classes = get_permission_classes()


class EstablishmentGalleryCreateDestroyView(EstablishmentMixinViews,
                                            CreateDestroyGalleryViewMixin):
    """
    ## Establishment gallery image Create/Destroy view
    ### *POST*
    #### Description
    Attaching existing **image** by `image identifier` to **establishment** by `establishment slug`
    in request kwargs.
    ##### Request
    ```
    No body
    ```
    ##### Response
    E.g.:
    ```
    No content
    ```

    ### *DELETE*
    #### Description
    Delete existing **gallery image** from **establishment** gallery, by `image identifier`
    and `establishment slug` in request kwargs.

    **Note**:
    > Image wouldn't be deleted after all.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """
    lookup_field = 'slug'
    serializer_class = serializers.EstablishmentBackOfficeGallerySerializer
    permission_classes = get_permission_classes()

    def get_object(self):
        """
        Returns the object the view is displaying.
        """
        establishment_qs = self.filter_queryset(self.get_queryset())

        establishment = get_object_or_404(establishment_qs, slug=self.kwargs.get('slug'))
        gallery = get_object_or_404(establishment.establishment_gallery,
                                    image_id=self.kwargs.get('image_id'))

        # May raise a permission denied
        self.check_object_permissions(self.request, gallery)

        return gallery


class EstablishmentGalleryListView(EstablishmentMixinViews,
                                   generics.ListAPIView):
    """
    ## Establishment gallery image list view
    ### *GET*
    #### Description
    Returning paginated list of establishment images by `establishment slug`,
    with cropped images.
    ##### Response
    E.g.:
    ```
    {
      "count": 1,
      "next": null,
      "previous": null,
      "results": [
        {
          "id": 11,
          ...
        }
      ]
    }
    ```
    """
    lookup_field = 'slug'
    serializer_class = serializers.ImageBaseSerializer
    permission_classes = get_permission_classes()

    def get_object(self):
        """Override get_object method."""
        qs = super(EstablishmentGalleryListView, self).get_queryset()
        establishment = get_object_or_404(qs, slug=self.kwargs.get('slug'))

        # May raises a permission denied
        self.check_object_permissions(self.request, establishment)

        return establishment

    def get_queryset(self):
        """Override get_queryset method."""
        return self.get_object().crop_gallery


class EstablishmentCompanyListCreateView(EstablishmentMixinViews,
                                         generics.ListCreateAPIView):
    """
    ## List|Create establishment company view.
    ### *GET*
    #### Description
    Return paginated queryset of establishment companies by establishment slug.
    ##### Response
    E.g.:
    ```
    {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                ...
            }
        ]
    }
    ```
    ### *POST*
    #### Description
    Create a new company to establishment by establishment slug.
    ##### Request
    Required
    * name (`str`) - company name
    Available
    * phones (`list`) - list of phone numbers, e.g. ["+79000000000"]
    * faxes (`list`) - list of fax numbers, e.g. ["+79000000000"]
    * legal_entity (`str`) - legal entity
    * registry_number (`str`) - registry number
    * vat_number (`str`) - VAT identification number
    * sic_code (`int`) - SIC code
    * address (`int`) - identifier of address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """

    lookup_field = 'slug'
    serializer_class = serializers.EstablishmentCompanyListCreateSerializer
    permission_classes = get_permission_classes()

    def get_object(self):
        """Returns the object the view is displaying."""
        establishment_qs = super(EstablishmentCompanyListCreateView, self).get_queryset()
        filtered_qs = self.filter_queryset(establishment_qs)

        establishment = get_object_or_404(filtered_qs, slug=self.kwargs.get('slug'))

        # May raise a permission denied
        self.check_object_permissions(self.request, establishment)

        return establishment

    def get_queryset(self):
        """An overridden get_queryset method."""
        return self.get_object().companies.all()


class EstablishmentCompanyRUDView(EstablishmentMixinViews,
                                  generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve|Update|Destroy establishment company view.
    ### *GET*
    #### Description
    Retrieve a company detail information by establishment slug and company identifier.
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a company object by an establishment slug and company identifier.
    ##### Request
    Available
    * phones (`list`) - list of phone numbers, e.g. ["+79000000000"]
    * faxes (`list`) - list of fax numbers, e.g. ["+79000000000"]
    * legal_entity (`str`) - legal entity
    * registry_number (`str`) - registry number
    * vat_number (`str`) - VAT identification number
    * sic_code (`int`) - SIC code
    * address (`int`) - identifier of address
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete an instance of company object by establishment slug an company identifier.
    ##### Request
    ```
    No body
    ```
    ##### Response
    ```
    No content
    ```
    """

    lookup_field = 'slug'
    serializer_class = serializers.CompanyBaseSerializer
    permission_classes = get_permission_classes()

    def get_object(self):
        """Returns the object the view is displaying."""
        establishment_qs = super(EstablishmentCompanyRUDView, self).get_queryset()
        filtered_ad_qs = self.filter_queryset(establishment_qs)

        establishment = get_object_or_404(filtered_ad_qs, slug=self.kwargs.get('slug'))
        company = get_object_or_404(establishment.companies.all(), pk=self.kwargs.get('company_pk'))

        # May raise a permission denied
        self.check_object_permissions(self.request, company)

        return company


class EstablishmentNoteListCreateView(EstablishmentMixinViews,
                                      generics.ListCreateAPIView):
    """
    ## List/Create view
    ### *GET*
    #### Description
    Return paginated list of establishment notes by `establishment slug`.
    Establishments would filter by country code.
    ##### Response
    E.g.:
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Create new note for establishment by `establishment slug`
    ##### Request
    Required
    * text (`str`) - text of note
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    """

    lookup_field = 'slug'
    serializer_class = serializers.EstablishmentNoteListCreateSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )

    def get_object(self):
        """Returns the object the view is displaying."""
        establishment_qs = super(EstablishmentNoteListCreateView, self).get_queryset()
        filtered_establishment_qs = self.filter_queryset(establishment_qs)

        establishment = get_object_or_404(filtered_establishment_qs, slug=self.kwargs.get('slug'))

        # May raise a permission denied
        self.check_object_permissions(self.request, establishment)

        return establishment

    def get_queryset(self):
        """An overridden get_queryset method."""
        return self.get_object().notes.all()


class EstablishmentNoteRUDView(EstablishmentMixinViews,
                               generics.RetrieveUpdateDestroyAPIView):
    """
    ## Retrieve/Update/Destroy view
    ### *GET*
    #### Description
    Return paginated list of establishment notes by `establishment slug`.
    Establishments would filter by country code.
    ##### Response
    E.g.:
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
    ```
    ### *PUT*/*PATCH*
    #### Description
    Completely/Partially update a note object by an identifier.
    ##### Request
    Available
    * text (`str`) - text of note
    ##### Response
    E.g.:
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete establishment note by `establishment slug`
    ##### Response
    E.g.:
    ```
    No content
    ```
    """

    lookup_field = 'slug'
    serializer_class = serializers.EstablishmentNoteBaseSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentAdministrator,
        IsEstablishmentManager,
    )

    def get_object(self):
        """Returns the object the view is displaying."""
        establishment_qs = super(EstablishmentNoteRUDView, self).get_queryset()
        filtered_establishment_qs = self.filter_queryset(establishment_qs)

        establishment = get_object_or_404(filtered_establishment_qs, slug=self.kwargs.get('slug'))
        note = get_object_or_404(establishment.notes.all(), pk=self.kwargs['note_pk'])

        # May a raise a permission denied
        self.check_object_permissions(self.request, note)

        return note


class EstablishmentEmployeeCreateView(EstablishmentLastUpdatedMixin, generics.CreateAPIView):
    """
    ## Create employee position for establishment
    ### *POST*
    #### Description
    Creating position for an employee for establishment,
    by `establishment identifier`, `employee identifier` and
    `position identifier`.

    ##### Request data
    Available fields:
    * from_date - datetime (datetime in a format `ISO-8601`), by default `timezone.now()`
    * to_date - datetime (datetime in a format `ISO-8601`), by default `null`

    ##### Response data
    E.g.:
    ```
    {
      "id": 47405,
      "from_date": "2020-02-06T11:01:04.961000Z",
      "to_date": "2020-02-06T11:01:04.961000Z"
    }
    ```
    """
    serializer_class = serializers.EstablishmentEmployeeCreateSerializer
    queryset = models.EstablishmentEmployee.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'


class EstablishmentEmployeeDeleteView(EstablishmentLastUpdatedMixin, generics.DestroyAPIView):
    """
    ## Delete employee position for establishment
    ### *DELETE*
    #### Description
    Deleting position for an employee from establishment, by `position identifier`.


    ##### Response data
    ```
    If no employee was found by `position identifier` - HTTP_404_NOT_FOUND,
    If employee had status `ACCEPTED` - HTTP_200_OK,
    Else - HTTP_204_NO_CONTENT
    ```
    """
    queryset = EstablishmentEmployee.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_establishment_for_last_updated_mixin(self):
        return self.get_object().establishment

    def delete(self, request, *args, **kwargs):
        employee = get_object_or_404(models.EstablishmentEmployee, id=kwargs['pk'])

        if employee.status == employee.ACCEPTED:
            employee.to_date = timezone.now()
            employee.save()
            return Response(status=status.HTTP_200_OK)
        else:
            self.perform_destroy(employee)
            return Response(status=status.HTTP_204_NO_CONTENT)


class EstablishmentPositionListView(generics.ListAPIView):
    """Positions in establishment  - list view.
      ###  *GET*
      #### Description
        Returning positions list of establishment.
        Available for roles EstablishmentManager,  EstablishmentAdministrator
        Order by Position.priority
      ##### Response
        ```
        {
          "count": 58,
          "next": 2,
          "previous": null,
          "results": [
            {
                "id": 1,
                ...
            }
          ]
        }
        ```
    """

    pagination_class = None
    queryset = models.Position.objects.all()
    serializer_class = serializers.PositionBackSerializer
    filter_class = filters.PositionsByEstablishmentFilter
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        return super().get_queryset().order_by('priority')


class EstablishmentAdminView(generics.ListAPIView):
    """
    ## List establishment admins
    ### *GET*
    #### Description
    Returning paginated list of establishment administrators by establishment slug.
    ##### Response
    ```
    {
      "count": 58,
      "next": 2,
      "previous": null,
      "results": [
        {
            "id": 1,
            ...
        }
      ]
    }
```    """
    serializer_class = serializers.EstablishmentAdminListSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        establishment = get_object_or_404(
            models.Establishment, slug=self.kwargs['slug'])
        return User.objects.establishment_admin(establishment).distinct()


class EstablishmentGuideCreateDestroyView(generics.GenericAPIView):
    """Add/Remove establishment from guide."""
    establishment_lookup_url_kwarg = 'slug'
    guide_lookup_url_kwarg = 'guide_id'
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_establishment_queryset(self):
        """Get Establishment queryset."""
        return EstablishmentMixinViews.get_queryset(self)

    def get_guide_queryset(self):
        """Get Guide queryset."""
        queryset = Guide.objects
        if hasattr(self, 'request') and hasattr(self.request, 'country_code'):
            return queryset.by_country_code(self.request.country_code)
        return queryset.none()

    def get_establishment(self):
        queryset = self.get_establishment_queryset()

        # Perform the lookup filtering.
        lookup_url_kwarg = getattr(self, 'establishment_lookup_url_kwarg', None)

        assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
        )

        filters = {'klass': queryset, lookup_url_kwarg: self.kwargs.get(lookup_url_kwarg)}
        obj = get_object_or_404(**filters)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_guide(self):
        queryset = self.get_guide_queryset()

        # Perform the lookup filtering.
        lookup_url_kwarg = getattr(self, 'guide_lookup_url_kwarg', None)

        assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
        )

        obj = get_object_or_404(klass=queryset, id=self.kwargs.get(lookup_url_kwarg))

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def post(self, request, *args, **kwargs):
        """Implement GET-method."""
        establishment = self.get_establishment()
        guide = self.get_guide()
        guide.extend_establishment_guide(establishment.id)
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        """Implement DELETE-method."""
        establishment = self.get_establishment()
        guide = self.get_guide()
        guide.remove_establishment(establishment.id)
        return Response(status=status.HTTP_204_NO_CONTENT)


class MenuDishesListCreateView(generics.ListCreateAPIView):
    """
    ## Menu (dessert, main_course, starter) list/create view.
    Available for roles Establishment-manager, Establishment-Administrator
    ### *GET*
    #### Description
    Return a paginated QuerySet of available menu for role.
    Available filters
    * category (`str`) - filter by a category
    * is_drinks_included (`bool`) - filter by is_drinks_included field
    * establishment_id (`int`) - filter by an establishment id
    ##### Response
    E.g.
    ```
    {
      "count": 1534,
      "next": 2,
      "previous": null,
      "results": [
        {
          "id": 11,
          ...
        }
      ]
    }
    ```
    ### *POST*
    #### Description
    Create a new Menu.
    ##### Request
    Required
    * establishment (`integer`) - Establishment id

    Available
    * category (`str`) - category like {"en-GB":"some text"}
    * is_drinks_included (`boolean`) - Drinks included
    * schedule (`list of integer`) - schedules
    ##### Response
    E.g.
    ```
    {
        "id": 1,
        ...
    }
    ```
    """
    serializer_class = serializers.MenuDishesSerializer
    queryset = models.Menu.objects.with_schedule_plates_establishment().dishes().distinct()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )
    filter_class = filters.MenuDishesBackFilter


class MenuDishesRUDView(generics.RetrieveUpdateDestroyAPIView):
    """Menu (dessert, main_course, starter) RUD view."""
    serializer_class = serializers.MenuDishesRUDSerializers
    queryset = models.Menu.objects.dishes().distinct()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )


class MenuGalleryListView(generics.ListAPIView):
    """Resource for returning gallery for menu for back-office users."""
    serializer_class = serializers.ImageBaseSerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )
    queryset = models.Menu.objects.with_schedule_plates_establishment().with_gallery().dishes()

    def get_object(self):
        """Override get_object method."""
        qs = super(MenuGalleryListView, self).get_queryset()
        menu = get_object_or_404(qs, pk=self.kwargs.get('pk'))

        # May raise a permission denied
        # self.check_object_permissions(self.request, menu)

        return menu

    def get_queryset(self):
        """Override get_queryset method."""
        return self.get_object().crop_gallery


class MenuGalleryCreateDestroyView(CreateDestroyGalleryViewMixin):
    """Resource for a create gallery for menu for back-office users."""
    serializer_class = serializers.MenuGallerySerializer
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        """Override get_queryset method."""
        qs = models.Menu.objects.with_schedule_plates_establishment().with_gallery().dishes()
        return qs

    def create(self, request, *args, **kwargs):
        _ = super().create(request, *args, **kwargs)
        news_qs = self.filter_queryset(self.get_queryset())
        return response.Response(
            data=serializers.MenuDishesRUDSerializers(get_object_or_404(news_qs, pk=kwargs.get('pk'))).data
        )

    def get_object(self):
        """
        Returns the object the view is displaying.
        """
        menu_qs = self.filter_queryset(self.get_queryset())

        menu = get_object_or_404(menu_qs, pk=self.kwargs.get('pk'))
        gallery = get_object_or_404(menu.menu_gallery, image_id=self.kwargs.get('image_id'))

        # May raise a permission denied
        self.check_object_permissions(self.request, gallery)

        return gallery


class StatusesListView(ChoseToListMixin, generics.ListAPIView):
    """Possible project establishment statuses"""
    chose_field = models.Establishment.STATUS_CHOICES


class TeamMemberListView(generics.ListAPIView):
    """Show team for certain establishment"""
    pagination_class = None
    serializer_class = serializers.TeamMemberSerializer
    queryset = User.objects.all()
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_queryset(self):
        establishment = get_object_or_404(klass=models.Establishment, pk=self.kwargs['establishment_id'])
        return super().get_queryset().filter(roles__role=Role.ESTABLISHMENT_ADMINISTRATOR,
                                             userrole__establishment=establishment,
                                             userrole__state__in=[UserRole.VALIDATED, UserRole.PENDING])


class TeamMemberDeleteView(generics.DestroyAPIView):
    """Delete user from team"""
    permission_classes = get_permission_classes(
        IsEstablishmentManager,
        IsEstablishmentAdministrator
    )

    def get_object(self):
        return UserRole.objects.get(role__role=Role.ESTABLISHMENT_ADMINISTRATOR, user_id=self.kwargs['user_id'],
                                    establishment_id=self.kwargs['establishment_id'])

    def perform_destroy(self, instance):
        instance.delete()
        from account.tasks import team_role_revoked
        establishment = models.Establishment.objects.get(pk=self.kwargs['establishment_id'])
        if settings.USE_CELERY:
            team_role_revoked.delay(self.kwargs['user_id'], self.request.country_code, establishment.name)
        else:
            team_role_revoked(self.kwargs['user_id'], self.request.country_code, establishment.name)


class EstablishmentAwardCreateAndBind(EstablishmentLastUpdatedMixin, generics.CreateAPIView, generics.DestroyAPIView):
    """
    ### *POST*
    #### Description
    Add a new award to establishment
    ##### Response
    return award object
    ```
    {
        "id": 1,
        ...
    }
    ```
    ### *DELETE*
    #### Description
    Delete relations establishment_id - award_id
    """

    queryset = main_models.Award.objects.with_base_related().all()
    permission_classes = get_permission_classes()
    serializer_class = serializers.BackEstablishmentAwardCreateSerializer
    establishment_search_field_name = 'pk'
    establishment_search_kwarg_key = 'establishment_id'

    def _award_list_for_establishment(self, establishment_id: int, status: int) -> Response:
        awards = main_models.Award.objects.with_base_related().filter(
            object_id=establishment_id,
            content_type=ContentType.objects.get_for_model(models.Establishment)
        )
        response_serializer = main_serializers.AwardBaseSerializer(awards, many=True)
        headers = self.get_success_headers(response_serializer.data)
        return Response(response_serializer.data, status=status, headers=headers)

    def create(self, request, *args, **kwargs):
        """Create method."""
        super().create(request, args, kwargs)
        return self._award_list_for_establishment(kwargs['establishment_id'], status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        establishment = get_object_or_404(models.Establishment, id=kwargs['establishment_id'])
        establishment.remove_award(kwargs['award_id'])
        return self._award_list_for_establishment(kwargs['establishment_id'], status.HTTP_200_OK)
