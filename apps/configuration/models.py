from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel


class TranslationSettings(SingletonModel):
    """Translation settings solo model."""
    default_language = models.CharField(
        _('default language'), max_length=10, default='en-GB')
