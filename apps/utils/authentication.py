"""Custom authentication based on JWTAuthentication class"""
from rest_framework import HTTP_HEADER_ENCODING
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.settings import api_settings

from authorization import tasks
from django.conf import settings
from utils.methods import get_token_from_cookies
from utils.exceptions import NotValidAccessTokenError

AUTH_HEADER_TYPES = api_settings.AUTH_HEADER_TYPES

if not isinstance(api_settings.AUTH_HEADER_TYPES, (list, tuple)):
    AUTH_HEADER_TYPES = (AUTH_HEADER_TYPES,)

AUTH_HEADER_TYPE_BYTES = set(
    h.encode(HTTP_HEADER_ENCODING)
    for h in AUTH_HEADER_TYPES
)


class GMJWTAuthentication(JWTAuthentication):
    """
    An authentication plugin that authenticates requests through a JSON web
    token provided in a request cookies.
    """

    def authenticate(self, request):
        try:
            token = get_token_from_cookies(request)
            # Return non-authorized user if token not in cookies
            assert token

            raw_token = self.get_raw_token(token)
            # Return non-authorized user if cant get raw token
            assert raw_token

            validated_token = self.get_validated_token(raw_token)
            user = self.get_user(validated_token)
            assert user

            # Check record in DB
            token_is_valid = user.access_tokens.valid() \
                                               .by_jti(jti=validated_token.payload.get('jti'))

            if not token_is_valid.exists():
                raise NotValidAccessTokenError

        except NotValidAccessTokenError:
            # Сreate access token in-place
            tokens = user.create_jwt_tokens()
            request.COOKIES['updated_access_token'] = tokens.get('access_token')

        except:
            # Return non-authorized user if token is invalid or raised an error when run checks.
            return None
        else:
            return user, None


def send_confirmation_email(user_id: int, country_code: str, is_mobile: bool = False):
    # Send verification link on user email
    if settings.USE_CELERY:
        tasks.send_confirm_email.delay(user_id=user_id, country_code=country_code, is_mobile=is_mobile)
    else:
        tasks.send_confirm_email(user_id=user_id, country_code=country_code, is_mobile=is_mobile)
