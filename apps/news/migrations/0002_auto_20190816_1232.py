# Generated by Django 2.2.4 on 2019-08-16 12:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='newstype',
            options={'verbose_name': 'news type', 'verbose_name_plural': 'news types'},
        ),
    ]
