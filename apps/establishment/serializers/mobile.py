from establishment.serializers import MobileEstablishmentDetailSerializer
from rest_framework import serializers

from product.serializers import ProductMobilePreviewSerializer


class MobileEstablishmentProducerDetailSerializer(MobileEstablishmentDetailSerializer):
    products_preview = serializers.SerializerMethodField()

    class Meta(MobileEstablishmentDetailSerializer.Meta):
        fields = MobileEstablishmentDetailSerializer.Meta.fields + [
            'products_preview'
        ]

    def get_products_preview(self, instance):
        qs = instance.products.order_by('-modified')[:2]
        return ProductMobilePreviewSerializer(qs, many=True).data
