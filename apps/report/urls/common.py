"""Common URL patterns for application report."""
from django.urls import path

from report.views import common as views

app_name = 'report'
urlpatterns = [
    path('<int:pk>/', views.ReportRetrieveView.as_view(), name='report-retrieve')
]
