from rest_framework import serializers

from account.models import User
from account.serializers import BackUserSerializer
from main import models
from main.serializers import CarouselListSerializer, SiteSerializer


class PanelSerializer(serializers.ModelSerializer):
    """Serializer for Custom panel."""
    user_id = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        source='user',
        write_only=True
    )
    user = BackUserSerializer(read_only=True)

    class Meta:
        model = models.Panel
        fields = [
            'id',
            'name',
            'display',
            'description',
            'query',
            'created',
            'modified',
            'user',
            'user_id'
        ]


class BackCarouselListSerializer(CarouselListSerializer):
    """Serializer for retrieving list of carousel items."""
    city_name = serializers.CharField(allow_null=True, read_only=True)
    producer_name = serializers.CharField(allow_null=True, read_only=True)

    class Meta:
        """Meta class."""

        model = models.Carousel
        fields = CarouselListSerializer.Meta.fields + [
            'type',
            'subtypes',
            'city_name',
            'producer_name',
            'object_model',
            'object_id',
        ]


class BackSiteSerializer(SiteSerializer):

    low_price = serializers.IntegerField(read_only=True)
    high_price = serializers.IntegerField(read_only=True)

    class Meta(SiteSerializer.Meta):
        """Meta class."""
        fields = SiteSerializer.Meta.fields + [
            'default_locale',
            'low_price',
            'high_price',
        ]
