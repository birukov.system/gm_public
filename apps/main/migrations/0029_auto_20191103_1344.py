# Generated by Django 2.2.4 on 2019-11-03 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0028_auto_20191103_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currency',
            name='sign',
            field=models.CharField(max_length=5, verbose_name='sign'),
        ),
    ]
