"""Models for app comment."""
from django.contrib.contenttypes import fields as generic
from django.db import models
from django.utils.translation import gettext_lazy as _

from account.models import User
from translation.models import Language
from utils.models import ProjectBaseMixin
from utils.querysets import ContentTypeQuerySetMixin
from django.core.validators import MaxValueValidator, MinValueValidator


class CommentQuerySet(ContentTypeQuerySetMixin):
    """QuerySets for Comment model."""

    def by_user(self, user: User):
        """Return comments by author"""
        return self.filter(user=user)

    def published(self):
        """Return only published comments."""
        return self.filter(status=self.model.PUBLISHED)

    def annotate_is_mine_status(self, user):
        """Annotate belonging status"""
        return self.annotate(is_mine=models.Case(
            models.When(
                models.Q(user=user if user.is_authenticated else None),
                then=True
            ),
            default=False,
            output_field=models.BooleanField()
        ))

    def public(self, user):
        """
        Return QuerySet by rules:
        1 With status PUBLISHED
        2 With status WAITING if comment author is <user>
        """
        qs = self.published()
        if isinstance(user, User):
            waiting_ids = list(
                self.filter(status=self.model.WAITING, user=user)
                    .values_list('id', flat=True))
            published_ids = list(qs.values_list('id', flat=True))
            waiting_ids.extend(published_ids)
            qs = self.filter(id__in=tuple(waiting_ids))
        return qs

    def with_base_related(self):
        return self.prefetch_related("content_object").select_related("user", "content_type")


class Comment(ProjectBaseMixin):
    """Comment model."""

    WAITING = 0
    PUBLISHED = 1
    REJECTED = 2
    DELETED = 3

    STATUSES = (
        (WAITING, _('Waiting')),
        (PUBLISHED, _('Published')),
        (REJECTED, _('Rejected')),
        (DELETED, _('Deleted')),
    )

    text = models.TextField(verbose_name=_('Comment text'))
    mark = models.PositiveIntegerField(validators=[
        MinValueValidator(0), MaxValueValidator(10),
    ], blank=True, null=True, default=None, verbose_name=_('Mark'))
    user = models.ForeignKey('account.User', related_name='comments', on_delete=models.CASCADE, verbose_name=_('User'))
    old_id = models.IntegerField(null=True, blank=True, default=None)
    is_publish = models.BooleanField(default=False, verbose_name=_('Publish status'))
    status = models.PositiveSmallIntegerField(choices=STATUSES,
                                              default=WAITING,
                                              verbose_name=_('status'))
    site = models.ForeignKey('main.SiteSettings', blank=True, null=True,
                             on_delete=models.SET_NULL, verbose_name=_('site'))
    content_type = models.ForeignKey(generic.ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    objects = CommentQuerySet.as_manager()

    class Meta:
        """Meta class"""
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')

    def __str__(self):
        """String representation"""
        return str(self.user)
