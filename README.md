# gm-backend

## Build

1. ``git clone ssh://git@gl.id-east.ru:222/gm/gm-backend.git``
1. ``cd ./gm-backend``
1. ``git checkout develop``
1. ``docker-compose build``
1. First start database: ``docker-compose up db``
1. ``docker-compose up -d``
### Migrate data

1.Connect to container with django ``docker exec -it gm-backend_gm_app_1 bash``

#### In docker container(django) 

1. Migrate ``python manage.py migrate``
1. Create super-user ``python manage.py createsuperuser``
  
Backend is available at localhost:8000 or 0.0.0.0:8000

URL for admin http://0.0.0.0:8000/admin  
URL for swagger http://0.0.0.0:8000/docs/  
URL for redocs  http://0.0.0.0:8000/redocs/

## Start and stop backend containers

Demonize start ``docker-compose up -d``  
Stop ``docker-compose down``   
Stop and remove volumes ``docker-compose down -v``