# Generated by Django 2.2.7 on 2020-02-19 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('advertisement', '0011_auto_20200219_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertisement',
            name='source',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Mobile'), (1, 'Web'), (2, 'All')], default=0, verbose_name='Source'),
        ),
    ]
