from django.core.management.base import BaseCommand

from product.models import Product
from transfer.models import Products


class Command(BaseCommand):
    help = """Add average price to product from legacy table products."""

    def handle(self, *args, **kwarg):
        update_products = []
        old_products = Products.objects.values_list('id', 'price')
        for old_id, price in old_products:
            product = Product.objects.get(old_id=old_id)
            product.average_price = price
            update_products.append(product)

        Product.objects.bulk_update(update_products, ['average_price', ])
        self.stdout.write(self.style.WARNING(f'Updated products: {len(update_products)}'))
