FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN apt-get update; apt-get --assume-yes install binutils libproj-dev gdal-bin gettext
RUN mkdir /code /requirements
ADD ./requirements /requirements
RUN pip install --no-cache-dir -r /requirements/base.txt && \
    pip install --no-cache-dir -r /requirements/development.txt
WORKDIR /code
ADD . /code/