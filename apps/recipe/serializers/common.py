"""Recipe app common serializers."""
from rest_framework import serializers
from recipe import models


class RecipeBaseSerializer(serializers.ModelSerializer):
    """Base serializer for model Recipe."""

    title_translated = serializers.CharField(allow_null=True, read_only=True)
    subtitle_translated = serializers.CharField(allow_null=True, read_only=True)
    in_favorites = serializers.BooleanField(read_only=True)

    class Meta:
        """Meta class."""

        model = models.Recipe
        fields = (
            'id',
            'title_translated',
            'subtitle_translated',
            'author',
            'created_by',
            'published_at',
            'in_favorites',
        )


class RecipeListSerializer(RecipeBaseSerializer):
    """Serializer for list of recipes."""

    class Meta(RecipeBaseSerializer.Meta):
        """Meta class."""

        extra_kwargs = {
            'author': {'read_only': True},
            'created_by': {'read_only': True},
            'published_at': {'read_only': True},
        }


class RecipeDetailSerializer(RecipeListSerializer):
    """Serializer for more information about the recipe."""

    description_translated = serializers.CharField(allow_null=True, read_only=True)

    class Meta(RecipeListSerializer.Meta):
        """Meta class."""

        fields = RecipeListSerializer.Meta.fields + ('description_translated',)
