# Generated by Django 2.2.4 on 2019-11-05 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('establishment', '0056_auto_20191105_1401'),
    ]

    operations = [
        migrations.AddField(
            model_name='establishmentemployee',
            name='old_id',
            field=models.IntegerField(blank=True, null=True, verbose_name='Old id'),
        ),
    ]
