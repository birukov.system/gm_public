from django.core.management.base import BaseCommand
from django.db import connections
from tqdm import tqdm

from location.models import Country
from main.models import AwardType
from utils.methods import namedtuplefetchall


class Command(BaseCommand):
    help = '''Add award types from old db to new db.
    Run after migrate country code!'''

    def award_types_sql(self):
        with connections['legacy'].cursor() as cursor:
            cursor.execute('''
                            SELECT 
                                DISTINCT
                                at.id, TRIM(at.title) AS name, 
                                s.country_code_2 AS country_code
                            FROM award_types as at
                            JOIN sites s on s.id = at.site_id
                            WHERE LENGTH(TRIM(at.title))>0               
                           ''')
            return namedtuplefetchall(cursor)

    def handle(self, *args, **kwargs):
        to_create = []
        for a in tqdm(self.award_types_sql(), desc='Add award types: '):
            country = Country.objects.filter(code=a.country_code).first()
            award_type_qs = AwardType.objects.filter(old_id=a.id)
            if country and not award_type_qs.exists():
                award_type = AwardType(name=a.name, old_id=a.id)
                award_type.country = country
                to_create.append(award_type)
        AwardType.objects.bulk_create(to_create)
        self.stdout.write(self.style.WARNING(f'Created {len(to_create)} an award types objects.'))
