#!/usr/bin/env bash
# ПОЛЬЗОВАТЕЛИ

# Перенос пользователей из модели Accounts в User
# --------------------------
# id -> old_id
# email -> email
# unconfirmed_email -> unconfirmed_email
# confirmed_at (boolean) -> email_confirmed
# encrypted_password -> password
# nickname -> username
# locale -> locale
# city -> city
# confirmed_at -> confirmed_at
./manage.py add_account --settings=project.settings.transfer

# Добавление к уже перенесенным пользователям image_url по old_id
# --------------------------
# image_url -> image_url
./manage.py add_image --settings=project.settings.transfer

# Заполнение модели из identities в UserSocialAuth
# --------------------------
# пользователь -> user
# provider -> provider
# uid -> uid
./manage.py add_social --settings=project.settings.transfer

# !!! Заполнение модели OldRole, UserRole (должны быть заполнены Role и SiteSettings)
# --------------------------
./manage.py add_affilations --settings=project.settings.transfer

# Заполнение моделей Country, Region, City из ruby_data
# для City перенос из старой бд, заполнение флагов и фото
# !!! для нового дампа обязательно нужно генерировать файл ruby_data.py
# --------------------------
./manage.py transfer --setup_clean_db --settings=project.settings.transfer > apps/transfer/log/data_clean_db_transfer.log 2>&1
./manage.py transfer --set_unused_regions --settings=project.settings.transfer > apps/transfer/log/data_set_unused_regions.log 2>&1
./manage.py transfer --set_country_phone_code --settings=project.settings.transfer > apps/transfer/log/data_set_country_phone_code.log 2>&1

# Заполнение модели Address из Locations
# --------------------------
# id
# city_id
# zip_code
# latitude
# longitude
# address

# Заполнение модели WineRegion из WineLocations
# --------------------------
# id
# name
# desc
# latitude
# longitude

# Заполнение модели WineSubRegion из WineVillage
# --------------------------
# id
# name
# parent_id

# Заполнение модели WineSubRegion из WineLocations
# --------------------------
# id
# name
# parent_id
./manage.py transfer -d --settings=project.settings.transfer > apps/transfer/log/data_dictionaries_transfer.log 2>&1

# Fix address, clear number field and fill street_name_1 like in old db
./manage.py fix_address --settings=project.settings.transfer
./manage.py add_site_settings --settings=project.settings.transfer
./manage.py add_site_features --settings=project.settings.transfer
./manage.py update_default_language --settings=project.settings.transfer

# !!! Трансфер заведений
./manage.py transfer -e --settings=project.settings.transfer > apps/transfer/log/data_establishments_transfer.log 2>&1
./manage.py transfer --establishment_note --settings=project.settings.transfer > apps/transfer/log/data_establishment_note_transfer.log 2>&1
./manage.py transfer -l --settings=project.settings.transfer > apps/transfer/log/data_location_establishment_transfer.log 2>&1
./manage.py transfer --purchased_plaques --settings=project.settings.transfer > apps/transfer/log/data_purchase_plugin_transfer.log 2>&1
./manage.py transfer --a_la_cartes --settings=project.settings.transfer
./manage.py transfer --menu --settings=project.settings.transfer
# оптимизация изображений
./manage.py update_establishment_image_urls --settings=project.settings.transfer # удаляет неотображаемые картинки из модели заведения
./manage.py add_tags --settings=project.settings.transfer  # трансфер тэгов для заведений
./manage.py fix_tags_category --settings=project.settings.transfer  # исправления тегов заведений согласно GM-366

./manage.py add_artisan_subtype --settings=project.settings.transfer
./manage.py add_closed_at_timetable --settings=project.settings.transfer

./manage.py upd_transportation --settings=project.settings.transfer
./manage.py transfer -p --settings=project.settings.transfer

# !!! Заполнение модели из OwnershipAffs в UserRole (запускать после переноса заведений)
# --------------------------
# user -> user,
# role -> role,
# establishment -> establishment,
# owner.state -> state,
# requester -> requester
./manage.py add_ownership --settings=project.settings.transfer

# сотрудники с позициями для заведений
#
# transfer_employee:
#   Profiles -> Employee
#   ---
#   Requirements for Employee:
#   - User
#
#   Поля:
#     old_id=profile.id,
#     user_id=get_user_id(profile.account_id),
#     name=get_name(profile.firstname, profile.lastname, profile.email),
#     last_name=profile.lastname,
#     sex=get_gender(profile.gender),
#     birth_date=profile.dob,
#     email=profile.email,
#     phone=get_phone(profile.phone_country_code, profile.phone_local_number),
#     created=profile.created_at,
#     created_by_id=get_user_id(profile.requester_id),
#     available_for_events=True if profile.available_for_events == 1 else False
#
# transfer_position:
#   Affiliations -> Position
#   ---
#   Requirements for Position:
#
#   Поля:
#     name={"en-GB": role.capitalize()}
#     establishment_type
#     establishment_subtype
#
# transfer_establishment_employee:
#   Affiliations -> EstablishmentEmployee
#   ---
#   Requirements for EstablishmentEmployee:
#   - Establishment
#   - Employee
#   - Position
#
#   Поля:
#     from_date=item.start_date,
#     to_date=item.end_date,
#     old_id=item.id,
#     establishment=establishment,
#     position=position,
#     employee=employee,
#     created_by_id=get_user_id(item.requester_id),
#     created=item.created_at,
#     status=EstablishmentEmployee.ACCEPTED if item.state == 'validated' else EstablishmentEmployee.IDLE
#
# transfer_employee_photo
#   ProfilePictures -> Image
#   ---
#   Requirements:
#   - Employee
#
#   Поля:
#     orientation=Image.HORIZONTAL,
#     title=item.attachment_file_name,
#     image=item.attachment_suffix_url,
#     created=item.created_at,
./manage.py transfer --employees --settings=project.settings.transfer

# миграция авардов
./manage.py add_award_type --settings=project.settings.transfer
./manage.py add_award --settings=project.settings.transfer

# Новости
# --------------------------
# clear_old_news - удаление ранее перенесенных новостей по old_id
#
# transfer_news: перенос из PageTexts в News по фильтру page__type__in=('News', 'StaticPage', 'Recipe')
# - создание NewsType 'news', 'staticpage', 'recipe'
# поля:
# id -> old_id
# page__created_at -> created
# page__account_id -> created_by
# page__account_id -> modified_by
# page__state -> state
# page__template -> template
# page__site__country_code_2 -> country
# locale:slug -> slugs
# locale:body -> description
# locale:title -> title
# page__root_title -> backoffice_title
# locale:summary -> subtitle
# locale:True -> locale_to_description_is_active
# page__published_at -> publication_date
# page__published_at -> publication_time
#
# make_gallery - создание галерей из page__attachment_suffix_url (NewsGallery, Image)
#
# update_en_gb_locales - добавление локали по умолчанию в News (en-GB)
#
# add_views_count - добавление кол-ва просмотров в News из PageCounters (count -> count)
#
# add_tags - создание категории тегов TagCategory = 'category' и 'tag'
# добавление их в NewsType = 'news', создание тегов, добавление тегов к новостям
./manage.py transfer -n --settings=project.settings.transfer > apps/transfer/log/data_news_transfer.log 2>&1

# команда для удаления картинок с относительным урлом из news.description
./manage.py rm_empty_images --settings=project.settings.transfer > apps/transfer/log/data_rm_empty_images_transfer.log 2>&1

# сжимает картинки в описаниях новостей
./manage.py news_optimize_images --settings=project.settings.transfer
./manage.py transfer --rating_count --settings=project.settings.transfer

# !!! ----------------------------------------------

#./manage.py transfer --fill_city_gallery > apps/transfer/log/data_fill_city_gallery_transfer.log 2>&1
# products
./manage.py transfer --wine_characteristics --settings=project.settings.transfer > apps/transfer/log/data_wine_characteristic_transfer.log 2>&1
./manage.py add_product_type --settings=project.settings.transfer > apps/transfer/log/data_product_type_transfer.log 2>&1
./manage.py add_product_sub_type --settings=project.settings.transfer > apps/transfer/log/data_product_subtype_transfer.log 2>&1
./manage.py transfer --product --settings=project.settings.transfer > apps/transfer/log/data_product_transfer.log 2>&1
./manage.py add_wine_origin_address --settings=project.settings.transfer > apps/transfer/log/data_wine_origin_address_transfer.log 2>&1
./manage.py transfer --product_note --settings=project.settings.transfer > apps/transfer/log/data_product_note_transfer.log 2>&1
./manage.py transfer --souvenir --settings=project.settings.transfer > apps/transfer/log/data_souvenir_transfer.log 2>&1
./manage.py add_product_tag --settings=project.settings.transfer > apps/transfer/log/data_product_tag_transfer.log 2>&1
./manage.py add_assemblage_to_product --settings=project.settings.transfer > apps/transfer/log/data_product_assemblage_transfer.log 2>&1
./manage.py add_average_price --settings=project.settings.transfer > apps/transfer/log/data_average_price_transfer.log 2>&1

# review и переводы для них
#
# transfer_languages
#   ReviewTexts -> Language
#   Поля:
#     id - old_id
#     locale - locale
./manage.py transfer --languages --settings=project.settings.transfer

./manage.py transfer --overlook --settings=project.settings.transfer
./manage.py transfer --inquiries --settings=project.settings.transfer > apps/transfer/log/data_inquiries_transfer.log 2>&1
./manage.py transfer --product_review --settings=project.settings.transfer
./manage.py transfer --transfer_text_review --settings=project.settings.transfer
./manage.py add_review_priority --settings=project.settings.transfer
./manage.py fix_review_statuses --settings=project.settings.transfer

./manage.py add_public_mark --settings=project.settings.transfer > apps/transfer/log/data_public_mark_transfer.log 2>&1
# Утеряна четкая связь между последовательностью миграций для импорта тегов продуктов,
# что может привести к удалению уже импортированных тегов командой выше.
./manage.py check_serial_number --settings=project.settings.transfer > apps/transfer/log/data_check_serial_transfer.log 2&> 1
./manage.py transfer --assemblage --settings=project.settings.transfer > apps/transfer/log/data_assemblage_transfer.log 2>&1
./manage.py transfer --guide_complete --settings=project.settings.transfer
./manage.py transfer --collections --settings=project.settings.transfer
./manage.py add_cepage_tag --settings=project.settings.transfer
./manage.py fix_cepage_tag_categories --settings=project.settings.transfer
./manage.py add_cepage_tag_to_wine_region --settings=project.settings.transfer
./manage.py revert_wine_region_cepage_tags --settings=project.settings.transfer
./manage.py add_wine_color_tag --settings=project.settings.transfer


./manage.py transfer -s --settings=project.settings.transfer # подписчики
./manage.py transfer --newsletter_subscriber --settings=project.settings.transfer # подписчики

./manage.py transfer -w --settings=project.settings.transfer # карусели
./manage.py add_home_page_carousel --settings=project.settings.transfer # карусели

./manage.py add_footers --settings=project.settings.transfer # футер
./manage.py add_panels --settings=project.settings.transfer
./manage.py move_title_to_tjson --settings=project.settings.transfer

./manage.py transfer -g --settings=project.settings.transfer # галлереи

./manage.py transfer --comment --settings=project.settings.transfer # коменты
./manage.py add_comment_publish_data --settings=project.settings.transfer # коменты
./manage.py add_status_to_comments --settings=project.settings.transfer # коменты