from django.core.management.base import BaseCommand
from django.db import connections
from tqdm import tqdm

from main.models import SiteSettings
from translation.models import Language
from utils.methods import namedtuplefetchall
import json


class Command(BaseCommand):
    help = "Update SiteSettings from legacy DB."

    def handle(self, *args, **kwargs):

        def site_sql():
            with connections['legacy'].cursor() as cursor:
                cursor.execute('''
                                select
                                    distinct 
                                    id, 
                                    country_code_2 as code, 
                                    pinterest_page_url, 
                                    twitter_page_url, 
                                    facebook_page_url, 
                                    contact_email, 
                                    config, 
                                    released, 
                                    instagram_page_url, 
                                    ad_config
                                from sites as s                         
                             ''')
                return namedtuplefetchall(cursor)

        def get_default_language(config: dict):
            try:
                config = json.loads(config)
                locale = config.get('default_locale')
                if locale:
                    language_qs = Language.objects.filter(locale=locale)
                    if language_qs.exists():
                        return language_qs.first()
            except:
                return None

        objects = []
        for s in tqdm(site_sql(), desc='Add site settings'):
            default_language = get_default_language(s.config)
            site_settings_qs = SiteSettings.objects.filter(old_id=s.id)

            if site_settings_qs.exists():
                site_settings = site_settings_qs.first()
                site_settings.default_language = default_language
                objects.append(site_settings)

        SiteSettings.objects.bulk_update(
            objects, ['default_language', ]
        )

        self.stdout.write(self.style.WARNING(f'Updated {len(objects)} SiteSettings.'))
